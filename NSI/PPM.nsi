;--------------------------------
;Product Info
Name "PPM 2.0" ;Define your own software name here
!define PRODUCT "PPM" ;Define your own software name here
!define VERSION "2.0" ;Define your own software version here

CRCCheck On
; Script create for version 2.0b4 1.40 (from 09.sep.03) with GUI NSIS (c) by Dirk Paehl. Thank you for use my program

 !include "MUI.nsh"
 !include "x64.nsh"

 
;--------------------------------
;Configuration
 
   OutFile "PPM${VERSION}Setup.exe"

  ;Folder selection page
   InstallDir "$PROGRAMFILES\PPM ${VERSION}"

;Remember install folder
InstallDirRegKey HKCU "Software\${PRODUCT} ${VERSION}" ""

;--------------------------------
;Pages
!insertmacro MUI_PAGE_LICENSE "PPM.txt"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

 !define MUI_ABORTWARNING

 
;--------------------------------
 ;Language
 
  !insertmacro MUI_LANGUAGE "English"
;--------------------------------
Icon "${NSISDIR}\Contrib\Graphics\Icons\win-install.ico"
UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\win-uninstall.ico"

;Installer Sections
     
Section "section_1" section_1
SetOutPath "$INSTDIR"
FILE /r "\Users\Alex\PPM\build_\*"
SectionEnd

Section Shortcuts
CreateDirectory "$SMPROGRAMS\PPM ${VERSION}"
WriteIniStr "$INSTDIR\PPM.url" "InternetShortcut" "URL" "http://forge.epn-campus.eu/projects/ppm"
CreateShortCut "$SMPROGRAMS\PPM ${VERSION}\PPM Home page.lnk" "$INSTDIR\PyMca.url" "" "$INSTDIR\PPM.url" 0
CreateShortCut "$SMPROGRAMS\PPM ${VERSION}\PPM GUI Scalar.lnk" "$INSTDIR\ppmguiScalar.exe" "" "$INSTDIR\ppmguiScalar.exe" 0
CreateShortCut "$SMPROGRAMS\PPM ${VERSION}\PPM GUI Tensorial.lnk" "$INSTDIR\ppmguiTensorial.exe" "" "$INSTDIR\ppmguiTensorial.exe" 0
CreateShortCut "$SMPROGRAMS\PPM ${VERSION}\PPM XML.lnk" "$INSTDIR\ppmxml.exe.exe" "" "$INSTDIR\ppmxml.exe" 0
CreateShortCut "$SMPROGRAMS\PPM ${VERSION}\PPM XML Tens.lnk" "$INSTDIR\ppmxmlTens.exe" "" "$INSTDIR\ppmxmlTens.exe" 0
SectionEnd

Section Uninstaller
CreateShortCut "$SMPROGRAMS\PPM ${VERSION}\Uninstall.lnk" "$INSTDIR\uninst.exe" "" "$INSTDIR\uninst.exe" 0
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PPM ${VERSION}" "DisplayName" "${PRODUCT} ${VERSION}"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PPM ${VERSION}" "DisplayVersion" "${VERSION}"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PPM ${VERSION}" "URLInfoAbout" "http://www.esrf.fr"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PPM ${VERSION}" "Publisher" "A. Mirone - ESRF SciSoft Group"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PPM ${VERSION}" "UninstallString" "$INSTDIR\Uninst.exe"
WriteRegStr HKCU "Software\${PRODUCT} ${VERSION}" "" $INSTDIR
WriteUninstaller "$INSTDIR\Uninst.exe"
 
 
SectionEnd
 
;--------------------------------  
;Descriptions 
                                    
 
;--------------------------------
    
;Uninstaller Section
   
Section "Uninstall" 
 
  ;Add your stuff here
  ;Delete Files
  RMDir /r "$INSTDIR"
      
  ;Delete Start Menu Shortcuts
  Delete "$SMPROGRAMS\PPM ${VERSION}\*.*"
  RmDir "$SMPROGRAMS\PPM ${VERSION}"
  SetShellVarContext all
  Delete "$SMPROGRAMS\PPM ${VERSION}\*.*"
  RmDir "$SMPROGRAMS\PPM ${VERSION}"
  ;Delete Uninstaller And Unistall Registry Entries
  DeleteRegKey HKEY_CLASSES_ROOT "Applications\ppmguiScalar.exe"
  DeleteRegKey HKEY_CLASSES_ROOT "Applications\ppmguiTensorial.exe"
  DeleteRegKey HKEY_CLASSES_ROOT "Applications\ppmxml.exe"
  DeleteRegKey HKEY_CLASSES_ROOT "Applications\ppmxmlTens.exe"
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\PPM ${VERSION}"
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\PPM ${VERSION}"
  DeleteRegKey HKEY_CURRENT_USER "SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MenuOrder\Start Menu\Programs\PPM ${VERSION}"
  DeleteRegKey HKEY_CURRENT_USER "SOFTWARE\PPM ${VERSION}"
  RMDir /r "$INSTDIR"
             
SectionEnd
               
   
;eof
