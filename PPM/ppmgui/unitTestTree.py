

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import unittest
from general_Tree import Tree
from TypeObject import DictObject
from Tree_App import *
from Tree_View import *
from DocMenager import *
from iqt import *

in_closeAll="""<system>
  <scanlist>
    <scan></scan>
  </scanlist>
</system>"""

out_closeAll="""+<system>
</system>"""
f=open('essai1_1.xml','r')

int_openRoot="""<system>
  <scanlist>
    <scan>
      <s>DATA/data_ref_w.dat</s>
      <f>"wavelenghts_col"  3 </f>
      <f>"angles_col"  1 </f>
    </scan>
  </scanlist>
  <s>data.txt </s>
  <f>"weight_col"  1.0 </f>
  <f>"angle_factor"  arccos(-1)/180.0 </f>
  <f>"norm" nomrLE 1191671.0 100000 20000000 </f>
  <f>"noise"  3.031721e-07 1.0e-9 1.0e-6 </f>
  <f>"CutOffRatio"  0.009644845 0.00025 0.2 </f>
</system>"""

out_openRoot='''-<system >
  +<scanlist >
  </scanlist>
  <s>data.txt </s>
  <f>"weight_col"  1.0 </f>
  <f>"angle_factor"  arccos(-1)/180.0 </f>
  <f>"norm" nomrLE 1191671.0 100000 20000000 </f>
  <f>"noise"  3.031721e-07 1.0e-9 1.0e-6 </f>
  <f>"CutOffRatio"  0.009644845 0.00025 0.2 </f>
</system>
'''
in_getKnot='''<system>
   <scanlist>
     <scan>
      <s>DATA/data_ref_w.dat</s>
      <f>"wavelenghts_col" Gen_Rough 3 </f>
      <layer> Fe_Layer
         <f> "thickness"  Fe_Thick 4.0 5.6 2.3 </f>
         <f> "roughness" Gen_Rough    </f>
         <ifo   key="material">
            <f> Fe_ind_one 4.5 </f>
            <f>  6.432456   5.5  8.5  </f>
        </ifo>
      </layer>
     </scan>
   </scanlist>
   <s>data.txt </s>
   <f>"weight_col"  1.0 </f>
 </system> '''
out_getKnot1='''<f>  6.432456   5.5  8.5  </f>'''

out_getKnot2_1='''+<layer> Fe_Layer  </layer>''' # layer ferme

out_getKnot2_2='''-<layer> Fe_Layer
         <f> "thickness"  Fe_Thick 4.0 5.6 2.3 </f>
         <f> "roughness" Gen_Rough    </f>
         +<ifo   key="material">
         </ifo>
      </layer>''' #layer ouvert
out_copyRoot='''-<system >
  +<scanlist >
  </scanlist>
  <s>data.txt </s>
  <f>"weight_col"  1.0 </f>
  <f>"angle_factor"  arccos(-1)/180.0 </f>
  <f>"norm" nomrLE 1191671.0 100000 20000000 </f>
  <f>"noise"  3.031721e-07 1.0e-9 1.0e-6 </f>
  <f>"CutOffRatio"  0.009644845 0.00025 0.2 </f>
</system>
'''
out_CopyClosed='''+<layer> Fe_Layer
      </layer>''' #layer ferme

out_CopyOpened='''-<layer> Fe_Layer
         <f> "thickness" Fe_Thick 4.0 5.6 2.3 </f>
         <f> "roughness" Gen_Rough    </f>
         +<ifo   key="material">
         </ifo>
      </layer>''' #layer ouvert
out_CopyPoint='''<f> "roughness" Gen_Rough </f>'''

out_CopySpecial='''<scan>
     		<f>"wavelenghts_col" Gen_Rough 3
		<layer> Fe_Layer
         		<ifo   key="material">
           			 <f> Fe_ind_one 4.5
          			 <f>  6.432456   5.5  8.5'''

out_InsertRoot="""-<system>
  +<ifo   key="material">
  </ifo>
  +<scanlist>
  </scanlist>
  <s>data.txt </s>
  <f>"weight_col"  1.0 </f>
</system>"""

out_InserKnot='''-<scan>
      <s>DATA/data_ref_w.dat</s>
      <f>"wavelenghts_col" Gen_Rough 3 </f>
      -<layer>
         <f> "thickness"   4.0 5.6 2.3 </f>
         <f> "roughness" Gen_Rough    </f>
         +<ifo   key="material">
        </ifo>
      </layer>
      -<layer> Fe_Layer
         <f> "thickness"  Fe_Thick 4.0 5.6 2.3 </f>
         <f> "roughness" Gen_Rough    </f>
         +<ifo   key="material">
        </ifo>
      </layer>
     </scan> '''

out_InsertIn2Trees='''-<system>
	-<layer> Fe_Layer
         	<f> "thickness"  Fe_Thick 4.0 5.6 2.3 </f>
         	<f> "roughness" Gen_Rough    </f>
        	 -<ifo   key="material">
          		  <f> Fe_ind_one 4.5 </f>
         		   <f>  6.432456   5.5  8.5  </f>
       		 </ifo>
      	</layer>
      </system>'''

out_DelSimple="""-<system>
  +<scanlist>
  </scanlist>
  <f>"weight_col"  1.0 </f>
</system>"""

in_delDefinition='''<system>
   <f>"weight_col" Gen_Weight 1.0 </f>
   <scan>
     <s>DATA/data_ref_w.dat</s>
     <f>"wavelenghts_col" Gen_Rough 3 </f>
     <layer> Fe_Layer
         <f> "thickness"  Gen_Rough </f>
         <f> "roughness" Gen_Rough    </f>
         <ifo   key="material">
            <f> Fe_ind_one 4.5 </f>
            <f>  6.432456   5.5  8.5  </f>
        </ifo>
     </layer>
   </scan>
   <f>"weight_col" Gen_Weight</f>
   <s>data.txt </s>

 </system> '''

out_delDefSimple='''-<system>
   <f>"weight_col" Gen_Weight 1.0 </f>
   +<scan>
   </scan>
   <f>"weight_col" Gen_Weight</f>
   <s>data.txt </s>
 </system> '''

out_delDefinition='''-<system>
   <f>"weight_col" Gen_Weight 1.0 </f>
   <f>"weight_col" Gen_Weight</f>
   <s>data.txt </s>
 </system> '''

out_delPointer1='''-<system>
   <f>"weight_col" Gen_Weight 1.0 </f>
   +<scan>
   </scan>
   <s>data.txt </s>
 </system> '''
out_delPointer2='''-<system>
   +<scan>
   </scan>
   <s>data.txt </s>
 </system> '''

in_delDefWithoutRef='''<system>
   <f>"weight_col" Gen_Weight 1.0 </f>
   <scan>
     <s>DATA/data_ref_w.dat</s>
     <f>"wavelenghts_col" Gen_Rough 3 </f>
     <layer> Fe_Layer
         <f> "thickness"  Gen_Rough </f>
         <f> "roughness" Gen_Weight    </f>
         <ifo   key="material">
            <f> Fe_ind_one 4.5 </f>
            <f>  6.432456   5.5  8.5  </f>
        </ifo>
     </layer>
   </scan>
   <f>"weight_col" Gen_Rough</f>
   <s>data.txt </s>

 </system> '''

out_delDefWithoutRef='''-<system>
   <f>"weight_col" Gen_Weight 1.0 </f>
   +<scan>
   </scan>
   <f>"weight_col" Gen_Rough</f>
   <s>data.txt </s>
 </system> '''

in_provachiave="""<system>
  <scanlist>
    <f> "chiave0" 1 </f>
    <scan key="chiave1">
    </scan>
  </scanlist>
</system>"""

in_getKeyNone="""<system>
  <scanlist>
    <f>  1 </f>
    <scan>
    </scan>
  </scanlist>
</system>"""


out_xmlBlockDef='''<layer> Fe_Layer
         <f> "thickness"  Fe_Thick 4.0 5.6 2.3 </f>
         <f> "roughness" Gen_Rough    </f>
         <ifo   key="material">
            <f> Fe_ind_one 4.5 </f>
            <f>  6.432456   5.5  8.5  </f>
        </ifo>
      </layer> '''
out_xmlBlockRefF='''<f>"wavelenghts_col" Gen_Rough 3 </f>'''

in_xmlBlockRef='''<system>
   <scanlist>
     <scan>
      <s>DATA/data_ref_w.dat</s>
      <f>"wavelenghts_col" Gen_Rough 3 </f>
      <layer> Fe_Layer
         <f> "thickness"  Fe_Thick 4.0 5.6 2.3 </f>
         <f> "roughness" Gen_Rough    </f>
         <ifo   key="material">
            <f> Fe_ind_one 4.5 </f>
            <f>  6.432456   5.5  8.5  </f>
        </ifo>
       </layer>
       <ifo   key="material">Fe_Thick
       </ifo>
     </scan>
   </scanlist>
   <f> Fe_Layer </f>
   <layer> Fe_Layer
   </layer>
 </system> '''

out_xmlBlockRef='''<layer> Fe_Layer
         <f> "thickness"  Fe_Thick 4.0 5.6 2.3 </f>
         <f> "roughness" Gen_Rough    </f>
         <ifo   key="material">
            <f> Fe_ind_one 4.5 </f>
            <f>  6.432456   5.5  8.5  </f>
        </ifo>
      </layer>'''
out_xmlBlockRefS='''<s>DATA/data_ref_w.dat</s>'''

out_xmlBlockRefOnF='''<f> "thickness"  Fe_Thick 4.0 5.6 2.3 </f> '''

out_testDictOfChildScan='''s
			   f
			   layer
			   ifo'''

out_testDictOfChildLayer='''f
			   ifo'''

in_getAttrKey='''<system>
<stack repetitions="29">
	<layer> U_Layer
		<dv key="thickness"> U_Thick <s>   par(period_var)-par(Fe_Thick)  </s>  </dv>
			<f> "roughness"   U_Rough   </f>
			<ifo   key="material">
			<f>  UKK </f>
			<f> U_RelDen "RelativeDensity"  1.018063 0.1 1.1   </f>
			</ifo>
		</layer>
		<layer> Fe_Layer
	</layer>
</stack >
<stack key="29">
</stack>
</system>'''

in_checkF='''<system>
    <f>  name  </f>
    <f>"roughness" name arccos(-1)/180 </f>
</system>
'''
out_checkF='''-<system>
    <f> name </f>
    <f>"roughness" name arccos(-1)/180 </f>
</system>
'''

in_checkFName='''<system>
    <f>  name 10 </f>
    <f>"roughness" name arccos(-1)/180 </f>
</system>
'''
out_checkFName='''-<system>
    <f> name 10 </f>
    <f>"roughness" name arccos(-1)/180 </f>
</system>
'''

in_check= '''<system>
    <layer>NameLayer
    </layer>
    <layer>NameLayer
      <f>"thickness" 180 </f>
      <f>"roughness" name arccos(-1)/180 </f>
      <f>"material"  arccos(-1)/180 </f>
    </layer>
</system>
'''

out_check= '''-<system>
    <layer>NameLayer
    </layer>
    -<layer>NameLayer
      <f>"thickness" 180 </f>
      <f>"roughness" name arccos(-1)/180 </f>
      <f>"material"  arccos(-1)/180 </f>
    </layer>
</system>
'''
in_checkName= '''<system>
    <layer>NameLayer
	<f>"thickness" 2 </f>
      	<f>"roughness" 10 </f>
      	<f>"material"  arccos(-1)/180 </f>
    </layer>
    <layer>NameLayer
      <f>"thickness" 180 </f>
      <f>"roughness" name arccos(-1)/180 </f>
      <f>"material"  arccos(-1)/180 </f>
    </layer>
</system>
'''

out_checkName= '''-<system>
    -<layer>NameLayer
	<f>"thickness" 2 </f>
      	<f>"roughness" 10 </f>
      	<f>"material"  arccos(-1)/180 </f>
    </layer>
    -<layer>NameLayer
      <f>"thickness" 180 </f>
      <f>"roughness" name arccos(-1)/180 </f>
      <f>"material"  arccos(-1)/180 </f>
    </layer>
</system> '''

in_validationSmDef= '''<system>
	<tf0>
    		<s>toto.txt </s>
 	</tf0>
	<layer>NameLayer
		<f>"thickness" 2 </f>
      		<f>"roughness" 10 </f>
      		<f>"material" name 20 </f>
    	</layer>
</system>
'''

out_validationSmDef= '''
	<s> hasn't the value
	<f> name hasn't the value'''

in_validationLen= '''<system>
	<tf0>
 	</tf0>
	<layer>NameLayer
		<f>"thickness" 2 </f>
      		<f>"roughness" 10 </f>
      		<ifo key="material" >
        		<f> UKK</f>
       			 <f>"RelativeDensity" U_RelDen 1.018063 0.1 1.1 </f>
      		</ifo>
		<f> </f>
    	</layer>
</system>
'''

out_validationLen= '''
	<tf0> must have the sub-elements
	<layer> NameLayer hasn't the good number of sub-elements'''

in_copyCom='''<system>
	<!-- Salut
	Bonjour
	Cao -->
	<tf0>
 	</tf0>
	<layer>NameLayer
		<f>"thickness" 2 </f>
      		<f>"roughness" 10 </f>
      		<ifo key="material" >
        		<f> UKK</f>
       			 <f>"RelativeDensity" U_RelDen 1.018063 0.1 1.1 </f>
      		</ifo>
		<f> </f>
    	</layer>
</system>
'''
out_copyCom='''
	<!-- Salut
	Bonjour
	Cao --> '''


#Transformation du string : suppretion de \t,\n et de blanc
def Transformation_str(text):
	text_res=text.replace('\t','')
	text_res=text_res.replace('\n','')
	text_res=text_res.replace(' ','')
	return text_res

#------Calss des unit-testes-------------------
class testCmlTreeView(unittest.TestCase):
    def setUp(self):
        pass
    
    def tearDown(self):
        pass
#---------------------Tests d'initialisation---------------
#Le noeud "system" est ferme
    def testInitializCloseAll(self):
        tv=Tree()
        tv.initialise(in_closeAll)
        out_res = tv.getText()

	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_closeAll)

        self.assertEquals(out_res,out_data)

#Le noeud "system" est ouvert
    def testInitializeOpenRoot(self):
    	tv=Tree()
        tv.initialise(int_openRoot)
	tv.root.open_close()#ouverture du noeud "system"
        out_res = tv.getText()

	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_openRoot)

        self.assertEquals(out_res,out_data)

    def testInitializeOpenCloseRoot(self):
    	tv=Tree()
        tv.initialise(int_openRoot)
	tv.root.open_close()# root est ouvert

        out_res = tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_openRoot)

        self.assertEquals(out_res,out_data)

	tv.root.open_close()#root est ferme
        out_res = tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_closeAll)

        self.assertEquals(out_res,out_data)

#-------------------Tests de rechercherche--------------------
# Recherche du noeud-"feuil"
    def testGetKnotToend(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot_res=tv.getKnot([0,0,2,2,1])# "<f>  6.432456   5.5  8.5  </f>"

	out_res =tv.unitTest_getText(knot_res)#le contenue du noeud
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_getKnot1)

        self.assertEquals(out_res,out_data)

#Recherche du noeud pas racine et pas "feuil"
    def testGetKnotNotOpened(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot_res=tv.getKnot([0,0,2])# "<layer> Fe_Layer ..."

	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_getKnot2_1)

        self.assertEquals(out_res,out_data)

#Recherche du noeud pas racine et pas "feuil" qu'on ouvre
    def testGetKnotOpened(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot_res=tv.getKnot([0,0,2])# "<layer> Fe_Layer ..."
	knot_res.open_close()# le noeud est ouvert


	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_getKnot2_2)

        self.assertEquals(out_res,out_data)

#Recherche du noeud qui n'existe pas
    def testGetKnotApsent(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot_res=tv.getKnot([1,0,2])

	assert knot_res==None,'Ce resultat doit etre vide'

#-------------------Tests de parent--------------------------
# Le parent doit etre le noeud-racine
    def testParentRoot(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0])
	parent=knot.parent

	assert parent.tag=='system','le parent de <scanlist> doit etre <system> '

#Verification du parent pour n'importe quel noeud
    def testParentKnot(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2,1])# "<f> 6.432456 5.5 8.5 </f>"
	parent=knot.parent

	assert parent.tag=='layer','le parent de <f> doit etre <layer> '

#---------------------Tests de copy-------------------------
#Copy de l'arbre entier
    def testCopyRootOpen(self):
	tv=Tree()
        tv.initialise(int_openRoot)
	knot_res=tv.copy(tv.root)
	knot_res.open_close()#on ouvre la copy

	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_copyRoot)

        self.assertEquals(out_res,out_data)


#Copy du noeud ferme
    def testCopyClosed(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2]) # "<layer> Fe_Layer..."
	knot_res=tv.copy(knot)

	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_CopyClosed)

        self.assertEquals(out_res,out_data)

#Copy du pointer
    def testCopyPointer(self):
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2,1]) # "<f> "roughness" Gen_Rough </f>"
	knot_res=tv.copy(knot)

	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_CopyPoint)

        self.assertEquals(out_res,out_data)


#La meme chose mais on ouvre le noeud-copy
    def testCopyOpened(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2])# "<layer> Fe_Layer..."
	knot_res=tv.copy(knot)
	knot_res.open_close()

	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_CopyOpened)

        self.assertEquals(out_res,out_data)

# Verification que le changement dans l'arbre ne change pas la copy
    def testCopyArbreChange(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2,2])# "ifo"
	knot_res=tv.copy(knot)
	knot.open_close()#on ouvre le noeud dans l'arbre,la copy est ferme !!!

	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=tv.unitTest_getText(knot)
	out_data=Transformation_str(out_data)

        assert out_res!=out_data, 'Le changemant dana l"arbre ne change pas la copi'

    def testCopyComment(self):
	tv=Tree()
        tv.initialise(in_copyCom)
	knot=tv.getKnot([0])# "<!-- ... -->"
	knot_res=tv.copy(knot)

	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=tv.unitTest_getText(knot)
	out_data=Transformation_str(out_data)

        assert out_res==out_data, 'les probleme avec la copy du commentaire'

# Verification que le changement dans la copy ne change pas l'arbre
    def testCopyChange(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2,2])# "ifo"
	knot_res=tv.copy(knot)
	knot_res.open_close()#on ouvre la copy,le noeud dans l'arbre est ferme

	out_res =tv.unitTest_getText(knot_res)
	out_res=Transformation_str(out_res)
	out_data=tv.unitTest_getText(knot)
	out_data=Transformation_str(out_data)

        assert out_res!=out_data, 'Le changemant dana l"arbre ne change pas la copi'
#-----------------test de copy Special------------------------------
    def testCopySpecial(self):
	tv=Tree()
        tv.initialise(in_delDefWithoutRef)
	knot=tv.getKnot([1])# "<scan>..."

	out_res =tv.getTextForCopy(knot)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_CopySpecial)
	self.assertEquals(out_res,out_data)


#---------------- test d'inserction du noeud dans l'abre-----
#Inserction comme l'enfant du noeud racine
    def testInserctionRoot(self):
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2,2]) # "ifo"
	knot_copy=tv.copy(knot) #on fait la copy de "ifo"
	tv.insert(0,tv.root,knot_copy) #inserction comme le premier enfant
	tv.root.open_close() #ouverture de la racine

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_InsertRoot)

	self.assertEquals(out_res,out_data)

# Inserction a l'interieur de l'arbre
    def testInsertionKnot(self):
    	tv=Tree()
        tv.initialise(in_getKnot)

	knot=tv.getKnot([0,0,2])# "<layer> Fe_Layer..."
	knot.open_close()
	knot_copy=tv.copy(knot) # la copy de " <layer> Fe_Layer..."
	tv.checkNames(knot_copy) # on verifie si touts les noms sont uniques

	parent=knot.parent # le parent est ceux du noeud dont la copy on a fait,i.e. <scan>
	tv.insert(2,parent,knot_copy)
	parent.open_close()

	out_res=tv.unitTest_getText(parent)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_InserKnot)

	self.assertEquals(out_res,out_data)

    def testParentAfterInsert(self):
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2])# "<layer> Fe_Layer..."
	knot_copy=tv.copy(knot) # la copy de " f "
	parent=knot.parent # le parent est ceux du noeud dont la copy on a fait,i.e. <scan>
	tv.insert(2,parent,knot_copy)

	assert knot_copy.parent==parent, 'Le champ parent doit etre remplir apres insert'

    def testInsertin2Trees(self):
    	tv1=Tree()
        tv1.initialise(in_getKnot)
	#tv1.root.openAll()
	knot=tv1.getKnot([0,0,2])# "<layer> Fe_Layer..."
	knot_copy=tv1.copy(knot) # la copy de " f "
	#knot_copy.openAll()
	#print knot.pointer,knot_copy.pointer

	tv2=Tree()
        tv2.initialise('<system></system>')
	tv2.checkNames(knot_copy) # on verifie si touts les noms sont uniques

	parent=tv2.root
	tv2.insert(0,parent,knot_copy)
	tv2.root.openAll()

	out_res=tv2.unitTest_getText(parent)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_InsertIn2Trees)

	self.assertEquals(out_res,out_data)

#-------------teste del -----------------------
#On enleve le noeud simple i.e. noeud-"feuil" sans def et sans ref
    def testDelSimple(self):
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([1]) # "<s>data.txt </s>"
	knot.parent.open_close()#ouvertur de la racine
	tv.delete(knot)

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_DelSimple)

	self.assertEquals(out_res,out_data)

# On enleve le noeud avec def mais sans les enfants
    def testDelDefSimple(self):
    	tv=Tree()
        tv.initialise(in_delDefinition)
	knot=tv.getKnot([0]) # '<f>"weight_col" Gen_Weight 1.0 </f>'
	knot.parent.open_close()
	tv.delete(knot)# ce noeud ne doit pas etre supprime

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_delDefSimple)

	self.assertEquals(out_res,out_data)

#On enleve le noeud qui contient les def et tout les ref sur eux
    def testDelDefWithRef(self):
     	tv=Tree()
        tv.initialise(in_delDefinition)
	knot=tv.getKnot([1]) #'<scan>...'
	knot.parent.open_close()
	tv.delete(knot) #ce noeud doit etre supprime

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_delDefinition)

	self.assertEquals(out_res,out_data)

#On enleve le noeud qui contient les def et les ref sur eux de l'exterieur
    def testDelDefWithoutRef(self):
     	tv=Tree()
        tv.initialise(in_delDefWithoutRef)
	knot=tv.getKnot([1]) #'<scan>...'
	knot.parent.open_close()
	tv.delete(knot) # ce noeud ne doit pas etre supprime

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_delDefWithoutRef)

	self.assertEquals(out_res,out_data)

#on essaye d'enlever la definition sur laquel il y a la reference plus bas, ce ne marche pas
#puis on supprime cette reference et seulement apres on peut supprimer la defonition
    def testDelPointer(self):
    	tv=Tree()
        tv.initialise(in_delDefinition)
	knot_def=tv.getKnot([0]) #<f>"weight_col" Gen_Weight 1.0 </f> la definition
	knot_def.parent.open_close()
	tv.delete(knot_def) # ce noeud ne doit pas etre supprime car il y a des reference sur lui

	knot_point=tv.getKnot([2])#<f>"weight_col" Gen_Weight</f> la reference
	tv.delete(knot_point)#on supprime le reference

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_delPointer1)
	self.assertEquals(out_res,out_data)

	tv.delete(knot_def)#on essaye de suppremer encore une fois la definition et on peut maint.

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_delPointer2)

	self.assertEquals(out_res,out_data)

#la verification de bon retablissements des references sur la definition apres l'essaye de suppression
    def testDelDefPointer(self):
    	tv=Tree()
        tv.initialise(in_delDefWithoutRef)
	knot_def=tv.getKnot([0])# <f>"weight_col" Gen_Weight 1.0 </f>
	knot_def.parent.open_close()
	tv.delete(knot_def) # ce noeud ne doit pas etre supprime car il y a des reference sur lui

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_delDefWithoutRef)
	self.assertEquals(out_res,out_data)

	knot_point=tv.getKnot([1])#<scan> avec la reference sur la definition precedente et avec 	une autre reference
	tv.delete(knot_point)#ce noeud ne doit pas etre supprime car il possede des reference

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_delDefWithoutRef)
	self.assertEquals(out_res,out_data)

	tv.delete(knot_def)#on essaye de supprimer le premier noeud encore une fois pour verifier 	que tous les fererences ont ete bien retablie apres la suppression precedente

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_delDefWithoutRef)
	self.assertEquals(out_res,out_data)


# ---------Tests pour la fonction WhereAmI------------------------

    def testWhereAmI(self):  # test de la methode whereAmI( para, col)
	tv=Tree()
        tv.initialise(in_provachiave)
	tv.root.openAll()
	knotToFind=tv.getKnot([0])
	foundedKnot, wheremsg,  posObj = tv.whereAmI(5,9)
	self.assertEquals( knotToFind, foundedKnot)
	self.assertEquals(wheremsg ,  "Inside Tag End" )

    def testWhereAmIFStart(self):  # test de la methode whereAmI( para, col)
	tv=Tree()
        tv.initialise(in_provachiave)
	tv.root.openAll()
	knotToFind=tv.getKnot([0,0])
	foundedKnot, wheremsg,  posObj = tv.whereAmI(2,5)
	self.assertEquals( knotToFind, foundedKnot)
	self.assertEquals(wheremsg ,  "Inside Tag Start" )

    def testWhereAmIFKey(self):  # test de la methode whereAmI( para, col)
	tv=Tree()
        tv.initialise(in_provachiave)
	tv.root.openAll()
	knotToFind=tv.getKnot([0,0])
	foundedKnot, wheremsg,  posObj = tv.whereAmI(2,8)
	self.assertEquals( knotToFind, foundedKnot)
	self.assertEquals(wheremsg ,  "On Key" )

    def testWhereAmIFValue(self):  # test de la methode whereAmI( para, col)
	tv=Tree()
        tv.initialise(in_provachiave)
	tv.root.openAll()
	knotToFind=tv.getKnot([0,0])
	foundedKnot, wheremsg,  posObj = tv.whereAmI(2,18)
	self.assertEquals( knotToFind, foundedKnot)
	self.assertEquals(wheremsg ,  "On Values" )

# -----------tests pour DictObject--------------
    def testDictObject(self):
    	myDict=DictObject()
	myDict.append('one')
	myDict.CountPlusOne()
	myDict.append('two')
	myDict.CountPlusOne()
	myDict.append('three')
	myDict.CountPlusOne()

	keyFound=myDict.getKey('two')
	keyToFound=1

	self.assertEquals(keyFound , keyToFound)
# -----------test pour ecriture du XML bloque-------------------
     # creer le text xml pour le root
    def testWriteXMLBlockRoot(self):
     	tv=Tree()
        tv.initialise(in_getKnot)

	out_res=tv.getXMLBlock(tv.root)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(in_getKnot)
	self.assertEquals(out_res,out_data)

     #ecrire le text xml pour la definition
    def testWriteXMLBlockDef(self):
     	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2]) # layer ...

	out_res=tv.getXMLBlock(knot)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_xmlBlockDef)
	self.assertEquals(out_res,out_data)
# reference <f> sur <f>

    def testWriteXMLBlockRefFOnF(self):
     	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2,1]) # f..

	out_res=tv.getXMLBlock(knot)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_xmlBlockRefF)
	self.assertEquals(out_res,out_data)
    
# reference <layer> sur <layer>
    def testWriteXMLBlockRef(self):
     	tv=Tree()
        tv.initialise(in_xmlBlockRef)
	knot=tv.getKnot([2]) # layer..

	out_res=tv.getXMLBlock(knot)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_xmlBlockRef)
	self.assertEquals(out_res,out_data)
# le cas de l'objet <s>
    def testWriteXMLBlockS(self):
     	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,0]) # s.

	out_res=tv.getXMLBlock(knot)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_xmlBlockRefS)
	self.assertEquals(out_res,out_data)
# reference de <ifo> sur <f>
    def testWriteXMLBlockRefOnF(self):
     	tv=Tree()
        tv.initialise(in_xmlBlockRef)
	knot=tv.getKnot([0,0,3])

	out_res=tv.getXMLBlock(knot)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_xmlBlockRefOnF)
	self.assertEquals(out_res,out_data)
#reference de <f> sur <layer>
    def testWriteXMLBlockRefFOnLayer(self):
     	tv=Tree()
        tv.initialise(in_xmlBlockRef)
	knot=tv.getKnot([1]) # s.

	out_res=tv.getXMLBlock(knot)
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_xmlBlockRef)
	self.assertEquals(out_res,out_data)

#--------------tests de dictionaire des enfants possible
    #on a cree le dict des enfants possible par les main
    '''def testDictOfChild(self):
     	tv=Tree()
        tv.initialise(in_xmlBlockRef)

	assert len(tv.possibleChild.dictOfList)==5,'La longeur de ce dictionaire doit etre egale a 5'

	assert tv.possibleChild.values('system')==['scanlist','f','layer'], 'On a remplli mal les enfants de <system>'
	assert tv.possibleChild.values('scanlist')==['scan'], 'On a remplli mal les enfants de <scanlist>'
	assert tv.possibleChild.values('scan')==['s','f','layer','ifo'], 'On a remplli mal les enfants de <scan>'
	assert tv.possibleChild.values('layer')==['f','ifo'], 'On a remplli mal les enfants de <layer>'
	assert tv.possibleChild.values('ifo')==['f'], 'On a remplli mal les enfants de <ifo>'

    def testDictOfChildString(self):
   	tv=Tree()
        tv.initialise(in_xmlBlockRef)

	out_res=tv.possibleChild.getStrOfList('scan')
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_testDictOfChildScan)
	self.assertEquals(out_res,out_data)

	out_res=tv.possibleChild.getStrOfList('layer')
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_testDictOfChildLayer)
	self.assertEquals(out_res,out_data)

# -------------testes de dictionnaire de valeures et de cles des attrubues possibles-----------------

    def testDictOfAttrVal(self):
     	tv=Tree()
        tv.initialise(in_xmlBlockRef)
	# valeures
	#assert len(tv.possibleAttrVal)==2,'La longeur de ce dictionaire doit etre egale a 2'
	assert tv.possibleAttrVal['scan']==['wavelenghts_col','material'], 'On a remplli mal les attr. de <scan>'
	assert tv.possibleAttrVal['layer']==['thickness','roughness','material'],'On a remplli mal les attr de <layer>'

    def testDictOfAttrKey(self):
     	tv=Tree()
        tv.initialise(in_getAttrKey)
	#cles
	#assert len(tv.possibleAttrKey)==4,'La longeur de ce dictionaire doit etre egale a 2'
	assert tv.possibleAttrKey['stack']==['repetitions','key'], 'On a remplli mal les attr. de <stack>'
	assert tv.possibleAttrKey['dv']==['key'],'On a remplli mal les attr de <dv>'
	assert tv.possibleAttrKey['f']==['key'],'On a remplli mal les attr de <f>'
	assert tv.possibleAttrKey['ifo']==['key'],'On a remplli mal les attr de <ifo>'


#----------Teste pour getAttrVal du noeud---------------
    def testGetKey(self):  # test de la methode
	tv=Tree()
        tv.initialise(in_provachiave)
	knot=tv.getKnot([0])

	chiave1= knot.getchildren()[1].getAttrVal()
	chiave0= knot.getchildren()[0].getAttrVal()

	self.assertEquals( chiave1,"chiave1")
	self.assertEquals( chiave0,"chiave0")

    def testGetKeyNone(self):  # test de la methode getKey()
	tv=Tree()
        tv.initialise(in_getKeyNone)
	knot=tv.getKnot([0])

	chiave1= knot.getchildren()[1].getAttrVal()
	chiave0= knot.getchildren()[0].getAttrVal()

	self.assertEquals( chiave1,"")
	self.assertEquals( chiave0,"")'''


#---------- testes pour la creationdu nouvel noeud a partir de l'aide----------
    def testCreationNoeud(self):
     	tv=Tree()
        newKnot=tv.creatNewKnot('ift',['matirial'])
	newKnotToend=tv.creatNewKnot('f',['phikness'])

	assert newKnot.__class__.__name__=='General_object','Le type de <ift> doit etre General_object'
	assert newKnot.getAttrVal()==['matirial'],'Le nom de l"attribue doit etre ["matirial"]'

	assert newKnotToend.__class__.__name__=='Float_object'
	assert newKnotToend.getAttrVal()==['phikness'],'Le nom de l"attribue doit etre ["phikness"] '
	assert len(newKnotToend.getchildren())==1,'Le nombre des child de f doit etre egal a 1 (c"est variable)'

# ----------------------testes pour la suppretion des enfants---------------
    def testDelChildren(self):
    	tv=Tree()
        tv.initialise(in_getKnot)

	assert tv.root.__len__()==3,'Le nombre des enfants doit etre egal a 3'
	tv.deleteChildren(tv.root)
	assert tv.root.__len__()==0,'Le nombre des enfants doit etre egal a 0'

    def testDelChildreForF(self):
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([2])

	assert knot.__len__()==1,'Le nombre des enfants doit etre egal a 1'
	tv.deleteChildren(knot)
	assert knot.__len__()==0,'Le nombre des enfants doit etre egal a 0'


#--------------testes pour DocManager-----------------------------------------
    # creation du document (deja avec un view) et recuperation de la liste des views
    def testCreateDoc(self):
    	win=TreeApp()
    	docManager=DocMenager(win)# on recupere mainWidget comme la pripriete de win

	numberOfDocsBefore=docManager.numberOfDocuments()#le nonbre des documents
	numberOfViewsBefore=docManager.numberOfView()# le nombre des view

	newDoc=docManager.createDocument(Tree,TreeView)# on a cree le nouveau document
	views=docManager.view(newDoc) #on a recupere la liste de touts les views du newDoc

	numberOfDocsAfter=docManager.numberOfDocuments()#le nonbre des documents
	numberOfViewsAfter=docManager.numberOfView()# le nombre des view

	assert newDoc!=None,'Aucun documents n"a pas ete cree!'
	assert len(views)==1,'Aucun view n"a pas ete cree!'

	assert numberOfDocsBefore==0,'Aucun documents n"existe pas encore!'
	assert numberOfViewsBefore==0,'Aucun view n"existe pas encore!'
	assert numberOfDocsAfter==1,'Aucun documents n"a pas ete ajoute!'
	assert numberOfViewsAfter==1,'Aucun view n"a pas ete ajoute!'

    #recuperation du document a partir du view
    def testDocFromViews(self):
	win=TreeApp()
    	docManager=DocMenager(win)# on recupere mainWidget comme la pripriete de win

	newDoc=docManager.createDocument(Tree,TreeView)# on a cree le nouveau document
	view=docManager.view(newDoc)[0]#on a recupere le view de la liste des view

	doc=docManager.document(view)

	assert newDoc==doc,'C"est pas le bon document'

    # Fremeture du view (le document correspondant doit etre aussi ferme si le view est dernier)
    def testCloseView(self):
	win=TreeApp()
    	docManager=DocMenager(win)# on recupere mainWidget comme la pripriete de win

	newDoc=docManager.createDocument(Tree,TreeView)# on a cree le nouveau document
	numberOfDocsBefore=docManager.numberOfDocuments()#le nonbre des documents
	numberOfViewsBefore=docManager.numberOfView()# le nombre des view
	view=docManager.view(newDoc)[0]

	docManager.closeView(view)
	numberOfDocsAfter=docManager.numberOfDocuments()# quand on ferme le dernier view, le document est ferme aussi
	numberOfViewsAfter=docManager.numberOfView()

	assert numberOfViewsBefore>numberOfViewsAfter,'View n"a pas ete ferme!'
	assert numberOfDocsBefore>numberOfDocsAfter,'Document n"a pas ete ferme!'

    #recuperation du documnet selon du nom du fichier (None s'il n'y a pas encore)
    def testDocumnetWhithName(self):
	win=TreeApp()
    	docManager=DocMenager(win)# on recupere mainWidget comme la pripriete de win

	fileName='/mntdirect/_qt_users/vorobyev/TestCode/test3_xml.xml'
	newDoc=docManager.createDocument(Tree,TreeView)
	newDoc.setFileName(fileName)
	document=docManager.documentWhithName(fileName)

	self.assertEquals(newDoc,document)

	fileName='/mntdirect/_qt_users/vorobyev/TestCode/test_xml.xml'
	document=docManager.documentWhithName(fileName)

	self.assertEquals(document,None)
    '''
    def testCloseDocument(self):
	win=TreeApp()
    	docManager=DocMenager(win)# on recupere mainWidget comme la pripriete de win

	newDoc=docManager.createDocument(Tree,TreeView)# on a cree le nouveau document
	numberOfDocsBefore=docManager.numberOfDocuments()#le nonbre des documents
	numberOfViewsBefore=docManager.numberOfView()# le nombre des view

	docManager.closeDocument(newDoc)
	numberOfDocsAfter=docManager.numberOfDocuments()# quand on ferme le dernier view, le document est ferme aussi
	numberOfViewsAfter=docManager.numberOfView()

	#print numberOfViewsBefore,numberOfViewsAfter
	#print numberOfDocsBefore,numberOfDocsAfter

	assert numberOfViewsBefore>numberOfViewsAfter,'View n"a pas ete ferme!'
	assert numberOfDocsBefore>numberOfDocsAfter,'Document n"a pas ete ferme!'
    # Fermeture de touts les documents
    def testCloseAllDocuments(self):
	win=TreeApp()
    	docManager=DocMenager(win)# on recupere mainWidget comme la pripriete de win

	Doc1=docManager.createDocument(Tree,TreeView)# on a cree le nouveau document
	Doc2=docManager.createDocument(Tree,TreeView)# on a cree le nouveau document
	Doc3=docManager.createDocument(Tree,TreeView)# on a cree le nouveau document

	docManager.closeAll()

	#print numberOfDocsAfter,numberOfViewsAfter

	numberOfDocsAfter=docManager.numberOfDocuments()# quamd on ferme le dernier view, le document est ferme aussi
	numberOfViewsAfter=docManager.numberOfView()

	assert numberOfDocsAfter==0,'touts les documents doivent etre fermes'
	assert numberOfViewsAfter==0,'touts les views doivent etre fermes'
	'''

# ---------------testes pour modified() du Tree-----------------------------

    def testModifiedAfterDelete(self):
    	modified=0
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([1]) # "<s>data.txt </s>"
	if (tv.delete(knot)):
		modified=1
	res=tv.isModified()
	assert res==modified,"L'arbre doit etre modifie !!!"

    def testModifiedAfterNotDelete(self):
    	modified=0
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.root # "<s>data.txt </s>"
	if (tv.delete(knot)):
		modified=1

	assert tv.isModified()==modified,"L'arbre ne doit etre modifie !!!"

    def testModifiedAfterInsert(self):
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot=tv.getKnot([0,0,2,2]) # "ifo"
	knot_copy=tv.copy(knot) #on fait la copy de "ifo"
	tv.insert(0,tv.root,knot_copy) #inserction comme le premier enfant

	assert tv.isModified()==1,"L'arbre doit etre modifie !!!"

    def testModifiedDeleteChildren(self):
	tv=Tree()
        tv.initialise(in_getKnot)
	tv.deleteChildren(tv.root)

	assert tv.isModified()==1,"L'arbre doit etre modifie !!!"

#--------------tests de remplissage de la liste doubleName_list (listes des objets avec les noms pas uniques
    def testListOfObjectDoubleName(self):
    	tv=Tree()
	newName='Fe_Thick'
        tv.initialise(in_getKnot)

	knot1=tv.getKnot([0,0,2]) # "<Layer> Fe_Layer ...
	knot1.setName(newName)
	tv.refreshVariables()

	assert len(tv.doubleName_list)==2,"La liste doubleName_list doit posseder 2 objets !!!"

	knot2=tv.getKnot([2]) # "<f>"weight_col"  1.0 </f>
	knot2.setName(newName)
	tv.refreshVariables()

	assert len(tv.doubleName_list)==4,"La liste doubleName_list doit posseder 4 objets !!!"

	knot1.setName('Fe_Layer ')
	tv.refreshVariables()

	assert len(tv.doubleName_list)==2,"La liste doubleName_list doit posseder 2 objets !!!"

	knot2.setName('name')
	tv.refreshVariables()

	assert len(tv.doubleName_list)==0,"La liste doubleName_list ne doit pas les objets !!!"

    #on verifie avant sauvegarder le doc s'il y a les objets problematiques...
    def testSaveDocNotCorrect(self):
    	tv=Tree()
	newName='Fe_Thick'
        tv.initialise(in_getKnot)

	knot1=tv.getKnot([0,0,2]) # "<Layer> Fe_Layer ...
	knot1.setName(newName)
	tv.refreshVariables()

	assert tv.isCorrect()==0,'On ne peut pas sauvegarder le document car il les object avec les noms pas uniques'

	knot1.setName('Fe_Layer ')
	tv.refreshVariables()
	assert tv.isCorrect()==1,'On peut sauvegarder le document '

#-----------------------------------tests pout la fonction de noeud isType--------

    def testIsType(self):
    	tv=Tree()
        tv.initialise(in_getKnot)
	knot1=tv.getKnot([0,0,2,2]) # "ifo"
	knot2=tv.getKnot([0,0,1]) # "f"
	knot3=tv.getKnot([1]) # "s"

	assert knot1.isType('ifo')==1,'Le type de ce noeud doit etre ifo '
	assert knot2.isType('f')==1,'Le type de ce noeud doit etre f'
	assert knot3.isType('f')==0,'Le type de ce noeud ne doit pas etre f'

#-------------------------testes pour  la verification que le document qu'on ouvre est correct-----------
#pour <f>
    # verifie le cas la ref. est plus haut que la def. (sans le sauvgardage du nom)
    def testIsFCorrect(self):
    	tv=Tree()
        tv.slotInitialise(in_checkF)
	#tv.updateRefForProblemObj()

	#tv.root.open_close()
	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_checkF)
	self.assertEquals(out_res,out_data)

    # verifie le cas des nom pas uniques
    def testIsFCorrectName(self):
    	tv=Tree()
        tv.slotInitialise(in_checkFName)
	#tv.updateRefForProblemObj()

	#tv.root.open_close()
	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_checkFName)
	self.assertEquals(out_res,out_data)

#pour les autres tag
    # verifie le cas la ref. est plus haut que la def. (sans le sauvgardage du nom)
    def testIsCorrect(self):
    	tv=Tree()
        tv.slotInitialise(in_check)
	tv.root.openAll()

	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_check)
	self.assertEquals(out_res,out_data)

    #verifi que la reference etait bien chage
    def testUpdateRefForProblemObj(self):
	tv=Tree()
        tv.slotInitialise(in_check)

	out_res=tv.getKnot([0]).pointer # on recupere le pointeur
	out_data=tv.getKnot([1])

	self.assertEquals(out_res,out_data)

    # verifie le cas des nom pas uniques
    def testIsCorrectName(self):
    	tv=Tree()
        tv.initialise(in_checkName)
	tv.updateRefForProblemObj()

	tv.root.openAll()
	out_res=tv.getText()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_checkName)

	self.assertEquals(out_res,out_data)

 #-----------------tests pour verifier que le documetn est valide-----------
    '''def testValidationSimpleDef(self):
    	tv=Tree()
        tv.initialise(in_validationSmDef)

	knotS=tv.getKnot([0,0]) #<s> tot.txt...
	variable=knotS.getVariable()
	variable.setValue('',0)

	knotf=tv.getKnot([1,2]) #<f> name 20...
	variable=knotS.getVariable()
	variable.setValue('',0)

	out_res=tv.isValidated()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_validationSmDef)
	self.assertEquals(out_res,out_data)

    def testValidationLenChildren(self):
    	tv=Tree()
        tv.initialise(in_validationLen)

	out_res=tv.isValidated()
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(out_validationLen)
	self.assertEquals(out_res,out_data)'''


#------------test pour Undo------------------------------------
    def testUndoDel(self):
    	tv=Tree()
	tv.initialise(in_getKnot)
	tv.root.open_close()#ouvertur de la racine
	Before=tv.getText()

	knot=tv.getKnot([1]) # "<s>data.txt </s>"
	tv.delete(knot)

	tv.Undo()
	After=tv.getText()

	out_res=Before
	out_res=Transformation_str(out_res)
	out_data=Transformation_str(After)

	self.assertEquals(out_res,out_data)

#--------------Test de la fonction getValKnot qui recupere la valeur d'enfant recherche a partir de son parent
    def testgetValKnot(self):
    	tv=Tree()
	tv.initialise(int_openRoot)
	text1=tv.getValKnot('scan','s')
	text2=tv.getValKnot('system','s')
	text3=tv.getValKnot('scan','f')
	text4=tv.getValKnot('system','scan')

	assert text1=='DATA/data_ref_w.dat','Le text de s de scan doit etre egal a "DATA/data_ref_w.dat"'
	assert text2=='data.txt','Le text de s de system doit etre egal a "data.txt"'
	assert text3=='3','Le text de  premier f de scan doit etre egal a 3'
	assert text4=='','Le text de scan doit etre vide'

#-----------teste pour la fonction de Variables qui revoie la liste de touts les noms-------
    def testAllVariables(self):
    	tv=Tree()
	tv.initialise(in_getKnot)
	names=tv.variables.allVariables()
	knot=tv.getKnot([0,0,2,0])

	assert len(names)==1,'la longueur de la liste doit etre =1 !'
	assert knot==names[0],"c'est doit etre <f>!"
	'''assert names[0]=='Gen_Rough', 'le 1er element doit etre Gen_Rough'
	assert names[1]=='Fe_Layer', 'le 1er element doit etre Fe_Layer'
	assert names[2]=='Fe_Thick', 'le 1er element doit etre Fe_Layer'
	assert names[3]=='Fe_ind_one', 'le 1er element doit etre Fe_Layer'
	'''


if __name__ == "__main__":
    unittest.main()
