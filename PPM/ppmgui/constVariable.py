

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import os
import glob

import tipo
TENSORIAL=tipo.TENSORIAL

import dabax

DABAX_DIR=dabax.DABAX_DIR

# print " DABAX_DIR " , DABAX_DIR

help_for_tf0 = "The f0 table.( giving the x-ray form factors). In reality For multilayers calculation only the averaged density is used but... we are using the general purpose Dabax tables. You have to specify a filename that will be looked for inside directory :DABAX_DIR = " + DABAX_DIR+""

s_like_the_child_of_tf0 =" give a filename for tf0. They are looked for in directory :DABAX_DIR = " + DABAX_DIR+" You can choose in the list : "+str(map(os.path.basename,  glob.glob(os.path.join(DABAX_DIR,"f0*.dat" ))  ))


help_for_tf12 = "The f12 table. It gives the anomalous f1 and f2 form factors. You have to specify a filename that will be looked for inside directory :DABAX_DIR = " + DABAX_DIR+""

s_like_the_child_of_tf12=" give a filename for tf12. They are looked for in directory :DABAX_DIR = " +DABAX_DIR+" You can choose in the list : "+str(map(os.path.basename, glob.glob(os.path.join(DABAX_DIR,"f1f2*.dat" ))  )  )


f_like_the_child_of_comparison_write="put a reference to a fit object "
s_like_the_child_of_comparison_write="the root of the filename to which data are written.  For each scan a file is written pospending the number 1 to n, where n is the number of scans. The file contain columns : first is the calculation, second the wavelenght, third, the angle, fourth the data"


help_for_f="multipurpose tag : it can be used to reference another object when it contains just a name(put reference to true in the edit panel that you get with ctrl-e). It can be used also as  a numeric value or a variable if you specify value, min, max "

root_of_document="root of document : you can items double cliking in the add_a_new_element window on your right. For each item in such window you get context sensitive information by clicking F1. You can also start typing a name in the lin-editor below add_a_new_element window and all items matching are shown"

help_for_s="this tag creates a string. Just type your string when you edit the item (ctrl-e)"

help_for_layer="This object is used to build a stack. A stack is your model and is composed by several layer. You can create layer inside a stack, or create them outside and reference them with a variable name inside a stack. The first layer you enter in a stack is at the bottom and is considered as a semiinfinite  substrate regardeless of its thickness. The last layer you add to a stack is at the top"

help_for_stack="A stack is your model and is composed by several layer. You can create layer inside a stack, or create them outside and reference them with a variable name inside a stack. The first layer you enter in a stack is at the bottom and is considered as a semiinfinite  substrate regardeless of its thickness. The last layer you add to a stack is at the top. Each time the tag stack appears in your file the layers inside are added to your model. You have also two properties accessible with ctrl-e : the number of repetition and the stack id. This latter means that you can have sevral model in your system. By default the stack-id (nstack) is 0 but it can go up to 9"


help_for_bff="Reads the absorption from a file. This tag is meant to provide an object usable in a KK transformation.  The important keys are filename, shift and factor.  The file must be formed by two columns, the first giving the energy, the second should be something proportional to beta, the imaginary part of the optical index. Rescale X lambda is set to one when one has raw data  for absorption. In that case beta is proportional to absorption  time lambda/lambda0 where lambda0 is the  middle of the scan."


help_for_scan="Reads a scan from a multicolumns file specified by filename. wavelenghts_col can be integer to indicate the wavelenght columns, or a float to specify manually the wavelenght. Same thing for angle_col. refle_col is the columns of reflectivity. weight_col is  the columns of statisthical weight or a float ( for example 1.0). angle_factor  is a factor to transform your angles in radians, for example if your angles are degree you can enter an expressione like  arccos(-1)/180.0. norm (optional) is a factor by which you multiply the calculated reflectivity before comparing it to data. CutOffRatio (optional) is the geometrical cutoff ratio. SYNTHETIC SCANS can be generated omitting the filename and specifiyng only wavelenghts_col angles_col angle_factor.wavelenghts_col and   wavelenghts_col  can be either fixed to a float or given as range , for example [0.01,50,0.02] for angle_col generates a angle scan going from 0.01 to 50 with 0.02 steps.  it is important when you give ranges or expressions like arccos(-1)/180.0. not to leave spaces. For scalar case polarisation is implicitely S."
if TENSORIAL :
    help_for_MagScatterer="Builds a magnetic-scatterer object which returns a dielectric tensor. Yu must provide either two files or two beta objects defining the optical constant for the two opposites elicity. In case of files, files must contain three columns : energy, real part, imaginary part"
    help_for_scan=help_for_scan+" For the tensorial case you have to use the argument with keyword  polarisation. Polarisation can be defined to be circular, elliptical or linear. The polarisation must be a list of triplets. Each triplet is formed by a weight factor, a toggle index, and a vector formed by complex numbers. The vector has two complex component : S and P amplitude. Their squares modulus must sum up to 1. The toggle index (0/1) tells if instead of angle theta in the calculation one considers PI-theta. Finally the weight weight the reflectivity result of each triplet in the list, and total sum of wighted reflectivity is the result of the scan. For example the expression  [(0.5,0,(1.0/math.sqrt(2.0),1.0j/math.sqrt(2.0))),(-.5,1,(1.0/math.sqrt(2.0),1.0j/math.sqrt(2.0)))] calculate the dichroic reflectivity ( half difference) for circular polarisation"


s_like_the_child_of_scan="name of a  multicolumns file specified by filename(string)"

help_for_scanlist="All the scans you create inside the scanlist tags are calculated. You can have several scanlists in your document, the lists will be merged and all the scans considered."


help_for_ift="You can, by this tag, create an optical index representing a material. IFT means index from table. The tables used are those specified by the last tf0, tf12 object. The arguments are a list of elements names each one of them followed immediately by its partial density given in grams per cubic centimeter."

help_for_ifo=  "IFO means index from Object filters an index object  adding a multiplicative factor for the density. Handy to filter object like KK "

help_for_dv="DV means dependent variable. It has one argument, a string. The string may be a python expression. In such expression you can reference other variables. To use other variables you need to envelop them with the par() macro. In the expression you can use self.depth which is the layer number starting from zero at the substrate or self.VarCounter which is the number of times the dependent variable has been accessed when you retrieve the multilayer structure ( starting from the bottom)"

help_for_KK="it creates an index object which gives optical index as a result of a KK transformation. Arguments can be parametrized and therefore fitted. it means also that at each calculation of the model the KK is calculated once. If your KK does not contain free variables ( or dependent from free ) you can avoid repetitions of KK trnasformation writing once for all  your index to a file with KK_write and reading it later  with index from file(iff). The list of arguments is quite long, help for argument is retrievable argument-wise by populating the tag and with F1. To resume the most important things you need to provide   an absorption describing  a resonance. This absorption is a betaObject like the ones that you can create with bff, bfc, bfl, bsum, bjoin and must cover the whole range of integration. The  result  is joined to an optical index which is provided thorugh  the material argument. This argument  can create or reference objects created by ift, iff etcetera. The other arguments tells how to do the inegrations, the extrema, how to join calculation to tables, the number of points, the grid to use to be more precise close to the resonances... "



help_for_KKFFT=" All as KK, using fft on a interpolated uniform grid to replace integrations "


help_for_iff="IFF means index from file. Two arguments are necessary : the file name  and the relative density ( a multiplicative factor). The file must contain a serie of lines containing  (Energy, Real(n), Imm(n)) in ascending energy order" 

help_for_KK_write="Allows to write on a file the result of KK but also of an index object.  Arguments : a KK object the starting energy, the final energy, the number of points and a string with the filename."

help_for_Beta_write="Allows to write on a file the result of KK.  arguments : a KK object the starting energy, the final energy, the number of points and a string with the filename."



help_for_Beta_write="Allows to write on a file the result of a bet aobject .  arguments : a beta object the starting energy, the final energy, the number of points and a string with the filename."

help_for_comparison_write="Write the calculations for your models and compared to the specified scans. it has two argument : one <f> reference to a fit object  and a string argument for the filename to which data are written. For each scan a file is written pospending the number 1 to n, where n is the number of scans."


help_for_fit="This tag is almost entirely authomatic : the programs knows how to create a fit using the scanlist, your stacks, noises, normalisation factors etctera. You need nonetheless to create a fit object if you want to use comparison_write or minimisation. The best thing is to reference it : create something like <fit> myfit </fit>  and reference it by myfit. You can add a smoothing parameter with keyword width. Units are number of points. "


help_for_Minimise="This tag launches an annealed amoeba minimisation. You need to specify the variable referencing your fit created by fit tag. Other arguments are there but you are helped with default values and generally you dont need to change them. This minimisation usually creates jobs that lasts for long long times. You can monitor them looking at partial result in the PARTIAL directory. Another minimisation method based on Levenberg-Marquart is available which uses Armando python GeFit routing and is accessible with the tag MinimiseGeFit."


help_for_MinimiseGeFit=" Armando Sole python GeFit routing using Levenberg-Marquardt. Just give your fit as argument. Derivatives of first order are calculated internally using finite differences. Differences are 0.00001 times the parameters"

help_for_bfc="Beta from continuum. When you syntethize beta you need peacks, and step for transitions to continuum. this tag takes as arguments 'E0', 'step', 'pente', 'arctanfact'. E0 is the position of the the step, step is the step height, pente is the slope after E0 and arctanfact enters this expression modulating the step =result*(0.5+Numeric.arctan( ( energies-E0)* par(self.arctanfact) ) / Numeric.pi"


help_for_bfl="BFL means beta from lorentz. Gives a lorentzian centered at E0, you can use different values for gamma-right (gammaR) and gamma-left(gammaL). To get a symmetric lorentzian you can reference the value given for gammaL with a variable for gammaR"


help_for_bsum="bsum as beta from sum. As arguments a list of couples (beta-object , coefficient)"

help_for_ifos="ifos like index from objects, it is similar to IFO which  index object  adding a multiplicative factor for the density, but it mixes several objects. Arguments are a list of couples (index-object, factor)"

help_for_Write_Stack="Helps you inspect what is happening. Writes to a file for each layer a line containing the thikness, the roughness, the energy and the optical index ( real and imaginary part ). You give as argument the stack you want ( for exaple stacks[0] ), the energy at which you want the indexes and the files for output"

help_for_arrofvar=" Create an array of variables. The use of this is with an object which does interpolation from an array of variables like bfi. The created array must be added to the list of known variables, as usually you want to optimise them with the tag addvars. The arguments are the number of variables  NP, then each created variable is associated to an energy going from  Xmin to Xmax. The starting  value for each variable  is given by value. and also min, max as for f"

help_for_addvars="If you have created an array of variable, you have to reference it "


help_for_bfi="a beta obtained by intepolation over an array of variable. Arguments : a beta object to initialise the variables(BetaObject), an array or an array of array of variables(arrs), a name of file to monito what is happening, each time the object is used it writes the result to a file for monitoring (nameForView) "


help_for_bjoin="Joins a beta object defining beta on a small range  to a beta defined on a large extent. Arguments : betaobject the beta on a small range, material an index object generally coming from tables, trimleft and trimright in eV to restrain trimming the range over which the betaobject is used "

#-----------le dict des enfants possibles avec la precision des types pour les valeurs des attributs
possibleChild_={"ift":{"generic":['s','f'], },"iff":{"generic":['s','f'], 
                                                     "NameFile":['s'],
                                                     "RelativeDensity":['f','dv']},
                "bfc":{"generic":['f',"dv"], },
                "layer":{"generic":['f', 'ift','ifo','ifos','iff','dv'],
                         "material":['f', 'ift','ifo','ifos','iff'],
                         'thickness':['f','dv'], 'roughness':['f','dv']
                         },
               "bff":{"generic":['s','f'], "filename":["s"],  'shift':['f','dv'], 'factor':['f'], 'rescaleXlambda':['f']},
               "fit":{"generic":['f'], },
               "scan":{"generic":['s','f'],  'filename':['s'], 'wavelenghts_col':['f'], 'angles_col':['f'],
                       'refle_col':['f'],'weight_col':['f'], 'angle_factor':['f'], 'norm':['f'], 'noise':['f'], 'CutOffRatio':['f']},
               "KK":{"generic":['f',"bjoin","bsum","bff","bfi","ift",'iff',"ifo","ifos",],"filename_or_betaObject":['f',"bjoin","bsum","bff","bfi"],"material":['f',"ift","ifo","ifos",'iff'] ,
                     "E1":['f'], "E2":['f'],"N":['f'],"e1":['f'],"e2":['f'],"Fact":['f'], #"Dx":['f'],
                     "maglia":['f'],"Nmaglia":['f'] },
               "KKFFT":{"generic":['f',"bjoin","bsum","bff","bfi","ift",'iff',"ifo","ifos",],"filename_or_betaObject":['f',"bjoin","bsum","bff","bfi"],"material":['f',"ift","ifo","ifos",'iff'] ,
                     "E1":['f'], "E2":['f'],"N":['f'],"e1":['f'],"e2":['f'],"Fact":['f'], #"Dx":['f'],
                     "maglia":['f'],"Nmaglia":['f'] },
                
               "bfl":{"generic":['f','dv'], },
               "tf12":{"generic":['s'], },
               "tf0":{"generic":['s'], },
               "system":{"generic":['tf0', 'tf12', 'scanlist', 'bff', 'ift','iff',"ifos","ifo", 'stack', 'f','layer',"dv",
                          'bfl','bfc','bsum','bjoin','KK','KKFFT','fit','KK_write','comparison_write','MinimiseGeFit', "Minimise","Beta_write",], },
               "stack":{"generic":['layer','stack'], },
               "bjoin":{"generic":['f','ift'], },
               "MinimiseGeFit":{"generic":['f'], },
               "bsum":{"generic":['f'], },
               "dv":{"generic":['s'], },
               "KK_write":{"generic":["KK","KKFFT","ifo","ifos","ift","iff",'f','s'], "KKobj":["KK","KKFFT","ifo","ifos","ift","iff"],
                           "minE":['f'], "maxE":['f'],"n":['f'] , "outn":['s']},
               "Beta_write":{"generic":['f','s'], "Bobj":['f'],
                           "minE":['f'], "maxE":['f'],"n":['f'] , "outn":['s']},
               "ifos":{"generic":['f','f']},
               "ifo":{"generic":['f'],"material":['f'],"RelativeDensity":['f'] },
               "comparison_write":{"generic":['f','s'],  "fit":['f'],"filename":["s"] },
               "scanlist":{"generic":['scan'], },
                "Minimise":{"generic":['f',"s"], "fit":['f'],"temperature":["s"],"max_refusedcount":['f'] ,
                            "max_isthesame":['f'],"centershiftFact":['f'] }

              }

if TENSORIAL :
    scandic=possibleChild_["scan"]
    scandic["Polarisation"]=["f"]

    possibleChild_["MagScatterer"]={"generic":["f","s"],"NameFilePlus":["f","s"], "NameFileMinus":["f","s"],
                                    "versor":["f"],"RelativeDensity":["f"], "Saturation":["f"]}
    possibleChild_["system"]["generic"].append("MagScatterer")
    possibleChild_["fit"]["meritfunction"]=["s"]
    
## possibleChild={}
## for key in possibleChild_.keys():
##     possibleChild[key]=possibleChild_[key]["generic"]
    
cPossibleChild=possibleChild_


#----------------le dict des valeurs des atrtibuts pour les elements-parents
cPossibleAttrVal={"tf0":['Name'],
                 "tf12":['Name'],
                 "bfc":['E0', 'step', 'pente', 'arctanfact',"min","max"],
                 "layer":['thickness', 'roughness','material'],
                 "bff":["filename", 'shift', 'factor', 'rescaleXlambda'],
                 "fit":['width']+TENSORIAL*["meritfunction"],
                 "scan":['filename','wavelenghts_col', 'angles_col', 'refle_col',
                         'weight_col', 'angle_factor', 'norm', 'noise', 'CutOffRatio']+TENSORIAL*["Polarisation"],
                 "KK":['filename_or_betaObject', 'material', 'E1', 'E2', 'N',
                       'e1', 'e2', 'Fact',  'maglia', 'Nmaglia'],
                 "KKFFT":['filename_or_betaObject', 'material', 'E1', 'E2', 'N',
                       'e1', 'e2', 'Fact',  'maglia', 'Nmaglia'],
                 "KK_write":["KKobj","minE", "maxE","n" , "outn"],
                  "iff":[ "NameFile","RelativeDensity"],
                 "Beta_write":["Bobj","minE", "maxE","n" , "outn"],
                 "bfl":['E0', 'height','gammaL', 'gammaR', 'min', 'max'],
                 "bjoin":['betaobject','material','trim_left','trim_right'],
                 "MinimiseGeFit":['fit'],
                 "ifo":['material','RelativeDensity'],
                 "comparison_write":["fit","filename"] ,
                 "Minimise":["fit","temperature","max_refusedcount" ,
                             "max_isthesame","centershiftFact"]
                 }
if TENSORIAL:
    cPossibleAttrVal["MagScatterer"]=["NameFilePlus", "NameFileMinus","versor","RelativeDensity", "Saturation"]



#possibleAttrKey={"ift":['key'],"f":['key'],"bjoin":['key'],"dv":['key'],"ifo":['key'],"stack":['repetitions']}

#-----------le dict des valeur par defaut pour certain elements selon la valeur d'attribut
cDefaultValues={"noise":'0.0', "temperature":".05*exp(-0.2*x)","max_refusedcount":"100",
               "max_isthesame":"10","centershiftFact": "0.5"}


filename_bff="name of the file. The file must be formed by two columns, the first giving the energy, the second should be something proportional to beta, the imaginary part of the optical index."
shift_bff=    'Want to shift in energy?'
factor_bf=    'rescale the absorption'
rescaleXlambda_bff=    'Rescale X lambda is set to one when one has raw data  for absorption. In that case beta is proportional to absorption  time lambda/lambda0 where lambda0 is the  middle of the scan'


E0_bfl     ="center of lorentzian"
height_bfl ="lorentzian height" 
gammaL_bfl="gamma for left part"
gammaR_bfl="gamma for right part"
min_bfl    ="min of the range. The range is considered in bjoin. Optional"
max_bfl    ="max of the range. The range is considered in bjoin. Optional"


E0_bfc=" Center of the step"
step_bfc= " height of the step" 
pente_bfc= "  pente is the slope after E0 "
arctan_bfc=" arctanfact enters this expression modulating the step =result*(0.5+Numeric.arctan( ( energies-E0)* par(self.arctanfact) ) / Numeric.pi" 


NameFile_iff = "name of the file containing three columns : Energy (eV) Real(n), Imm(n)) in ascending energy order" 

RelativeDensity_iff = "Defaults to 1.0 ( optional ). If different from zero this factor is the rescaling factor for the density, that means that the rescale index will be n'= 1+(n-1)*RelativeDensity "





filename_or_betaObject_KK="an absorption describing  a resonance. This absorption is a betaObject like the ones that you can create with bff, bfc, bfl, bsum, bjoin and must cover the whole range of integration. The  result  is joined to an optical index which is provided thorugh  the material argument."
material_KK="this is an optical index (for example ift ) to which the KK transformation is joined at e1, e2"
E1_KK="The KK integral starts at E1, must be in the range of the beta object"
E2_KK="The KK integral ends at E2, must be in the range of the beta object"
N_KK="this is indicatively the number of point for the integration"
e1_KK="The result of KK transformation is sticked to the material at this energy"
e2_KK="The result of KK transformation is sticked to the material at this energy"
Fact_KK="this factor multiplies  the beta before transformation"
# Dx_KK="if you really want to shift your absortpion...."
maglia_KK="close to resonance you have more details. The point of integration might be preferable to be finer. Giving here a betaobject which is read from a file, the energy points contained in the file are used to build the finer grid that you need to integrate the resonance"
Nmaglia_KK=" the steps provided by the above maglia argument, are further subdivided by this factor"

betaobject = "an absorption describing  a resonance. This absorption is a betaObject like the ones that you can create with bff, bfc, bfl"



#---------------------------------le dic de help general
cGeneralHelp={"f": help_for_f,
           "s":  help_for_s ,
           "layer":help_for_layer,
           "bff":help_for_bff,
           "stack": help_for_stack,
           "scan":help_for_scan,
           "scanlist":help_for_scanlist,
           "ift": help_for_ift ,
           "ifo":help_for_ifo,
           "dv": help_for_dv,
           "KK": help_for_KK,
           "KKFFT": help_for_KKFFT,
           "bjoin": help_for_bjoin,
           "iff": help_for_iff,
           "KK_write": help_for_KK_write,
           "tf0":help_for_tf0,
           "tf12":help_for_tf12,
           "Beta_write": help_for_Beta_write,
           "comparison_write":help_for_comparison_write,
           "fit":help_for_fit,
           "Minimise": help_for_Minimise ,
           "MinimiseGeFit": help_for_MinimiseGeFit ,
           "bfc": help_for_bfc ,
           "bfl": help_for_bfl ,
           "bsum":help_for_bsum,
           "ifos": help_for_ifos,
           "Write_Stack":  help_for_Write_Stack,
           "arrofvar": help_for_arrofvar,
           "addvars":  help_for_addvars,
           "bfi": help_for_bfi,
           "setLayerInterpolation":'help for Interpolation',
           "system":root_of_document,
            }

if TENSORIAL:
    cGeneralHelp["MagScatterer"]=help_for_MagScatterer

tag_translation_4_WhatsThisAttr={
    "KKFFT":"KK"
}
#whatsThisTag={'stack':'stack is very complex element\n There are different possibilities to use it'}

#------------------le dict avec la sinification de chaque valeur d'attribut
cWhatsThisAttr={

    "wavelenghts_col":" wavelenghts_col can be integer to indicate the wavelenght columns, or a float to specify manually the wavelenght ",
    "angles_col":"angles_col can be integer to indicate the angles columns, or a float to specify manually the angle",
    'refle_col':"refle_col is the columns of reflectivity (to be omitted for synthetic scans ) ",
    'weight_col':"weight_col is  the columns of statisthical weight or a float ( for example 1.0)(to be omitted for synthetic scans ) ",
    'angle_factor':"angle_factor  is a factor to transform your angles in radians, for example if your angles are degree you can enter an expressione like  arccos(-1)/180.0",
    'norm':"norm (optional) is a factor by which you multiply the calculated reflectivity before comparing it to data",
    'noise':"optinal , an offset to be added to reflectivity",
    'CutOffRatio': "CutOffRatio (optional) is the geometrical cutoff ratio. If you provide the cutoffratio (cor) reflectivity is rescale by  CutOffRescale = Numeric.clip(angles/par( cor ), 0.0,1.0) ", 

    "filename_bff":filename_bff,
    'shift_bff':shift_bff,
    'factor_bff':factor_bf,
    'rescaleXlambda_bff':rescaleXlambda_bff, 

    "E0_bfl":     E0_bfl,
    "height_bfl" :height_bfl,
    "gammaL_bfl":gammaL_bfl,
    "gammaR_bfl":gammaR_bfl,
    "min_bfl":    min_bfl,
    "max_bfl":    max_bfl,

    "E0_bfc":     E0_bfc,
    "step_bfc":   step_bfc ,
    "pente_bfc":   pente_bfc,
    "arctan_bfc":  arctan_bfc,
    "min_bfc":    min_bfl,
    "max_bfc":    max_bfl,

    "NameFile_iff":NameFile_iff ,
    "RelativeDensity_iff" : RelativeDensity_iff, 

    'filename_or_betaObject':filename_or_betaObject_KK,
    'material_KK':material_KK,
    'E1_KK':E1_KK,
    'E2_KK':E2_KK,
    'N_KK':N_KK,
    'e1_KK':e1_KK,
    'e2_KK':e2_KK,
    'Fact_KK':Fact_KK,
    # 'Dx_KK':Dx_KK,
    'maglia_KK':maglia_KK,
    'Nmaglia_KK':Nmaglia_KK,

    'betaobject':betaobject,

    'thickness':'thickness of the layer in angstroems( can be <f> , <dv> or a reference to it )',
    'roughness':'roughness in angstroems( can be <f> , <dv> or a reference to it )',
    'material':'this argument must be an optical index, generated by tags ift, KK, ifos  etcetera...',
    "width":"This is an optional argument. Width parameters triggers a gaussain smooting. Width unit is number of points. A width equal to  0 means no smoothing",

    "KKobj":"A KK object, but also an index object should work. Such object index ( real and imaginary part)will be written on a file for the energy range you specify  ",
    "minE":"lower extrema of the energy range to plot",
    "maxE":"upper extrema of the energy range to plot",
    "n":" the number of points in the range" ,
    "outn":"the output file name",
    "Bobj":"A reference to a Beta object. Such object will be written on a file  ",
    
    }


#-----------------------------the help for the elements like the child
if TENSORIAL:
    cWhatsThisAttr["Polarisation"]=" EXAMPLE  [(0.5,0,(1.0/math.sqrt(2.0),1.0j/math.sqrt(2.0))),(-.5,1,(1.0/math.sqrt(2.0),1.0j/math.sqrt(2.0)))]  ----- For the tensorial case you have to use the argument with keyword  polarisation. Polarisation can be defined to be circular, elliptical or linear. The polarisation must be a list of triplets. Each triplet is formed by a weight factor, a toggle index, and a vector formed by complex numbers. The vector has two complex component : S and P amplitude. Their squares modulus must sum up to 1. The toggle index (0/1) tells if instead of angle theta in the calculation one considers PI-theta. Finally the weight weight the reflectivity result of each triplet in the list, and total sum of wighted reflectivity is the result of the scan. For example the expression  [(0.5,0,(1.0/math.sqrt(2.0),1.0j/math.sqrt(2.0))),(-.5,1,(1.0/math.sqrt(2.0),1.0j/math.sqrt(2.0)))] calculate the dichroic reflectivity ( half difference) for circular polarisation"

    cWhatsThisAttr["NameFilePlus"]="Object or file giving the index for elicity Plus"
    cWhatsThisAttr["NameFileMinus"]="Object or file giving the index for elicity Minus"
    cWhatsThisAttr["versor"]="expression giving the direction of magnetization in its x,y,z components. X longitudinal, in the sample plane. Z vertical. An example is Numeric.array([1.0,.0,0.0])"
    cWhatsThisAttr["RelativeDensity"]=" Factor to rescale the density of the objects giving the indexes "
    cWhatsThisAttr["Saturation"]="modulate the magnetic saturation. if you give 0 you will end up with a non magnetic material, giving one you the two indexes for plus and minus elicities are those given by NameFilePlus and  NameFilePlus ( apart from RelativeDensity rescaling ). You can give floating point numbers, 0.5 will give you something in the middle, 1.5 something even more magnetic.."

    cWhatsThisAttr["meritfunction"]="By default the merit function is the sum of absolute value of logarithm of the ratio between calculation and experiment. In case of magnetism, when you fit to dichroic signal you may get negative value. You can choose diffroot option, wby which the error function is the sum of squared differences, or you can choose sin4 where the difference is further multiplied by the fourth power of the sinus of the angle before squaring. Just type sin4 or diffroot"

    
                                    
#-------------------------------the help for the elements like the child
childrenIft={"s":'enter the name of an element. This tag is always coupled in ift to an f tag containing the density',
             "f":'Enter the partial density of the element in g/cc . This tag is always coupled in ift to an s tag containing the element name'}
childrenIfos={"f":'For each couple of entry the first is an index object the second is the weight. The weight are considered as relative density and rescales (n-1)'}
# childrenBfc={"f":'f like the child of bfc means...'}
#childrenLayer={"f":'f like the child of layer means...',"ift":'ift like the child of Layer means...',
#		"ifo":'ifo like the child of layer means...',"dv":'dv like the child of layer means...'}
childrenBff={"s":'s like the child of bff means...',"f":'f like the child of bff means...'}
childrenFit={"f":'f like the child of fit means...'}
childrenScan={"s": s_like_the_child_of_scan,}
# childrenKK={"bjoin":'bjoin like the child of KK means...',"f":'f like the child of KK means...'}
#childrenBfl={"f":'f like the child of bfl means...'}
childrenTf12={"s": s_like_the_child_of_tf12}
childrenTf0={"s":s_like_the_child_of_tf0}
# childrenStack={"layer":'layer like the child of stack means...'}
childrenBjoin={"f":'f like the child of bjoin means...',"ift":'ift like the child of bjoin means...'}
childrenMinGeFit={"f":'put here a reference to your fit'}
childrenBsum={"f":'bsum takes an even number of arguments, the first of each couple is a reference to a beta-object the other a multiplicative factor',
              }
childrenDv={"s":'s like the child of dv means...'}
# childrenKKWrite={"f":'f like the child of KK_write means...',"s":'s like the child of KK_write means...'}
# childrenIfo={"f":'f like the child of ifo means...'}
childrenCompWrite={"f":f_like_the_child_of_comparison_write ,"s":s_like_the_child_of_comparison_write}
# childrenScanlist={"scan":'scan like the child of scanlist means...'}

cWhatLikeChildren={"ift":childrenIft,
                   "ifos":childrenIfos,
                  #"bfc":childrenBfc,
                  # "layer":childrenLayer,
                  #"bff":childrenBff,
		#"fit":childrenFit,
		"scan":childrenScan,
		# "KK":childrenKK,
                  # "bfl":childrenBfl,
		"tf12":childrenTf12,"tf0":childrenTf0,# "stack":childrenStack,
                  #"bjoin":childrenBjoin,
		  "MinimiseGeFit":childrenMinGeFit,
                  "bsum":childrenBsum,
                  #"dv":childrenDv,
                  # "KK_write":childrenKKWrite,
                  # "ifo":childrenIfo,
                  "comparison_write":childrenCompWrite,
                  # "scanlist":childrenScanlist
                  }


perCoppia={"ift":["s","f"],"bsum":["f","f"],"ifos":["f","f"]}
