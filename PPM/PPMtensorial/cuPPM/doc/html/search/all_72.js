var searchData=
[
  ['reflectivity',['Reflectivity',['../classcuda_reflectivity.html#a3d5ad2bc1b80ce6dfc6eb0f874b31e58',1,'cudaReflectivity']]],
  ['releasecudadeprecmemory',['releaseCudaDeprecMemory',['../class_p_p_m_tensorial__cuda.html#ad0c07d50d3f525bac145f061055635bb',1,'PPMTensorial_cuda']]],
  ['releasecudamemory',['releaseCudaMemory',['../class_p_p_m_tensorial__cuda.html#aaa82f83336d571cd610810730c5c8e5a',1,'PPMTensorial_cuda']]],
  ['releasecudaremainmemory',['releaseCudaRemainMemory',['../class_p_p_m_tensorial__cuda.html#a43995c9d91e1342545df615acb8e6383',1,'PPMTensorial_cuda']]],
  ['resettimers',['resetTimers',['../class_p_p_m_tensorial__cuda.html#ab3e1c002698fd43a9b63045f93b76d7c',1,'PPMTensorial_cuda']]]
];
