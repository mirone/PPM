
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include "cuda_runtime_api.h"
#include "cuComplex.h"

#ifndef MODES_H
#include "cuPPMextension_modes.h"
#define MODES_H
#endif

#define blockSize 32
#define CACHELINE_BOUNDARY 128
#define CBOUND CACHELINE_BOUNDARY
//__PRETTY_FUNCTION__ -- GCC, MetroWerks, Digital Mars and ICC
//__FUNCSIG__ -- MSVC
//__FUNCTION__ -- Intel and IBM
//__FUNC__ -- Borland
//__func__ -- ANSI C99

#define CERR(_expr)                                                         \
   do {                                                                             \
     cudaError_t _err = _expr;                                                           \
     if (_err == cudaSuccess)                                                        \
       break;                                                                       \
     sprintf(msg,"%s (@ %d): %s\n",__FUNCTION__,__LINE__,cudaGetErrorString(_err)); \
       throw msg; \
   } while (0)

cuOnda::cuOnda(){
  cuOnda_GPU_time=0.0;
}

cuOnda::~cuOnda(){

}

/**
 * getModesResults is only used when debug_flag is set in cuPPM.
 * In a normal execution it is not needed (and should not be used to avoid latency).
 * getModesResults returns the results of cuOnda.
 *
 * @param scanLen The number of scanpoints
 * @param uniquelayers The number of layers with a unique set of dielectric matrices
 */
void cuOnda::getModesResults(int &scanLen,int &uniquelayers,cuDoubleComplex *d_Kz,cuDoubleComplex *d_E,cuDoubleComplex *d_B,
                        complex<double> *Kz,complex<double> *E,complex<double>*B){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  char msg[1000];

  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);
/**/
  /*cuErr = */cudaEventRecord(event_s);
/**/
  /*cuErr = */cudaMemcpy(Kz,d_Kz,scanLen * 4 * (uniquelayers) * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
  /*cuErr = */cudaMemcpy(E,d_E,scanLen * 4 * (uniquelayers) * 3 * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
  /*cuErr = */cudaMemcpy(B,d_B,scanLen * 4 * (uniquelayers) * 3 * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
/**/
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
/**/
  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": elapsed time: " << cuTime_ms << " (ms) " << std::endl;
#endif
  
  //cuOnda_GPU_time += cuTime_ms;

  return;
}

/**
 * getModesResults_debug is only used when debug_flag is set in cuPPM.
 * In a normal execution it is not needed (and should not be used to avoid latency).
 * getModesResults_debug returns intermediate results of cuOnda
 *
 * @param scanLen The number of scanpoints
 * @param uniquelayers The number of layers with a unique set of dielectric matrices
 */
void cuOnda::getModesResults_debug(int &scanLen,int &uniquelayers,int &summode,double *d_Kx,cuDoubleComplex *d_Sistem, int *d_NonZeri,
                                  cuDoubleComplex *d_As, double *Kx, complex<double> *Sistem, int *NonZeri, complex< double > *As){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  char msg[1000];

  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);
/**/
  /*cuErr = */cudaEventRecord(event_s);
/**/
  /*cuErr = */cudaMemcpy(Kx,d_Kx,scanLen * sizeof(double),cudaMemcpyDeviceToHost);
  /*cuErr = */cudaMemcpy(As,d_As,scanLen * 4 * (uniquelayers) * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
  /*cuErr = */cudaMemcpy(Sistem,d_Sistem,scanLen * 4 * summode * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
  /*cuErr = */cudaMemcpy(NonZeri,d_NonZeri,scanLen * 4 * (uniquelayers) * sizeof(int),cudaMemcpyDeviceToHost);
/**/
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
/**/
  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": elapsed time: " << cuTime_ms << " (ms) " << std::endl;
#endif
  
  return;
}

//The kernels to be used for Modes. Visible only to the kernel calling functions
__global__ void setKx(double *d_Kx);
__global__ void getAs(double *d_Kx, cuDoubleComplex *d_die, cuDoubleComplex *d_As); //It transposes As!
__global__ void getKz(cuDoubleComplex *d_As,cuDoubleComplex *d_Kz);
__global__ void setSistem(cuDoubleComplex *d_Sistem, double *d_Kx, cuDoubleComplex *d_Kz, cuDoubleComplex *d_die);
__global__ void calcNonzeri(cuDoubleComplex *d_Sistem, cuDoubleComplex *d_E, int *d_NonZeri); //d_E should also be transposed for coalescion!
__global__ void calcZeri(cuDoubleComplex *d_Sistem, cuDoubleComplex *d_E, int *d_NonZeri);
__global__ void calcB(double *d_Kx, cuDoubleComplex *d_Kz, cuDoubleComplex *d_E, cuDoubleComplex *d_B);
__global__ void cuModesScalar(double *d_Kx, cuDoubleComplex *d_Kz, cuDoubleComplex *d_die, cuDoubleComplex *d_E, cuDoubleComplex *d_B);
__global__ void cuModesTensor_Vide(double *d_Kx, cuDoubleComplex *d_Kz, cuDoubleComplex *d_die, cuDoubleComplex *d_E, cuDoubleComplex *d_B);
 

/**
 * CreateModes invokes the kernels found in cuPPMkerners_modes.cu.
 * Layers are treated as a bunch and are calculated all at once.
 *
 * @param scanLen Number of scanpoints
 * @param uniquelayers Number of layers with unique dieletric matrices
 */
//CreateModes is the actual processor. Goes through all the layers including the substrate
void cuOnda::CreateModes(int &scanLen,int &uniquelayers,double *d_Kx,cuDoubleComplex *d_die,cuDoubleComplex *d_As,cuDoubleComplex *d_Kz,cuDoubleComplex *d_Sistem,
                                cuDoubleComplex *d_E,cuDoubleComplex *d_B, int *d_NonZeri, cuDoubleComplex *As){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms,totTime_ms=0;
  dim3 block,grid,gridl;
  char msg[1000];

  grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),1,1);
  block=dim3(blockSize,1,1);


  CERR(cudaEventCreate(&event_s));
  CERR(cudaEventCreate(&event_e));
  
  CERR(cudaEventRecord(event_s));
  setKx<<<grid,block>>>(d_Kx);
  CERR(cudaEventRecord(event_e));
  CERR(cudaEventSynchronize(event_s));
  CERR(cudaEventSynchronize(event_e));
  CERR(cudaEventElapsedTime(&cuTime_ms,event_s,event_e));

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel setKx status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  gridl=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),uniquelayers,1);

  CERR(cudaEventCreate(&event_s));
  CERR(cudaEventCreate(&event_e));

  CERR(cudaEventRecord(event_s));
  getAs<<<gridl,block>>>(d_Kx,d_die,d_As);
  CERR(cudaEventRecord(event_e));
  CERR(cudaEventSynchronize(event_s));
  CERR(cudaEventSynchronize(event_e));
  CERR(cudaEventElapsedTime(&cuTime_ms,event_s,event_e));

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel getAs status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  /*cuErr = */cudaEventRecord(event_s);
  getKz<<<gridl,block>>>(d_As,d_Kz);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel getKz status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif  
  totTime_ms+=cuTime_ms;

  /*cuErr = */cudaEventRecord(event_s);
  setSistem<<<gridl,block>>>(d_Sistem,d_Kx,d_Kz,d_die);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
   
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel setSistem status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  /*cuErr = */cudaEventRecord(event_s);
  calcNonzeri<<<gridl,block>>>(d_Sistem,d_E,d_NonZeri);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel calcNonzeri status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  /*cuErr = */cudaEventRecord(event_s);
  calcZeri<<<gridl,block>>>(d_Sistem,d_E,d_NonZeri);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel calcZeri status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  /*cuErr = */cudaEventRecord(event_s);
  calcB<<<gridl,block>>>(d_Kx,d_Kz,d_E,d_B);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel calcB status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  /*cuErr = */cudaEventRecord(event_s);
  cuModesScalar<<<gridl,block>>>(d_Kx,d_Kz,d_die,d_E,d_B);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel cuModesScalar status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  /*cuErr =*/ cudaEventRecord(event_s);
  cuModesTensor_Vide<<<gridl,block>>>(d_Kx,d_Kz,d_die,d_E,d_B);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel cuModesTensor_Vide status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms): "<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);
   
#ifdef _VERBOSE    
  std::cout << __FUNCTION__ << ": Total processing time: " <<totTime_ms << " (ms)" <<std::endl;
#endif  

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

  cuOnda_GPU_time += totTime_ms;
return;
}

//CreateModesTensor runs only one layer tensorially. For testing
void cuOnda::CreateModesTensor(int &scanLen,double *d_Kx,cuDoubleComplex *d_die,cuDoubleComplex *d_As,cuDoubleComplex *d_Kz,cuDoubleComplex *d_Sistem,
                                cuDoubleComplex *d_E,cuDoubleComplex *d_B, int *d_NonZeri, cuDoubleComplex *As){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  dim3 block,grid;
  char msg[1000];
  
  grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),1,1);
  block=dim3(128,1,1);

  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);
/**/
  /*cuErr = */cudaEventRecord(event_s);
  setKx<<<grid,block>>>(d_Kx);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel setKx status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventRecord(event_s);
  getAs<<<grid,block>>>(d_Kx,d_die,d_Kz);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel getAs status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  //Warning! Kz here is the transpose of the C++ version of PPM
  /*cuErr = */cudaEventRecord(event_s);
  getKz<<<grid,block>>>(d_As,d_Kz);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel getKz status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventRecord(event_s);
  setSistem<<<grid,block>>>(d_Sistem,d_Kx,d_Kz,d_die);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel setSistem status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventRecord(event_s);
  calcNonzeri<<<grid,block>>>(d_Sistem,d_E,d_NonZeri);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel calcNonzeri status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventRecord(event_s);
  calcZeri<<<grid,block>>>(d_Sistem,d_E,d_NonZeri);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel calcZeri status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventRecord(event_s);
  calcB<<<grid,block>>>(d_Kx,d_Kz,d_E,d_B);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel calcB status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

  return;
}

//CreateModesScalar runs only on layer the scalar way. For testing
void cuOnda::CreateModesScalar(int &scanLen,double *d_Kx,cuDoubleComplex *d_die,cuDoubleComplex *d_Kz,cuDoubleComplex *d_Sistem,
                                cuDoubleComplex *d_E,cuDoubleComplex *d_B, int *d_NonZeri, cuDoubleComplex *As){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  dim3 block,grid;
  char msg[1000];

  grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),1,1);
  block=dim3(128,1,1);

  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);

  /*cuErr = */cudaEventRecord(event_s);
  setKx<<<grid,block>>>(d_Kx);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel setKx status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventRecord(event_s);
  cuModesScalar<<<grid,block>>>(d_Kx,d_Kz,d_die,d_E,d_B);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel createModesScalar status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }
return;
}

//createModesTensor_Vide is meant to run on the free surface. For testing
void cuOnda::createModesTensor_Vide(int &scanLen,double *d_Kx,cuDoubleComplex *d_die,cuDoubleComplex *d_Kz,cuDoubleComplex *d_Sistem,
                                cuDoubleComplex *d_E,cuDoubleComplex *d_B, int *d_NonZeri, complex<double> *As){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  dim3 block,grid;
  char msg[1000];

  grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),1,1);
  block=dim3(128,1,1);

  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);
/**/
  /*cuErr = */cudaEventRecord(event_s);
  setKx<<<grid,block>>>(d_Kx);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel setKx status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventRecord(event_s);
  cuModesTensor_Vide<<<grid,block>>>(d_Kx,d_Kz,d_die,d_E,d_B);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
  std::cout << __FUNCTION__ << ": kernel createModesTensor_Vide status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;

  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }
return;
}

