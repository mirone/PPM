
nvcc $1 $2 -Xptxas=-v -arch=sm_20 --compiler-options '-fPIC' -o libcuPPM.so --shared -I/usr/local/cuda/include cuPPMextension_base.cu cuPPMextension_modes.cu cuPPMextension_other.cu cuPPMkernels_modes.cu cuPPMkernels_other.cu -lcudart
