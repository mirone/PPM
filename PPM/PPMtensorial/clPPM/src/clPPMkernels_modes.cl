
/**
 * \file
 * \brief Modes Kernels
 *
 * Defines the OpenCL kernels that compute the Electic and Magnetic fields
 */
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

//OpenCL extensions are silently defined by opencl compiler at compile-time:
#ifdef cl_amd_printf
  #pragma OPENCL EXTENSION cl_amd_printf : enable
  //#define printf(...)
#elif defined(cl_intel_printf)
  #pragma OPENCL EXTENSION cl_intel_printf : enable
#else
  #define printf(...)
#endif

#ifdef cl_amd_fp64
  #pragma OPENCL EXTENSION cl_amd_fp64 : enable
#elif defined(cl_khr_fp64)
  #pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

// #include "clDcomplex.cl"

/**
 * \brief Return the real part of a complex double
 */
inline double clCreal(double2 a){
  return a.x;
}

/**
 * \brief Return the imaginary part of a complex double
 */
inline double clCimag(double2 a){
  return a.y;
}

/**
 * \brief Return the conjugate of a complex double
 */
inline double2 clConj(double2 a){
  return (double2)( a.x , -a.y);
}

/**
 * \brief Addition of two complex doubles
 */
inline double2 clCadd(double2 a, double2 b){
  return (double2)(a.x + b.x, a.y + b.y);
};

/**
 * \brief Substraction of two complex doubles
 */
inline double2 clCsub(double2 a, double2 b){
  return (double2)(a.x - b.x, a.y - b.y);
};

/**
 * \brief Multiplication of two complex doubles
 */
inline double2 clCmul(double2 a, double2 b){
  return (double2)( (a.x * b.x) - (a.y * b.y) ,
                    (a.x * b.y) + (a.y * b.x) );
};

/**
 * \brief Division of two complex doubles
 */
inline double2 clCdiv(double2 a, double2 b){
  double denom = ( (b.x * b.x) + (b.y * b.y) );
  double real  = (a.x * b.x) + (a.y * b.y);
  double imag  = (a.y * b.x) - (a.x * b.y);
  return (double2)(real/denom, imag/denom);
};

/**
 * \brief Multiplication of a complex double with a real double
 */
inline double2 clCRmul(double2 a, double b){
  return (double2)(a.x * b, a.y * b);
};

/**
 * \brief Division of a complex double with a real double
 */
inline double2 clCRdiv(double2 a, double b){
  return (double2)(a.x / b, a.y / b);
};

/**
 * \brief Absolute value of a complex double
 */
inline double clCabs(double2 a){
  return sqrt( (a.x * a.x) + (a.y * a.y) );
}

/**
 * \brief Square root of a complex double
 */
inline double2 clCsqrt(double2 a){
  double r,th;
  double2 res;
  r = sqrt ( (a.x * a.x) + (a.y * a.y) );
  th= atan2( a.y, a.x);
  res= (double2)( sqrt(r) * cos(th/2), sqrt(r) * sin(th/2) );
  return res;
};

/**
 * \brief Exponent of a complex double
 */
inline double2 clCexp(double2 a){
  return (double2)( exp(a.x) * cos(a.y), exp(a.x) * sin(a.y) );
};

/**
 * \brief Negation of a complex double
 */
inline double2 negC(double2 a){
  return (double2)( -a.x, -a.y );
};

//MAP complex cuda PPM function calls to the openCL ones
#define cuCadd clCadd
#define cuCsub clCsub
#define cuCmul clCmul
#define cuCdiv clCdiv
#define cuCRmul clCRmul
#define cuCRdiv clCRdiv
#define cuCsqrt clCsqrt
#define cuCexp clCexp
#define cuCreal clCreal
#define cuConj clConj
#define cuCabs clCabs


//Kx is the same for ALL layers! Thus it must be calculated with a grid -> scanLen once!
__kernel void
setKx(__global double *d_Kx, __constant int *scanLen){

    int idx = get_global_id(0);
    if(idx<scanLen[0]){
      d_Kx[idx] = cos(d_Kx[idx]);
    }
};

//revise index - Maybe it would be better to make a transpose kernel
#define d(i,j)  (d_die[ Offsets_die[blockIdx.y] +  ((j)-1) + 3*((i)-1)  + 9*idx ])
#define dS(i)   ( (d_die[ idx + Offsets_die[blockIdx.y] ] ) )
//Fix d_Sistem coallescion
#define d_Sistem(i,j) ( d_Sistem[ Offsets[blockIdx.y] + idx + scanLen[0] * (iwaves +  4*( (i) + 3*( (j) ) ) ) ] )
#define d_E(j,k)     d_E[ idx + scanLen[0]*( (j) + 4* ( (k) + 3 * blockIdx.y ) )]
/*#define d_E(j,k)     d_E[ idx + scanLen*( blockIdx.y + gridDim.y*( (j) + 4*(k) ) )]*/
#define d_B(j,k)     d_B[ idx + scanLen[0]*( (j) + 4* ( (k) + 3 * blockIdx.y ) )]
#define KZ(i) d_Kz[idx + scanLen[0] *(i + 4* blockIdx.y )]

__kernel void
getAs(__global const double *d_Kx, __global double2 *d_die, __global double2 *d_As,
      __constant int *scanLen, __constant int *uniquelayers, __constant int *Modes,
      __constant int *Offsets_die){

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double kx,kx2,kx3,kx4;
  double2 a0,a1,a2,a3,a4,ah,ah0;

  //
  a0 = (double2)(0.0,0.0);
  a1 = (double2)(0.0,0.0);
  a2 = (double2)(0.0,0.0);
  a3 = (double2)(0.0,0.0);
  //

  if(idx < scanLen[0] && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers[0]-1)){
    kx = d_Kx[idx];
    kx2= kx*kx;
    kx3= kx2*kx;
    kx4= kx2*kx2;
    a4= d(3,3)  ;

a0= cuCRmul( d(1,1), kx4);
     ah= cuCRmul ( cuCmul( d(1,2) , d(2,1) ) ,kx2 );//
     a0= cuCadd (a0 , ah);

     ah= cuCRmul ( cuCmul( d(1,1) , d(2,2) ) ,kx2 );
     a0= cuCsub (a0 , ah);

     ah= cuCRmul ( cuCmul ( d(1,3) , d(3,1) ) ,kx2);//
     a0= cuCadd (a0 , ah);

     ah= cuCRmul ( cuCmul ( d(1,1) , d(3,3) ) ,kx2);
     a0= cuCsub (a0 , ah);

     ah= cuCmul ( cuCmul ( d(1,3) , d(2,2) ) , d(3,1) );//
     a0= cuCsub (a0 , ah);

     ah= cuCmul ( cuCmul ( d(1,2) , d(2,3) ) , d(3,1) );//
     a0= cuCadd (a0 , ah);

     ah= cuCmul ( cuCmul ( d(1,3) , d(2,1) ) , d(3,2) );//
     a0= cuCadd (a0 , ah);

     ah= cuCmul ( cuCmul ( d(1,1) , d(2,3) ) , d(3,2) );//
     a0= cuCsub (a0 , ah);

     ah= cuCmul ( cuCmul ( d(1,2) , d(2,1) ) , d(3,3) );//
     a0= cuCsub (a0 , ah);




     ah= cuCmul ( cuCmul ( d(1,1) , d(2,2) ) , d(3,3) );
     a0= cuCadd (a0 , ah);
//
    a3= cuCadd ( cuCRmul( d(1,3) , kx ) , cuCRmul( d(3,1), kx) ) ;

    a2= cuCadd( cuCRmul( d(1,1) , kx2) , cuCRmul( d(3,3) , kx2) );
    ah= cuCadd( cuCmul( d(1,3) , d(3,1) ) , cuCmul( d(2,3) , d(3,2) ) );

    a2= cuCadd( a2, ah);
    ah= cuCadd( cuCmul( d(1,1) , d(3,3) ) , cuCmul( d(2,2) , d(3,3) ) );
    a2= cuCsub( a2, ah);

    a1= cuCadd( cuCRmul( d(1,3) , kx3 ) , cuCRmul( d(3,1) , kx3 ) );
    ah= cuCRmul( cuCmul( d(1,3) , d(2,2) )  , kx);
    a1= cuCsub( a1, ah);

    ah= cuCRmul( cuCmul( d(1,2) , d(2,3) )  , kx);
    a1= cuCadd( a1, ah);

    ah= cuCRmul( cuCmul( d(2,2) , d(3,1) )  , kx);
    a1= cuCsub( a1, ah);

    ah= cuCRmul( cuCmul( d(2,1) , d(3,2) )  , kx);
    a1= cuCadd( a1, ah);


    a3=  cuCdiv( a3 , a4 );
    a2=  cuCdiv( a2 , a4 );
    a1=  cuCdiv( a1 , a4 );
    a0=  cuCdiv( a0 , a4 );

  }
  barrier(CLK_LOCAL_MEM_FENCE);
  if(idx < scanLen[0] && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers[0]-1)){
      d_As[idx + scanLen[0]*(3 + 4 * blockIdx.y )] = a3 ;
      d_As[idx + scanLen[0]*(2 + 4 * blockIdx.y )] = a2 ;
      d_As[idx + scanLen[0]*(1 + 4 * blockIdx.y )] = a1 ;
      d_As[idx + scanLen[0]*(0 + 4 * blockIdx.y )] = a0 ;
  }

}

#define quattro 4.0
#define mezzo 0.5
//1e-7 ratio
__kernel void
getKz(__global double2 *d_As,__global double2 *d_Kz, __constant int *scanLen,
      __constant int *uniquelayers, __constant int *Modes){


  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 sol1,sol2,sol3,sol4;
  double2 ah,ah1,a0,a1,a2,a3;


  if(idx<scanLen[0] && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers[0]-1)){
    sol1=sol2=sol3=sol4=(double2)(0.0,0.0);
    a0 = d_As[idx + scanLen[0]*(0 + 4 * blockIdx.y )];
    a1 = d_As[idx + scanLen[0]*(1 + 4 * blockIdx.y )];
    a2 = d_As[idx + scanLen[0]*(2 + 4 * blockIdx.y )];
    a3 = d_As[idx + scanLen[0]*(3 + 4 * blockIdx.y )];

    for(int i=0; i<3; i++){

      ah= cuCmul( cuCmul( sol1 ,sol1 ) , a3 );
      ah= cuCadd( a1, ah);
      ah= cuCadd( a0, cuCmul(sol1 , ah));
      ah= cuCRmul( ah, quattro );
      ah1=cuCmul( a2, a2);
      ah= cuCsub( ah1, ah);
      ah = cuCsqrt(ah);
      ah1= cuCadd( negC(a2) , ah);
      ah=  cuCRmul( ah1, mezzo);
      sol1=cuCsqrt(ah);


      ah= cuCmul( cuCmul( sol2 ,sol2 ) , a3 );
      ah= cuCadd( a1, ah);
      ah= cuCmul( sol2, ah);
      ah= cuCadd( a0, ah);
      ah= cuCRmul( ah, quattro );
      ah1=cuCmul( a2, a2);
      ah= cuCsub( ah1, ah);

      ah = cuCsqrt(ah);
      ah1= cuCsub( negC(a2) , ah );
      ah=  cuCRmul( ah1, mezzo);
      sol2=cuCsqrt(ah);


      ah= cuCmul( cuCmul( sol3 ,sol3 ) , a3 );
      ah= cuCadd( ah, a1);
      ah= cuCmul( ah, sol3);
      ah= cuCadd( ah, a0);
      ah= cuCRmul( ah, quattro );
      ah1=cuCmul( a2, a2);
      ah= cuCsub( ah1, ah);

      ah = cuCsqrt(ah);
      ah1= cuCadd( negC(a2) , ah);
      ah=  cuCRmul( ah1, mezzo);
      sol3= negC( cuCsqrt(ah) );


      ah= cuCmul( cuCmul( sol4 ,sol4 ) , a3 );
      ah= cuCadd( ah, a1);
      ah= cuCmul( ah, sol4);
      ah= cuCadd( ah, a0);
      ah= cuCRmul( ah, quattro );
      ah1=cuCmul( a2, a2);
      ah= cuCsub( ah1, ah);

      ah = cuCsqrt(ah);
      ah1= cuCsub( negC(a2) , ah);
      ah=  cuCRmul( ah1, mezzo);
      sol4= negC( cuCsqrt(ah) );
    }
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  if(idx < scanLen[0] && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers[0]-1)){
    d_Kz[idx + scanLen[0]*(0 + 4 * blockIdx.y )] = sol1;
    d_Kz[idx + scanLen[0]*(1 + 4 * blockIdx.y )] = sol2;
    d_Kz[idx + scanLen[0]*(2 + 4 * blockIdx.y )] = sol3;
    d_Kz[idx + scanLen[0]*(3 + 4 * blockIdx.y )] = sol4;
  }
}

#undef quattro
#undef mezzo

//d_Kz[idx + scanLen*(iwave + 4 * blockIdx.y )]
//#define d_Sistem(i,j) ( d_Sistem[ Offsets[blockIdx.y] + idx + scanLen * (iwaves +  4*( (i) + 3*( (j) ) ) ) ] )
__kernel void
setSistem(__global double2 *d_Sistem, __global double *d_Kx, __global double2 *d_Kz, __global double2 *d_die,
          __constant int *scanLen, __constant int *uniquelayers, __constant int *Modes, __constant int *Offsets,
          __constant int *Offsets_die){

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 K0,K1,K2;

  if(idx < scanLen[0] && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers[0]-1)){

    K0 = (double2)(d_Kx[idx] , 0.0);
    K1 = (double2)(0.0 , 0.0);
    for (int iwaves=0; iwaves<4; iwaves++){
      K2= d_Kz [idx + scanLen[0] *(iwaves + 4 * blockIdx.y)];

      //       x,y             x, y   ; Fast is x (while on C its y)
      d_Sistem(0,0) = cuCmul( K0, K0) ;
      d_Sistem(0,1) = cuCmul( K0, K1) ;//0
      d_Sistem(0,2) = cuCmul( K0, K2) ;
      d_Sistem(1,0) = cuCmul( K1, K0) ;//0
      d_Sistem(1,1) = cuCmul( K1, K1) ;//0
      d_Sistem(1,2) = cuCmul( K1, K2) ;//0
      d_Sistem(2,0) = cuCmul( K2, K0) ;
      d_Sistem(2,1) = cuCmul( K2, K1) ;//0
      d_Sistem(2,2) = cuCmul( K2, K2) ;

      d_Sistem(0,0) = cuCadd( d_Sistem(0,0) , d(1,1) );
      d_Sistem(0,1) = cuCadd( d_Sistem(0,1) , d(1,2) );//0
      d_Sistem(0,2) = cuCadd( d_Sistem(0,2) , d(1,3) );
      d_Sistem(1,0) = cuCadd( d_Sistem(1,0) , d(2,1) );//0
      d_Sistem(1,1) = cuCadd( d_Sistem(1,1) , d(2,2) );
      d_Sistem(1,2) = cuCadd( d_Sistem(1,2) , d(2,3) );//0
      d_Sistem(2,0) = cuCadd( d_Sistem(2,0) , d(3,1) );
      d_Sistem(2,1) = cuCadd( d_Sistem(2,1) , d(3,2) );//0
      d_Sistem(2,2) = cuCadd( d_Sistem(2,2) , d(3,3) );


      d_Sistem(0,0) = cuCsub( d_Sistem(0,0) , cuCmul( K0, K0));
      d_Sistem(1,1) = cuCsub( d_Sistem(1,1) , cuCmul( K0, K0));
      d_Sistem(2,2) = cuCsub( d_Sistem(2,2) , cuCmul( K0, K0));

      d_Sistem(0,0) = cuCsub( d_Sistem(0,0) , cuCmul( K2, K2));
      d_Sistem(1,1) = cuCsub( d_Sistem(1,1) , cuCmul( K2, K2));
      d_Sistem(2,2) = cuCsub( d_Sistem(2,2) , cuCmul( K2, K2));

    }
  }
}

//#define d_Sistem(i,j) ( d_Sistem[ Offsets[blockIdx.y] + idx + scanLen * (iwaves +  4*( (i) + 3*( (j) ) ) ) ] )
__kernel void
calcNonzeri(__global double2 *d_Sistem, __global double2 *d_E, __global int *d_Zeri, __constant int *scanLen,
            __constant int *uniquelayers, __constant int *Modes, __constant int *Offsets){

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 minori[9],mh;
  double moduli[3];
  double stima,massimo;
  int nonzeri,imassimo;

  if(idx < scanLen[0] && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers[0]-1)){
    for(int iwaves=0;iwaves<4;iwaves++){

      nonzeri = 0;
      massimo = 0.0;
      stima=0;

      //TODO optimise loop order
      for (int i=0; i<3; i++) {
          for (int j=0; j<3; j++) {
              stima=stima +
              cuCreal( cuCmul( d_Sistem(i,j) , cuConj(d_Sistem(i,j)) ) ) ;
          }
      }
      stima=stima*stima;

      for (int i=0; i<3; i++) {
          int i1,i2;
          i1=(i+1)%3;
          i2=(i+2)%3;
          moduli[i]=0.0;
          for (int j=0; j<3; j++) {
              int j1,j2;
              j1=(j+1)%3;
              j2=(j+2)%3;

              minori[j + 3*i]= cuCmul( d_Sistem(i1,j1), d_Sistem(i2,j2) );
              mh= cuCmul( d_Sistem(i1,j2), d_Sistem(i2,j1) );
              minori[j + 3*i]= cuCsub( minori[j + 3*i], mh);

              mh= cuCmul( minori[j + 3*i], cuConj( minori[j + 3*i]) );
              moduli[i] += cuCreal(mh);

//                 mh= cuCmul( d_Sistem[idx + scanLen*( iwaves + 4 * (i + 3*j) ) ], cuConj( d_Sistem[idx + scanLen*( iwaves + 4 * (i + 3*j) ) ] ) );
//                 moduliriga[i] +=   cuCreal(mh);
          }

          if (moduli[i]>1.0e-15*stima) {
            nonzeri++;
          }
          if (moduli[i]>=massimo) {
              imassimo=i;
              massimo=moduli[i];
          }
      }
      d_Zeri[idx + scanLen[0]*(iwaves + 4*blockIdx.y)]=nonzeri;
      if(nonzeri){
        massimo=sqrt(massimo);
        d_E( iwaves  , 0 ) = cuCRdiv( minori[0 + 3 * imassimo],massimo);
        d_E( iwaves  , 1 ) = cuCRdiv( minori[1 + 3 * imassimo],massimo);
        d_E( iwaves  , 2 ) = cuCRdiv( minori[2 + 3 * imassimo],massimo);
      }

    }
  }
}

//#define d_Sistem(i,j) ( d_Sistem[ Offsets[blockIdx.y] + idx + scanLen * (iwaves +  4*( (i) + 3*( (j) ) ) ) ] )
__kernel void
calcZeri(__global double2 *d_Sistem, __global double2 *d_E, __global int *d_Zeri, __constant int *scanLen,
        __constant int *uniquelayers, __constant int *Modes, __constant int *Offsets){

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 newvects[9],mh;
  double massimoriga, moduliriga[3], massimocol,norma;
  int imassimoriga, icol, zeri;

  if(idx < scanLen[0] && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers[0]-1)){
    for(int iwaves=0;iwaves<4;){
      zeri = d_Zeri[idx + scanLen[0]*(iwaves + 4*blockIdx.y)];
      if(!zeri){

        massimoriga=0;
        imassimoriga=0;
        massimocol=0;
        icol=0;
        norma=0.0;

        //TODO optimise loop order
        for (int i=0; i<3; i++) {

          moduliriga[i] = 0;
          for (int j=0; j<3; j++) {
            mh= cuCmul( d_Sistem(i,j), cuConj( d_Sistem(i,j) ) );
            moduliriga[i] +=   cuCreal(mh);
          }

          if (moduliriga[i]>=massimoriga) {
              imassimoriga=i;
              massimoriga=moduliriga[i];
          }
        }

        massimoriga = sqrt(massimoriga);
        for (int k=0; k<3; k++)  {
            newvects[k] = cuCRdiv( d_Sistem(imassimoriga,k) , massimoriga );

            if ( cuCabs(newvects[k])>=massimocol) {
                icol=k;
                massimocol =cuCabs(newvects[k] );
            }
        }

        newvects[(icol+2)%3 +3] = (double2)( 0.0 , 0.0);
        newvects[(icol+1)%3 +3] = newvects[icol];
        newvects[icol +3      ] = negC(newvects[(icol+1)%3 ]);

        mh = cuCadd( cuCmul( newvects[icol +3], cuConj(newvects[icol +3])) , cuCmul( newvects[(icol+1)%3 +3], cuConj(newvects[(icol+1)%3 +3]) ) );
        norma = cuCreal(mh);
        norma=sqrt(norma);
        newvects[ (icol+1)%3 +3] = cuCRdiv( newvects[ (icol+1)%3 +3], norma);
        newvects[ icol +3      ] = cuCRdiv( newvects[ icol +3      ], norma);

        for (int k=0; k<3; k++) {
          newvects[k + 6]= cuCsub( cuCmul( newvects[(k+1)%3], newvects[(k+2)%3 +3]) , cuCmul( newvects[(k+2)%3], newvects[(k+1)%3 +3]) ) ;
          d_E( iwaves  , k ) = newvects[k + 3];
        }
        iwaves++;
        for (int k=0; k<3; k++) {
          d_E( iwaves  , k ) = cuCsub( cuCmul( newvects[(k+1)%3], newvects[(k+2)%3 +3]) , cuCmul( newvects[(k+2)%3], newvects[(k+1)%3 +3]) ) ;
        }
        iwaves++;
      } else{
        iwaves+=4;
      }
    }
  }
}

__kernel void
calcB(__global double *d_Kx, __global double2 *d_Kz, __global double2 *d_E, __global double2 *d_B,
      __constant int *scanLen, __constant int *uniquelayers, __constant int *Modes){

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 K[3], E2B[9];
  double kx;
  if(idx < scanLen[0] && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers[0]-1)){
    kx=d_Kx[idx];
    for (int colo=0; colo<4; colo++)
    {
        K[1]= (double2)(0.0,0.0);
        K[0]= (double2)(kx, 0.0);
        K[2]= d_Kz[idx + scanLen[0]*(colo + 4*blockIdx.y)];
        for(int i=0;i<9;i++)E2B[i]= (double2)(0.0,0.0);
        E2B[1]= negC(K[2]);
        E2B[3]= K[2];
        E2B[2 + 3]=negC(K[0]);
        E2B[1 + 6]=K[0];
        E2B[6]=negC(K[1]);
        E2B[2]=K[1];

//#define E(j,k)      E[  scanPoint*3*4     +(j)*3        +k     ]
//#define B(j,k)      B[  scanPoint*3*4     +(j)*3        +k     ]

        for (int j=0; j<3; j++) {
            d_B(colo,j)= (double2)(0.0,0.0);

            for (int k=0; k<3; k++) {
                d_B(colo,j) = cuCadd( d_B (colo,j), cuCmul(E2B[k + 3*j] , d_E(colo,k  ) ) );
            }
        }
    }
  }

}

__kernel void
clModesScalar(__global double *d_Kx, __global double2 *d_Kz, __global double2 *d_die, __global double2 *d_E,
              __global double2 *d_B, __constant int *scanLen, __constant int *uniquelayers, __constant int *Modes,
              __constant int *Offsets_die){

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 eps,kx2,kx,one;

  one = (double2)( 1.0, 0.0);

  if(idx <scanLen[0] && Modes[blockIdx.y]==1 && blockIdx.y!=(uniquelayers[0]-1)){
    eps = dS(0);
    kx  = (double2)( d_Kx[idx], 0.0);
    kx2 = cuCmul( kx, kx);
    for ( int i=0; i<2; i++) {
        KZ(i)= cuCsqrt( cuCsub( cuCmul(one,eps), kx2 ) );
    }
    for ( int i=2; i<4; i++) {
        KZ(i)= negC( cuCsqrt( cuCsub( cuCmul(one,eps), kx2 ) ) );
    }

    d_E( 0 , 0 ) = (double2)( 0.0, 0.0);
    d_E( 2 , 0 ) = (double2)( 0.0, 0.0);

    d_E( 1 , 0 ) = negC( KZ(1) );
    d_E( 3 , 0 ) = negC( KZ(3) );


    d_E( 0 , 1 ) = one;
    d_E( 2 , 1 ) = one;
    d_E( 1 , 1 ) = (double2)( 0.0, 0.0);
    d_E( 3 , 1 ) = (double2)( 0.0, 0.0);


    d_E( 0 , 2 ) = (double2)( 0.0, 0.0);
    d_E( 2 , 2 ) = (double2)( 0.0, 0.0);
    d_E( 1 , 2 ) = kx;
    d_E( 3 , 2 ) = kx;

    /////////////////////////////////////////////////////
    d_B( 0 , 0 ) = negC( KZ(0) );
    d_B( 2 , 0 ) = negC( KZ(2) );

    d_B( 1 , 0 ) = (double2)( 0.0, 0.0);
    d_B( 3 , 0 ) = (double2)( 0.0, 0.0);


    d_B( 0 , 1 ) = (double2)( 0.0, 0.0);
    d_B( 2 , 1 ) = (double2)( 0.0, 0.0);
    d_B( 1 , 1 ) = negC( cuCmul( one, eps) );
    d_B( 3 , 1 ) = negC( cuCmul( one, eps) );


    d_B( 0 , 2 ) = kx;
    d_B( 2 , 2 ) = kx;
    d_B( 1 , 2 ) = (double2)( 0.0, 0.0);
    d_B( 3 , 2 ) = (double2)( 0.0, 0.0);
  }

}

//Here in the test the last layer was tensorial... but because it is last it is treated like this:
#define dSv(i)   ( (d_die[ 9*idx + Offsets_die[blockIdx.y] ] ) )
__kernel void
clModesTensor_Vide(__global double *d_Kx, __global double2 *d_Kz, __global double2 *d_die, __global double2 *d_E,
              __global double2 *d_B, __constant int *scanLen, __constant int *uniquelayers, __constant int *Modes,
              __constant int *Offsets_die){

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 eps,kx2,kx,one;

  one = (double2)( 1.0, 0.0);

  if(idx <scanLen[0] && blockIdx.y==(uniquelayers[0]-1) ){
    if(Modes[uniquelayers[0]-1]==3)eps = dSv(0);
    else eps = dS(0);
    kx  = (double2)( d_Kx[idx], 0.0);
    kx2 = cuCmul( kx, kx);
    for ( int i=0; i<2; i++) {
        KZ(i)= cuCsqrt( cuCsub( cuCmul(one,eps), kx2 ) );
    }
    for ( int i=2; i<4; i++) {
        KZ(i)= negC( cuCsqrt( cuCsub( cuCmul(one,eps), kx2 ) ) );
    }

        d_E( 0 , 0 ) = (double2)( 0.0, 0.0);
        d_E( 2 , 0 ) = (double2)( 0.0, 0.0);

        d_E( 1 , 0 ) = negC(KZ(1));
        d_E( 3 , 0 ) = negC(KZ(3));


        d_E( 0 , 1 ) = one;
        d_E( 2 , 1 ) = one;
        d_E( 1 , 1 ) = (double2)( 0.0, 0.0);
        d_E( 3 , 1 ) = (double2)( 0.0, 0.0);


        d_E( 0 , 2 ) = (double2)( 0.0, 0.0);
        d_E( 2 , 2 ) = (double2)( 0.0, 0.0);
        d_E( 1 , 2 ) = kx;
        d_E( 3 , 2 ) = kx;

        /////////////////////////////////////////////////////
        d_B( 0 , 0 ) = negC(KZ(0));
        d_B( 2 , 0 ) = negC(KZ(2));

        d_B( 1 , 0 ) = (double2)( 0.0, 0.0);
        d_B( 3 , 0 ) = (double2)( 0.0, 0.0);


        d_B( 0 , 1 ) = (double2)( 0.0, 0.0);
        d_B( 2 , 1 ) = (double2)( 0.0, 0.0);
        d_B( 1 , 1 ) = negC(one);
        d_B( 3 , 1 ) = negC(one);


        d_B( 0 , 2 ) = kx;
        d_B( 2 , 2 ) = kx;
        d_B( 1 , 2 ) = (double2)( 0.0, 0.0);
        d_B( 3 , 2 ) = (double2)( 0.0, 0.0);

  }

}
