
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "cuda_runtime_api.h"
#include "cuComplex.h"

#include "cuPPMextension.h"

#define blockSize 128
#define CACHELINE_BOUNDARY 128
#define CBOUND CACHELINE_BOUNDARY

#define CERR(_expr)                                                         \
   do {                                                                             \
     cudaError_t _err = _expr;                                                           \
     if (_err == cudaSuccess)                                                        \
       break;                                                                       \
     sprintf(msg,"%s (@ %d): %s\n",__FUNCTION__,__LINE__,cudaGetErrorString(_err)); \
       throw msg; \
   } while (0)

using namespace std;

/**
 * Initialisation of pointers and flags
 */
PPMTensorial_cuda::PPMTensorial_cuda(){

  scanLen     = 0;
  numthick    = 0;
  uniquelayers= 0;
  debug_flag  = 0;
  first_run_flag=1;
  cudaDevice_id=0;
  resident_alloc=0;
  angles      = NULL;
  wheretolook = NULL;
  Kx          = NULL;
  die         = NULL;
  Kz          = NULL;
  E           = NULL;
  B           = NULL;
  Sistem      = NULL;
  NonZeri     = NULL;
  Modes       = NULL;
  Offsets     = NULL;
  Offsets_die = NULL;

  K0          = NULL;
  Roughness   = NULL;
  Thickness   = NULL;
  Prop        = NULL;
  Conditions  = NULL;
  Conditions_inv = NULL;
  Interfaces  = NULL;
  Reflectivity= NULL;
  Overall_GPU_ms = 0.0;
}

/**
 * Initialisation of pointers and flags. debug_flag is set to 1 which
 * enables the transfers of intermediate results from the GPU to the host.
 */
PPMTensorial_cuda::PPMTensorial_cuda(int debug){

  scanLen     = 0;
  numthick    = 0;
  uniquelayers= 0;
  debug_flag  = 1;
  first_run_flag=1;
  cudaDevice_id=0;
  resident_alloc=0;
  angles      = NULL;
  wheretolook = NULL;
  Kx          = NULL;
  die         = NULL;
  Kz          = NULL;
  E           = NULL;
  B           = NULL;
  Sistem      = NULL;
  NonZeri     = NULL;
  Modes       = NULL;
  Offsets     = NULL;
  Offsets_die = NULL;

  K0          = NULL;
  Roughness   = NULL;
  Thickness   = NULL;
  Prop        = NULL;
  Conditions  = NULL;
  Conditions_inv = NULL;
  Interfaces  = NULL;
  Reflectivity= NULL;
  Overall_GPU_ms = 0.0;
}

/**
 * Before any execution may begin, cuPPM needs a local reference of the input
 * as well as some of the problem's characteristics.
 *
 * Debug checks - assertions.
 * Keep local copies of top-level memory pointers.
 * Calculate some quantities needed for the allocations.
 */
void PPMTensorial_cuda::getInput(const int &scanLen_,const int &numthick_,const int &uniquelayers_,
                                  int *modes, complex<double> *indexes,
                                  double *angles, double *roughness,
                                  double *thickness, double *k0,
                                  int *lookup, complex<double> *reflectivity){

#ifdef _VERBOSE  
  std::cout<<"Initialising PPMTensorial_cuda" <<std::endl;
#endif  

  //sum up modes! Mode is the nd size. So 1 for Scalar and 3 for tensor. 1x1 Scalar, 3x3Tensor
  //Position 0 is the substrate which is always scalar
  summode=0;
  this->scanLen = scanLen_;
  this->numthick= numthick_;
  this->uniquelayers = uniquelayers_;

  assert(scanLen > 0);
  assert(numthick > 0);
  assert(uniquelayers > 0);
  assert(modes);
  assert(indexes);
  assert(angles);
  assert(roughness);
  assert(thickness);
  assert(k0);
  assert(lookup);
  assert(reflectivity);

  wheretolook = lookup;
  Modes = modes;

  for(int imode =0;imode<uniquelayers;imode++){
    summode+=Modes[imode] * Modes[imode];
    //std::cout<< "Mode "<< imode << " : " << Modes[imode] <<std::endl;
  } 

  //std::cout<<summode << std::endl;

  Offsets=new int [uniquelayers];
  Offsets_die=new int [uniquelayers];

  Offsets[0]=0;
  Offsets_die[0]=0;

  int mmode;
  for(int imode=1;imode<uniquelayers;imode++){
    mmode = Modes[imode-1] * Modes[imode-1] ;
    assert(mmode==1 || mmode==9);//Hook for uninitialised elements

    Offsets[imode] = Offsets[imode-1] + (scanLen * 4 * mmode);
    Offsets_die[imode] = Offsets_die[imode-1] + (scanLen * mmode);

  }

  //Allocate host side arrays

  this->angles = angles; //Has size scanLen and is same for all layers
  K0 = k0;

  //Indexes, Roughness and thickness must be compacted as they are separated
  // on the Python level for the Substrate and the layers
  die    = indexes;
//   new complex<double>[scanLen * summode];
// 
//   for(int iscan=0;iscan<scanLen;iscan++)die[iscan] = Sindex[iscan];
//   for(int iscan=0;iscan<scanLen*(summode-1);iscan++)die[iscan + scanLen] = indexes[iscan];
//   for(int ilayer=0;ilayer<uniquelayers;ilayer++){
//     for(int iscan=0;iscan<Modes[ilayer]*scanLen;iscan++){
//       if(ilayer==0) die[iscan] = Sindex[iscan];
//       else die[iscan + Offsets_die[ilayer]] = indexes[iscan + Offsets_die[ilayer-1]];
//     }
//   }

//   FILE *tp;
//   tp=fopen("basecuindexes_layer2.txt","w");
//   for(int i=0;i<scanLen*9;i++)fprintf(tp,"%+.10e %+.10e\n",die[scanLen*2 + i].real(),die[scanLen*2 +i].imag());
//   fclose(tp);

  Roughness = roughness;
  Thickness = thickness;
//   Roughness = new double[numthick];
//   Thickness = new double[numthick];
//   for(int ilayer=0;ilayer<numthick;ilayer++){
//     if(ilayer==0){
//       Roughness[(ilayer)] = subrough;
//       Thickness[(ilayer)] = subthick; //substrate value is 0 in fact
//     }else{
//       Roughness[(ilayer)] = roughness[(ilayer-1)];
//       Thickness[(ilayer)] = thickness[(ilayer-1)];
//     }    
//   }

  Reflectivity = reflectivity;
  //allocate the rest
  try{
    hostAllocations();
  }catch(const std::bad_alloc&){
    char msg[1000];
    sprintf(msg,"%s (@ %d): Caught a bad_alloc in hostAllocations\n",__FUNCTION__,__LINE__);
    throw msg;
  }
  return;
}

PPMTensorial_cuda::~PPMTensorial_cuda(){

  //delete[] Reflectivity;
  //delete[] die;
  //delete[] Modes;
  delete[] Offsets;
  delete[] Offsets_die;
  //delete[] Roughness;
  //delete[] Thickness;

  if(debug_flag){
    delete[] Kx;
    delete[] Kz;
    delete[] As;
    delete[] E;
    delete[] B;
    delete[] Sistem;
    delete[] NonZeri;
    delete[] Prop;
    delete[] Interfaces;
    delete[] Conditions;
    delete[] Conditions_inv;
  }

}

/**
 * A dummy method since now all memory is provided by the top level.
 * If debug_flag is set, it calls hostAllocations_debug().
 * host_allocations() is called by getInput().
 */
void PPMTensorial_cuda::hostAllocations(){

  if(debug_flag) hostAllocations_debug();
  //Reflectivity = new complex<double> [scanLen * 4];
  return;
}

/**
 * Debug level host-side allocations are used to store the
 * results of the kernels on each step
 */
void PPMTensorial_cuda::hostAllocations_debug(){

  printf("Debug level allocations\n");
  Kx     = new double [scanLen];
  Kz     = new complex<double> [scanLen * 4 * (uniquelayers)] ;
  As     = new complex<double> [scanLen * 4 * (uniquelayers)] ;
  E      = new complex<double> [scanLen * 4 * 3 * (uniquelayers)];
  B      = new complex<double> [scanLen * 4 * 3 * (uniquelayers)];
  Sistem = new complex<double> [scanLen * 4 * summode];
  NonZeri= new int [scanLen * 4 * (numthick + 1)];
  Prop   = new complex<double> [scanLen * 4 * 4 * (numthick) ];
  Conditions     = new complex<double> [scanLen * 4 * 4 * (numthick) ];
  Conditions_inv = new complex<double> [scanLen * 4 * 4 * (numthick) ];
  Interfaces     = new complex<double> [scanLen * 4 * 4 * (numthick-1) ];
  Reflectivity   = new complex<double> [scanLen * 4];
  return;
}

/**
 * Execute kernels. The kernel enqeueing and configuration is done in the appropriate class.
 * Upon first invocation, setCudaDevice will be called first in order to create a context with
 * a CUDA device.
 * It allocates the required device memory and copies the input there and then calls
 * the appropriate kernel invocation classes.
 *
 * @param dev An integer with the id of the specific device to be used.
 */
void PPMTensorial_cuda::launchPPMTensorial_cuda(int dev){

#ifdef _VERBOSE  
  std::cout<<"Running PPMTensorial_cuda" <<std::endl;
#endif
  
  //Cuda Initialisations
  if(dev != cudaDevice_id || first_run_flag){
    setCudaDevice(dev);
  }

  resetTimers();
  allocateCudaBaseMemory();
  setCudaSymbols();
  setInputMemory();
  //
  //Eigenmodes
  if(debug_flag){
    eigens.CreateModes(scanLen,uniquelayers,d_Kx,d_die,d_As,d_Kz,d_Sistem,d_E,d_B,d_NonZeri,NULL);
  }else{
    eigens.CreateModes(scanLen,uniquelayers,d_Kx,d_die,d_Kz,d_Kz,d_Sistem,d_E,d_B,d_NonZeri,NULL);
  }
  if(debug_flag){
    eigens.getModesResults(scanLen,uniquelayers,d_Kz,d_E,d_B,Kz,E,B);
    eigens.getModesResults_debug(scanLen,uniquelayers,summode,d_Kx,d_Sistem,d_NonZeri,d_As,Kx,Sistem,NonZeri,As);
  }
  addGpuTime(eigens.cuOnda_GPU_time);

  releaseCudaDeprecMemory();
  allocateCudaOtheMemory();
  
  //Propagations
  props.Propagations(scanLen,numthick,d_Props,d_Kz,d_K0);
  if(debug_flag){
    props.getPropagationsResults(scanLen,numthick,d_Props,Prop);
  }
  addGpuTime(props.cudaPropagation_GPU_time);

  //Interfaces
  inters.Interfaces(scanLen,numthick,d_Kz,d_E,d_B,d_Conditions,d_Conditions_inv,d_K0,d_Interfaces);
  if(debug_flag){
    inters.getInterfacesResults(scanLen,numthick,d_Interfaces,Interfaces,d_Conditions,d_Conditions_inv,
                                 Conditions,Conditions_inv);
  }
  addGpuTime(inters.cudaInterfaces_GPU_time);

  //Reflectivity
  refls.Reflectivity(scanLen,d_Reflectivity,d_Interfaces,d_Props,d_propa);
  refls.getReflResults(scanLen,d_Reflectivity,Reflectivity);
  addGpuTime(refls.cudaReflectivity_GPU_time);

  releaseCudaRemainMemory();

#ifdef _VERBOSE  
  std::cout<< "eigens GPU time: "<< eigens.cuOnda_GPU_time << std::endl;
  std::cout<< "props GPU time: "<< props.cudaPropagation_GPU_time << std::endl;
  std::cout<< "inters GPU time: "<< inters.cudaInterfaces_GPU_time << std::endl;
  std::cout<< "refls GPU time: "<< refls.cudaReflectivity_GPU_time << std::endl;
  std::cout<< "Total GPU time: "<< Overall_GPU_ms << std::endl;
#endif
  
  first_run_flag=0;
  return;
}

/**
 * Perform benchmark by executing kernels multiple times.
 * The basic process is similar to launchPPMTensorial_cuda().
 * The kernel enqeueing and configuration is done in the appropriate class.
 * Upon first invocation, setCudaDevice will be called first in order to create a context with
 * a CUDA device.
 * It allocates the required device memory and copies the input there and then calls
 * the appropriate kernel invocation classes.
 *
 * In benchPPMTensorial_cuda(), the execution of kernels is done in a loop of Nruns. If the
 * library is called for the first time, Nruns is extended by +1 and the first iteration
 * counts as a warm-up execution. Results from the warm-up execution are not taken into account 
 *
 * @param dev An integer with the id of the specific device to be used.
 * @param Nruns An integer with the desired number of iterations for the benchmark. Default value is 100.*
 */
void PPMTensorial_cuda::benchPPMTensorial_cuda(int dev, int Nruns){

  std::cout<<"Benchmarking benchPPMTensorial_cuda" <<std::endl;
  
  float totalBenchTime=0.0;
  float totalEigensTime=0.0;
  float totalPropsTime=0.0;
  float totalInterTime=0.0;
  float totalReflTime=0.0;
  int warm_up=0;
  
  if(first_run_flag)warm_up=1;
  
  //Cuda Initialisations
  if(dev != cudaDevice_id || first_run_flag){
    setCudaDevice(dev);
  }
  
  for(int testrun=0;testrun<(Nruns + first_run_flag);testrun++){

    resetTimers();
    allocateCudaBaseMemory();
    setCudaSymbols();
    setInputMemory();
    //
    //Eigenmodes
    eigens.CreateModes(scanLen,uniquelayers,d_Kx,d_die,d_Kz,d_Kz,d_Sistem,d_E,d_B,d_NonZeri,NULL);
    addGpuTime(eigens.cuOnda_GPU_time);
    if(!warm_up)totalEigensTime += eigens.cuOnda_GPU_time;

    releaseCudaDeprecMemory();
    allocateCudaOtheMemory();

    //Propagations
    props.Propagations(scanLen,numthick,d_Props,d_Kz,d_K0);
    addGpuTime(props.cudaPropagation_GPU_time);
    if(!warm_up)totalPropsTime += props.cudaPropagation_GPU_time;

    //Interfaces
    inters.Interfaces(scanLen,numthick,d_Kz,d_E,d_B,d_Conditions,d_Conditions_inv,d_K0,d_Interfaces);
    addGpuTime(inters.cudaInterfaces_GPU_time);
    if(!warm_up)totalInterTime += inters.cudaInterfaces_GPU_time;

    //Reflectivity
    refls.Reflectivity(scanLen,d_Reflectivity,d_Interfaces,d_Props,d_propa);
    addGpuTime(refls.cudaReflectivity_GPU_time);
    if(!warm_up)totalReflTime += refls.cudaReflectivity_GPU_time;

    releaseCudaRemainMemory();
    warm_up=0;

  }

  totalBenchTime = totalEigensTime + totalPropsTime +
                    totalInterTime + totalReflTime;

  std::cout<< "Cuda Benchmark results for " << Nruns << " runs (averaged)" <<std::endl;
  std::cout<<"_t_cuda_eigens "<< totalEigensTime / (float)Nruns<<std::endl;
  std::cout<<"_t_cuda_props  "<< totalPropsTime / (float)Nruns<<std::endl;
  std::cout<<"_t_cuda_inters "<< totalInterTime / (float)Nruns<<std::endl;
  std::cout<<"_t_cuda_refl   "<< totalReflTime / (float)Nruns<<std::endl;
  std::cout<<"_t_cuda_total  "<< totalBenchTime / (float)Nruns<<std::endl;

  first_run_flag=0;
  
  return;
}

/**
 * setCudaDevice is resposible for initialing a context with a CUDA device and
 * evaluate it's possibility to use double precision.
 *
 * @param dev An integer with the id of the specific device to be used.
 */
void PPMTensorial_cuda::setCudaDevice(int dev){

  int dev_count;
  cudaError_t cuErr;
  char msg[1000];

  cuErr=cudaGetDeviceCount(&dev_count);

  assert(dev_count);

  if((dev > dev_count -1) || dev <0){
    sprintf(msg,"%s (@ %d): Invalid device id: %d. Available devices: %d. Status: %s\n"
        ,__FUNCTION__,__LINE__,dev,dev_count,cudaGetErrorString(cuErr));
    throw msg;
  }

  cuErr=cudaGetDeviceProperties(&(cudaProperties),dev);
  
    if(cudaProperties.minor < 3 && cudaProperties.major <2){
    sprintf(msg,"%s (@ %d): Device %d (%s) does not meet compute capability 1.3 or higher requirement: %d.%d\n",
            __FUNCTION__ , __LINE__ , dev ,cudaProperties.name,cudaProperties.major, cudaProperties.minor );
    throw msg;
  }

  cuErr=cudaSetDevice(dev);
  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr));
    throw msg;
  }
  cudaDevice_id = dev;

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": Got device " << cudaProperties.name <<std::endl;
#endif
  
  return;
};

/**
 * \brief Base device memory allocations.
 *
 * At this point the memory that is needed for Modes
 * is allocated.
 */
void PPMTensorial_cuda::allocateCudaBaseMemory(){

  size_t req_alloc=0;
  size_t dtype_size;
  char msg[1000];

  cudaError_t cuErr;
  dtype_size = sizeof(cuDoubleComplex);

  req_alloc += scanLen * sizeof(double); //d_Kx - Input
  req_alloc += scanLen * sizeof(double); //d_K0 - Input
  req_alloc += scanLen * 4 * (uniquelayers) * dtype_size; //d_Kz - Result - needed by Other kernels
  req_alloc += scanLen * 4 * (uniquelayers) * 3 * dtype_size; //d_E - Result - needed by Other kernels
  req_alloc += scanLen * 4 * (uniquelayers) * 3 * dtype_size; //d_B - Result - needed by Other kernels
  resident_alloc = req_alloc;
  
  if(debug_flag){
    req_alloc += scanLen * 4 * (uniquelayers) * dtype_size; //d_As - auxiliary storage for intermediate testing
  }
  req_alloc += scanLen * summode * dtype_size; //d_die - Input - not needed by Other kernels
  req_alloc += scanLen * 4 * summode * dtype_size; //d_Sistem - Auxiliary storage - not needed by Other kernels
  req_alloc += scanLen * 4 * (uniquelayers) * sizeof(int); //d_NonZeri - Auxiliary storage - not needed by Other kernels

#ifdef _VERBOSE  
  printf("Dev %f, req %f\n",(float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0,(float)req_alloc / 1024.0 / 1024.0);
#endif
  
  //5MB are always allocated on a device. There is also approximatelly 68MB of overhead. However we cannot check for the actual used memory
  if((cudaProperties.totalGlobalMem - 74*1024*1024)<= req_alloc){
    sprintf(msg,"%s (@ %d): Device (%d) does not meet memory requirements. %f requested, but only %f available. Take into account ~74MB of overhead.\n",
            __FUNCTION__,__LINE__,cudaDevice_id,(float)req_alloc / 1024.0 / 1024.0,
            (float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0);
    throw msg;
  }

  CERR(cudaMalloc((void **)&(d_Kx),             scanLen * sizeof(double) ) );
  CERR(cudaMalloc((void **)&(d_K0),             scanLen * sizeof(double) ) );
  CERR(cudaMalloc((void **)&(d_Kz),             scanLen * 4 * (uniquelayers) * dtype_size ) );
  CERR(cudaMalloc((void **)&(d_E),              scanLen * 4 * (uniquelayers) * 3 * dtype_size ) );
  CERR(cudaMalloc((void **)&(d_B),              scanLen * 4 * (uniquelayers) * 3 * dtype_size ) );
  
  if(debug_flag){
    CERR(cudaMalloc((void **)&(d_As),             scanLen * 4 * (uniquelayers) * dtype_size ) );
  }
  CERR(cudaMalloc((void **)&(d_die),            scanLen * summode * dtype_size ) );
  CERR(cudaMalloc((void **)&(d_Sistem),         scanLen * 4 * summode * dtype_size ) );
  CERR(cudaMalloc((void **)&(d_NonZeri),        scanLen * 4 * (uniquelayers) * sizeof(int) ) );  

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s (%f Mb)\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr),(float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0);
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": Base Memory allocated " << (float)req_alloc / 1024.0 / 1024.0 << " Mbyte " <<std::endl;
#endif
  
  return;  
}

/**
 * \brief Other device memory allocations
 *
 * Allocates memory for the
 * Propagations, Interfaces and Reflectivity kernels.
 */
void PPMTensorial_cuda::allocateCudaOtheMemory(){

  size_t req_alloc=0;
  size_t dtype_size;
  char msg[1000];

  cudaError_t cuErr;
  dtype_size = sizeof(cuDoubleComplex);

  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Props - Result - intermediate
  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Conditions - Auxiliary storage
  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Conditions_inv - Auxiliary storagr
  req_alloc += scanLen * 4 * 4 * (numthick - 1) * dtype_size ; //d_Interfaces - Result - intermediate
  req_alloc += scanLen * 4 * dtype_size ; //d_Reflectivity - The result
  req_alloc += scanLen * 4 * 4 * dtype_size ; //d_propa - Auxiliary storage

#ifdef _VERBOSE
  printf("Dev %f, req %f, req + resident %f\n",(float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0,(float)req_alloc / 1024.0 / 1024.0,
    float((float)req_alloc + (float)resident_alloc)/1024.0/1024.0);
#endif  

  //5MB are always allocated on a device. There is also approximatelly 68MB of overhead. However we cannot check for the actual used memory
  if((cudaProperties.totalGlobalMem - 74*1024*1024)<= req_alloc + resident_alloc){
    sprintf(msg,"%s (@ %d): Device (%d) does not meet memory requirements. %f requested, but only %f available. Take into account ~74MB of overhead.\n",
            __FUNCTION__,__LINE__,cudaDevice_id,(float)req_alloc / 1024.0 / 1024.0,
            (float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0);
    throw msg;
  }  

  CERR(cudaMalloc((void **)&(d_Props),          scanLen * 4 * 4 * (numthick) * dtype_size) );
  CERR(cudaMalloc((void **)&(d_Conditions),     scanLen * 4 * 4 * (numthick) * dtype_size) );
  CERR(cudaMalloc((void **)&(d_Conditions_inv), scanLen * 4 * 4 * (numthick) * dtype_size) );
  CERR(cudaMalloc((void **)&(d_Interfaces),     scanLen * 4 * 4 * (numthick - 1) * dtype_size) );
  CERR(cudaMalloc((void **)&(d_Reflectivity),   scanLen * 4 * dtype_size) );
  CERR(cudaMalloc((void **)&(d_propa),          scanLen * 4 * 4 * dtype_size) );

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s (%f Mb)\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr),(float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0);
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": Other Memory allocated " << (float)req_alloc / 1024.0 / 1024.0 << " Mbyte " <<std::endl;
  std::cout << __FUNCTION__ << ": Total Memory in use " << float((float)req_alloc + (float)resident_alloc) / 1024.0 / 1024.0 << " Mbyte " <<std::endl;
#endif
  
  return;
}

/**
 * Release memory that was used for the execution of Modes and is not needed anymore.
 * This is done because PPM requires many buffers and otherwise may fail on big problems.
 */
void PPMTensorial_cuda::releaseCudaDeprecMemory(){

  char msg[1000];
  cudaError_t cuErr;

  CERR(cudaFree(d_Kx) );  
  CERR(cudaFree(d_die) );
  CERR(cudaFree(d_Sistem) );
  CERR(cudaFree(d_NonZeri) );
  if(debug_flag)CERR(cudaFree(d_As) );

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr));
    throw msg;
  }
  
  return;
}

/**
 * Release the remaining buffers
 */
void PPMTensorial_cuda::releaseCudaRemainMemory(){

  char msg[1000];
  cudaError_t cuErr;

  CERR(cudaFree(d_K0) );
  CERR(cudaFree(d_Kz) );
  CERR(cudaFree(d_E) );
  CERR(cudaFree(d_B) );
  CERR(cudaFree(d_Props) );
  CERR(cudaFree(d_Conditions) );
  CERR(cudaFree(d_Conditions_inv) );
  CERR(cudaFree(d_Interfaces) );
  CERR(cudaFree(d_Reflectivity) );
  CERR(cudaFree(d_propa) );

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr));
    throw msg;
  }
  
  return;
}

/**
 * Alternative to allocating the memory in two parts and deallocating
 * deprecated memory in between, one may allocate all the memory at once.
 */
void PPMTensorial_cuda::allocateCudaMemory(){

  size_t req_alloc=0;
  size_t dtype_size;
  char msg[1000];

  cudaError_t cuErr;
  dtype_size = sizeof(cuDoubleComplex);

  req_alloc += scanLen * sizeof(double); //d_Kx - Input
  req_alloc += scanLen * sizeof(double); //d_K0 - Input
  if(debug_flag){
    req_alloc += scanLen * 4 * (uniquelayers) * dtype_size; //d_As - Result - intermediate
  }
  req_alloc += scanLen * 4 * (uniquelayers) * dtype_size; //d_Kz - Result - intermediate
  req_alloc += scanLen * 4 * (uniquelayers) * 3 * dtype_size; //d_E - Result - intermediate
  req_alloc += scanLen * 4 * (uniquelayers) * 3 * dtype_size; //d_B - Result - intermediate
  req_alloc += scanLen * summode * dtype_size; //d_die - Input
  req_alloc += scanLen * 4 * summode * dtype_size; //d_Sistem - Auxiliary storage
  req_alloc += scanLen * 4 * (uniquelayers) * sizeof(int); //d_NonZeri - Auxiliary storage
  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Props - Result - intermediate
  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Conditions - Auxiliary storage
  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Conditions_inv - Auxiliary storagr
  req_alloc += scanLen * 4 * 4 * (numthick - 1) * dtype_size ; //d_Interfaces - Result - intermediate
  req_alloc += scanLen * 4 * dtype_size ; //d_Reflectivity - The result
  req_alloc += scanLen * 4 * 4 * dtype_size ; //d_propa - Auxiliary storage

#ifdef _VERBOSE
  printf("Dev %f, req %f\n",(float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0,(float)req_alloc / 1024.0 / 1024.0);
#endif
  
  //5MB are always allocated on a device. There is also approximatelly 68MB of overhead. However we cannot check for the actual used memory
  if((cudaProperties.totalGlobalMem - 74*1024*1024)<= req_alloc){
    sprintf(msg,"%s (@ %d): Device (%d) does not meet memory requirements. %f requested, but only %f available. Take into account ~74MB of overhead.\n",
            __FUNCTION__,__LINE__,cudaDevice_id,(float)req_alloc / 1024.0 / 1024.0,
            (float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0);
    throw msg;
  }

  CERR(cudaMalloc((void **)&(d_Kx),             scanLen * sizeof(double) ) );
  CERR(cudaMalloc((void **)&(d_K0),             scanLen * sizeof(double) ) );
  CERR(cudaMalloc((void **)&(d_Kz),             scanLen * 4 * (uniquelayers) * dtype_size ) );
  if(debug_flag){
    CERR(cudaMalloc((void **)&(d_As),             scanLen * 4 * (uniquelayers) * dtype_size ) );
  }
  CERR(cudaMalloc((void **)&(d_E),              scanLen * 4 * (uniquelayers) * 3 * dtype_size ) );
  CERR(cudaMalloc((void **)&(d_B),              scanLen * 4 * (uniquelayers) * 3 * dtype_size ) );
  CERR(cudaMalloc((void **)&(d_die),            scanLen * summode * dtype_size ) );
  CERR(cudaMalloc((void **)&(d_Sistem),         scanLen * 4 * summode * dtype_size ) );
  CERR(cudaMalloc((void **)&(d_NonZeri),        scanLen * 4 * (uniquelayers) * sizeof(int) ) );
  CERR(cudaMalloc((void **)&(d_Props),          scanLen * 4 * 4 * (numthick) * dtype_size) );
  CERR(cudaMalloc((void **)&(d_Conditions),     scanLen * 4 * 4 * (numthick) * dtype_size) );
  CERR(cudaMalloc((void **)&(d_Conditions_inv), scanLen * 4 * 4 * (numthick) * dtype_size) );
  CERR(cudaMalloc((void **)&(d_Interfaces),     scanLen * 4 * 4 * (numthick - 1) * dtype_size) );
  CERR(cudaMalloc((void **)&(d_Reflectivity),   scanLen * 4 * dtype_size) );
  CERR(cudaMalloc((void **)&(d_propa),          scanLen * 4 * 4 * dtype_size) );

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s (%f Mb)\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr),(float)cudaProperties.totalGlobalMem / 1024.0 / 1024.0);
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": Memory allocated " << (float)req_alloc / 1024.0 / 1024.0 << " Mbyte " <<std::endl;
#endif
  
  return;
};

/**
 * Releases all CUDA buffers to be used if they were allocated by
 * allocateCudaMemory().
 */
void PPMTensorial_cuda::releaseCudaMemory(){

  char msg[1000];
  cudaError_t cuErr;

  CERR(cudaFree(d_Kx) );
  CERR(cudaFree(d_K0) );
  if(debug_flag)CERR(cudaFree(d_As) );
  CERR(cudaFree(d_Kz) );
  CERR(cudaFree(d_E) );
  CERR(cudaFree(d_B) );
  CERR(cudaFree(d_die) );
  CERR(cudaFree(d_Sistem) );
  CERR(cudaFree(d_NonZeri) );
  CERR(cudaFree(d_Props) );
  CERR(cudaFree(d_Conditions) );
  CERR(cudaFree(d_Conditions_inv) );
  CERR(cudaFree(d_Interfaces) );
  CERR(cudaFree(d_Reflectivity) );
  CERR(cudaFree(d_propa) );

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr));
    throw msg;
  }

  return;    
};

/**
 * Copy input memory to device buffers
 */
void PPMTensorial_cuda::setInputMemory(){

  float cuTime_ms;
  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  char msg[1000];

  cuErr = cudaEventCreate(&event_s);
  cuErr = cudaEventCreate(&event_e);

  cuErr = cudaEventRecord(event_s);

  cuErr = cudaMemcpy(d_Kx,angles,scanLen * sizeof(double),cudaMemcpyHostToDevice); 
  cuErr = cudaMemcpy(d_K0,K0,scanLen * sizeof(double),cudaMemcpyHostToDevice); 
  cuErr = cudaMemcpy(d_die,die,scanLen * summode * sizeof(cuDoubleComplex),cudaMemcpyHostToDevice);

  cuErr = cudaEventRecord(event_e);
  cuErr = cudaEventSynchronize(event_s);
  cuErr = cudaEventSynchronize(event_e);
  cuErr = cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

  cuErr = cudaEventDestroy(event_s);
  cuErr = cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": elapsed time: " << cuTime_ms << " (ms) " << std::endl;
#endif  

  return;
}

cudaError_t setCudaSymbols_modes_cu(const int &scanLen, int * modes, int * offsets, int * offsets_die,const int &uniquelayers);
cudaError_t setCudaSymbols_other_cu(const int &scanLen, int * wheretolook, double * Thickness, double * Roughness,
                                    const int &numthick, const int &uniquelayers);

/**
 * Copy constant buffers to the CUDA constant memory space
 */
void PPMTensorial_cuda::setCudaSymbols(){

  setCudaSymbols_modes_cu(scanLen,Modes,Offsets,Offsets_die,uniquelayers);
  setCudaSymbols_other_cu(scanLen,wheretolook,Thickness,Roughness,numthick,uniquelayers);
return;
};

/**
 * Simple method to add the execution time of another class to the internal total timer
 *
 * @param time Time value (in ms) to be added
 */
void PPMTensorial_cuda::addGpuTime(const float time){

  Overall_GPU_ms += time;
  return;
}

/**
 * Reset all timers each time we call cuPPM, since the object might be active and called
 * multiple times
 */
void PPMTensorial_cuda::resetTimers(){

  eigens.cuOnda_GPU_time=0.0;
  props.cudaPropagation_GPU_time=0.0;
  inters.cudaInterfaces_GPU_time=0.0;
  refls.cudaReflectivity_GPU_time=0.0;
  Overall_GPU_ms=0.0;
  return;
}