

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import unittest
import xmltreeview
inputtext1="""<system >
  <scanlist >
    <scan >
      <s >DATA/data_ref_w.dat</s>
      <f >"wavelenghts_col"  3 </f>
      <f >"angles_col"  1 </f>
      <f >"refle_col"  2 </f>
      <f >"weight_col"  1.0 </f>
      <f >"angle_factor"  arccos(-1)/180.0 </f>
      <f >"norm" nomrLE 1191671.0 100000 20000000 </f>
      <f >"noise"  3.031721e-07 1.0e-9 1.0e-6 </f>
      <f >"CutOffRatio"  0.009644845 0.00025 0.2 </f>
    </scan>
  </scanlist>
</system>"""
partest1=2
coltest1=5
signal1 ="-"
inputtext1_impl="""<system >
  <scanlist >
    <scan >+++</scan>
  </scanlist>
</system>"""




class testCmlTreeView(unittest.TestCase):
    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def testInitializeWithText(self):
        tv=xmltreeview.XmlTreeView()
        tv.initializeString(inputtext1)
        tv.expandAll()
        content = tv.text()
        self.assertEquals(content, inputtext1)
        tv.signalat(partest1, coltest1,"-")
        content = tv.text()
        self.assertEquals(content, inputtext1_impl)


if __name__ == "__main__":
    unittest.main()
