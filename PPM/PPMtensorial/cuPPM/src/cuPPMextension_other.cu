
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include "cuda_runtime_api.h"
#include "cuComplex.h"

#ifndef OTHER_H
#include "cuPPMextension_other.h"
#define OTHER_H
#endif

#define blockSize 32
#define CACHELINE_BOUNDARY 128
#define CBOUND CACHELINE_BOUNDARY
//__PRETTY_FUNCTION__ -- GCC, MetroWerks, Digital Mars and ICC
//__FUNCSIG__ -- MSVC
//__FUNCTION__ -- Intel and IBM
//__FUNC__ -- Borland
//__func__ -- ANSI C99

extern float Overall_time_ms;

cudaPropagation::cudaPropagation(){
  cudaPropagation_GPU_time = 0.0;
};

cudaPropagation::~cudaPropagation(){

};

__global__ void cuPropagation(cuDoubleComplex *d_Prop, cuDoubleComplex *d_Kz, double * d_K0);

/**
 * Configures and calls propagation kernel in cuPPMkernels_other.cu
 *
 * @param scanLen The number of scanpoints
 * @param numthick The number of all layers
 */
void cudaPropagation::Propagations(int&scanLen, int&numthick, cuDoubleComplex *d_Prop, cuDoubleComplex *d_Kz, double * d_K0){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  dim3 block,grid;
  char msg[1000];

  grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),numthick,1);
  block=dim3(blockSize,1,1);


  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);

  /*cuErr = */cudaEventRecord(event_s);
  cuPropagation<<<grid,block>>>(d_Prop,d_Kz,d_K0);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel cuPropagation status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;
#endif  

  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

  cudaPropagation_GPU_time +=cuTime_ms;

  return;
};

/**
 * Returns the results of Propagations. There results are intermediate and not needed,
 * except for testing.
 *
 * @param scanLen The number of scanpoints
 * @param numthick The number of all layers
 */
void cudaPropagation::getPropagationsResults(int&scanLen, int&numthick, cuDoubleComplex *d_Prop,complex<double> *Prop){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  char msg[1000];

  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);
/**/
  /*cuErr = */cudaEventRecord(event_s);
/**/
  /*cuErr = */cudaMemcpy(Prop,d_Prop,scanLen * 4 * 4 * (numthick) * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
/**/
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
/**/
  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": elapsed time: " << cuTime_ms << " (ms) " << std::endl;
#endif  

  //cudaPropagation_GPU_time += cuTime_ms;

  return;
}

cudaInterfaces::cudaInterfaces(){
  cudaInterfaces_GPU_time = 0.0;
};

cudaInterfaces::~cudaInterfaces(){

};

__global__ void getConditions(cuDoubleComplex *d_E, cuDoubleComplex *d_B, cuDoubleComplex *d_Conditions,cuDoubleComplex *d_Conditions_inv);
__global__ void cuInterfaces(cuDoubleComplex *d_Kz, cuDoubleComplex *d_Conditions,cuDoubleComplex *d_Conditions_inv, double *d_K0,
                             cuDoubleComplex *d_Interfaces);

/**
 * Configures and calls interfaces kernels in cuPPMkernels_other.cu
 *
 * @param scanLen The number of scanpoints
 * @param numthick The number of all layers
 */
void cudaInterfaces::Interfaces(int&scanLen, int&numthick, cuDoubleComplex *d_Kz, cuDoubleComplex *d_E, cuDoubleComplex *d_B, cuDoubleComplex *d_Conditions,
                                cuDoubleComplex *d_Conditions_inv, double *d_K0, cuDoubleComplex *d_Interfaces){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms,totTime_ms=0;
  dim3 block,grid;
  char msg[1000];

  grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),numthick,1);
  block=dim3(blockSize,1,1);


  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);

  /*cuErr = */cudaEventRecord(event_s);
  getConditions<<<grid,block>>>(d_E,d_B,d_Conditions,d_Conditions_inv);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel getConditions status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  //We reset the gridDim.y dim for the calculation of the Interfaces
  grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),numthick-1,1);

  /*cuErr = */cudaEventRecord(event_s);
  cuInterfaces<<<grid,block>>>(d_Kz,d_Conditions,d_Conditions_inv,d_K0,d_Interfaces);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel cuInterfaces status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;
#endif
  
  totTime_ms+=cuTime_ms;

  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": Total processing time: " <<totTime_ms << " (ms)" <<std::endl;
#endif  

  cudaInterfaces_GPU_time += totTime_ms;

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

  return;
}

/**
 * Returns the results of Interfaces. There results are intermediate and not needed,
 * except for testing.
 *
 * @param scanLen The number of scanpoints
 * @param numthick The number of all layers
 */
void cudaInterfaces::getInterfacesResults(int &scanLen, int &numthick, cuDoubleComplex *d_Interfaces, complex<double> *Interfaces,
                                          cuDoubleComplex *d_Conditions,cuDoubleComplex *d_Conditions_inv,
                                          complex<double> *Conditions, complex<double> *Conditions_inv){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms=0;
  char msg[1000];

  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);
/**/
  /*cuErr = */cudaEventRecord(event_s);
  /*cuErr = */cudaMemcpy(Interfaces,d_Interfaces,scanLen * 4 * 4 * (numthick-1)  * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost); 
  /*cuErr = */cudaMemcpy(Conditions,d_Conditions,scanLen * 4 * 4 * (numthick)  * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
  /*cuErr = */cudaMemcpy(Conditions_inv,d_Conditions_inv,scanLen * 4 * 4 * (numthick)  * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
/**/
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
/**/
  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": elapsed time: " << cuTime_ms << " (ms) " << std::endl;
#endif  

  //cudaInterfaces_GPU_time += cuTime_ms;

  return;
}


cudaReflectivity::cudaReflectivity(){
  cudaReflectivity_GPU_time = 0.0;
};

cudaReflectivity::~cudaReflectivity(){

};

__global__ void cuReflectivity(cuDoubleComplex *d_Reflectivity,cuDoubleComplex *d_Interfaces, cuDoubleComplex *d_Propagations, 
                               cuDoubleComplex *d_propa);

/**
 * Configures and calls Reflectivity kernel in cuPPMkernels_other.cu
 *
 * @param scanLen The number of scanpoints
 */
void cudaReflectivity::Reflectivity(int&scanLen,cuDoubleComplex *d_Reflectivity,cuDoubleComplex *d_Interfaces, cuDoubleComplex *d_Propagations, 
                               cuDoubleComplex *d_propa){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  dim3 block,grid;
  char msg[1000];

  grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),1,1);
  block=dim3(blockSize,1,1);


  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);

  /*cuErr = */cudaEventRecord(event_s);
  cuReflectivity<<<grid,block>>>(d_Reflectivity,d_Interfaces,d_Propagations,d_propa);
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": kernel cuReflectivity status: " << cudaGetErrorString(cudaGetLastError())  << " in time (ms) :"<< cuTime_ms << ::endl;
#endif  

  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  cudaReflectivity_GPU_time += cuTime_ms;

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

  return;
};

/**
 * Returns the result of Reflectivity. This is the result that the cuPPM module must return back to PPM.
 */
void cudaReflectivity::getReflResults(int&scanLen, cuDoubleComplex *d_Reflectivity, complex<double> *Reflectivity){

  cudaError_t cuErr;
  cudaEvent_t event_s,event_e;
  float cuTime_ms;
  char msg[1000];

  /*cuErr = */cudaEventCreate(&event_s);
  /*cuErr = */cudaEventCreate(&event_e);
/**/
  /*cuErr = */cudaEventRecord(event_s);
/**/
  /*cuErr = */cudaMemcpy(Reflectivity,d_Reflectivity,scanLen * 4 * sizeof(cuDoubleComplex),cudaMemcpyDeviceToHost);
/**/
  /*cuErr = */cudaEventRecord(event_e);
  /*cuErr = */cudaEventSynchronize(event_s);
  /*cuErr = */cudaEventSynchronize(event_e);
  /*cuErr = */cudaEventElapsedTime(&cuTime_ms,event_s,event_e);
/**/
  /*cuErr = */cudaEventDestroy(event_s);
  /*cuErr = */cudaEventDestroy(event_e);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s",__FUNCTION__,__LINE__, cudaGetErrorString(cuErr));
    throw msg;
  }

#ifdef _VERBOSE
  std::cout << __FUNCTION__ << ": elapsed time: " << cuTime_ms << " (ms) " << std::endl;
#endif  

  //cudaReflectivity_GPU_time += cuTime_ms;

  return;
}