
/**
 * \file
 * \brief Other Kernels
 *
 * Defines the OpenCL kernels that compute the Propagations,
 * Interfaces and the Reflectivity
 */
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

//OpenCL extensions are silently defined by opencl compiler at compile-time:
#ifdef cl_amd_printf
  #pragma OPENCL EXTENSION cl_amd_printf : enable
  //#define printf(...)
#elif defined(cl_intel_printf)
  #pragma OPENCL EXTENSION cl_intel_printf : enable
#else
  #define printf(...)
#endif

#ifdef cl_amd_fp64
  #pragma OPENCL EXTENSION cl_amd_fp64 : enable
#elif defined(cl_khr_fp64)
  #pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

// #include "clDcomplex.cl"

/**
 * \brief Return the real part of a complex double
 */
inline double clCreal(double2 a){
  return a.x;
}

/**
 * \brief Return the imaginary part of a complex double
 */
inline double clCimag(double2 a){
  return a.y;
}

/**
 * \brief Return the conjugate of a complex double
 */
inline double2 clConj(double2 a){
  return (double2)( a.x , -a.y);
}

/**
 * \brief Addition of two complex doubles
 */
inline double2 clCadd(double2 a, double2 b){
  return (double2)(a.x + b.x, a.y + b.y);
};

/**
 * \brief Substraction of two complex doubles
 */
inline double2 clCsub(double2 a, double2 b){
  return (double2)(a.x - b.x, a.y - b.y);
};

/**
 * \brief Multiplication of two complex doubles
 */
inline double2 clCmul(double2 a, double2 b){
  return (double2)( (a.x * b.x) - (a.y * b.y) ,
                    (a.x * b.y) + (a.y * b.x) );
};

/**
 * \brief Division of two complex doubles
 */
inline double2 clCdiv(double2 a, double2 b){
  double denom = ( (b.x * b.x) + (b.y * b.y) );
  double real  = (a.x * b.x) + (a.y * b.y);
  double imag  = (a.y * b.x) - (a.x * b.y);
  return (double2)(real/denom, imag/denom);
};

/**
 * \brief Multiplication of a complex double with a real double
 */
inline double2 clCRmul(double2 a, double b){
  return (double2)(a.x * b, a.y * b);
};

/**
 * \brief Division of a complex double with a real double
 */
inline double2 clCRdiv(double2 a, double b){
  return (double2)(a.x / b, a.y / b);
};

/**
 * \brief Absolute value of a complex double
 */
inline double clCabs(double2 a){
  return sqrt( (a.x * a.x) + (a.y * a.y) );
}

/**
 * \brief Square root of a complex double
 */
inline double2 clCsqrt(double2 a){
  double r,th;
  double2 res;
  r = sqrt ( (a.x * a.x) + (a.y * a.y) );
  th= atan2( a.y, a.x);
  res= (double2)( sqrt(r) * cos(th/2), sqrt(r) * sin(th/2) );
  return res;
};

/**
 * \brief Exponent of a complex double
 */
inline double2 clCexp(double2 a){
  return (double2)( exp(a.x) * cos(a.y), exp(a.x) * sin(a.y) );
};

/**
 * \brief Negation of a complex double
 */
inline double2 negC(double2 a){
  return (double2)( -a.x, -a.y );
};

//MAP complex cuda PPM function calls to the openCL ones
#define cuCadd clCadd
#define cuCsub clCsub
#define cuCmul clCmul
#define cuCdiv clCdiv
#define cuCRmul clCRmul
#define cuCRdiv clCRdiv
#define cuCsqrt clCsqrt
#define cuCexp clCexp
#define cuCreal clCreal
#define cuConj clConj
#define cuCabs clCabs


/**
 * Calculates the Minors for the determinant of the m 4x4 matrix
 */
inline double2 cuMinor(__global double2 *m,  const int r0, const int r1, const int r2, const int c0,
                        const int c1, const int c2, __constant int *scanLen, __constant int *numthick){

#define m(iwave,j)     m[ idx + scanLen[0]* ( blockIdx.y + numthick[0]* ( (iwave) + 4 * (j) )  ) ]

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 res,tmp,tmp1;

  tmp  = cuCmul( m(c1,r1) , m(c2,r2) );
  tmp1 = cuCmul( m(c1,r2) , m(c2,r1));
  tmp1 = cuCsub( tmp, tmp1);
  res  = cuCmul( m(c0,r0) , tmp1);

  tmp  = cuCmul( m(c0,r1) , m(c2,r2) );
  tmp1 = cuCmul( m(c0,r2) , m(c2,r1) );
  tmp1 = cuCsub( tmp, tmp1);
  tmp1 = cuCmul( m(c1,r0) , tmp1);
  res  = cuCsub( res, tmp1);

  tmp  = cuCmul( m(c0,r1) , m(c1,r2) );
  tmp1 = cuCmul( m(c0,r2) , m(c1,r1) );
  tmp1 = cuCsub( tmp, tmp1);
  tmp1 = cuCmul( m(c2,r0) , tmp1);
  res  = cuCadd( res, tmp1);
  
#undef m
  return res;
}

/**
 * Calculates the Determinant of a 4x4 m Matrix
 */
inline double2 cuDet(__global double2 *m, __constant int *scanLen, __constant int *numthick){

//#define m(j,iwave)     m[ idx + scanLen[0]* ( blockIdx.y + numthick[0]* ( (iwave) + 4 * (j) )  ) ]
#define m(j,iwave)     m[ idx + scanLen[0]* ( blockIdx.y + numthick[0]* ( (iwave) + 4 * (j) )  ) ]
#define cuMinor(m,a,b,c,d,e,f) cuMinor(m,a,b,c,d,e,f,scanLen,numthick)

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 res,tmp;

  tmp  = cuCmul( m(0,0), cuMinor(m, 1, 2, 3, 1, 2, 3) );
  res  = tmp;

  tmp  = cuCmul( m(0,1), cuMinor(m, 1, 2, 3, 0, 2, 3) );
  res  = cuCsub( res, tmp);

  tmp  = cuCmul( m(0,2), cuMinor(m, 1, 2, 3, 0, 1, 3) );
  res  = cuCadd( res, tmp);

  tmp  = cuCmul( m(0,3), cuMinor(m, 1, 2, 3, 0, 1, 2) );
  res  = cuCsub( res, tmp);

#undef m
#undef cuMinor
  return res;  
}


/**
 * \brief Calculates the DOT product of 4x4 matrix "a" with 4x4 matrix b
 */
inline void cuMAPDOT4(__global double2 *a, __global double2 *b, __global double2 *res,
                      __constant int *scanLen, __constant int *numthick){

#define b(iwave,j)      b[  idx + scanLen[0]* ( blockIdx.y + numthick[0]*    ( (iwave) + 4 * (j) )  ) ]
#define a(iwave,j)      a[  idx + scanLen[0]* ( blockIdx.y + 1 + numthick[0]*    ( (iwave) + 4 * (j) )  ) ]
#define res(j,iwave)    res[idx + scanLen[0]* ( blockIdx.y + (numthick[0]-1)*( (iwave) + 4 * (j) )  ) ]

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  for(int ri=0;ri<4;ri++){
    for(int iw=0;iw<4;iw++){
      res(iw,ri) = (double2)(0.0,0.0);
      for(int j=0;j<4;j++){
//|        res(iw,ri) = cuCadd ( res(iw,ri) , cuCmul( a(j,ri) , b(iw,j) ) );|#
        res(iw,ri) = cuCadd ( res(iw,ri) , cuCmul( b(iw,j) , a(j,ri) ) );
      }
    }
  }

#undef a
#undef b
#undef res
  return;
}


/**
 * Calculates the INV of a 4x4 matrix contained in "in".
 */
inline void cuMAPINV4(__global double2 *in, __global double2 *out, __constant int *scanLen,
                      __constant int *numthick){

#define in(iwave,j)      in[ idx + scanLen[0]* ( blockIdx.y + numthick[0]* ( (iwave) + 4 * (j) )  ) ]
#define out(j,iwave)    out[ idx + scanLen[0]* ( blockIdx.y + numthick[0]* ( (iwave) + 4 * (j) )  ) ]
#define cuMinor(m,a,b,c,d,e,f) cuMinor(m,a,b,c,d,e,f,scanLen,numthick)

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 det =  cuDet(in,scanLen,numthick);

  out(0,0) = cuCdiv( cuMinor(     in, 1, 2, 3,  1, 2, 3), det);
  out(1,0) = cuCdiv( negC(cuMinor(in, 1, 2, 3,  0, 2, 3)), det);
  out(2,0) = cuCdiv( cuMinor(     in, 1, 2, 3,  0, 1, 3), det);
  out(3,0) = cuCdiv( negC(cuMinor(in, 1, 2, 3,  0, 1, 2)), det);

  out(0,1) = cuCdiv( negC(cuMinor(in, 0, 2, 3,  1, 2, 3)), det);
  out(1,1) = cuCdiv( cuMinor(     in, 0, 2, 3,  0, 2, 3), det);
  out(2,1) = cuCdiv( negC(cuMinor(in, 0, 2, 3,  0, 1, 3)), det);
  out(3,1) = cuCdiv( cuMinor(     in, 0, 2, 3,  0, 1, 2), det);


  out(0,2) = cuCdiv( cuMinor(     in, 0, 1, 3,  1, 2, 3), det);
  out(1,2) = cuCdiv( negC(cuMinor(in, 0, 1, 3,  0, 2, 3)), det);
  out(2,2) = cuCdiv( cuMinor(     in, 0, 1, 3,  0, 1, 3), det);
  out(3,2) = cuCdiv( negC(cuMinor(in, 0, 1, 3,  0, 1, 2)), det);

  out(0,3) = cuCdiv( negC(cuMinor(in, 0, 1, 2,  1, 2, 3)), det);
  out(1,3) = cuCdiv( cuMinor(     in, 0, 1, 2,  0, 2, 3), det);
  out(2,3) = cuCdiv( negC(cuMinor(in, 0, 1, 2,  0, 1, 3)), det);
  out(3,3) = cuCdiv( cuMinor(     in, 0, 1, 2,  0, 1, 2), det);

#undef in
#undef out
#undef cuMinor
  return;
}


//-------------Kernels
#define dP(i) d_Propagations[idx + scanLen[0]*( (i) + 4* (blockIdx.y) )]
__kernel void
clPropagation(__global double2 *d_Propagations, __global double2 *d_Kz, __global double * d_K0,
              __constant int *scanLen, __constant int *wheretolook, __constant double *Thickness){

//   """ the propagations matrices are diagonal matrices obtained from
//       knowledge of Kz eigenvalues.
//       The return value is a list of NW X 4 arrays.
//       Where is dimension having lenght=4
//       correspond just the diagonal.
//       (NW X4 X 4 would be useless )
//   """
  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  int index;
  int whichlayer = wheretolook[blockIdx.y];
  double2 t;

  t = (double2)(0.0, Thickness[blockIdx.y]);

  //Protect from overflow of global memory
  if(idx < scanLen[0]){
    //Kz is already on the GPU at this point and it is already Transposed, as in the GPU version
    // it is calculated that way!
    for(int i=0;i<4;i++){
      index= idx + scanLen[0]*(i + 4*( whichlayer ) );
      dP(i) = cuCRmul( d_Kz[ index ], d_K0[idx]);
      dP(i) = cuCexp (cuCmul( dP(i), t) );
    }
  }

}
#undef dP

#define d_E(j,k)     d_E[ idx + scanLen[0]*( (j) + 4* ( (k) + 3 * wla ) )]
#define d_B(j,k)     d_B[ idx + scanLen[0]*( (j) + 4* ( (k) + 3 * wla ) )]
//Swapped iwave with j to do on the fly axes swap
#define d_C(iwave,j)     d_Conditions[ idx + scanLen[0]* ( blockIdx.y + numthick[0]* ( (iwave) + 4 * (j) )  ) ]
#define d_Ci(iwave,j)    d_Conditions_inv[ idx + scanLen[0]* ( blockIdx.y + numthick[0]* ( (iwave) + 4 * (j) )  ) ]
__kernel void
getConditions(__global double2 *d_E, __global double2 *d_B, __global double2 *d_Conditions,
              __global double2 *d_Conditions_inv, __constant int *scanLen, __constant int *numthick,
              __constant int *wheretolook){

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  int wla = wheretolook[blockIdx.y];
  //Protect from overflow of global memory
  if(idx < scanLen[0]) {
    //In GPU iwaves,k subarray is already transposed so we copy directly:
    for(int iwave=0;iwave<4;iwave++){
      d_C(iwave,0) = d_E(iwave,0);
      d_C(iwave,1) = d_E(iwave,1);
      d_C(iwave,2) = d_B(iwave,0);
      d_C(iwave,3) = d_B(iwave,1);
    }
  //Now we invert conditions and save it to conditions_inv
  //Warning, this kernel may look small, but it invokes many inlined device functions. that is the reason of this split
  cuMAPINV4(d_Conditions,d_Conditions_inv,scanLen,numthick);
  }

}
#undef d_E
#undef d_B
#undef d_C
#undef d_Ci

//Calculate the interfaces. Here the gridDim.y is numthick-1.
#define di(layer,j,i) d_Interfaces[idx + scanLen[0]*( (layer) + (numthick[0]-1)* ( (i) + 4* (j) ) )]
__kernel void
clInterfaces(__global double2 *d_Kz, __global double2 *d_Conditions,  __global double2 *d_Conditions_inv,
             __global double *d_K0, __global double2 *d_Interfaces, __constant int *scanLen,
             __constant int *numthick, __constant int *wheretolook, __constant double *Roughness){

//  """ this routine is in charge of creating
//      an array of matrices representing
//      the interfaces.
//  """

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  int wla0,wla1;
  double2 dw,toapp_inset,tmp;

  wla0 = wheretolook[blockIdx.y];
  wla1 = wheretolook[blockIdx.y+1];

  //Protect from overflow of global memory
  if(idx < scanLen[0]) {
    cuMAPDOT4( d_Conditions_inv, d_Conditions, d_Interfaces,scanLen,numthick);
 
  //And now the roughness:
    for (int k1=0;k1<4;k1++){
      for(int k2=0;k2<4;k2++){
        toapp_inset = di(blockIdx.y,k1,k2);
        tmp = cuCsub( d_Kz[idx + scanLen[0] *(k1 + 4* wla0 )] , d_Kz[idx + scanLen[0] *(k2 + 4* wla1 )] );
        dw  = cuCmul( (double2)(d_K0[idx] * Roughness[(blockIdx.y)],0.0) , tmp);

        tmp = cuCmul( negC(dw) , cuCRdiv( cuConj(dw) , 2.0 ) );
        tmp = cuCexp( tmp );
        di(blockIdx.y,k1,k2) = cuCmul( toapp_inset, tmp);
      }
    }
  }
}
#undef di

//Calculate the reflectivity for all the layers and the substrate
//The layers here are sequential as the formula is recursive! Mind that gridDim.y MUST be 0 when invoking this kernel!!!
__kernel void
clReflectivity(__global double2 *d_Reflectivity, __global double2 *d_Interfaces, __global double2 *d_Propagations,
               __global double2 *d_propa, __constant int *scanLen, __constant int *numthick )
{

#define dI(i,j) d_Interfaces[idx + scanLen[0]*( (0) + (numthick[0]-1)* ( (i) + 4* (j) ) )]
#define di(layer,i,j) d_Interfaces[idx + scanLen[0]*( (layer) + (numthick[0]-1)* ( (i) + 4* (j) ) )]
#define dP(layer,i) d_Propagations[idx + scanLen[0]*( (i) + 4*(layer) )]
#define dp(i,j) d_propa[idx + scanLen[0]*(  (i) + 4* (j) )]
#define dR(i,j) d_Reflectivity[idx + scanLen[0]*( (i) + 2* (j) )]

  int idx = get_global_id(0);

  int2 blockIdx;
  blockIdx.y = get_global_id(1);

  double2 det;
  double2 buf[4],buf1[4],res[4],tmp;

  //Protect from overflow of global memory
  if(idx < scanLen[0]){
    //First the substrate
    //For the substrate: res = PPMcoreTens.PPM_MAPDOT( inter[:,0:2,2:4 ]  , PPMcoreTens.PPM_MAPINV(inter[:,2:4,2:4 ])  )

    //Inverse matrix
    det = cuCsub ( cuCmul( dI(2,2) , dI(3,3) ) , cuCmul( dI(3,2) , dI(2,3) ) );
    buf[0] = cuCdiv( dI(3,3) , det ); //00
    buf[1] = negC( cuCdiv( dI(2,3) , det ) ); //01
    buf[2] = negC( cuCdiv( dI(3,2) , det ) ); //10
    buf[3] = cuCdiv( dI(2,2) , det ); //11

    //Dot product
    res[0] = cuCadd( cuCmul( dI(0,2) , buf[0] ) , cuCmul( dI(0,3) , buf[2] ) );//00
    res[1] = cuCadd( cuCmul( dI(0,2) , buf[1] ) , cuCmul( dI(0,3) , buf[3] ) );//01
    res[2] = cuCadd( cuCmul( dI(1,2) , buf[0] ) , cuCmul( dI(1,3) , buf[2] ) );//10
    res[3] = cuCadd( cuCmul( dI(1,2) , buf[1] ) , cuCmul( dI(1,3) , buf[3] ) );//11

    //Now the rest of the layers
    for(int ilayer=1;ilayer<numthick[0]-1;ilayer++){

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          dp(i,j) = cuCmul( di(ilayer,i,j) , dP(ilayer,j) );
        }
      }

      //we use buf as aR and buf1 as cR
      //aR
      buf[0] = cuCadd( cuCadd( cuCmul( dp(0,0) , res[0] ) , cuCmul( dp(0,1) , res[2] ) ) , dp(0,2) );
      buf[1] = cuCadd( cuCadd( cuCmul( dp(0,0) , res[1] ) , cuCmul( dp(0,1) , res[3] ) ) , dp(0,3) );
      buf[2] = cuCadd( cuCadd( cuCmul( dp(1,0) , res[0] ) , cuCmul( dp(1,1) , res[2] ) ) , dp(1,2) );
      buf[3] = cuCadd( cuCadd( cuCmul( dp(1,0) , res[1] ) , cuCmul( dp(1,1) , res[3] ) ) , dp(1,3) );

      //cR
      buf1[0] = cuCadd( cuCadd( cuCmul( dp(2,0) , res[0] ) , cuCmul( dp(2,1) , res[2] ) ) , dp(2,2) );
      buf1[1] = cuCadd( cuCadd( cuCmul( dp(2,0) , res[1] ) , cuCmul( dp(2,1) , res[3] ) ) , dp(2,3) );
      buf1[2] = cuCadd( cuCadd( cuCmul( dp(3,0) , res[0] ) , cuCmul( dp(3,1) , res[2] ) ) , dp(3,2) );
      buf1[3] = cuCadd( cuCadd( cuCmul( dp(3,0) , res[1] ) , cuCmul( dp(3,1) , res[3] ) ) , dp(3,3) );

      //Inversion of cR
      det    = cuCsub( cuCmul( buf1[0] , buf1[3] ) , cuCmul( buf1[1] , buf1[2] ) );

      tmp     = buf1[0];
      buf1[0] = cuCdiv( buf1[3] , det );
      buf1[1] = negC( cuCdiv( buf1[1] , det ) );
      buf1[2] = negC( cuCdiv( buf1[2] , det ) );
      buf1[3] = cuCdiv(     tmp , det );

      res[0] = cuCadd( cuCmul( buf[0] , buf1[0] ) , cuCmul( buf[1] , buf1[2] ) );
      res[2] = cuCadd( cuCmul( buf[2] , buf1[0] ) , cuCmul( buf[3] , buf1[2] ) );
      res[1] = cuCadd( cuCmul( buf[0] , buf1[1] ) , cuCmul( buf[1] , buf1[3] ) );
      res[3] = cuCadd( cuCmul( buf[2] , buf1[1] ) , cuCmul( buf[3] , buf1[3] ) );

    }//Loop of layers
    dR(0,0) = res[0];
    dR(1,0) = res[2];
    dR(0,1) = res[1];
    dR(1,1) = res[3];
  }//if(idx < scanLen[0])
}
