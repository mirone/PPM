var searchData=
[
  ['getinput',['getInput',['../class_p_p_m_tensorial__ocl.html#ae2b96907d1ea800166cc0fe1bdffc9cd',1,'PPMTensorial_ocl']]],
  ['getinterfacesresults',['getInterfacesResults',['../classcl_interfaces.html#ac26ad68eaf8571cae64344edfe8d0387',1,'clInterfaces']]],
  ['getmodesresults',['getModesResults',['../classcl_onda.html#a8146aa555df5b72f5605fddd6e5bf55e',1,'clOnda']]],
  ['getmodesresults_5fdebug',['getModesResults_debug',['../classcl_onda.html#a7b3a9f98e7968a7e71038dcde4e1a13b',1,'clOnda']]],
  ['getpropagationsresults',['getPropagationsResults',['../classcl_propagation.html#a58fee2df3229aaf56ffac589f8266033',1,'clPropagation']]],
  ['getreflresults',['getReflResults',['../classcl_reflectivity.html#ae48069650e398ceaa6c708cfe6d1286c',1,'clReflectivity']]]
];
