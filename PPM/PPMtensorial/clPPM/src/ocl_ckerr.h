/**
 * \file
 * \brief Macros for OpenCL API error handling.
 *
 * This set of macros automate the error-checking for ocl_tools and OpenCL.
 * There are implementations with different behaviour, typically
 * one that exits, one that returns an int and one that throws a
 * message.
 */

/*
 *   Project: Macros for OpenCL API error handling. Requires ocl_tools
 *
 *   Copyright (C) 2011 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *   Principal authors: D. Karkoulis (karkouli@esrf.fr)
 *   Last revision: 27/05/2011
 *    
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   and the GNU Lesser General Public License  along with this program.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef _WIN32
#define typeof(_expr) typeid(_expr)
#endif

/**
 * When function returns an error code. If this is not CL_SUCCESS print the
 * OpenCL error-code and exit with 1.
 */
#define CL_CHECK(_expr)                                                         \
   do {                                                                         \
     cl_int _err = _expr;                                                       \
     if (_err == CL_SUCCESS)                                                    \
       break;                                                                   \
     fprintf(stderr, "OpenCL: '%s' returned %d!\n", #_expr, (int)_err);   \
     exit(1);                                                                   \
   } while (0)

/**
 * When function takes error var as argument. Variable name must be _err.
 * If _err is not CL_SUCCESS print the OpenCL error code and exit with 1.
 */
#define CL_CHECK_ERR(_expr)                                                     \
   ({                                                                           \
     cl_int _err = CL_INVALID_VALUE;                                            \
     typeof(_expr) _ret = _expr;                                                \
     if (_err != CL_SUCCESS) {                                                  \
       fprintf(stderr, "OpenCL: '%s' returned %d!\n", #_expr, (int)_err); \
       exit(1);                                                                 \
     }                                                                          \
     _ret;                                                                      \
   })

/**
 * When function returns an error code. If this is not CL_SUCCESS print the
 * OpenCL error-string and exit with 1.
 */
#define CL_CHECK_PR(_expr)                                                          \
   do {                                                                             \
     cl_int _err = _expr;                                                           \
     if (_err == CL_SUCCESS)                                                        \
       break;                                                                       \
     fprintf(stderr, "OpenCL: '%s:%d' returned %s!\n",__FILE__,__LINE__, ocl_perrc(_err)); \
     exit(1);                                                                       \
   } while (0)

/**
 * When function returns an error code. If this is not CL_SUCCESS print the
 * OpenCL error-string and do nothing.
 */
#define CL_CHECK_PRN(_expr)                                                         \
   do {                                                                             \
     cl_int _err = _expr;                                                           \
     if (_err == CL_SUCCESS)                                                        \
       break;                                                                       \
     fprintf(stderr, "OpenCL: '%s:%d' returned %s!\n",__FILE__,__LINE__, ocl_perrc(_err)); \
       break; \
   } while (0)

/**
 * When function returns an error code. If this is not CL_SUCCESS print the
 * OpenCL error-string and return ocl_tools error code (-1).
 */
#define CL_CHECK_PR_RET(_expr)                                                         \
   do {                                                                             \
     cl_int _err = _expr;                                                           \
     if (_err == CL_SUCCESS)                                                        \
       break;                                                                       \
     fprintf(stderr, "OpenCL: '%s:%d' returned %s!\n",__FILE__,__LINE__, ocl_perrc(_err)); \
       return -1; \
   } while (0)

/**
 * When function takes error var as argument. Variable name must be _err.
 * If _err is not CL_SUCCESS print the OpenCL error string and exit with 1.
 */
#define CL_CHECK_ERR_PR(_expr)                                                        \
   ({                                                                                 \
     cl_int _err = CL_INVALID_VALUE;                                                  \
     typeof(_expr) _ret = _expr;                                                      \
     if (_err != CL_SUCCESS) {                                                        \
       fprintf(stderr, "OpenCL: '%s:%d' returned %s!\n",__FILE__,__LINE__, ocl_perrc(_err)); \
       exit(1);                                                                       \
     }                                                                                \
     _ret;                                                                            \
   })

/**
 * When function takes error var as argument. Variable name must be _err.
 * If _err is not CL_SUCCESS print the OpenCL error string and do nothing.
 */
#define CL_CHECK_ERR_PRN(_expr)                                                       \
   do{                                                                                 \
     cl_int _err = CL_INVALID_VALUE;                                                   \
     typeof(_expr) _ret = _expr;                                                      \
     if (_err != CL_SUCCESS) {                                                         \
       fprintf(stderr, "OpenCL: '%s:%d' returned %s!\n",__FILE__,__LINE__, ocl_perrc(_err));  \
     }                                                                                \
     _ret;                                                                            \
   }while(0)

/**
 * When function takes error var as argument. Variable name must be _err.
 * If _err is not CL_SUCCESS print the OpenCL error string and return
 * ocl_tools error-code (-1).
 */
#define CL_CHECK_ERR_PR_RET(_expr)                                                        \
   do{                                                                                 \
     cl_int _err = CL_INVALID_VALUE;                                                  \
     typeof(_expr) _ret = _expr;                                                      \
     if (_err != CL_SUCCESS) {                                                        \
       fprintf(stderr, "OpenCL: '%s:%d' returned %s!\n",__FILE__,__LINE__, ocl_perrc(_err)); \
       return -1;                                                                       \
     }                                                                                \
     _ret;                                                                            \
   }while(0)

/**
 * When function returns an error code. If this is not CL_SUCCESS print the
 * OpenCL error-string and throw a message with the OpenCL error-string.
 */
#define CL_CHECK_ERR_THROW(_expr)                                                        \
   do{                                                                                 \
     char msg[1000]; \
     cl_int _ret = _expr;                                                      \
     if (_ret != CL_SUCCESS) {                                                        \
       sprintf(msg, "%s (@%d) failed: %s!\n",__FILE__,__LINE__, ocl_perrc(_ret)); \
       throw msg;                                                                       \
     }                                                                                \
   }while (0)
