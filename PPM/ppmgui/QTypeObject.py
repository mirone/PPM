

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import sys
import os
import os.path
from PyQt4.Qt  import *
# from qttable import *
from TypeObject import Element

QTable=QTableWidget
PYSIGNAL=SIGNAL

class WindowForCopy(QMainWindow):
	def __init__(self,text,*args):
		apply(QMainWindow.__init__,(self,)+args)
		self.mainWidget=QWidget(self)
		self.textForLabel=text
		self.nbRows=text.count('\n')
		heiht=self.nbRows+5
		self.setWindowTitle(' Window for copy ')
		#self.setMaximumSize(QSize (600,heiht))
		self.setMinimumSize(QSize (500,min(300,heiht)))



		self.mainLayout=QVBoxLayout(self.mainWidget)

		#north---------------
		self.northLayout=QHBoxLayout(5,'north')
		self.fillGrid()

		# south------------
		self.southLayout=QHBoxLayout(5,'south')
		self.btOk=QPushButton('Ok',self.mainWidget)
		self.btOk.setMaximumSize(QSize (50,25))
		self.southLayout.addWidget(self.btOk)

		self.mainLayout.addLayout(self.northLayout)
		self.mainLayout.addLayout(self.southLayout)

		self.setCentralWidget(self.mainWidget)

		self.connect(self.btOk,SIGNAL('clicked()'),self.slotGetInforamation)

	#le cas de QTable
	def fillGrid(self):
		self.grid=QTable(self.nbRows,3,self.mainWidget)
		self.grid.setHScrollBarMode(QScrollView.Auto )
		self.grid.setVScrollBarMode(QScrollView.Auto )
		self.grid.setColumnWidth(0,125)
		self.grid.setColumnWidth(1,150)
		self.northLayout.addWidget(self.grid)
		# titres--------------
		fontLabel=QFont("Courier",12)
		fontEdit=QFont("Courier",11)
		fontLabel.setBold(1)
		self.grid.setFont(fontEdit)
		self.grid.horizontalHeader().setFont(fontLabel)
		self.grid.setHorizontalHeaderItem( 0, 'Add/keep name')
		self.grid.setHorizontalHeaderItem( 1, 'New name')
		self.grid.setHorizontalHeaderItem( 2, 'Object for copy')
		self.grid.setHverticalHeader().hide()
		self.grid.setLeftMargin(0)

		#rimplissage de la gride-----------------
		self.grid.setColumnReadOnly (2,1 )
		self.chechBox_list=[]
		fontLabel.setBold(0)
		i=0
		maxLen=0 #pour calculer la size de 3m colonne
		while (i<self.nbRows):
			# 1ere colonne -checkBox
			# self.chechBox_list.append(QCheckTableItem (self.grid, ''))
			self.chechBox_list.append(QCheckBox (self.grid, ''))
			self.grid.setCellWidget ( i, 0, self.chechBox_list[-1])
			# 2eme colonne -LineEdit
			self.grid.setItem(i,1,QTableWidgetItem(''))
			self.grid.item(i,1).setEnabled (0)
			# 3eme colonne -LineEdit
			index=self.textForLabel.find('\n')
			labelText=self.textForLabel[0:index] #recupere une ligne du texte
			maxLen=max(maxLen,len(labelText))
			self.grid.setItem(i,2,QTableWidgetItem(labelText))
			self.grid.ensureCellVisible(i,2)
			self.textForLabel=self.textForLabel[index+1:] # changemant du texte
			i+=1
		self.grid.setColumnWidth(2,max(150,maxLen*10))
		self.connect(self.grid,SIGNAL('cellChanged(int,int)'),self.slotToggled)


#--------------SLOTS WindowForCopy----------------------------------
	def slotToggled(self,(row,column)) :
		if(column==0):
			checked=self.chechBox_list[row].isChecked()
			if (checked):
				self.slotChecked((row, ))
			else:
				self.slotNotChecked((row, ))

	def slotChecked(self,(nLine, )):
		self.emit(PYSIGNAL('sigHasName'),(nLine,))

	def slotNotChecked(self,(nLine, )):
		self.grid.setItem(nLine,1,QTableWidgetItem(''))
		self.grid.item(nLine,1).setEnabled (0)


	def slotShowLineEdit(self,(nLine, )):
		self.grid.item(nLine,1).setEnabled(1)

	def slotGetInforamation(self):
		information_list=[]
		i=0
		while (i<self.nbRows):
			keepName=self.chechBox_list[i].isChecked ()
			self.grid.endEdit (i,1,1,1)
			name=str(self.grid.text(i,1))
			information_list.append(Element(name,keepName))
			i+=1
		#self.emit(PYSIGNAL('sigCopy'),(-1,information_list,))
		self.emit(PYSIGNAL('sigCopy'),(information_list,))
		self.hide()


# le tablau avec les attribus (lineEdint0 et le les enfants possibles (combobox)
#on n'utilise pas
class TableOfElemAttr(QTable):
	def __init__(self,*args):
		apply(QTable.__init__,(self,)+args)

		self.setColumnCount(2)
		self.setColumnStretchable(0,1)
		self.setColumnStretchable(1,1)
		self.setColumnReadOnly(0,1)

		self.horizontalHeader().setLabel ( 0, 'Attrubutes')
		self.horizontalHeader().setLabel ( 1, 'Elements')
		self.verticalHeader().hide()
		self.setLeftMargin(0)

		#self.setFocusPolicy(QWidget.TabFocus)

		self.key2action = {Qt.Key_Down: self.__class__.doDown ,
			      Qt.Key_Up:self.__class__.doUp,
			      Qt.Key_Left:self.__class__.doLeft,
			      Qt.Key_Right:self.__class__.doRight,
			      #Qt.Key_Tab:self.__class__.doTab,
			      Qt.Key_Enter:self.__class__.doEnter,
			      Qt.Key_Insert:self.__class__.doInsert}

		self.connect(self,PYSIGNAL('sigChangeCursor'),self.slotChangeCursor)
  		#doubleClicked ( int row, int col, int button, const QPoint & mousePos )
		self.connect(self,SIGNAL('cellDoubleClicked( int row, int column )'),self.slotDoubleClicked)

	def fillTable (self,listElement,textAttr):
		lst=QStringList()
		for element in listElement :
			lst.append(element)
		#self.nbRowsElements=textAttr.count('\n')
		#self.nbRowsAttr=len(listAttr)
		self.nbRowsAttr=textAttr.count('\n')+1 #une ligne supplemetaire pour un attr vide
		self.lenElements=len(listElement)
		self.setRowCount(self.nbRowsAttr)
		i=0
		while (i<self.nbRowsAttr-1):
			# 1ere colonne -LineEdit
			#editText=listAttr[i]
			index=textAttr.find('\n')
			editText=textAttr[0:index] #recupere une ligne du texte
			self.setItem(i,0,QTableWidgetItem(editText))
			#self.elements.item(i,0).setEnabled(0)

			# 2eme colonne -ComboBox
			# self.setItem(i,1,QComboTableItem(self,lst))
			tmp=QComboBox(self)
			tmp.addItems(lst)
			self.setItem(i,1,tmp)

			textAttr=textAttr[index+1:] # changemant du texte
			i+=1

			
		tmp=QComboBox(self)
		tmp.addItems(lst)
		self.setItem(i,1, tmp)
		# self.setItem(i,1,QComboTableItem(self,lst))
		self.setItem(i,0,QTableWidgetItem(''))

	def doLeft(self,row, column):
		if (column==1):
			self.emit(PYSIGNAL('sigChangeCursor'),(row, column-1,))

	def doRight(self,row, column):
		if (column==0):
			self.emit(PYSIGNAL('sigChangeCursor'),(row, column+1,))

	def doUp(self,row, column):
		self.emit(PYSIGNAL('sigChangeCursor'),((row-1)% self.nbRowsAttr, column,))

	def doDown(self,row, column):
		self.emit(PYSIGNAL('sigChangeCursor'),((row+1)% self.nbRowsAttr, column,))

	def doTab(self,row, column):
		self.setCurrentCell (0,0)

		#self.releaseKeyboard()
		#self.clearFocus ()
		self.emit(PYSIGNAL('sigChangeWindow'),(1,))

	def doEnter(self,row, column):
		if (column==1):
			index= self.item(row,column).currentItem()
			self.item(row,column).setCurrentIndex((index+1) % self.lenElements)

	def doInsert(self,row, column):
		key=str(self.text(row,0))
		tag=str(self.item (row,1).currentText())
		self.emit(PYSIGNAL('sigInsertHelp'),(key,tag,))


	def keyPressEvent (self,event):
		row=self.currentRow()
		column=self.currentColumn()
		cod=event.key()

		if( self.key2action.has_key(cod) ):
				action = self.key2action[cod]
				apply( action ,(self,)+(row, column))


	def activateNextCell(self):
		row=self.currentRow()
		column=self.currentColumn()
		if (column==1):
			index= self.item(row,column).currentItem()
			self.item(row,column).setCurrentIndex((index+1) % self.lenElements)


#----------------Slots de TableOfElemAttr
	def slotChangeCursor(self,(newRow, newColumn) ):
		self.setCurrentCell ( newRow, newColumn)

	def slotSetCurrentPosition(self):
		self.setCurrentCell (0,0)

	def slotDoubleClicked(self,(row, column,) ):



		self.doInsert(row, column)

#le tableau dans la fenetre Elements pour la liste des anfants et les commemtaires
class TableOfElem(QTable):
	def __init__(self,*args):
		apply(QTable.__init__,(self,)+args)
		# self.help=QWhatsThis(self)

		self.setColumnCount(2)

		self.horizontalHeader().setResizeMode(QHeaderView.Stretch)

		self.setEditTriggers(QAbstractItemView.NoEditTriggers);

		self.setHorizontalHeaderLabels ( QStringList('Elements')+QStringList('Info') )

		self.verticalHeader().hide()

		
	        # self.setLeftMargin(0)

		self.key2action = {Qt.Key_Insert: self.__class__.doInsert,
				   Qt.Key_Down: self.__class__.doDown ,
			      	   Qt.Key_Up:self.__class__.doUp,
			      	   Qt.Key_Left:self.__class__.doLeft,
			      	   Qt.Key_Right:self.__class__.doRight,}
		# self.connect(self,SIGNAL('doubleClicked(int,int,int,const QPoint &)'),self.slotDoubleClicked)
		self.connect(self,SIGNAL('cellDoubleClicked(int,int)'),self.slotDoubleClickedA)
		self.connect(self,PYSIGNAL('sigChangeCursor'),self.slotChangeCursor)

	def fillTable (self,listElement,listInfo):
		#self.nbRowsElements=textAttr.count('\n')
		#self.nbRowsAttr=len(listAttr)
		self.nbElements=len(listElement)
		self.setRowCount(self.nbElements)
		i=0
		while (i<self.nbElements):
			# 1ere colonne -LineEdit
			editText=listElement[i]
			infoText=listInfo[i]
			self.setItem(i,0,QTableWidgetItem(editText))
			# 2eme colonne -Line edit
			self.setItem(i,1,QTableWidgetItem(infoText))
			#self.item(i,1).setWordWrap (1)

			i+=1

	def text(self, i,j):
		return self.item(i,j).text()

	def doLeft(self,row, column):
		if (column==1):
			self.emit(PYSIGNAL('sigChangeCursor'),(row, column-1,))

	def doRight(self,row, column):
		if (column==0):
			self.emit(PYSIGNAL('sigChangeCursor'),(row, column+1,))

	def doUp(self,row, column):
		self.emit(PYSIGNAL('sigChangeCursor'),((row-1)% self.nbElements, column,))

	def doDown(self,row, column):
		self.emit(PYSIGNAL('sigChangeCursor'),((row+1)% self.nbElements, column,))

	def doInsert(self,row,column):
		tag=str(self.text(row,0))
		self.emit(PYSIGNAL('sigInsertHelp'),(tag,''))


	def keyPressEvent (self,event):
		row=self.currentRow()
		column=self.currentColumn()
		cod=event.key()

		if( self.key2action.has_key(cod) ):
				action = self.key2action[cod]
				apply( action ,(self,)+(row, column))

	def ShowWhatsThis(self):
		row=self.currentRow()
		text=self.text(row,1)

		posCursor=self.cursor().pos()
		# posCursor=self.cursorRect().topLeft()
		# posCursor=self.mapToGlobal(  self.cursor().pos()  )
		#posCursor=self.mapFromGlobal(QCursor.pos())
		#posCursor=QPoint ( self.curentPosition,self.curentLine*20)
		# self.help.display (text,posCursor)
		QWhatsThis.showText ( posCursor,  text,self)
		return text

		
#------------SLOTs of TableOfElem
	def slotDoubleClickedA(self, *args):




		(row, column)=args
		self.doInsert(row,column)

	def slotChangeCursor(self,(newRow, newColumn) ):
		self.setCurrentCell ( newRow, newColumn)

# c'est Lineedit  dans la fenetre Elements pour organiser le recherche rapide
class LnEditOfElem(QLineEdit):
	def __init__(self,*args):
		apply(QLineEdit.__init__,(self,)+args)
		self.key2action = {Qt.Key_Insert: self.__class__.doInsert}

	def doInsert(self):
		self.emit(PYSIGNAL('sigDoInsert'),())

	def keyPressEvent (self,event):
		cod=event.key()

		if( self.key2action.has_key(cod) ):
				action = self.key2action[cod]
				apply( action ,(self,))
		apply( self.__class__.__bases__[0].keyPressEvent, (self,event))

# le tableau pour la groupe Edit Element
class TableForEdit(QTable):
	insignalevaluechanged=0
	def __init__(self,*args):
		apply(QTable.__init__,(self,)+args)
		self.doSignal=1
		self.doSetText=1

		self.setColumnCount(1)
		self.horizontalHeader().setResizeMode(QHeaderView.Stretch)

		self.setRowCount(7)

		self.rowType=0

		self.rowPointer=1

		self.rowName=2

		self.rowAttrVal1=3# les lignes pour les attrubuts possibles
		self.rowAttrVal2=4
		self.rowValue=5 # avec une valeur pour <s>

		self.rowValueTab=6 # deuxieme variant pour value (avec 3 valeures) pour <f>

		self.setVerticalHeaderLabels ((( (QStringList('Type')+QStringList('Reference') )+QStringList('Name') )+
						 QStringList("Attr1s"))+QStringList("Attrs2")+QStringList("Value")+QStringList("Values\n(actu,min,max)"))
		
		self.horizontalHeader().hide()
		self.setRowHeight (self.rowValueTab, 40)
		#self.setTopMargin(0)
		#self.setLeftMargin(120)

		#pour la ligne "Pointer" -creation de la list pour combobox
		self.lstPointer=QStringList()
		self.lstPointer.append('false')
		self.lstPointer.append('true')


		#la creation de LineEdit pour le champ Name
		self.lnName=QLineEdit(self)
		self.setCellWidget(self.rowName,0,self.lnName)

		#la ligne avec 3 valeures
		self.creatTableForValue()
		self.setCellWidget(self.rowValueTab,0,self.tbOfValue)
		# self.cellWidget(self.rowValueTab,0).setSizePolicy ( QSizePolicy.Fixed, QSizePolicy.Ignored )



		self.hideRow(self.rowValueTab)
		self.hideRow(self.rowValue)
		#self.tbOfValue.hideRow(0)
		
		self.tbOfValue.show ()


		self.tbOfValue.hide ()

		self.connect(self,SIGNAL('cellChanged( int,int)'),self.slotValueChanged)
		
		
		self.connect(self.lnName,SIGNAL('textChanged(const QString &)'),self.slotValueChangedName)
		
		self.connect(self,SIGNAL('cellClicked (int,int) '),self.slotChangeRow)
		
		self.connect(self,SIGNAL('currentCellChanged (int, int)'),self.slotChangeRow)
		
		self.connect(self.tbOfValue,SIGNAL('cellClicked ( int, int)'),self.slotClickedVal)



	def slotChangeRefType(self, ind):

		# if hasattr( self,"pointerCombo"    ):
		self.emit(PYSIGNAL('sigPointerChanged'),(ind,))

	def clearAll(self):
		if TableForEdit.insignalevaluechanged:
			return
		TableForEdit.insignalevaluechanged=1
		i=0
		while(i< self.rowCount()-1):
			if(i!=self.rowName):
				self.setItem(i,0,QTableWidgetItem('' ))
			i+=1
		i=0
		while(i< self.tbOfValue.columnCount()):
			self.tbOfValue.setItem(0,i,QTableWidgetItem(''))
			i+=1
		self.tbOfValue.hide()
		TableForEdit.insignalevaluechanged=0

	def setName(self,text):
		if TableForEdit.insignalevaluechanged:
			return
		TableForEdit.insignalevaluechanged=1
		oldText=str(self.lnName.text())
		text=oldText+text
		self.lnName.setText(text)
		#self.lnName.setFocus()
		TableForEdit.insignalevaluechanged=0

	def creatTableForValue(self):
		TableForEdit.insignalevaluechanged=1
		self.tbOfValue=QTable(1,3)

		# self.tbOfValue.setSize(
		self.tbOfValue.verticalHeader().setStretchLastSection(1);

		
		# self.tbOfValue.setSizePolicy ( QSizePolicy.Fixed, QSizePolicy.Ignored )
		
		self.tbOfValue.horizontalHeader().hide()
		# self.tbOfValue.setTopMargin(0)
		self.tbOfValue.verticalHeader().hide()
		# self.tbOfValue.setLeftMargin(0)
		self.tbOfValue.setMinimumHeight (25)
		i=0		
		self.horizontalHeader().setResizeMode( QHeaderView.Stretch)
		self.connect(self.tbOfValue,SIGNAL('cellChanged ( int,int)'),self.slotValueChangedOfTb)
		TableForEdit.insignalevaluechanged=0


	def fillEdit(self, dictEdit ):
		if TableForEdit.insignalevaluechanged:
			return
		TableForEdit.insignalevaluechanged=1
		self.doSignal=0 # controle emission du signal TextChange
		self.clearAll()
		#remplissage du champ Type
		self.setItem(self.rowType,0,QTableWidgetItem(dictEdit['tag']))
		#remplissage du champ Reference

		
		# self.setItem(self.rowPointer,0,QComboTableItem(self,self.lstPointer))
					
		tmp=QComboBox(self)
		tmp.addItems(self.lstPointer)
		self.pointerCombo=tmp

		self.connect( self.pointerCombo ,SIGNAL('activated(int)'),  self.slotChangeRefType   )

		self.setCellWidget(self.rowPointer,0,tmp)
		self.cellWidget(self.rowPointer,0).setCurrentIndex(dictEdit['pointer'])
		#remplissage du champ Name
		if (self.doSetText):
			self.lnName.setText(dictEdit['name']) # ne doit pas provoquer le signal TextChange
		lenAttr=len(dictEdit['attrValue'])
		#remplissage du champ des Valeurs des attributs possibles
		lenMax=2
		i=0
		while(i<lenAttr):
			self.showRow(self.rowAttrVal1+i)#value
			self.setVerticalHeaderItem ( self.rowAttrVal1+i, QTableWidgetItem(QString(dictEdit['attrKey'][i])))
			self.setItem(self.rowAttrVal1+i,0,QTableWidgetItem(str(dictEdit['attrValue'][i])))
			i+=1
		while(i<lenMax):
			self.hideRow(self.rowAttrVal1+i)#key
			i+=1
		if(dictEdit.has_key('valDefault')):
			self.valDefault=dictEdit['valDefault']
		#remplissage du champ Value
		if (dictEdit.has_key('valueS')): #le cas des element terminal
			self.hideRow(self.rowValueTab)
			self.tbOfValue.hide ()
			self.showRow(self.rowValue)
			if (len(dictEdit['valueS'])!=0):
				self.setItem(self.rowValue,0,QTableWidgetItem(dictEdit['valueS'][0]))
			else:
				self.setItem(self.rowValue,0,QTableWidgetItem(''))
				
		elif(dictEdit.has_key('value')):
			self.hideRow(self.rowValue)
			self.showRow(self.rowValueTab)
			self.tbOfValue.show()
			i=0
			values=dictEdit['value']
			for val in values:
				self.tbOfValue.setItem(0,i,QTableWidgetItem(val))
				i+=1
			while(i<3):
				self.tbOfValue.setItem(0,i,QTableWidgetItem(""))
				i=i+1
		else:
			self.hideRow(self.rowValue)
			self.hideRow(self.rowValueTab)
			self.tbOfValue.hide()
		self.doSignal=1
		TableForEdit.insignalevaluechanged=0

	def getValue(self):
		value=''
		position=0
		row=self.currentRow()
		if (row==self.rowValue ):
			value=str(self.text(self.rowValue,0))
		elif (row==self.rowValueTab):
			position=self.tbOfValue.currentColumn()
			value=str(self.tbOfValue.item(0,position).text())
		return value,position

	def keyPressEvent (self,event):
		cod=event.key()
		row=self.currentRow()
		if (cod==Qt.Key_F4 and (row==self.rowValue or row==self.rowValueTab)):
			res=self.getValue()
			currentValue=res[0]
			position=res[1]
			self.emit(PYSIGNAL('sigShowBigText'),(currentValue,position,))

		if (cod==Qt.Key_F5 and (row==self.rowValue)):
			fileName = str(QFileDialog.getOpenFileName(self,"choose the filename", '',"all files (*)"))
			fileName=os.path.basename(fileName)
			# print " il risultato est " , fileName
			# print self.item(self.rowValue, 0)
			# print self.cellWidget(self.rowValue, 0)
			if( len(fileName)):
				self.emit(PYSIGNAL('sigValueChanged'),(fileName,0,))
		else:
			apply( self.__class__.__bases__[0].keyPressEvent, (self,event))


# SLOTS----------------------------------
	def slotValueChanged(self, *args ):

		if TableForEdit.insignalevaluechanged:
			return
		try:
			TableForEdit.insignalevaluechanged=1

			(row,column) =args
			if (row==self.rowType): # le changement de Type
				newType=str(self.item(self.rowType,0).text())
				self.emit(PYSIGNAL('sigTypeChanged'),(newType,))
			elif (row==self.rowPointer):# le changement de Reference



				# 			ind=self.item(self.rowPointer,0).currentItem()

				if hasattr( self,"pointerCombo"    ):
					ind = self.pointerCombo.currentIndex()
					self.emit(PYSIGNAL('sigPointerChanged'),(ind,))
			elif(row==self.rowValue):# le changement de  Valeur

				newVal=str(self.item(self.rowValue,0).text())

				self.emit(PYSIGNAL('sigValueChanged'),(newVal,0,))


			elif(row==self.rowAttrVal1 or row==self.rowAttrVal2):# le changement de valeurs des attributs
				newAttrVal=str(self.item(row,0).text())

				attrKey=str(self.verticalHeaderItem(row).text())
				if( hasattr(self,"valDefault")):
					valDefault=self.valDefault[attrKey]
				else:
					valDefault=None
				self.emit(PYSIGNAL('sigAttrValueChanged'),(newAttrVal,attrKey,valDefault))
		except:
			TableForEdit.insignalevaluechanged=0
			raise
		TableForEdit.insignalevaluechanged=0
		
	def slotValueChangedName(self,*args):
		if TableForEdit.insignalevaluechanged:
			return
		TableForEdit.insignalevaluechanged=1

		text, = args
		# pour que se signal ne passe pas dans le boucle infini on cotrole son emission
		self.setCurrentCell (self.rowName,0)
		self.doSetText=0
		if (self.doSignal):

			self.emit(PYSIGNAL('sigNameChanged'),(str(text),))
		self.doSetText=1
		TableForEdit.insignalevaluechanged=0

	def slotValueChangedOfTb(self, *args):
		row,column = args 



		if TableForEdit.insignalevaluechanged:
			return
		TableForEdit.insignalevaluechanged=1
		newVal=str(self.tbOfValue.item(0,column).text() )
		newVal=newVal.replace(' ','')
		self.emit(PYSIGNAL('sigValueChanged'),(newVal,column,))
		TableForEdit.insignalevaluechanged=0

	def slotChangeRow(self,*args  ):
		if TableForEdit.insignalevaluechanged:
			return
		TableForEdit.insignalevaluechanged=1
		(row,column)=args

		self.setCurrentCell (row,column)
		if (row==self.rowType):
			self.emit(PYSIGNAL('sigDoChangeType'),())
		if(row==self.rowValue or row==self.rowValueTab):
			strMessage='Press F4 to edit value in big window'
			self.emit(PYSIGNAL('sigShowMessageWarning'),(strMessage,0,))
		else:
			self.emit(PYSIGNAL('sigShowMessageWarning'),('',0,))
		TableForEdit.insignalevaluechanged=0


	def slotClickedVal(self,*args):
		if TableForEdit.insignalevaluechanged:
			return
		TableForEdit.insignalevaluechanged=1
		(row, column) =args
		self.setCurrentCell(self.rowValueTab,0)
		strMessage='Press F4 to edit value in big window'
		self.emit(PYSIGNAL('sigShowMessageWarning'),(strMessage,0,))
		TableForEdit.insignalevaluechanged=0


#class pour modifier et ajouter les commentaire dans la fenetre Edit an element
# class TextForComment(QTextEdit):
class TextForComment(QTextEdit):
	def __init__(self,*args):
		apply(QTextEdit.__init__,(self,)+args)
		#self.setPlainText()
		self.setFont(QFont("Courier",11))
		#self.setWordWrap (QTextEdit.FixedColumnWidth )
		#self.currentLine=0
		#self.currentColumn=0
		#self.connect(self,SIGNAL('textChanged()'),self.slotTextChange)
		#self.connect(self,SIGNAL('clicked(int,int)'),self.slotClicked)

		'''self.key2action = {Qt.Key_Down: self.__class__.doDown ,
			      Qt.Key_Up:self.__class__.doUp,
			      Qt.Key_Left:self.__class__.doLeft,
			      Qt.Key_Right:self.__class__.doRight}
			      #Qt.Key_Enter:self.__class__.doEnter}'''

	def getComment(self):
		return str(self.toPlainText() )

	def fillComment(self,text):
		#self.doSignal=0
		self.setPlainText(text)
		#self.doSignal=1

	def getCurrentPosition(self):
		return self.curentLine, self.curentPosition

	def ChangeCursor(self,newLine,newColumn):
		self.currentLine=newLine
		self.currentColumn=newColumn
		self.setCursorPosition(newLine,newColumn)

	def doUp(self,line, pos):
		self.ChangeCursor(line-1,pos)

	def doDown(self,line, pos):
		self.ChangeCursor(line+1,pos)

	def doRight(self,line, pos):
		self.ChangeCursor(line,pos+1)

	def doLeft(self,line,pos):
		if(pos!=0):
			self.ChangeCursor(line,pos-1)

	def doEnter(self,line,pos,event):
		apply( self.__class__.__bases__[0].keyPressEvent, (self,event))
		self.ChangeCursor(line+1,0)

	def keyPressEvent (self,event ):

		line=self.textCursor().blockNumber()
		pos = self.textCursor().position() -  self.textCursor().block().position()
		# res=self.cursor().pos()
		# print res
		# print dir(res)
		# # res=self.getCursorPosition()
		# line=res[0]
		# pos=res[1]
		cod=event.key()
		if (cod==Qt.Key_Enter):
			self.doEnter(line,pos,event)
		#si c'est le simple touche
		try:
			if( self.key2action.has_key(cod) ):
				action = self.key2action[cod]
				apply( action ,(self,)+(line, pos))
			else:
				apply( self.__class__.__bases__[0].keyPressEvent, (self,event))
		except:
			apply( self.__class__.__bases__[0].keyPressEvent, (self,event))

	def slotTextChange (self):
		if(self.doSignal):
			newText=str(self.text())
			self.emit(PYSIGNAL('sigDoChangeComment'),(newText,))
			self.ChangeCursor(self.currentLine,self.currentColumn+1)

	def slotClicked(self,(line,coursor) ):
		self.ChangeCursor(line,coursor)



# class pour line edit pour le tableau des enfants avec attrib
class LnOfChildren(QLineEdit):
	def __init__(self,index,*args):
		apply(QLineEdit.__init__,(self,)+args)
		self.index=index
		self.connect(self,SIGNAL('textChanged ( const QString & )'),self.slotTextChange)

	def slotTextChange(self,*args):
		try:
			(text,)=args[0]
		except:
			text = args[0]

		self.emit(PYSIGNAL('sigTextChanged'),(text,self.index,)) # a connecter dans TableForchildren

#le tableua pour la groupe Add children with attributes
class TableForchildren(QTable):
	def __init__(self,*args):
		apply(QTable.__init__,(self,)+args)
		self.setColumnCount(3)
		self.horizontalHeader().setResizeMode(QHeaderView.Stretch)
		self.setEditTriggers(QAbstractItemView.NoEditTriggers);
		self.setColumnWidth (2,40)
		self.setHorizontalHeaderLabels ( (QStringList('Attributes')+QStringList('Type') )+QStringList('Add') )

		self.verticalHeader().hide()
		# self.setLeftMargin(0)
		
		self.maxRows=15 # c'est pas definit!!!!!!!

		self.leList=[]
		self.setRowCount(self.maxRows)
		i=0
		while (i<self.maxRows):
			self.leList.append(LnOfChildren (i,'',self))
			self.setCellWidget(i,1,self.leList[-1])
			self.connect(self.leList[-1],PYSIGNAL('sigTextChanged'),self.slotTextChange)
			i+=1

	def fillChildren(self,listAttrVal,listTypePossible):
		self.doSignal=0
		self.numRows=len(listAttrVal)
		self.listAttrVal=listAttrVal
		self.listTypePossible=listTypePossible
		#self.setNumRows(self.numRows)
		i=0
		while (i<self.numRows):
			self.showRow (i)
			#la premiere colonne
			self.setItem(i,0,QTableWidgetItem(listAttrVal[i]))
			#la deuxieme ligne
			self.leList[i].show()
			self.leList[i].setText(listTypePossible[i][0])
			#la troisieme colonne
			# self.setItem ( i, 2, QCheckTableItem(self,''))
			self.setCellWidget ( i, 2, QCheckBox('', self))
			self.cellWidget(i,2).setChecked(1)
			i+=1
		while (i<self.maxRows):
			self.hideRow ( i )
			self.leList[i].hide()
			i+=1
		self.doSignal=1
	'''
	def activateNextCell(self):
		row=self.currentRow()
		column=self.currentColumn()
		if (column==1):
			apply( self.__class__.__bases__[0].activateNextCell, (self,))
	'''

#--------------SLOTS of TableForchildren

	def slotGetChildrenChecked(self):
		i=0
		listKeyChecked=[]
		listTypeChecked=[]
		listIndice=[]
		allTypeGood=1
		while (i<self.numRows and allTypeGood):
			if(self.cellWidget(i,2).isChecked()):
				key=str(self.item(i,0).text())
				tag=str(self.leList[i].text())
				if (not (tag in self.listTypePossible[i])):
					val=key
					allTypeGood=0
				listKeyChecked.append(key)
				listIndice.append(i)
				listTypeChecked.append(tag)
			i+=1
		self.setCurrentCell (0,0)
		if(allTypeGood):
			self.emit(PYSIGNAL('sigTakeChildrenChecked'),(listKeyChecked,listTypeChecked,listIndice))
		else:
			strError='The type of element for the value '+ val+' is not correct'
			self.emit(PYSIGNAL('sigMessageError'),(strError,))



	def slotTextChange(self,(text,index) ):
		#row=self.currentRow()
		if (self.doSignal):
			self.emit(PYSIGNAL('sigTextChanged'),(text,self.leList[index],self.listTypePossible[index],))


	#le slot de clique sur bouton "Ccoisir tout"
	def slotCooseAll(self):
		i=0
		while (i<self.numRows):
			self.cellWidget(i,2).setChecked(1)
			i+=1

	def slotNoCoice(self):
		i=0
		while (i<self.numRows):
			self.cellWidget(i,2).setChecked(0)
			i+=1

	def slotContCoice(self):
		i=0
		while (i<self.numRows):
			checked=self.cellWidget(i,2).isChecked()
			self.cellWidget(i,2).setChecked(not checked)
			i+=1


#-------------classe for Style de View------
class SyntaxOfView(QSyntaxHighlighter):
	def __init__(self,*args):
		apply(QSyntaxHighlighter.__init__,(self,)+args)
		self.font=QFont("Courier",12)
		self.fontComment=QFont("Courier",12,QFont.Normal ,1)
		self.font.setBold(1)

		self.colorTag=QColor (170,0,170)
		self.colorSign=QColor (0,0,255)
		self.colorAttr=QColor (0,0,200)

## 	def highlightBlock(sekf, *args):
## 		print " han chiamato highlightBlock " , args

	def highlightBlock(self,textOfParagraph):

		endStateOfLastPara=self.previousBlockState()
		indStart=[]
		indEnd=[]
		text=str(textOfParagraph)
		# le traitement du tag d'ouverture
		nb=text.count('<')
		i=0
		start=0
		while(i<nb):
			ind=text.find('<',start)
			self.setFormat ( ind, 1,self.font)
			self.setFormat ( ind, 1, self.colorTag)
			start=ind+1
			indStart.append(start)
			i+=1

		# le traitement du bar de fermeture
		nb=text.count('>')
		i=0
		start=0
		while(i<nb):
			ind=text.find('>',start)
			self.setFormat ( ind, 1, self.colorTag)
			self.setFormat ( ind, 1,self.font)
			start=ind+1
			indEnd.append(ind)
			i+=1

		# le traitement du minus
		if ('-' in text):
			ind=int(text.find('-'))
			self.setFormat ( ind,1, self.colorSign)

		# le traitement du plus
		if ('+' in text):
			ind=int(text.find('+'))
			self.setFormat ( ind,1, self.colorSign)


		# le traitement de type du tag
		if(len(indEnd)==len(indStart)):
			i=0
			while (i<len(indEnd)):
				# tag
				listOfWord=text[indStart[i]:indEnd[i]].split()# ???? ou indEnd[i]+1
				tag=''
				if (len(listOfWord)!=0): tag=listOfWord[0]
				self.setFormat(indStart[i],len(tag),self.font)
				# les attributs
				lenAttr=indEnd[i]-indStart[i]-len(tag)
				if (lenAttr>0):
					self.setFormat(indStart[i]+len(tag),lenAttr,self.colorAttr)
				i+=1

		# le traitement des attr
		if ('"' in text):
			start=text.find('"')
			end=text.rfind('"')
			self.setFormat(start,end-start+1,self.colorAttr)

		# le traitement du bar de fermeture
		if ('/' in text):
			ind=text.rfind('/')
			self.setFormat ( ind,1, self.colorTag)
			self.setFormat ( ind,1,self.font )

		#traitemnet des commentaire
		if ('<!--' in text):
			ind=text.find('<!--')
			self.setFormat(ind,len(text),self.fontComment)

		if ('-->' in text):
			ind=text.find('-->')
			self.setFormat(0,len(text),self.fontComment)

		if not(('>' in text)and('>' in text)):
			self.setFormat(0,len(text),self.fontComment)

		return None

#----------Window pour recherch des variables------
class WindowForFind(QMainWindow):
	def __init__(self,*args):
		apply(QMainWindow.__init__,(self,)+args)
		self.setWindowTitle (' Find variables ')
		#self.setMaximumSize(QSize (300,300))
		#self.setMinimumSize(QSize (100,100))
		self.setFixedSize (QSize ( 300, 150 ))

		self.mainWidget=QWidget(self)
		self.mainLayout=QVBoxLayout(self.mainWidget) # ,5,1)

		# self.mainWidget.setLayOut(self.mainLayout)
		self.grRadionBut=QGroupBox('Choice of the variant')


		layout = QVBoxLayout()
		
		self.rbAllVariables=QRadioButton( 'All variables')
		self.rbAllVariables.setChecked(1)
		
		self.rbOneVariable=QRadioButton( 'One variable')

		layout.addWidget(self.rbAllVariables)
		layout.addWidget(self.rbOneVariable)
		self.grRadionBut.setLayout(layout)


		
		# self.grRadionBut.insert ( self.rbAllVariables, 0)
		# self.grRadionBut.insert ( self.rbOneVariable, 1)

		#self.mainLayout.addWidget(self.mainWidget)


		
		self.nameLayout=QHBoxLayout()

		self.name=QLineEdit()
		self.nameTitle=QLabel('Name ')		
		self.name.hide()
		self.nameTitle.hide()

		self.southLayout=QHBoxLayout()
		self.btOk=QPushButton('Ok')
		self.btOk.setMaximumSize(QSize (50,25))
		self.warning=QLabel()
		
		palette= self.warning.palette()
		palette.setColor(self.warning.backgroundRole(), QColor (255,183,45));
		self.warning.setPalette(palette);
		self.warning.setAutoFillBackground (1);

		self.mainLayout.addWidget(self.grRadionBut)
		
		self.southLayout.addWidget(self.btOk)
		self.southLayout.addWidget(self.warning)

		self.nameLayout.addWidget(self.nameTitle)
		self.nameLayout.addWidget(self.name)
		
		self.mainLayout.addLayout(self.nameLayout)
		self.mainLayout.addLayout(self.southLayout)

		self.setCentralWidget(self.mainWidget)


		# ------------------------------------------------------------------------------

		self.connect(self.rbOneVariable,SIGNAL('toggled ( bool)'),self.slotToggledOne)
		self.connect(self.rbAllVariables,SIGNAL('toggled ( bool)'),self.slotToggledAll)
		self.connect(self.btOk,SIGNAL('clicked()'),self.slotClicked)

	def slotToggledOne(self, isOn ):
		if isOn :
			self.name.show()
			self.nameTitle.show()

	def slotToggledAll(self, isOn):
		if isOn :
			self.name.hide()
			self.nameTitle.hide()
			self.warning.setText ('')

	def slotClicked(self):
		varToFind=''
		canBeClosed=1
		if (self.rbOneVariable.isChecked()):
			varToFind=str(self.name.text())
			if(len(varToFind)==0):
				#il faut donner la faute
				self.warning.setText('Give the name !!!')
				canBeClosed=0
		if canBeClosed:
			self.emit(PYSIGNAL('sigFind'),(varToFind,))
			self.hide()
