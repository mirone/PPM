

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



class Element:
	def __init__(self,object):
		self.refList=[]
		self.object=object

	def append(self,objectRef):
		self.refList.append(objectRef)

	def delRef(self,objectRef):
		try:
			self.refList.remove(objectRef)
		except: pass

	def lenRef(self):
		return len(self.refList)

	def reference(self):
		return self.refList

	def definition(self):
		return self.object

class Variable_reference:
	def __init__(self):
		#self.variables_dict={}
		self.variables_list=[]

	def append(self,objectDef):
		#if not self.variables_dict.has_key(key):
			#self.variables_dict[key]=[]
		element=Element(objectDef)
		#self.variables_dict[key].append(element)
		self.variables_list.append(element)

	def index(self,nameDef):
		index=-1
		i=0
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=nameDef):
			i+=1
		if (i!=len_list):
			index=i
		return index


	#recherche de reference dans la liste des variables
	def getPointer(self,nameDef,objectRef):
		res=None
		'''i=0
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=nameDef):
			i+=1
		if (i!=len_list):'''
		ind=self.index(nameDef)
		if(ind!=-1):
			res=self.variables_list[ind].object
			self.variables_list[ind].append(objectRef)
		return res

	def clearAll(self):
		self.variables_list=[]

	def delReference(self,nameDef,objectRef):
		'''i=0
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=nameDef):
			i+=1
		if (i!=len_list):'''
		ind=self.index(nameDef)
		if(ind!=-1):
			self.variables_list[ind].delRef(objectRef)

	def getNbReference(self,nameDef):
		res=0
		'''i=0
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=nameDef):
			i+=1
		if (i!=len_list):'''
		ind=self.index(nameDef)
		if(ind!=-1):
			res=self.variables_list[ind].lenRef()
		return res

	def reference(self,indOfDef):
		return self.variables_list[indOfDef].reference()

	def definition (self,indOfDef):
		return self.variables_list[indOfDef].definition()

	def internRef(self,listOfDef):
		i=0
		noRef=1
		while(i<len(listOfDef) and noRef):
			name=listOfDef[i].getName()
			nbReference=self.getNbReference(name)

			if (nbReference!=0):noRef=0
			i+=1
		return noRef

	def allVariables(self):
		n=len(self.variables_list)
		i=0
		res=[]
		while(i<n):
			defObject=self.variables_list[i].definition()
			if(defObject.isVariable() and defObject.__len__()==3):#si c'est varaible, on prend <f>
				defObject=defObject.parent
				res.append(defObject)
			i+=1
		return res





	'''def CountPlusOne(self,key,name):
		i=0
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=name):
			i+=1
		if (i!=len_list):
			self.variables_list[i].count+=1'''

'''
class Element:
	def __init__(self,object):
		self.count=0
		self.object=object

class Variable_reference:
	def __init__(self):
		#self.variables_dict={}
		self.variables_list=[]

	def append(self,key,object):
		#if not self.variables_dict.has_key(key):
			#self.variables_dict[key]=[]
		element=Element(object)
		#self.variables_dict[key].append(element)
		self.variables_list.append(element)

	#recherche de reference dans la liste des variables
	def getPointer(self,key,name):
		i=0
		res=None
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=name):
			i+=1
		if (i!=len_list):
			res=self.variables_list[i].object
			self.variables_list[i].count+=1
		return res

	def clearAll(self):
		self.variables_list=[]

	def delReference(self,key,name):
	# on n'a pas fait try-exept pour le key !
		i=0
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=name):
			i+=1
		if (i!=len_list):
			self.variables_list[i].count-=1

	def getNbReference(self,key,name):
	# on n'a pas fait try-exept pour le key !
		res=0
		i=0
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=name):
			i+=1
		if (i!=len_list):
			res=self.variables_list[i].count
		return res

	def CountPlusOne(self,key,name):
		i=0
		len_list=len(self.variables_list)
		while (i< len_list and self.variables_list[i].object.name!=name):
			i+=1
		if (i!=len_list):
			self.variables_list[i].count+=1
'''


