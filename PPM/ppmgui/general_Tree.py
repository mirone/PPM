

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



#structure des noeuds d'arbre
from elementtree.ElementTree import _ElementInterface

import string
import sys
import os
import pickle



from xml.sax.handler import ContentHandler
# from xml.sax.saxlib import LexicalHandler
from xml.sax import make_parser
from xml.sax.handler import feature_namespaces

#les test pour parseur (les noms qui ne sont pas les references)
from Testargument import isMathExp,isName,isFloat
# l'object avec tous les definitions des variables et le conteur des references
from Variable_reference import Variable_reference
#les differents types  str, bool, dict, list plus complexes
from TypeObject import *
#les dictionnaires -constante pour realiser le control et pour l'aide
from constVariable import *

from PyQt4.Qt  import *
PYSIGNAL=SIGNAL

#global objet avec tous les definitions des variables et le conteur des references
variables=Variable_reference()


#remplace l'instince de xml.AttributesImpl par le simple dictionniare pour garder les attributs d'element
def replaceByDict(attrs):
	keys=attrs.keys()
	vals=attrs.values()
	i=0
	dict={}
	while (i<len(attrs)):
		dict[keys[i]]=vals[i]
		i+=1
	return dict

#-------------------------------LES CLASS----------------------------------

#le type de tous les noeuds dans l'arbre sauf <f> et <s> qui sont specifies
class General_object (_ElementInterface):
	def __init__(self,parent,name,attrs):
		_ElementInterface.__init__(self,name,attrs)
		#la reference sur l'element -parent
		self.parent=parent
		#le nom de cet element
		self.name=''
		#la reference sur l'element-definition pour les references (par defaut on cree touours la defenition)
		self.pointer=None
		#l'indice des elements terminaux (pour tout les element 0 sauf <s> et <f>)
		self.toend=0
		#l'indice pour montrer les sous-elements (=1) ou non (=0)
		self.opened=0

#--------------le parsing du xml et la construction d'arbre----------
	def process_startElement(self,name,attrs):
		pass

	def process_endElement(self, name):
		pass


	def process_characters(self,ch):
		line=''
		#la supressiond des symboles " \n\t"
		for c in ch:
          		if( (c not   in " \n\t") ):
             			line=line+c
		if (len(line)!=0):
			#recherche dans la listes des definition
			#res=variables.getPointer(self.tag,line)
			res=variables.getPointer(line,self)
			if (res!=None): #si on trouve, cet objet est la reference
				self.pointer=res


			else: #sinon, c'est la difinition
				self.name=line
				#on ajoute dans la liste des variables
				#variables.append(self.tag,self)
				variables.append(self)


	def write_xml_text(self,file_xml):
		pass

	''' A l'aide de cette foction on peut verifier si le document qu'on ouvre est correcte. Notamment apres la 		creation complete du noeud on verifie si il est correct selon 2 cas possible'''
	def checkKnot(self):
		# si c'est la def. mais sans les enfants -> on la considere comme une ref. qui est plus haut que la def.
		if (self.pointer==None and self.__len__()==0):
			#il faut faire le pointer sur self
			self.pointer=self
			#self.setName('')

		# si c'est la ref. mais avec les enfants -> on la considere comme def. avec le nom qui deja existe
		elif (self.pointer!=None and self.__len__()!=0):
			# il faut supprimer vider le champ pointer
			self.setName(self.pointer.getName())
			self.pointer=None

#-------------------------------la creation du text xml------------

# cree un text xml a partir d'arbre sans regardant si l'elemant ouvert ou non (pour l'ecriture dans un fichier .xml et pour visu de la definition )
	def getXMLBlock_knot(self,str_doc,depth,forDef):
		#les tabs pour decalage
		line_tab='\t\t'
		line_tab=line_tab.expandtabs(depth)
		# composition de la ligne des attrubus
		line_attr=''
		for key in self.keys():
			line_attr+=key+'="'+ self.attrib[key]+'" '
		if len(line_attr)!=0:#si elle n'est pas vide
			line_attr=' '+line_attr #on l'aajoute dans la ligne-resulatat
		if (self.pointer!=None and not(self.toend)):
			#si l'objet est la reference, on ajoute le nom de l'objet-definition
			str_doc.append(line_tab+'<'+self.tag+line_attr+'>'+self.pointer.name+'\n')
		else: #sinon
			if (self.toend):# element est terminal
				str_doc.append(line_tab+'<'+self.tag+line_attr+'>'+self.name)
				#ecriture du text entre balises
				self.getText_value(str_doc)
			else:#pas terminal
				str_doc.append(line_tab+'<'+self.tag+line_attr+'>'+self.name+'\n')
				#parcour des enfants et applel recursif de cette fonction pour chaque enfant
				list_child=self.getchildren()
				for child in list_child:
					child.getXMLBlock_knot(str_doc,depth+1,forDef)
		# on ajoute les tags de fermeture
		if(self.toend):
			str_doc.append('</'+self.tag+'>'+'\n')
		else:
			str_doc.append(line_tab+'</'+self.tag+'>'+'\n')

#----------------------------------pour manipulation d'arbre--------------
	#la creation du string du contenue pour une branche d'arbre et du dictionnaire qui fait lien entre l'arbre et la visu
	def getText_knot(self,str_doc,linkKnot_view,depth):
		#les tabs pour decalage
		line_tab='\t\t'
		line_tab=line_tab.expandtabs(depth)
		str_doc.append(line_tab)
		#on ajoute les signes '+' ou '-' pour les element qui ne sont pas terminaux et les references
		if (self.opened and not(self.toend) and self.pointer==None): str_doc.append('-')
		elif not(self.opened or self.toend or self.pointer!=None ):str_doc.append('+')
		# composition de la ligne des attrubus
		line_attr=''
		for key in self.keys():
			line_attr+=key+'="'+ self.attrib[key]+'" '
		if len(line_attr)!=0:

			line_attr=' '+line_attr
		if (self.pointer!=None and not(self.toend)):
			#si l'objet est la reference, on ajoute le nom de l'objet-definition
			str_doc.append('<'+self.tag+line_attr+'>'+self.pointer.name+'\n')
			#etablissement des lien
			linkKnot_view.append(self,1)
			#augmentation de nobre des lignes dans la visu
			linkKnot_view.CountPlusOne()
		else: #sinon
			str_doc.append('<'+self.tag+line_attr+'>'+self.name)
			#etablissement des lien
			flague=not self.toend
			linkKnot_view.append(self,flague)
			if (self.toend):# element est terminal
				#ecriture du text entre balises
				self.getText_value(str_doc)
			else:#pas terminal
				str_doc.append('\n')
				#augmentation de nobre des lignes dans la visu
				linkKnot_view.CountPlusOne()
				if (self.opened): #si le noeud est ouvert pour la visu
					#parcour des enfants et applel recursif de cette fonction pour chaque enfant
					list_child=self.getchildren()
					for child in list_child:
						child.getText_knot(str_doc,linkKnot_view,depth+1)
		if(self.toend):
			str_doc.append('</'+self.tag+'>'+'\n')
			#augmentation de nobre des lignes dans la visu
			linkKnot_view.CountPlusOne()
		else:
			str_doc.append(line_tab+'</'+self.tag+'>'+'\n')
			#On ajoute l'element qui correspond a la tag fermeture
			linkKnot_view.append(self,0)
			#augmentation de nobre des lignes dans la visu
			linkKnot_view.CountPlusOne()


	def searchPosInText_knot(self,str_doc,linkKnot_view, search,depth):
		line_tab='\t\t'
		line_tab=line_tab.expandtabs(depth)

                #search.status=(self.parent, "before a child", self)
		search.status=(self, "before Tag Start", self)
		str_doc.append_searchRise(line_tab, search)

		if (self.opened and not(self.toend) and self.pointer==None): str_doc.append_searchRise('-', search)
		elif not(self.opened or self.toend or self.pointer!=None ):str_doc.append_searchRise('+', search)

		line_attr=''
		for key in self.keys():
			line_attr+=key+'="'+ self.attrib[key]+'" '
		if len(line_attr)!=0:

			line_attr=' '+line_attr
		if (self.pointer!=None):

                        search.status=(self, "Inside Tag Start", None)
			str_doc.append_searchRise('<'+self.tag+line_attr+'>'+self.pointer.name+'\n', search)
			linkKnot_view.append(self,1)
			linkKnot_view.CountPlusOne()
		else:
                        search.status=(self, "Inside Tag Start", None)
			str_doc.append_searchRise('<'+self.tag+line_attr+'>'+self.name, search)
			flague=not self.toend
			linkKnot_view.append(self, flague)
                        search.status=(self, "Start Of Body", None)
			if (self.toend):# element est terminal
				self.searchPosInText_value(str_doc, search)
			else:#pas terminal
				str_doc.append_searchRise('\n', search)
				linkKnot_view.CountPlusOne()
				if (self.opened):
					list_child=self.getchildren()
					for child in list_child:
						child.searchPosInText_knot(str_doc,linkKnot_view, search,depth+1)
		if(self.toend):
			str_doc.append_searchRise('</'+self.tag+'>'+'\n',search)
			linkKnot_view.CountPlusOne()
		else:
                        search.status=(self, "Inside Tag End", None)
			str_doc.append_searchRise(line_tab+'</'+self.tag+'>'+'\n',search)
			linkKnot_view.append(self,0)
			linkKnot_view.CountPlusOne()

	#la creation du string pour la copy special (seulement les definitions sans les tags de fermeture)
	def getTextForCopy_knot(self,str_doc,linkKnot_view,depth):
		if (self.pointer==None and (not self.isType('s'))):#????le tag <s>
			#les tabs pour decalage
			line_tab='\t\t'
			line_tab=line_tab.expandtabs(depth)
			str_doc.append(line_tab)
			# composition de la ligne des attrubus
			line_attr=''
			for key in self.keys():
				line_attr+=key+'="'+ self.attrib[key]+'" '
			if len(line_attr)!=0:

				line_attr=' '+line_attr
			str_doc.append('<'+self.tag+line_attr+'>'+self.name)
			#etablissement des lien
			linkKnot_view.append(self)
			if (self.toend):# element est terminal
				#ecriture du text entre balises
				#linkKnot_view.append(self.getchildren()[0])
				self.getText_value(str_doc)
				str_doc.append('\n')
				#augmentation de nobre des lignes dans la visu
				linkKnot_view.CountPlusOne()
			else:#pas terminal
				#linkKnot_view.append(self)
				str_doc.append('\n')
				#augmentation de nobre des lignes dans la visu
				linkKnot_view.CountPlusOne()
				#parcour des enfants et applel recursif de cette fonction pour chaque enfant
				list_child=self.getchildren()
				for child in list_child:
					child.getTextForCopy_knot(str_doc,linkKnot_view,depth+1)

	#permet de recuperer le value de childKnot a partir de son parent parentKnot
	def getValKnot(self,text,parentTag,childTag):
		#si element courant n'est pas encore le parent recherche on organise le parcours de ces sous-elements
		if (self.tag!=parentTag):
			children=self.getchildren()
			for child in children:
				child.getValKnot(text,parentTag,childTag)
		else: #on a trouve ce parent
			#on cherche l'enfant avec la tag childTag
			children=self.getchildren()
			i=0
			while(i< len(children) and children[i].tag!=childTag):
				i+=1
			if(i!=len(children)):
				if(children[i].toend):
					variable=children[i].getVariable()
					if(variable!=None):
						text.append(variable.value[0])


			

	def getText_value(self):
		pass


	#rend la reference sur le noeud d'arbre selon les indice index_list
	def getKnot(self,index_list):
		if (len (index_list)!=0):
			try:
				child=self.__getitem__(index_list[0])
				### del(index_list[0])
                                res=child.getKnot(index_list[1:])
			except:
				res=None
			return res
		else:
			return self
	#rend le copy d'une neoud
	def copyKnot(self, parent,dict_linkCopy=None,spetialInformation=None):
		#la creation du copy selon la tag
		if (self.isType('f')):
			object=Float_object(parent,self.tag,self.attrib)
			object.key=self.key
		elif(self.isType('s')):
			object=String_object(parent,self.tag,self.attrib)
		elif(self.isType('v')):
			object=Variable_object(parent)
			object.value.extend(self.value)
		else:
			object=General_object(parent,self.tag,self.attrib)
		#l'etablissement du lien avec le parent
		if (parent!=None):
			parent.append(object)
		#copy des proprietes
		object.toend=self.toend
		object.opened=self.opened
		if (self.pointer!=None): #le cas de reference
			object.pointer=self.pointer
		else: #le cas d'objet -definition
			#le cas de simple copy (pour <s> c'est toujours simple copy)
			if (spetialInformation==None or self.isType('s')):
				object.name=self.name
				#le parcour des enfants et appel recursif de cette fonction
				list_child=self.getchildren()
				for child in list_child:
					child.copyKnot(object)
			#le cas de copy special
			else:
				numberOfLine=dict_linkCopy.getKey(self)#a ajouter dans dictObjetct
				#on garde le name de cet objet et on cree la copy -reference
				if(  numberOfLine!=-1 and  spetialInformation[numberOfLine].isTrue()):
					newName=spetialInformation[numberOfLine].getObect()
					#si c'est <f> on prend les donnes de son enfant (<v>)
					if (self.toend):objectForCopy=self.getVariable()
					else:objectForCopy=self
					if(len(newName)!=0):
						objectForCopy.name=newName
					object.pointer=objectForCopy
				#le parcour des enfants et appel recursif de cette fonction
				else:
					list_child=self.getchildren()
					for child in list_child:
						child.copyKnot(object,dict_linkCopy,spetialInformation)

		return object

	#le parcour de la branche et verification que les noms sont unique pour tous l'arbre
	def checkNames(self,variables):
		if (self.pointer==None):# c'est la definition
			if(variables.index(self.name)!=-1):# le meme nom est deja existe
				self.name=''
		list_child=self.getchildren()
		for child in list_child:
			child.checkNames(variables)

	# la verification avant la supression d'une brache d'arbre
	def canBeDeleted(self,listReference,variables):
		#si c'est la reference
		if (self.pointer!=None):
			name=self.pointer.name
			#la diminuation du conteur dans la liste des definition dea variables
			#variables.delReference(tag,name)
			variables.delReference(name,self)
		else:#sinon
			 #si c'est la difinition
			if len(self.name)!=0 :
				#cas de la definition->nombre de reference
				name=self.name

				#on ajoute cette difinition dans la liste temporaire
				listReference.append(self)
			#on passe aux enfants
			list_child=self.getchildren()
			for child in list_child:
				child.canBeDeleted(listReference,variables)

	# la redefinition de variable
	def refreshVariables(self,problemObjects,doubleName_list,variables):
	 	if (self.pointer!=None): #si c'est le pointeur on verifie
			#tag=self.pointer.tag
			name=self.pointer.getName()
			#if (variables.getPointer(tag,name)==None): #si la definition est plus bas que la reference
			knotDef=variables.getPointer(name,self)
			if (knotDef==None): #si la definition est plus bas que la reference ou il n'existe pas
				problemObjects.append(self)

			else:
				self.pointer=knotDef # on assure la bonne reference
		else:#sinon
			if len(self.name)!=0 :
				#on verifie si ce nomm est deja existe
				indDef=variables.index(self.name)
				if(indDef!=-1):# le meme nom est deja existe
					#on ajoute dans la liste des objects avec les noms pas unique
					if(self.isVariable()):#soit <f> si c'est variable
						doubleName_list.append(self.parent)
					else: #soit le meme objet
						doubleName_list.append(self)
					fistDef=variables.definition(indDef)
					if(fistDef.isVariable()):#soit <f> si c'est variable
						doubleName_list.append(fistDef.parent)
					else: #soit le meme objet
						doubleName_list.append(fistDef)
				else: #on ajoute cette difinition dans variable
					variables.append(self)
			#on passe aux enfants
			list_child=self.getchildren()
			for child in list_child:
				child.refreshVariables(problemObjects,doubleName_list,variables)

	#ouvrir tous ancetres de l'objets problematique
	def showKnots(self):
		if(not self.parent.isType('system')):
			if(not self.parent.opened):
				self.parent.open_close()
			self.parent.showKnots()

	def findVariables_knot(self,listVar):
		list_child=self.getchildren()
		for child in list_child:
			child.findVariables_knot(listVar)

	# creation du dict des atrr possibles (on n'utilise pas)
	'''def creatPossibleAttr(self,possibleAttrVal,possibleAttrKey):
		attrVal=self.getAttrVal()
		attrKey=self.getAttrKey()
		if (len(attrVal)!=0):
			tag=self.parent.tag
			#possibleAttrVal.append(tag,attrVal)
			possibleAttrKey.append(self.tag,attrKey)
		list_child=self.getchildren()
		for child in list_child:
			child.creatPossibleAttr(possibleAttrVal,possibleAttrKey)'''

	#changement de la propriete opened dans le noeud
	def open_close(self):
		self.opened=not self.opened

	def openAll(self):
		self.opened=1
                for child in self.getchildren():
                     child.openAll()
	# renvoi l'indice du noeud dans la cliste des enfants
	def getIndex(self):
		parent=self.parent
		list_child=parent.getchildren()
		return list_child.index(self)
	# pour savoire si c'est variable
	def isVariable(self):
		return 0 # dans le cas general c'est pas le variable

	#pour savoir si c'est le commentaire
	def isComment(self):
		return 0

	# rendre les valeurs d'attr d'un objet
        def getAttrVal(self):
		attrVal=self.attrib.values()
		return attrVal

	def getAttrKey(self):
		attrKey=self.keys()
		return attrKey


	# definir la valeur d'attribut d'un objet
        def setKey(self,val,key):
		self.set(key,val)

	#supprime l'attribut key avec la valeur val de la liste des attribut
	def delKey(self,key):
		if self.attrib.has_key(key):
			del self.attrib[key]

	def getName(self):
		return self.name

	def setName(self,newName):
		self.name=newName

	def isType(self,typeKnot):
		return self.tag==typeKnot
	
	def Type(self):
		return self.tag




#************Class Variable_object
#l'object qui garde le contenue entre les tags
class Variable_object(General_object):
	def __init__(self,parent):
		General_object.__init__(self,parent,'v',{})
		self.value=[]

	# pour savoire si c'est variable
	def isVariable(self):
		return 1 # c'est le cas de variable

	def setValue(self,newVal,index=0):
		if (index>=len(self.value)):
			if (len(newVal)!=0):
				self.value.append(newVal)
		else:
			if(len(newVal)!=0):
				self.value[index]=newVal
			else:
				del self.value[index]

	def getName(self):
		return self.name

	def __len__(self):
		return len(self.value)

	'''def findVariables_knot(self,listVar):
		pass'''

#**************Classe Comment_Object
# l'objet qui garde les commenttaires
class Comment_object(General_object):
	def __init__(self,parent,tag='c',attr={},comment=''):
		General_object.__init__(self,parent,tag,attr)
		self.text=comment

	# spesification d'ecriture du texte dans le fishier
	def getXMLBlock_knot(self,str_doc,depth,forDef):
		if not(forDef):
			#les tabs pour decalage
			line_tab='\t\t'
			line_tab=line_tab.expandtabs(depth)
			str_doc.append(line_tab+'<!--'+self.text+'-->'+'\n')

	# spesification d'ecriture du texte pour l'affichage
	def getText_knot(self,str_doc,linkKnot_view,depth):
		#les tabs pour decalage
		line_tab='\t\t'
		line_tab=line_tab.expandtabs(depth)
		str_doc.append(line_tab)
		str_doc.append('<!--')
		n=self.text.count('\n')
		i=0
		start=0
		flague=0
		while(i<n):
			ind=self.text.find('\n',start)
			text=self.text[start:ind+1]
			start=ind+1
			str_doc.append(text)#
			#etablissement des lien
			linkKnot_view.append(self,flague)
			linkKnot_view.CountPlusOne()
			i+=1
		text=self.text[start:]
		str_doc.append(text)#
		#etablissement des lien
		linkKnot_view.append(self,flague)
		linkKnot_view.CountPlusOne()
		str_doc.append('-->'+'\n')

	# specification du copy d'une neoud
	def copyKnot(self, parent,dict_linkCopy=None,spetialInformation=None):
		object = Comment_object(parent,'c',{},self.text)
		#l'etablissement du lien avec le parent
		if (parent!=None):
			parent.append(object)
		return object


	def getTextForCopy_knot(self,str_doc,linkKnot_view,depth):
		pass

	def isComment(self):
		return 1

	def getComment(self):
		return self.text

	def setComment(self,newComment):
		self.text=newComment

	def checkNames(self,variables):
		pass

	def canBeDeleted(self,listReference,variables):
		pass

	# la redefinition de variable
	def refreshVariables(self,problemObjects,doubleName_list,variables):
		pass

	def findVariables_knot(self,listVar):
		pass


#**********Class  General_toendObject
# le type general pour les moeuds-"feilles" (<s> et <f>) qui unit leur fonctions similaire
class General_toendObject(General_object):
	def __init__(self,*args):
		apply(General_object.__init__,(self,)+ args)
		self.toend=1
		self.opened=1
		#_ElementInterface.__init__(self,name,attrs)
		#apply(_ElementInterface.__init__,(self,)+args)

	#on ajoute dans la str_doc le contenue d'objet <f> et de <s>
	def getText_value(self,str_doc):
		if (hasattr(self,"key")):str_doc.append(self.key+' ') # pour <f>
		if (self.pointer!=None):#c'est une reference
			str_doc.append(self.pointer.getName())
		else:# c'est un vraie objet
			child=self.getVariable() #getchildren()[0]
			str_doc.append(child.name+' ')
			for val in child.value:
				str_doc.append(val+' ')

	def getVariable(self):
		try: variable=self.getchildren()[0]
		except:variable=None
		return variable

	def addVariable(self):
		vo=Variable_object(self)
		self.append(vo)
		return  vo

	def getName(self):
		try: name=self.getVariable().name
		except: name=''
		return name

	def setName(self,newName):
		try:self.getVariable().name=newName
		except: pass

	# pour les elemenets "feiulles" il ne faut pas verifier
	def checkKnot(self):
		pass

#************Class Float_object
class Float_object(General_toendObject):
	def __init__(self,*args):
		#apply(General_object.__init__,(self,)+ args)
		apply(General_toendObject.__init__,(self,)+args)
		#self.toend=1 #???
		self.key='' #?????
		#self.opened=1 #pour visualiser dans la meme ligne

	def process_characters(self,ch):
        	chl = string.split(ch)
		#on calcule combien de valeures (float ou math expres) il y a dans le text
        	Nfloat=0
        	for ch in chl:
          		if(isFloat(ch) or isMathExp(ch) or (not isName(ch))):
	      			Nfloat=Nfloat+1

		# la creation de la variable
		vo=self.addVariable()
  	 	for token in chl: # parcour mot par mot du text entre balise
             		if ( isFloat(token) or isMathExp(token)  or (not isName(token))): #si le mot est  numerique
				#on ajoute un chiffre dans la loste des valeurs
				vo.value.append(token)
			else: # si c'est soi name soi attr
                		if(token[0]!="\""): #si ce n'est pas le cle
					if (Nfloat!=0): #le cas de la definition de variable
						vo.name=token
						#variables.append(vo.tag,vo)
						variables.append(vo)
					else:# le cas de reference sur une autre variable
						#recherche dans l'arbre
						res=variables.getPointer(token,self)
						if (res!=None): #on a trouve la reference
							self.pointer=res
						else:#pas trouve -> c'est la faute!!!
							self.pointer=self # on fait la reference sur lui meme
							vo.name=token # a verifier que ca marche !!!!!!
	       			else:#c'est le cle
					self.key=token
	'''
	#on ajoute dans la str_doc le contenue d'objet <f>
	def getText_value(self,str_doc):
		str_doc.append(self.key+' ')
		if (self.pointer!=None):#c'est une reference
			str_doc.append(self.pointer.name)
		else:# c'est un vraie objet
			child=self.getchildren()[0]
			str_doc.append(child.name+' ')
			for val in child.value:
				str_doc.append(val+' ')
	'''

	def searchPosInText_value(self,str_doc,search):
                search.status=(self,"On Key", None)
		str_doc.append_searchRise(self.key+' ', search)
		child=self.getVariable()
		if (child.pointer!=None):#la reference doit etre dans <f> mais pas dans v !!!!
                     search.status=(self,"Before Name", None)
                     str_doc.append_searchRise(' ', search)
                     search.status=(self,"On Name", None)
                     str_doc.append_searchRise(child.pointer.name,search)
		else:# c'est un vraie objet
                        search.status=(self,"On Values", None)
			str_doc.append_searchRise(child.name+' ', search)
			for val in child.value:
				str_doc.append_searchRise(val+' ',search)
		#str_doc+='\n'

	def getAttrVal(self):
		attrVal=''
		if (len(self.key)!=0):
			attrVal=str(self.key)[1:-1]
		#return attrVal
		return [attrVal]

	def getAttrKey(self):
		return ['key']

	def setKey(self,val,key):
		self.key='"'+ val+ '"'

	def findVariables_knot(self,listVar):
		if (self.pointer==None):
			var=self.getVariable()
			if (var!=None and len(var)==3):
				listVar.append(self)


#************Class String_object
class String_object (General_toendObject):
	def __init__(self,*args):
		#apply(General_object.__init__,(self,)+args)
		apply(General_toendObject.__init__,(self,)+args)
		#self.toend=1 #???
		#self.opened=1 #pour visualiser dans la meme ligne

	def process_characters(self,ch):
		vo=self.addVariable()
		line=''
		for c in ch:
          		if( (c not   in " \n\t") ):
             			line=line+c
		vo.value.append(line)

	def findVariables_knot(self,listVar):
		pass
	'''
	def getText_value(self,str_doc):
		child=self.getchildren()[0]
		if (len(child.value)!=0):
			text=child.value[0]
			str_doc.append(text)
	'''


from PyQt4.Qt  import *
from PyQt4.QtGui  import *
from PyQt4.QtCore  import *

#************* class PPMHandler
# class Tree(ContentHandler,LexicalHandler,QObject):
class Tree(ContentHandler,QObject):
    classes_to_use = {"f": Float_object, "s": String_object, "layer":General_object,
    			"bff":General_object,"stack":General_object, 							 			"scan":General_object,"scanlist":General_object,
			"ift":General_object, "ifo":General_object,
			"dv":General_object,"KK":General_object,"KKFFT":General_object,"bff":General_object,
			"bjoin":General_object, "iff":General_object,
			"KK_write":General_object, "tf0":General_object, "tf12":General_object,
			"Beta_write":General_object,"comparison_write":General_object,
			"fit":General_object, "Minimise":General_object ,
			"MinimiseGeFit":General_object , "prova": General_object,
                        "bfc":General_object , "bfl":General_object ,
			"bsum":General_object,"bfi":General_object ,
                        "ifos":General_object, "Write_Stack": General_object,
                        "arrofvar":General_object,"addvars": General_object,
                        "bfinterp":General_object,"setLayerInterpolation":General_object,"MagScatterer":General_object,
			"c":Comment_object}

#----------------------------operation pour initialisation d'arbre--------------------------------
    def __init__(self,*args):
    	apply(QObject.__init__,(self,)+args)
	# le list des des objets dans l'ordre de leur apparition pour le parseur
        self.processors_list=[]
	#la reference sur le root d'arbre
	self.root=None
	# l'objet qui stoque tous les definitions avec les erf sur eux
	self.variables=Variable_reference()
	#le lien entre l'arbre et les lignes la visualisation
	self.dict_link=DictObject()
	#c'est la liste des definition pour la branche de noeud, construit dans Delet
	self.listDefOfBranche={}
	#liste des objets "avec les problemes"
	self.problemObjects_list=[]
	#liste des numero de ligne correspondante
	self.problemLink_list=[]
	#liste des objets avec les noms pas uniques (les definitions)
	self.doubleName_list=[]
	#liste des numero de ligne correspondante
	self.doubleNameLink_list=[]
	#le dict pour l'oparation copy special
	self.dict_linkCopy=DictObject()
	#name de fichier ou on sauvgarde le resultat
 	self.fileName=''
	#le titre pour la presentation de tree
	self.title='Untitled'
	#la propriete qui indique si l'arbre a ete modifie
	self.modified=0
	#la propriete qui contient la structure avant le changement (pour avoir la possibilite de revenir )
	self.oldText=ListObject()
	# la propriete qui contient la liste de touts les noms de variables
	self.listVariables=[]
	#propriete pour garde le flague du recherche
	self.all=''
	#la propriete qui dit si F3 appele pour la 1ere fois
	self.first=1
	#la propriete qui indique si l'initialisation est premiere
	#self.firstInitialisation=1


    #initailisation d'arbre soit par le fichier xml, soit par l'objet string
    def initialise(self,source):
    	global variables
	variables=self.variables
    	# Create a parser
    	parser = make_parser()
	# Tell the parser we are not interested in XML namespaces
	parser.setFeature(feature_namespaces, 0)
	# Tell the parser to use our handler
    	parser.setContentHandler(self)
	import xml.sax.handler
	# parser.setProperty(xml.sax.handler.property_lexical_handler,self)

    	#la verification du type d'argument
	if (source.__class__.__name__=='str'):
		import xml.sax
		xml.sax.parseString(source, self)
	elif(source.__class__.__name__=='file'):
		parser.parse(source)
	else:
		raise 'le type de source n"est pas correct'

    def getFileName(self):
	return self.fileName

    def setFileName(self,newFileName):
	self.fileName=newFileName
	self.title=os.path.basename(self.fileName)

    def getOldText(self):
    	return str(self.oldText)
#----------------------operation pour parser----------------------------------------------------
    def startElement(self, name, attrs):
        if name=="system": #return
		self.root=General_object(None,name,attrs)
		self.processors_list.append(self.root)
		return
        parent=None
	parent=self.processors_list[-1]
	#le remplissage de attrs par un simple dict
	attrs=replaceByDict(attrs)
        knot = self.classes_to_use[name](parent,name,attrs)
        #nuovo.process_startElement(name,attrs)
        parent.append(knot)
	self.processors_list.append(knot)

    def endElement(self, name):
    	knot=self.processors_list[-1]
	if (knot!=self.root ):
		knot.checkKnot()
	del self.processors_list[-1]

    def characters(self, ch):
        if self.processors_list==[]: return
	self.processors_list[-1].process_characters(ch)

    def comment(self, comments):
    	parent=self.processors_list[-1]
	knot = Comment_object(parent,'c',{},comments)
	parent.append(knot)

#------------------------operations pour  manipuler d'arbre------------------------------
    # pour afficher dans le View
    def getText(self):
	text=StrObject()
	self.dict_link.clear()
	depth=0
	self.root.getText_knot(text,self.dict_link,depth)
	return text.getString()

    def whereAmI(self,para,col):
        search=Search()
        search.para=para
        search.col=col
        res=(None, "Not Found",None)
	depth=0
        try:
             self.root.searchPosInText_knot(StrObject(),DictObject(), search,depth)
        except:
             if search.found:
                  res=search.status
             else:
                  import traceback
		  import sys
                  traceback.print_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)
        return res
   
    #-------------getTexte pout les unittests-----------
    def unitTest_getText(self, knot):
	text=StrObject()
	self.dict_link.clear()
	depth=0
        knot.getText_knot(text,self.dict_link,depth)
	return text.getString()
    #------------------fin -------------------------------------
    #recupere le texte pour copy speciel
    def getTextForCopy(self,knot):
    	self.dict_linkCopy.clear()
	text=StrObject()
	depth=0
	knot.getTextForCopy_knot(text,self.dict_linkCopy,depth)
	return text.getString()

    def getKnot(self,index_list):
    	 return self.root.getKnot(index_list)

    def copy(self,knot,spetialInformation=None):
    	if (spetialInformation==None): #c'est une simple copy
		copyKnot=knot.copyKnot(None)
	else:#c'est une copy special
		# il faut memoriser la structure car copy special peut ajouter les noms
		self.memoriseOldText()
		copyKnot=knot.copyKnot(None,self.dict_linkCopy,spetialInformation)
    	return copyKnot

    # verifie pour le noeud si ses noms sont uniques
    def checkNames(self,knot):
	knot.checkNames(self.variables)

    # !!!! probleme du calcul de l'indice
    def insert(self,index,parent,knot):
	parent.insert(index,knot)
	knot.parent=parent
	self.modified=1
	if (self.title[0]!='*'):
		self.title='*'+self.title
		self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))
	self.refreshVariables()

    #la supression du deoud
    def deleteKnot(self,knot):
       	parent=knot.parent
	index=knot.getIndex()
	parent.__delitem__(index)
	self.modified=1
	if (self.title[0]!='*'):
		self.title='*'+self.title
		self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))

    #on verifie si on peut supprimer cette branche, si oui, on le supprime
    def delete(self,knot):
    	res=0
	#listDefOfBranche=ListObject()
	#self.listDefOfBranche.clear()
	self.listDefOfBranche=[]
    	if (knot!=self.root):# on ne peut pas supprimer la root
    		##on verifie si on peut suprimer cette branch
		knot.canBeDeleted(self.listDefOfBranche,self.variables)
		#si il n'y a pas des definitions ou toutes les references sont aussi dans cette branche
		#if (self.listDefOfBranche.length()==0 or self.listDefOfBranche.internRef(variables)):
		if (len(self.listDefOfBranche)==0 or self.variables.internRef(self.listDefOfBranche)):
			#on memorise la structure avant le changement
			self.memoriseOldText()
			#on supprime cette neoud
			self.deleteKnot(knot)
			res=1
		self.refreshVariables()
	return res #noReference.getBoolValue()

    #chnagement des ref. pour les defintions dans self.listDefOfBranche sur newDef (ou sur eux -meme dans le cas None)
    def updateRef(self,newDef=None):
    	#on parcoure tout les definitions de la liste listOfDef
	for defObject in self.listDefOfBranche:
		nameDef=defObject.getName()
		ind=self.variables.index(nameDef)
		if(ind!=-1):
			# pour chaque definition on parcoure la liste de references sur elle
			listRef=self.variables.reference(ind)
			#on change le noeud de reference pour chanque cette reference
			for ref in listRef:
				if(newDef==None):
					ref.pointer=ref
				else :
					ref.pointer=newDef

    # changement des ref. pour les objets problematiques
    def updateRefForProblemObj(self):
	for obj in self.problemObjects_list:
		if (obj.pointer!=None):
			nameDef=obj.pointer.getName()
			knotDef=self.variables.getPointer(nameDef,obj)
			if(knotDef!=None):
				obj.pointer=knotDef
				obj.setName('')



    #on parcour l'arbre et redefnie variables
    def refreshVariables(self):
	self.variables.clearAll()
	#la list des object problematiques
	self.problemObjects_list=[]
	#la liste des lignes corrspondante
	self.problemLink_list=[] # ???????????????/
	#la liste des definitions avec les noms pas uniques
	self.doubleName_list=[]
	#la liste des lignes corrspondante
	self.doubleNameLink_list=[]
	self.root.refreshVariables(self.problemObjects_list,self.doubleName_list,self.variables)
	self.updateRefForProblemObj()

    # construction de la liste avec des numero de lignes pour les objets "probleme" et double noms
    def formProblemLink(self):
	#construction de la liste avec des numero de lignes pour les objets "probleme"
        self.problemLink_list=[]
    	for object in self.problemObjects_list :
		try:
			#on cherche le numero des lignes pour objets 'problematiques'
			key=self.dict_link.getKey(object)
			self.problemLink_list.append(key)
		except:
			pass #le noeud est ferme
	# construction de la liste avec des numero de lignes pour les objets avec double noms
	self.doubleNameLink_list=[]
    	for object in self.doubleName_list :
		try:
			#on cherche le numero des lignes pour objets 'problematiques'
			key=self.dict_link.getKey(object)
			self.doubleNameLink_list.append(key)
		except:
			pass #le noeud est ferme

    #ouvrir des objets avec probleme dans l'arbre
    def showProblemKnots(self):
   	for object in self.problemObjects_list :
		object.showKnots()
	for object in self.doubleName_list :
		object.showKnots()

    def isCorrect(self):
    	correct=1
	if (len(self.problemObjects_list)>0 or len(self.doubleName_list)>0):
		correct=0
	return correct

    def getXMLBlock(self,knot,forDef=0):
	text=StrObject()
	depth=0
	if (knot.pointer!=None): # reference
		#on regarde sur quoi on referance
		if (knot.pointer.isVariable()): # sur le variable
			knot.pointer.parent.getXMLBlock_knot(text,depth,forDef)# c'est ref dans f il faut
		else: # autre cas
			knot.pointer.getXMLBlock_knot(text,depth,forDef)# c'est ref dans f il faut
	else:knot.getXMLBlock_knot(text,depth,forDef) # definition
	return text.getString()


     # cree un nouveau noeud a partir de l'aide
    def creatNewKnot(self,tag,nameAttr,attrKey=['key']):
     	knot=self.classes_to_use[tag](None,tag,{})
	i=0
	while(i<len(nameAttr)):
		#if (len(nameAttr)!=0):
		if (len(nameAttr[i])!=0):
			#knot.setKey(nameAttr,attrKey)
			knot.setKey(nameAttr[i],attrKey[i])
		i+=1
	if (knot.toend):# il faut creer l'objet 'v'
		knot.addVariable()
		if cDefaultValues.has_key(nameAttr[0]):
			varaible=knot.getVariable()
			varaible.setValue(cDefaultValues[nameAttr[0]])

	return knot

    # supprime touts les enfants du knot donne comme le parametre
    def deleteChildren(self, knot):
    	start=0#knot.getchildren()[0]
	stop=knot.__len__()#knot.getchildren()[knot.__len__()-1]
    	knot.__delslice__(start,stop)
	self.modified=1
	if (self.title[0]!='*'):
		self.title='*'+self.title
		self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))

    def isModified(self):
    	return self.modified

    def setModified(self):
	self.modified=0
	if (self.title[0]=='*'):
		self.title=self.title[1:]
		self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))

    def isValidated(self):
    	return ''

    #verifie si les enfants de ce noeud possedes les attribues
    def knotHasKey(self,knot):
    	res=0
	if (cPossibleAttrVal.has_key(knot.tag)):
		res=1
	return res

    def memoriseOldText(self):
    	#on memorise la structure avant le changement
	self.oldText.append(pickle.dumps(self.root))
	if (self.oldText.length()==1): # le premier changement
		status=1
		self.emit(PYSIGNAL('sigEnabledUndo'),(status,))

    def getValKnot(self,parentTag,childTag):
    	text=StrObject()
    	self.root.getValKnot(text,parentTag,childTag)
	return text.getString()

    #donne l'information pour l'element problematique qu'est-ce qu'il y a :
    #return 1 si l'element-reference a perdu le lien avec l'element-definition
    #return 0 si on a place l'element-reference plus haut que son l'element-definition correspondant
    def defLost(self,knot):
    	#on recupere l'objet sur lequel cet element reference
    	defKnot=knot.pointer
	if (defKnot==knot): # si cet element a perdu son l'element-definition
		return 1
	else:# on a place l'element-reference plus haut que son l'element-definition correspondant
		return 0

    def Undo(self):
    	oldText=self.oldText.getOldText()
	if (len(oldText)!=0):
		self.root=pickle.loads(oldText)
		self.refreshVariables()
		self.oldText.delThisText()
		if (self.oldText.length()==0): # le premier changement
			status=0
			self.emit(PYSIGNAL('sigEnabledUndo'),(status,))

    def findVariables(self):
    	self.root.findVariables_knot(self.listVariables)



#-----SLOTS---------------------------------------------------------
    def slotInitialise(self,source):

	#verifier s'il faut reinitialiser self.processors_list
	'''
	self.variables=Variable_reference()
	self.dict_link=DictObject()
	self.listDefOfBranche={}
	self.problemObjects_list=[]
	self.problemLink_list=[]
	self.doubleName_list=[]
	self.doubleNameLink_list=[]
	self.dict_linkCopy=DictObject()'''

    	self.initialise(source)
	self.root.open_close()

	self.problemLink_list=[]
	#pour afficher touts les fautes du fichier initial
	self.refreshVariables()
	#self.updateRefForProblemObj()
	self.showProblemKnots() #on ouvre les elements problematiques
	textNew=self.getText()
	self.formProblemLink() #ceration de la liste des lignes correspondantes
	#self.firstInitialisation=0


    	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

 #pour creer arbre ???? le probleme avec le dictionnaire des enfants et des cles possible
    def slotNew(self):
    	#variables.clearAll()
	#self.variables=Variable_reference()
	#self.fileName=''
	#self.title='Untitled'
	#self.problemObjects_list=[]
	#self.problemLink_list=[]
    	self.root=General_object(None,'system',{})
	self.root.open_close()
	textNew=self.getText()
	#self.firstInitialisation=0

    	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

    def slotGetStrForSave(self,isSaveAs):
    	forDef=0
	strForSave=self.getXMLBlock(self.root,forDef)
	self.emit(PYSIGNAL('sigSave'),(strForSave,isSaveAs,))

    def slotOpenClose(self,knot):
    	knot.open_close()
	#ajouter ouvrir des objets avec probleme
	self.showProblemKnots()
	textNew=self.getText()
	self.formProblemLink()

	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

    def slotDelete(self,knot):
    	if (knot==self.root):
		strWarning='The element <system> can not be deleted'
		self.emit(PYSIGNAL('sigMessageWarning'),(strWarning,))
	else: # pour les autres noeuds
		#on memorise la structure avant le changement
		#self.memoriseOldText()
    		if (not self.delete(knot)):
			strQuestion='This element has definitions wich are referented!\n Are you shure to delete it ?'
			self.emit(PYSIGNAL('sigMessageQuestion'),(strQuestion,self.slotDeleteKnot,knot,))
			#self.emit(PYSIGNAL('sigMessageQuestion'),(knot,strQuestion,self.slotDeleteKnot,))

		#ajouter ouvrir des objets avec probleme
		self.showProblemKnots()
		textNew=self.getText()
		self.formProblemLink()
		self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

    #supprime le noeud  meme dans le cas des definitiions apres al reponse positive de l'utilisateur
    def slotDeleteKnot(self,knot):
    	#on memorise la structure avant le changement
	self.memoriseOldText()
    	#avant supprimer tout les enfants il faut changer les references sur les definitions dans cette branche
	self.updateRef()
	#variables.updateRef(self.listDefOfBranche)
    	self.deleteKnot(knot)
	self.refreshVariables()

    #on a sauvgarde la copy dans le buffer
    def slotCopy(self,knot,spetialInf=None):
    	if (knot!=self.root):#on ne fait pas la copy de system
		copyKnot=self.copy(knot,spetialInf)
		if (spetialInf!=None):
			self.refreshVariables()
			self.showProblemKnots()
			textNew=self.getText()
			self.formProblemLink()
			self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))
		self.emit(PYSIGNAL('sigFillBuffer'),(copyKnot,))

    def slotInsert(self,knot,parentKnot=None,afterKnot=None):
	if (knot!=None):
		copyBuffer=self.copy(knot)
		self.checkNames(copyBuffer) # pour regarder si dans le doc il y deja les meme noms

    		if (parentKnot!=None ): #on ajoute l'enfant
			if(parentKnot.pointer==None):
				#on memorise la structure avant le changement
				self.memoriseOldText()
				index=0
				self.insert(index,parentKnot,copyBuffer)
				if (not parentKnot.opened):
					parentKnot.open_close()
		else: #on ajoute frere
			# Verifier que c'est oas system !!!!
			if (afterKnot==self.root):
				strWarning='The element can not be inserted like the sibling of <system> !!! '
				self.emit(PYSIGNAL('sigMessageWarning'),(strWarning,))
			else:
				#on memorise la structure avant le changement
				self.memoriseOldText()
				parent=afterKnot.parent
				index=afterKnot.getIndex()+1
				self.insert(index,parent,copyBuffer)
		self.showProblemKnots() #on ouvre les elements problematiques
		textNew=self.getText()
		self.formProblemLink() #ceration de la liste des lignes correspondantes
		self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

    #insertion d'element dans l'rdre concret (pour les enfants avec les attribut,Children)
    def slotInsertOrder(self,knot,parent,ind):
    	#on recupere la liste des valeurs des attributs possibles
	allAttr=[] #la liste de tout les valeurs des attributs possibles
	existAttr=[]
	existChild=[] #le dict des valeur existes avec les reference sur les enfants correspondant
	exist=0
	if(cPossibleAttrVal.has_key(parent.tag)):
    		allAttr=cPossibleAttrVal[parent.tag]
	#la creation de la liste des valeurs des attributs que deja existe pour ce parent
	if (len(allAttr)!=0):
		children=parent.getchildren()
		for child in children:
			val=child.getAttrVal()
			if(len(val)!=0):
				existAttr+=val
				existChild.append(child)
		lenAttr=len(existAttr)
		if (lenAttr!=0):
			#la recherche de la place pour le nouveau element dans la liste des enfants qui deja existent
			i=0
			while (i<lenAttr and ind>allAttr.index(existAttr[i])):
				i+=1
			if (i!=lenAttr and ind==allAttr.index(existAttr[i])):
				#cet element deja existe-> on n'ajoute plus
				exist=1
			else:
				#ind=i
				#il faut chercher le vrai indice dans la list children
				if(i==lenAttr):
					ind=len(children)
				else:
					child=existChild[i]
					ind=children.index(child)
		else:#c'est le premier element avec l'attribut
			ind=len(children) #on l'ajoute apres tout les elements sans les attributs
	if (exist):
		#cet element deja existe-> on n'ajoute plus
		pass
	else:
		copyBuffer=self.copy(knot)
		self.insert(ind,parent,copyBuffer)
		if (not parent.opened):
			parent.open_close()
		self.showProblemKnots() #on ouvre les elements problematiques
		textNew=self.getText()
		self.formProblemLink() #ceration de la liste des lignes correspondantes
		self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))



    #ce slot cree le string pour le copy special
    def slotCopySpecial(self,knot):
    	if (knot!=self.root):#on ne fait pas la copy special de system
		textNew=self.getTextForCopy(knot)
		self.emit(PYSIGNAL('sigShowWindowCopy'),(textNew,))# a conecter dans App

   #verifi si pour pour le noeud il faut garder le nom ou l'ajouter (dans la feneter de copy special)
    def slotHasName(self,knot,nLine):
	noName=0
	nameVer=knot.getName()
    	if (len(nameVer)==0):
		noName=1
	self.emit(PYSIGNAL('sigWriteName'),(noName,nLine,))# a conecter dans App

    #pour passer a la ligne de la definition
    def slotFindDef(self,knot):
    	if(knot.pointer!=None):
		#on prend la definition selon le type de l'objet
		if(knot.pointer.isVariable()):#si c'est varaible, on prend <f>
			defObject=knot.pointer.parent
		else:#sinon on prend la vrai definition
			defObject=knot.pointer
		#on le fait visible pour le view
		defObject.showKnots()
		textNew=self.getText()
		self.formProblemLink()

		self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))
		#on recupere la ligne de la definition
		line=self.dict_link.getKey(defObject)
		if(line!=-1):
			self.emit(PYSIGNAL('sigGoToDef'),(line,))
		else:
			strWarning='The definition is not in this document'
			self.emit(PYSIGNAL('sigMessageWarning'),(strWarning,))




     #on recupere la liste des noeud a jouter pour la feneitre Elements (c'est la list dans le dict possibleChild)
    def slotGetHelp(self,childKnots=None,siblingKnots=None,genericCase=1):
	listElements=[]
	listInfo=[]
	#tag=''
	if (childKnots!=None):
		#tag=childKnots.tag
		parent=childKnots #pour le help
	#elif(siblingKnots!=self.root):
	else:
		#tag=siblingKnots.parent.tag
		parent=siblingKnots.parent #pour le help
	if (parent!=None and cPossibleChild.has_key(parent.tag)):
		#on prend tout les types soit des enfants soit des sibling
		listElements=cPossibleChild[parent.tag]['generic']
		if not (genericCase):  #on prend les types qui correspondent au valeur d'attribut de siblingKnots
			if(siblingKnots!=None):
				listValAttr=siblingKnots.getAttrVal()
				if(len(listValAttr)!=0):
					valAttr=listValAttr[0]
					dictType=cPossibleChild[parent.tag]
					if (dictType.has_key(valAttr)):
						listElements=cPossibleChild[parent.tag][valAttr]
	# recuperer la liste des info pour chaque tag
	for element in listElements :
		help=''
		if(cGeneralHelp.has_key(element)):
			#on defeni tout de suit par le help le plus general
			help=cGeneralHelp[element]
			#on essaye de trouver le help comme pour l'enfant
			if (cWhatLikeChildren.has_key(parent.tag)):
				helpChild=cWhatLikeChildren[parent.tag]
				if (helpChild.has_key(element)):
					help=helpChild[element]
		listInfo.append(help)
	self.emit(PYSIGNAL('sigShowHelp'),(listElements,listInfo,))# a conecter dans App

    #on cree le dict des valeurs des attributs avec les types des elements par defauts
    def slotGetChildren(self,knot):
	listTypePossible=[]
	listAttrVal=[]
	#on cherche tou les valeur des attributs
    	if (knot!=self.root and knot.pointer==None):
		if (cPossibleAttrVal.has_key(knot.tag)):
			listAttrVal=cPossibleAttrVal[knot.tag]
		#on defeni le type d'element par defaut pour chaque valeur d'attribut
		if(cPossibleChild.has_key(knot.tag)):
			dictTypePossible=cPossibleChild[knot.tag]
			for attrVal in listAttrVal:
				listType=[]
				#si on a defini le type par defaut dans le dict
				if dictTypePossible.has_key(attrVal):
					listType=dictTypePossible[attrVal]
				#on n'a pas precise
				else:
					#on le calcul
					#on recupere la liste des types possible des enfants pour ce noeud
					listAllType=dictTypePossible['generic']
					if ('f' in listAllType):
						listType.append('f')
						for tp in listAllType:
							if (tp!='f'):
								listType.append(tp)
					else:
						listType=listAllType
				listTypePossible.append(listType)
	#self.emit(PYSIGNAL('sigShowChildren'),(listAttrVal,))# a conecter dans App
	self.emit(PYSIGNAL('sigShowChildren'),(listAttrVal,listTypePossible,))

    #creation du dict avec lea propritete du noeud pour remplir le fenetre Midification
    def slotGetEdit(self, knot):
    	hasValues=0
	listAttrVal=[]
    	dictEdit={}
	dictEdit['tag']=knot.tag

	#on donne comme les attributs tout les valeurs avec le cle pas 'key'
	attrKey=knot.getAttrKey()
	attrVal=knot.getAttrVal()
	visuKey=[]
	visuVal=[]
	i=0
	#remplissage par defaut des attributs pour l'element <stack>
	if(knot.isType('stack')):
		visuKey.append('nstack')
		if(attrKey.count('nstack')==0):
			visuVal.append(0)
		else:
			ind=attrKey.index('nstack')
			visuVal.append(attrVal[ind])
		visuKey.append('repetitions')
		if(attrKey.count('repetitions')==0):
			visuVal.append(1)
		else:
			ind=attrKey.index('repetitions')
			visuVal.append(attrVal[ind])
		dictEdit['valDefault']={'repetitions':'1','nstack':'0'}
		
	if(knot.isType('scan')):
		visuKey.append('nstack')
		if(attrKey.count('nstack')==0):
			visuVal.append(0)
		else:
			ind=attrKey.index('nstack')
			visuVal.append(attrVal[ind])
		dictEdit['valDefault']={'nstack':'0'}
	
	dictEdit['attrKey']=visuKey #'key'
	dictEdit['attrValue']=visuVal#

	if (knot.pointer!=None):
		dictEdit['pointer']=1
		dictEdit['name']=knot.pointer.getName()
		if (knot.pointer.isVariable()): #il faut recuperer les valeures
			hasValues=1
			knotDef=knot.pointer
	else:
		dictEdit['pointer']=0
		dictEdit['name']=knot.getName()
		if( knot.toend ): #il faut remplir les valeures
			knotDef=knot.getVariable()
			hasValues=1
	if (hasValues):
		valueList=[]
		for val in knotDef.value:
			valueList.append(val)
		if(knot.isType('s')):
			strKey='valueS'
		else:
			strKey='value'
		dictEdit[strKey]=valueList
	self.emit(PYSIGNAL('sigShowEdit'),(dictEdit,))

     # changement du type du noeud
    def slotChangeType(self,knot,newType):
    	listOfSiblings=[]
    	if (knot!=self.root):
    		parent=knot.parent
		if (cPossibleChild.has_key(parent.tag)):
			listOfSiblings=cPossibleChild[parent.tag]['generic']
			listValAttr=knot.getAttrVal()
			if(len(listValAttr)!=0):
				valAttr=listValAttr[0]
				dictType=cPossibleChild[parent.tag]
				if (dictType.has_key(valAttr)):
					listOfSiblings=cPossibleChild[parent.tag][valAttr]
		#on verifi si le tupe et bon


		
		if (newType in listOfSiblings):
			#il n' y a pas des enfants
			if (knot.__len__()==0 or knot.toend):
				self.slotReplaceKnot(knot,newType)
			else: #il y a des enfants
				strQuestion='This change deletes all subelements\n Are you shure to continue ?'
				self.emit(PYSIGNAL('sigMessageQuestion'),(strQuestion,self.slotReplaceKnot,knot,newType))
		#si le type n'est pas bon on donne le message d'erreur et on ne change pas le tupe
		else:
			strError='It is not the good type '
			self.emit(PYSIGNAL('sigMessageError'),(strError,))
			self.slotGetEdit(knot)
	else:
		self.slotGetEdit(knot)

    def slotReplaceKnot(self,knot,newType):
    	#on memorise la structure avant le changement
	self.memoriseOldText()
	# creer l'objet du nouveau type et remplacer ancien noeud
	attrVal=knot.getAttrVal()
	attrKey=knot.getAttrKey()
	newKnot=self.creatNewKnot(newType,attrVal,attrKey)# on a cree le nouveau noeud
	#on recupere les proprietes necessaires
	newKnot.pointer=knot.pointer
	newKnot.setName(knot.getName())

	self.listDefOfBranche=[]
    	#on verifie si on peut suprimer cette branch en construisant self.listDefOfBranche
	knot.canBeDeleted(self.listDefOfBranche,self.variables)
	self.updateRef()

	ind=knot.getIndex() # on garde l'indice
	self.deleteKnot(knot) #on a supprime ancien noeud
	parent=knot.parent
	self.insert(ind,parent,newKnot) # on a insere le nouveau

	self.showProblemKnots() #on ouvre les elements problematiques
	textNew=self.getText()
	self.formProblemLink() #ceration de la liste des lignes correspondantes
	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))


     # procedure evantuelle de la suppretion des enfants du noeud pour le changement de champ Pointer dans Modification
    def slotFillPointer(self, knot):
    	if (knot!=self.root):
		if (knot.toend):# si c'est le noeud "feille" il ne faut pas demander
			if(knot.isType('f')):
				self.slotDeleteChildren(knot)
		elif (knot.__len__()!=0):
			strQuestion='This change deletes all subelements\n Are you shure to continue ?'
			self.emit(PYSIGNAL('sigMessageQuestion'),(strQuestion,self.slotDeleteChildren,knot,))
			#self.emit(PYSIGNAL('sigMessageQuestion'),(knot,strQuestion,self.slotDeleteChildren,))
		else:
			#on memorise la structure avant le changement
			self.memoriseOldText()
			knot.pointer=knot
			knot.setName('')
			self.modified=1
			if (self.title[0]!='*'):
				self.title='*'+self.title
				self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))
		self.refreshVariables()
		self.showProblemKnots() #on ouvre les elements problematiques
		textNew=self.getText()
		self.formProblemLink() #ceration de la liste des lignes correspondantes
		self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))
	else:
		self.slotGetEdit(knot)

    #uppretion des enfants du noeud pour le changement de champ Pointer en True dans Modification
    def slotDeleteChildren(self, knot):
    	#on memorise la structure avant le changement
	self.memoriseOldText()

	self.listDefOfBranche=[]
    	#on verifie si on peut suprimer cette branch en construisant self.listDefOfBranche
	knot.canBeDeleted(self.listDefOfBranche,self.variables)
	self.updateRef()
    	knot.pointer=knot
	knot.setName('')
	self.deleteChildren(knot)

    # le changement de champ Pointer en False dans Modification
    def slotClearPonter(self,knot):
    	#on memorise la structure avant le changement
	self.memoriseOldText()
	knot.pointer=None
	if (knot.toend):# il faut creer l'objet 'v'
		knot.addVariable()
		#variable=Variable_object(knot)
		#knot.append(variable)
	self.modified=1
	if (self.title[0]!='*'):
		self.title='*'+self.title
		self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))
	self.refreshVariables()
	self.showProblemKnots() #on ouvre les elements problematiques
	textNew=self.getText()
	self.formProblemLink() #ceration de la liste des lignes correspondantes
	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

    #Le changement de champ Name dans Modification pour le pointer
    def slotChangeName(self,knot,textName):
	if (knot!=self.root):

		if(knot.pointer!=None): #cas de pointer => recherche de la def corespondate
			#resRef=variables.getPointer(knot.tag,textName)
			resRef=self.variables.getPointer(textName,self)
			if(resRef!=None):
				#on memorise la structure avant le changement
				self.memoriseOldText()# on memorise toujours
				knot.pointer=resRef
			else:
				if (knot.pointer!=knot):self.memoriseOldText()# on memorise juste la premiere fois
				knot.pointer=knot
		else: # cas de def => changement du nom
			#on memorise la structure avant le changement
			self.memoriseOldText()#on memorise toujours
			knot.setName(textName)
		self.modified=1
		if (self.title[0]!='*'):
			self.title='*'+self.title
			self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))
		self.refreshVariables()
		self.showProblemKnots() #on ouvre les elements problematiques
		textNew=self.getText()
		self.formProblemLink() #ceration de la liste des lignes correspondantes
		self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))
	else:
		self.slotGetEdit(knot)

     # changemanet de des valeurs des attributs dans le cas particuliers (<stack>)
    def slotChangeAttrValue(self,knot,val,key,valDefaut):
    	#on memorise la structure avant le changement
	self.memoriseOldText()
    	if(val!=valDefaut):
		knot.setKey(val,key)
	else:
		knot.delKey(key)
	self.modified=1
	if (self.title[0]!='*'):
		self.title='*'+self.title
		self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))
	# verifier qu'il ne faut pas faire d'autres operations !!!!!11
	textNew=self.getText()
	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

     # changemanet de la valeur du <f> ou de <s>
    def slotChangeValue(self, knot,newVal,index):

    	#on memorise la structure avant le changement
	self.memoriseOldText()
	if (knot.pointer!=None):
		objet=knot.pointer
	else:
		if hasattr(knot,"getVariable"):
			objet=knot.getVariable()
		else:
			return
	objet.setValue(newVal,index)
	self.modified=1
	if (self.title[0]!='*'):
		self.title='*'+self.title
		self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))
	# verifier qu'il ne faut pas faire d'autres operations !!!!!11
	textNew=self.getText()
	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

#on cree le string pour la fenetre Definition
    def slotGetDef(self,knot):
    	forDef=1
    	textDef=self.getXMLBlock(knot,forDef)

	self.emit(PYSIGNAL('sigShowDef'),(textDef,))# a conecter dans App

    def slotGiveWhatsThis(self,knot):
    	text=''

	# on verifie les valeurs des attribues
	attrVal=knot.getAttrVal()
	for val in attrVal:
		parenttag = knot.parent.tag
		# if parenttag=="KKFFT":
		# 	parenttag="KK"
		try:
			parenttag = tag_translation_4_WhatsThisAttr[parenttag]
		except:
			pass

		valForParent=val+'_'+parenttag
		if(cWhatsThisAttr.has_key(valForParent)):
			text+=cWhatsThisAttr[valForParent]+'\n'
		#if (whatsThisAttr.has_key(knot.getAttrVal())):
		elif (cWhatsThisAttr.has_key(val)):
			#text+=whatsThisAttr[knot.getAttrVal()]+'\n'
			text+=cWhatsThisAttr[val]+'\n'

	#on verifie le cle d'attribue
	attrKey=knot.getAttrKey()
	for val in attrKey:
		if (cWhatsThisAttr.has_key(val)):
			#text+=whatsThisAttr[knot.getAttrKey()]+'\n'
			text+=cWhatsThisAttr[val]+'\n'

	#on regarde s'il y a le help pour ce noeud comme pour l'enfant
	if (text==''and knot!=self.root and cWhatLikeChildren.has_key(knot.parent.tag)):
		helpChild=cWhatLikeChildren[knot.parent.tag]
		if (helpChild.has_key(knot.tag)):
			text+=helpChild[knot.tag]+'\n'
	if(cGeneralHelp.has_key(knot.tag)):
		if(text!=""): text=text+"========"+knot.tag+ "==========" 
		text+=cGeneralHelp[knot.tag]+'\n'
	self.emit(PYSIGNAL('sigShowWhatsThis'),(text,))# a connecter dans Tree_View
	return text

	# pour revenir a un pas arriere
    def slotUndo(self):
	self.Undo()
	self.showProblemKnots() #on ouvre les elements problematiques
	textNew=self.getText()
	self.formProblemLink() #ceration de la liste des lignes correspondantes
	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

    # pour savoir s'il faut fair Editable Undo
    def slotCanUndo(self):
    	status=1
	if (self.oldText.length()==0):
		status=0
	self.emit(PYSIGNAL('sigEnabledUndo'),(status,))

    #recuperer le texte d'element-commentaire
    def slotGetComment(self,knot):
	comment=knot.getComment()
	self.emit(PYSIGNAL('sigShowComment'),(comment,))

    #changer le text d'element commentaire
    def slotChangeComment(self,knot,text):
    	#on memorise la structure avant le changement
	self.memoriseOldText()
	self.modified=1
	if (self.title[0]!='*'):
		self.title='*'+self.title
		self.emit(PYSIGNAL('sigChangeCaption'),(self.title,))
	knot.setComment(text)
	self.showProblemKnots() #on ouvre les elements problematiques
	textNew=self.getText()
	self.formProblemLink() #ceration de la liste des lignes correspondantes
	self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))

    # le slot quand l'utilisateur tape CNTR+F
    def slotFindVariable(self,args):
        if type(args)==type(()):
		varToFind=args[0]
	else:
		varToFind=args
    	self.first=0
    	self.listVariables=[]
	self.all=''
	if(len(varToFind)==0):
		#le cas quand on veut de trouver touts les variable
		self.findVariables()
		self.slotFindNextVariable()
	else:
		self.all=varToFind
		#on recupere l'indice du element selon le nom
		index=self.variables.index(varToFind)
		if (index!=-1):
			#on recuper l'objet selon l'indice
			defObject=self.variables.definition(index)
			if(defObject.isVariable()):#si c'est varaible, on prend <f>
					defObject=defObject.parent
			#le cas de recherche du nom concret
			self.listVariables.append(defObject)
			self.slotFindNextVariable()
		else: #on n'a pas trouve cette string -> donner le message
			strWarning='Search string "'+varToFind+'" not found!'
			self.emit(PYSIGNAL('sigShowBoxMessageWarning'),(strWarning,))


    # le slot quand l'utilisateur tape F3
    def slotFindNextVariable(self):
	if(len(self.listVariables)!=0):
		# il faut montrer la 1ere variable de cette liste
		defObject=self.listVariables[0]
		defObject.showKnots()
		textNew=self.getText()
		self.formProblemLink()

		self.emit(PYSIGNAL('sigShowTree'),(textNew,self.problemLink_list,self.doubleNameLink_list,))
		#on recupere la ligne de la definition
		line=self.dict_link.getKey(defObject)
		if(line!=-1):

			self.emit(PYSIGNAL('sigGoToDef'),(line,))
		else:
			pass
			'''strWarning='The definition is not in this document'
			self.emit(PYSIGNAL('sigMessageWarning'),(strWarning,))'''
		del self.listVariables[0]

	else:
		varToFind=self.all
		if (self.first):
			self.slotFindVariable(varToFind)
		else:
			# il faut demander pour continuer
			strQuestion='End of document reached\nContinue from the beginning?'
			self.emit(PYSIGNAL('sigMessageQuestion'),(strQuestion,self.slotFindVariable,varToFind,))
