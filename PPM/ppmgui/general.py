

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



from elementtree.ElementTree import _ElementInterface
import string
import sys
from xml.sax import ContentHandler
from Testargument import testIfMathExp,testIfName
from Variable_reference import Variable_reference

#liste des variables utilisees
variables=Variable_reference()
#functions_list=['arccos','arcsin','beta', 'Nmaglia']

def isFloat(string):
     is_float=1
     try:
         float(string)
     except:
         is_float=0
     return is_float
#------------------parcour d'arbre, utillise pour le test------------------------
def afficher_variable(variable):
	if (variable.pointer==None):
		if (variable.name!=''): print variable.name
		for val in variable.value:
			print val
	else:
		print variable.pointer.name
def parcourt(elem):
	print elem.tag
	if (elem.pointer==None):
		if len(elem.name)!=0 :print elem.name
	else:
		print elem.pointer.name
	if len(elem.items())!=0:print elem.keys()[0],elem.attrib[elem.keys()[0]]
	list_child=elem.getchildren()
	for child in list_child:
		if (child.toend):
			print child.tag
			if (child.tag=='f' and len(child.key)!=0): print child.key
			if len(child.items())!=0: print child.items()
			afficher_variable(child.getchildren()[0])
			print child.tag
		else:
			parcourt(child)
	print elem.tag

#-------------------------------Les classes----------------------------------
class General_object (_ElementInterface):
	def __init__(self,*args):
		self.name=''
		self.pointer=None
		self.toend=0
		apply(_ElementInterface.__init__,(self,)+args)

	def process_startElement(self,name,attrs):
		pass

	def process_endElement(self, name):
		pass

	def process_characters(self,ch):
		line=''
		for c in ch:
          		if( (c not   in " \n\t") ):
             			line=line+c
		if (len(line)!=0):
			res=variables.getPointer(line)
			if (res!=None):
				self.pointer=res


			else:
				self.name=line
				variables.append(self)


	def write_xml_text(self,file_xml):
		pass


	def write_xml_block(self,file_xml):
		line_attr=' '
		for key in self.keys():
			line_attr+=key+'="'+ self.attrib[key]+'" '
		if (self.pointer!=None):
			file_xml.write('<'+self.tag+line_attr+'>'+self.pointer.name+'\n')
		else:
			if (self.toend):# element est terminal
				file_xml.write('<'+self.tag+line_attr+'>'+self.name)
				self.write_xml_text(file_xml)
			else:#pas terminal
				file_xml.write('<'+self.tag+line_attr+'>'+self.name+'\n')
				list_child=self.getchildren()
				for child in list_child:
					child.write_xml_block(file_xml)
		file_xml.write('</'+self.tag+'>'+'\n')

#************Class Variable_object

class Variable_object(General_object):
	def __init__(self):
		General_object.__init__(self,'',{})
		self.value=[]

#************Class Float_object
class Float_object(General_object):
	def __init__(self,*args):
		apply(General_object.__init__,(self,)+ args)
		self.toend=1 #???
		self.key='' #?????

	def process_characters(self,ch):
        	chl = string.split(ch)

        	Nfloat=0
        	#pos_float=None
        	for ch in chl:
          		if(isFloat(ch)):
	      			Nfloat=Nfloat+1
		vo = Variable_object()
         	self.append(vo)
  	 	for token in chl: # parcour mot par mot du text entre balise
             		if( isFloat(token)==0): #si le mot n'est pas numerique
                		if(token[0]!="\""): #si ce n'est pas le cle
					if (Nfloat!=0): #le cas de la definition de variable
						vo.name=token
						variables.append(vo)
					else:# le cas de reference sur une autre variable
						#recherche dans l'arbre
						res=variables.getPointer(token)
						if (res!=None): #on a trouve la reference
							vo.pointer=res
						else:#pas trouve
							'''if (isFunction(token)): #fonction math.
								vo.value.append(token)'''
							if (testIfName(token) or testIfMathExp(token)):
								vo.value.append(token)
							'''else: #erreur
								print token
								raise 'variable is not defined'''
	       			else:#c'est le cle
					self.key=token#?????
             		else:#on ajoute un chiffre dans la loste des valeurs
				vo.value.append(token)

	def write_xml_text(self,file_xml):
		file_xml.write(self.key+' ')
		child=self.getchildren()[0]
		if (child.pointer!=None):#c'est une reference
			file_xml.write(child.pointer.name)
		else:# c'est un vraie objet
			file_xml.write(child.name+' ')
			for val in child.value:
				file_xml.write(val+' ')

#************Class String_object
class String_object (General_object):
	def __init__(self,*args):
		apply(General_object.__init__,(self,)+args)
		self.toend=1 #???

	def process_characters(self,ch):
		vo = Variable_object()
         	self.append(vo)
		line=''
		for c in ch:
          		if( (c not   in " \n\t") ):
             			line=line+c
		vo.value.append(line)

	def write_xml_text(self,file_xml):
		child=self.getchildren()[0]
		text=child.value[0]
		file_xml.write(text)


#************* class PPMHandler
class Tree(ContentHandler):
    classes_to_use = {"f": Float_object, "s": String_object, "layer":General_object,
    			"bff":General_object,"stack":General_object, 							"scan":General_object,"scanlist":General_object,
			"ift":General_object, "ifo":General_object,
			"dv":General_object,"KK":General_object,"KKFFT":General_object,"bff":General_object,
			"bjoin":General_object, "iff":General_object,
			"KK_write":General_object, "tf0":General_object, "tf12":General_object,
			"Beta_write":General_object,"comparison_write":General_object,
			"fit":General_object, "Minimise":General_object ,
			"MinimiseGeFit":General_object , "prova": General_object,
                      "bfc":General_object , "bfl":General_object , "bsum":General_object,
                      "ifos":General_object, "Write_Stack": General_object,
                     "arrofvar":General_object,"addvars": General_object,
                       "bfinterp":General_object,"setLayerInterpolation":General_object}

    def __init__(self):
        self.processors_list=[]
	self.root=None


    def startElement(self, name, attrs):
        if name=="system": #return
		self.root=General_object(name,attrs)
		self.processors_list.append(self.root)
		return
        actual=None
	actual=self.processors_list[-1]
        nuovo = self.classes_to_use[name](name,attrs)
        #nuovo.process_startElement(name,attrs)
        actual.append(nuovo)
	self.processors_list.append(nuovo)

    def endElement(self, name):
        #if name=="system": return
        #self.processors_list[-1].process_endElement(name)
	del self.processors_list[-1]

    def characters(self, ch):
        if self.processors_list==[]: return
	self.processors_list[-1].process_characters(ch)




from xml.sax import make_parser
from xml.sax.handler import feature_namespaces

if __name__ == '__main__':
    # Create a parser
    parser = make_parser()

    # Tell the parser we are not interested in XML namespaces
    parser.setFeature(feature_namespaces, 0)

    # Create the handler
    tree = Tree()

    # Tell the parser to use our handler
    parser.setContentHandler(tree)


    # Parse the input
    parser.parse(sys.argv[1])
    #parcourt(dh.root)
    file_name='res.xml'
    f=open(file_name,'w')
    tree.root.write_xml_block(f)


