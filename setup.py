
import sys,os
import glob
import platform
import string
import shutil
import subprocess

from distutils.core import Extension, setup

try:
    import numpy
except ImportError:
    text  = "You must have numpy installed.\n"
    text += "See http://sourceforge.net/project/showfiles.php?group_id=1369&package_id=175103\n"
    raise ImportError, text


import distutils.sysconfig
global PPM_INSTALL_DIR
global PPM_SCRIPTS_DIR

jn = os.sep.join

# Append cvs tag if working from cvs tree

if os.path.isdir('.git') and os.path.isfile(os.sep.join(['.git', 'logs', 'HEAD'])):
    logs  = open( os.sep.join(['.git', 'logs', 'HEAD']) ).read()
    logs=string.replace(logs, '"""' , '"-"-"')
    open( jn( ["PPM", "logs.py"]) , "w").write('logs="""%s""" '%logs)

    os.system("git diff > diffoutput" )
    diffoutput= open("diffoutput").read()

    diffoutput=string.replace(diffoutput, '"""' , '"-"-"')
    open( jn(["PPM", "diffoutput.py"]), "w").write('diffoutput="""%s""" '%diffoutput)



    

packages = ['PPM', 'PPM.PPMscalar', 'PPM.dabax','PPM.Minimiser', "PPM.ppmgui", "PPM.ppmgui.elementtree",
            'PPM.PPMtensorial','PPM.PPMtensorial.cuPPM','PPM.PPMtensorial.clPPM']

if sys.platform == "win32":
    define_macros = [('WIN32',None)]
    script_files = []
else:
    define_macros = [('SPECFILE_USE_GNU_SOURCE', None)]

script_files = glob.glob(jn(['PPM', 'scripts','*']) )


files_to_check = glob.glob(jn(['PPM','octree','*.i']))

for file in files_to_check:
    stat_i = os.stat( file ).st_mtime
    ok=0
    wrapfile = file[:-2]+"_wrap.cxx"
    if os.path.isfile( wrapfile):
        stat_cxx = os.stat( wrapfile  ).st_mtime

        #print stat_cxx
        #print stat_i
        
        if stat_cxx >   stat_i:
            ok=1
    if not ok :
        print " GENERATING SWIG WRAPPER FOR ", file
        
        if sys.platform == "win32":
            raise Exception, "completa col comando per win32 "
        else:
            os.system("swig -c++ -python "+ file  )
                    
# Specify all the required PyMca data
data_files = [('PPM/data', glob.glob('PPM/data/*.dat') ),
              ('PPM/data/Icons', glob.glob('PPM/data/Icons/*') ),
              # ('PyMca/attdata', glob.glob('PyMca/attdata/*')),
              # ('PyMca/HTML', glob.glob('PyMca/HTML/*.*')),
              # ('PyMca/HTML/IMAGES', glob.glob('PyMca/HTML/IMAGES/*')),
              # ('PyMca/HTML/PyMCA_files', glob.glob('PyMca/HTML/PyMCA_files/*'))
]

for exdir in glob.glob("examples/*"):
    tok = (exdir, glob.glob(exdir+"/*"))
    print "aggiungo ", tok
    data_files.append(tok)
    


# data_files fix from http://wiki.python.org/moin/DistutilsInstallDataScattered
from distutils.command.install_data import install_data
class smart_install_data(install_data):
    def run(self):
        global PPM_INSTALL_DIR
        #need to change self.install_dir to the library dir
        install_cmd = self.get_finalized_command('install')
        self.install_dir = getattr(install_cmd, 'install_lib')
        PPM_INSTALL_DIR = self.install_dir
        print "PPM to be installed in %s" %  self.install_dir
        return install_data.run(self)


def build_ppmcore(ext_modules):
    print glob.glob(jn(['PPM','PPMscalar','*.cc']))
    module  = Extension(name = 'PPM.PPMscalar.PPMcore',
                        sources = glob.glob(jn(['PPM','PPMscalar','*.cc'])),
                        define_macros = define_macros,
                        include_dirs = [ numpy.get_include()])
    ext_modules.append(module)

def build_ppmcoretens(ext_modules):
    module = Extension('PPM.PPMtensorial.PPMcoreTens',
                       sources = glob.glob(jn(['PPM','PPMtensorial','*.cc']))+ glob.glob(jn(['PPM','PPMtensorial','*.c'])),
                       #libraries= libraries_list,
                       include_dirs=[ numpy.get_include()],
                       define_macros = define_macros,
                       # library_dirs=library_dirs_list,
                       ) 
    ext_modules.append(module)
    
def build_specfile(ext_modules):
    module  = Extension(name = 'PPM.specfile',
                        sources = glob.glob('PPM/specfile/src/*.c'),
                        define_macros = define_macros,
                        include_dirs = ['PPM/specfile/include',
                        numpy.get_include()])
    ext_modules.append(module)



def build_KK(ext_modules):
    module  = Extension(name = 'PPM.KKpy_c',
                        sources = glob.glob(jn(['PPM','dabax','*.c'])),
                        define_macros = define_macros,
                        include_dirs = [ numpy.get_include()]
                        )
    ext_modules.append(module)

def build_PPMextensions(ext_modules, internal_clibraries):

    #Options for CLPPM are DYNAMIC or DIRECT
    #            CUPPM are DYNAMIC or CUDAFE
    CLPPMenv='DIRECT'
    CUPPMenv='DYNAMIC'
#     CUPPMenv='CUDAFE'

    #Set environment variables for the child setup
    os.environ['CLPPM']=CLPPMenv
    os.environ['CUPPM']=CUPPMenv

    if(len(sys.argv)>= 2):
        #Get the installation instruction used for setup.py
    	setup_command=sys.argv[1:]
	print setup_command
	print ['python','setup_cuppm_clppm.py',"%s" % setup_command]
        #build the modules and wait to finish
	try:
	        subprocess.call(['python','setup_cuppm_clppm.py'] + setup_command )
	except OSError as error:
		print " !!Building PPM extensions clPPM and cuPPM failed:"
		print error
		print " !! "
    if sys.platform=='win32':
        if os.path.exists(jn(['lib','cuPPM.dll'])):
            internal_clibraries.append('cuPPM.dll')
        if os.path.exists(jn(['lib','clPPM.dll'])):
            internal_clibraries.append('clPPM.dll')
    else:
        if os.path.exists(jn(['lib','libcuPPM.so'])):
            internal_clibraries.append('libcuPPM.so')
        if os.path.exists(jn(['lib','libclPPM.so'])):
            internal_clibraries.append('libclPPM.so')

    print 'Internal C/C++ Libriaries: ',internal_clibraries
    return internal_clibraries
    
ext_modules = []
internal_clibraries = []

build_ppmcore(ext_modules)
build_specfile(ext_modules)
build_KK(ext_modules)
build_ppmcoretens(ext_modules)
internal_clibraries = build_PPMextensions(ext_modules, internal_clibraries )


# data_files fix from http://wiki.python.org/moin/DistutilsInstallDataScattered
from distutils.command.install_data import install_data

from distutils.command.install_scripts import install_scripts
class smart_install_scripts(install_scripts):
    def run (self):
        global PPM_SCRIPTS_DIR, internal_clibraries
        #I prefer not to translate the python used during the build
        #process for the case of having an installation on a disk shared
        #by different machines and starting python from a shell script
        #that positions the environment
        from distutils import log
        from stat import ST_MODE

        install_cmd = self.get_finalized_command('install')
        #This is to ignore the --install-scripts keyword
        #I do not know if to leave it optional ...
        if False:
            self.install_dir = os.path.join(getattr(install_cmd, 'install_lib'), 'PPM')
            self.install_dir = os.path.join(self.install_dir, 'bin')        
        else:
            self.install_dir = getattr(install_cmd, 'install_scripts')
            self.install_lib_dir = getattr(install_cmd, 'install_lib')
            
        PPM_SCRIPTS_DIR = self.install_dir        
        if sys.platform != "win32":
            print "PPM scripts to be installed in %s" %  self.install_dir
        self.outfiles = self.copy_tree(self.build_dir, self.install_dir)
        self.outfiles = []
        for filein in glob.glob(jn(['PPM','scripts','*'])):
            
            filedest = os.path.join(self.install_dir, os.path.basename(filein))
            filedest_py =  os.path.basename(filein)+".py"

            
                
            moddir = os.path.join(getattr(install_cmd,'install_lib'), "PPM")

            print " moddire est " , moddir
            if sys.platform == "win32":
                filedest=filedest+".BAT"
                text  ="@echo off"
                text +="set PYTHONBKP=%PYTHONPATH%\n"
                text +="set PATHBKP=%PATH%\n"
                text +="set PYTHONPATH=%sPYTHONPATH%s;%s\n"  %( "%","%", moddir)
                text +="set PATH=%sPATH%s;%s\n" %("%","%",moddir)
                # text=text+"set DABAX_DIR=%s\n"  % jn([moddir,"data"])
                # text=text+"set ICON_DIR=%s\n"  % jn([moddir,"data\\"])
                text += 'cmd /K python "%s" %s*\n' %   ( os.path.join(moddir,filedest_py ), "%" )
                text += "set PYTHONPATH=%PYTHONBKP%\n"
                text += "set PATH=%PATHBKP%\n"
            else:
                text  = "#!/bin/bash\n"
                text  = text+"namedir=`dirname $0`\n"
                text  = text+'if  [ "${namedir:0:1}" == "/" ]  ; then     \n'
                text  = text+'            dirtouse=$namedir                                \n'
                text  = text+'else                                \n'
                text  = text+'            dirtouse="${PWD}/${namedir}"      \n'
                text  = text+'fi                                \n'
                
                
                text += "export PYTHONPATH=${dirtouse}:${PYTHONPATH}\n" 
                text += "export LD_LIBRARY_PATH=${dirtouse}/PPM:${LD_LIBRARY_PATH}\n"
                text += "export LD_LIBRARY_PATH=${dirtouse}/PPM/PPMtensorial:${LD_LIBRARY_PATH}\n"
                # text += "export DABAX_DIR=${dirtouse}/data\n" 
                # text=text+"export ICON_DIR=%s\n"  % jn([moddir,"data/"])
                text += "exec python ${dirtouse}/PPM/%s $*\n" %  filedest_py 
                
            if os.path.exists(filedest):
                os.remove(filedest)
            
            f=open(filedest, 'w')
            f.write(text)
            f.close()
            #self.copy_file(filein, filedest)
            self.outfiles.append(filedest)

        for libfile in internal_clibraries:
            filedest = os.path.join(moddir,'PPMtensorial',libfile )
            if os.path.isfile(  filedest ):
                os.remove(filedest  )
                assert(not os.path.isfile( filedest  ))
            shutil.move(os.path.join('lib',libfile),filedest )
            assert(os.path.isfile( filedest ))
                
        if os.name == 'posix':
            # Set the executable bits (owner, group, and world) on
            # all the scripts we just installed.
            for file in self.get_outputs():
                if self.dry_run:
                    log.info("changing mode of %s", file)
                else:
                    mode = ((os.stat(file)[ST_MODE]) | 0555) & 07777
                    log.info("changing mode of %s to %o", file, mode)
                    os.chmod(file, mode)
   
description = ""
long_description = """
"""

distrib = setup(name="PPM",
                license = "GPL - Please read LICENSE.GPL for details",
                # version= logs,
                description = description,
                author = "Alessandro Mirone",
                author_email="mirone@esrf.fr",
                url = "http://forge.epn-campus.fr/projects/PPM",
                long_description = long_description,
                packages = packages,
                platforms='any',
                ext_modules = ext_modules,
               data_files = data_files,
##                package_data = package_data,
##                package_dir = {'': 'lib'},
                cmdclass = {
    'install_data':smart_install_data, 
    'install_scripts':smart_install_scripts},
                scripts=script_files
                )




















