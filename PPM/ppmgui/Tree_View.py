

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import sys
from PyQt4.Qt  import *
from general_Tree import Tree
from QTypeObject import SyntaxOfView
PYSIGNAL=SIGNAL
class TreeView(QTextEdit):
	#def __init__(self,tree,parent):
	def __init__(self,tree,actions,parent):
		
		QTextEdit.__init__(self)
		
		subWindow =parent.addSubWindow(self)
		subWindow.setAttribute(Qt.WA_DeleteOnClose)

		# parent.setViewMode(QMdiArea.SubWindowView)

		#la reference sur le document (modele) correspondant pour connecter les signals du document
		self.tree=tree
		# help
		# self.help=QWhatsThis(self)
		self.actions=actions

		self.curentLine=0
		self.curentPosition=0
		self.start=0
		self.end=1

		# pour garder le focus
		self.setFocusPolicy(Qt.ClickFocus)

		#la propriete qui permet de fermer le view (changement dans DocMenager)
		self.closeView=1
		# pour faire le style de view
		self.syntax=SyntaxOfView(self)

		# self.setTextFormat(self.PlainText)
		self.setWordWrapMode(QTextOption.WrapAnywhere)
		#self.setFocusPolicy (QWidget.ClickFocus)
		#---Dict de touche ---------------------
		self.key2action = {Qt.Key_Plus: self.__class__.doPlus,
			      Qt.Key_Down: self.__class__.doDown ,
			      Qt.Key_Up:self.__class__.doUp,
			      Qt.Key_Left:self.__class__.doLeft,
			      Qt.Key_Right:self.__class__.doRight,
			      Qt.Key_Delete:self.__class__.doDelete,
			      Qt.Key_Insert:self.__class__.doInsert}
			      #ctrlKeys.findKey(Qt.Key_Control+Qt.Key_C):self.__class__.doCopy}
     		self.ctrlKey2action={Qt.Key_Shift:self.__class__.doCopySpecial,
				     Qt.Key_C:self.__class__.doCopy,
				     Qt.Key_V:self.__class__.doInsert,
				     Qt.Key_X:self.__class__.doCut,
				     Qt.Key_Z:self.__class__.doUndo}

		self.connect(self.tree,PYSIGNAL('sigShowTree'),self.slotShowTree)
		self.connect(self.tree,PYSIGNAL('sigGoToDef'),self.slotGoToDef)
		self.connect(self.tree,PYSIGNAL('sigShowWhatsThis'),self.slotShowWhatsThis)
		self.connect(self.tree,PYSIGNAL('sigChangeCaption'),self.slotChangeCaption)

		self.connect(self,SIGNAL('clicked(int,int)'),self.slotClicked)
		# self.connect(self,SIGNAL('mouseDoubleClickEvent(QMouseEvent *)'),self.slotDoubleClicked)
		self.connect(self,PYSIGNAL('sigChangeCursor'),self.slotChangeCursor)



	def getCurrentPosition(self):
		return self.curentLine, self.curentPosition


	def doPlus(self,line, pos):
		self.emit(PYSIGNAL('sigViewPressed'),())

	def text(self, line):
		curs=self.textCursor()
		curs.setPosition(0,QTextCursor.MoveAnchor);
		curs.movePosition(  QTextCursor.NextBlock          , QTextCursor.MoveAnchor,line  );


		
		return curs.block().text().toAscii() 
		


		
	def doDown(self,line, pos):
		#le calcul de la position du coursor
		text=str(self.text(line+1))
		ind= text.find('+<')
		if (ind!=-1): #il y a le signe '+'
			pos=ind
		else:
			ind= text.find('-<')
			if (ind!=-1): #il y a le signe '-'
				pos=ind
			else:
				ind= text.find('<')
				if (ind!=-1): #il y a le signe '<'
					pos=ind
					
		self.setCursorPosition(line+1,pos)
		line=self.textCursor().blockNumber()
		pos = self.textCursor().position() -  self.textCursor().block().position()
		
		self.slotChangeCursor((line,pos))
		
		self.slotExplaineLines((line, ))
		self.slotUpdateWindows()
		self.emit(PYSIGNAL('sigShowHelp'),())
		self.emit(PYSIGNAL('sigSelectCurrentElement'),())


	def doUp(self,line, pos):
		text=str(self.text(line-1))
		ind= text.find('+<')
		if (ind!=-1): #il y a le signe '+'
			pos=ind
		else:
			ind= text.find('-<')
			if (ind!=-1): #il y a le signe '-'
				pos=ind
			else:
				ind= text.find('<')
				if (ind!=-1): #il y a le signe '<'
					pos=ind

		self.setCursorPosition(line-1,pos)
		line=self.textCursor().blockNumber()
		pos = self.textCursor().position() -  self.textCursor().block().position()

		self.slotChangeCursor((line,pos))
		self.slotExplaineLines((line, ))
		self.slotUpdateWindows()
		self.emit(PYSIGNAL('sigShowHelp'),())
		self.emit(PYSIGNAL('sigSelectCurrentElement'),())

	def doLeft(self,line, pos):
		if(pos!=0):
			#self.emit(PYSIGNAL('sigChangeCursor'),(line,pos-1,))
			self.slotChangeCursor((line,pos-1))

	def doRight(self,line, pos):
		#self.emit(PYSIGNAL('sigChangeCursor'),(line,pos+1,))
		self.slotChangeCursor((line,pos+1))

	def doDelete(self,line, pos):
		self.emit(PYSIGNAL('sigDelete'),())
		#self.emit(PYSIGNAL('sigGetDef'),(line,))

	def doCopy(self,line, pos):
		self.emit(PYSIGNAL('sigCopy'),())

	def doInsert(self,line, pos):
		self.emit(PYSIGNAL('sigInsert'),())
		#self.emit(PYSIGNAL('sigGetDef'),(line,))

	def doCut(self,line, pos):
		self.emit(PYSIGNAL('sigCut'),())
		#self.emit(PYSIGNAL('sigGetDef'),(line,))

	def doCopySpecial(self,line, pos):
		self.emit(PYSIGNAL('sigCopySpecial'),())


	def doUndo(self,line, pos):
		self.emit(PYSIGNAL('sigUndo'),())

	def doTab(self,line, pos):

		#self.releaseKeyboard()
		#self.clearFocus ()
		self.emit(PYSIGNAL('sigChangeWindow'),(0,))

	def doFindDef(self,line, pos):
		self.emit(PYSIGNAL('sigFindDef'),())

	def doWarning(self,cod):
		if(cod!=Qt.Key_Control ):
			warning="You can modify the element in the window 'Edit an element'"
			self.emit(PYSIGNAL('sigShowBoxMessageWarning'),(warning,))

	def keyPressEvent (self,event ):
		line=self.textCursor().blockNumber()
		pos = self.textCursor().position() -  self.textCursor().block().position()

		cod=event.key()
		#si c'est le simple touche
#		if (event.state()!=Qt.ControlButton):

		if (event.modifiers()!=Qt.ControlModifier):
			if(  self.key2action.has_key(cod) ):
				action = self.key2action[cod]
				apply( action ,(self,)+(line, pos))
			else:
				self.doWarning(cod)
		else: #  combinaison avec CTRL
			if(  self.ctrlKey2action.has_key(cod) ):
				action = self.ctrlKey2action[cod]
				apply( action ,(self,)+(line, pos))
		#apply( self.__class__.__bases__[0].keyPressEvent, (self,event))

	def createPopupMenu (self,pos):
		self.menuContext=QPopupMenu(self)
		self.actions['editDef'].addTo(self.menuContext)
		self.menuContext.insertSeparator()
		self.actions['editCut'].addTo(self.menuContext)
		self.menuContext.insertSeparator()
		self.actions['editCopy'].addTo(self.menuContext)
		self.actions['editCopySpec'].addTo(self.menuContext)
		self.menuContext.insertSeparator()
		self.actions['editPast'].addTo(self.menuContext)
		self.actions['editDel'].addTo(self.menuContext)
		return self.menuContext

	def closeEvent ( self,event):
		self.emit(PYSIGNAL('sigCloseView'),())
		if (self.closeView):
			apply( self.__class__.__bases__[0].closeEvent, (self,event))

	#block l'insertion par souri predefinie
	def paste (self):
		pass


#-----------------les SLOTS de View-----------------------------------

	def slotShowTree(self,args):
		vPos = self.verticalScrollBar().value()

		textNew,problemLine_list,doubleNameLine_list=args
		self.colorRef=QColor ( 255,0, 0)
		self.colorName=QColor ( 255,255, 0)
		self.colorCurElement=QColor(230,230,230)
		self.setPlainText(textNew)

		#		self.setCursorPosition(self.curentLine,self.curentPosition)

		curs=self.textCursor()
		curs.setPosition(0,QTextCursor.MoveAnchor);
		# curs.movePosition(  QTextCursor.Down          , QTextCursor.MoveAnchor,self.curentLine   -1);
		curs.movePosition(  QTextCursor.NextBlock          , QTextCursor.MoveAnchor,self.curentLine );
		curs.movePosition(  QTextCursor.NextCharacter , QTextCursor.MoveAnchor,self.curentPosition );
		self.setTextCursor(curs);

		oldpos=self.textCursor().position()
		
		self.slotUpdateWindows()
		
		for line in problemLine_list:
			self.setParagraphBackgroundColor(line,self.colorRef, curs)
		for line in doubleNameLine_list:
			self.setParagraphBackgroundColor(line,self.colorName, curs)
	
		self.setTextColor(QColor(0,0,0))
		self.emit(PYSIGNAL('sigSelectCurrentElement'),())
		curs.setPosition(oldpos)
		self.setTextCursor(curs);

		self.verticalScrollBar().setValue(vPos)

	def slotClicked(self,line,coursor):

		self.slotChangeCursor((line,coursor))
		self.slotExplaineLines((line, ))
		self.slotUpdateWindows()
		self.emit(PYSIGNAL('sigSelectCurrentElement'),())


	def mousePressEvent(self, *args):
		QTextEdit.mousePressEvent(self, *args)
		line=self.textCursor().blockNumber()
		pos = self.textCursor().position() -  self.textCursor().block().position()
		self.slotChangeCursor((line,pos))
		self.slotExplaineLines((line, ))
		self.slotUpdateWindows()
		self.emit(PYSIGNAL('sigShowHelp'),())
		self.emit(PYSIGNAL('sigSelectCurrentElement'),())




	def mouseDoubleClickEvent(self,ev):

		
		line=self.textCursor().blockNumber()
		pos = self.textCursor().position() -  self.textCursor().block().position()
		
		self.slotChangeCursor((line,pos))
		self.emit(PYSIGNAL('sigViewDoubleClicked'),(line,pos))

	def setCursorPosition(self, newLine,newPosition):

		curs=self.textCursor()
		curs.setPosition(0,QTextCursor.MoveAnchor);

		curs.movePosition(  QTextCursor.NextBlock     , QTextCursor.MoveAnchor, newLine     );
		curs.movePosition(  QTextCursor.NextCharacter , QTextCursor.MoveAnchor, newPosition );
		self.setTextCursor(curs);


	def slotChangeCursor(self,(newLine,newPosition) ):
		self.emit(PYSIGNAL('sigKeepFocus'),())
		self.curentLine=newLine
		self.curentPosition=newPosition


		self.setCursorPosition(newLine,newPosition)

		
		self.setFocus()# il faut verifier l'utilisation de se methode
		#if not(self.hasFocus()):
		self.emit(PYSIGNAL('sigChangeStatus'),(newLine,newPosition,))
		#self.grabKeyboard ()

	def slotSetCurrentPosition(self):
		#self.setCursor ( QCursor() )
		self.setCursorPosition(self.curentLine,self.curentPosition)

	def slotGoToDef(self,(line, )):
		self.slotChangeCursor((line,self.curentPosition))
		self.slotExplaineLines((line, ))
		self.emit(PYSIGNAL('sigSelectCurrentElement'),())
		self.slotUpdateWindows()

	def slotUpdateWindows(self):
		self.emit(PYSIGNAL('sigGetHelp'),())
		self.emit(PYSIGNAL('sigGetChildren'),())
		self.emit(PYSIGNAL('sigGetEdit'),())
		self.emit(PYSIGNAL('sigGetDef'),())


	def slotShowWhatsThis(self, (text, )):

		posCursor=self.cursor().pos() # self.cursorRect().topLeft()
		# self.mapToGlobal(QPoint ( self.curentPosition*5,self.curentLine*22-self.contentsY()))
		# posCursor=self.mapFromGlobal(posCursor)
		#posCursor=QPoint ( self.curentPosition,self.curentLine*20)                
		# self.help.display (text,posCursor)#QPoint(500,500))
		QWhatsThis.showText ( posCursor,  text,self)

	#donne le message qui explique les lignes rouges et lignes jaunes
	def slotExplaineLines(self,(line, )):
		warning=''
		if (self.paragraphBackgroundColor(line)==self.colorRef):
			#warning="There is problem with element-'reference'"
			
			self.emit(PYSIGNAL('sigDefinProblem'),())
		elif(self.paragraphBackgroundColor(line)==self.colorName):
			warning="The elements with the yellow line have the same name"
		if(len(warning)!=0):
			self.emit(PYSIGNAL('sigShowMessageWarning'),(warning,))

	def slotChangeCaption(self,(newCaption, )):
		self.setWindowTitle (newCaption)


	def paragraphBackgroundColor(self, i):
		curs=	QTextCursor(self.textCursor())
		curs.setPosition(0,QTextCursor.MoveAnchor);
		curs.movePosition(  QTextCursor.NextBlock,QTextCursor.MoveAnchor,i );
		
		bf = curs.blockFormat();
		color = bf.background().color()
		return color
		

	def setParagraphBackgroundColor(self, i,color ):


		oldpos=self.textCursor().position()
		
		self.setCursorPosition(i,0)
		
		curs=	QTextCursor(self.textCursor())
		
		curs.select(QTextCursor.BlockUnderCursor)
		
		text=curs.selectedText()
		

		
		curs.removeSelectedText()
		
		# self.setParagraphBackgroundColor(i,self.colorCurElement)
		bf = curs.blockFormat();
		bf.setBackground(color  );
		curs.mergeBlockFormat(bf);

		self.setTextCursor(curs);
		curs.insertText(text)

		curs.setPosition(oldpos)
		self.setTextCursor(curs)

	def setParagraphBackgroundColor(self, i,color, curs ):



		curs.setPosition(0,QTextCursor.MoveAnchor);
		curs.movePosition(  QTextCursor.NextBlock     , QTextCursor.MoveAnchor, i    );
		curs.select(QTextCursor.BlockUnderCursor)
		text=curs.selectedText()

		
		curs.removeSelectedText()
		curs.insertText(text)
		bf = curs.blockFormat();
		bf.setBackground(color  );
		curs.mergeBlockFormat(bf);


		
	#permet de selectionner l'element courrant
	def slotShowCurrentElement(self,start,end):

		self.start=start
		self.end=end
		oldpos=self.textCursor().position()


		curs=self.textCursor()
		curs.setPosition(0,QTextCursor.MoveAnchor);

		
		i=0
		white=QColor(255,255,255)
		while (i<=self.document().blockCount ()):
			
			if ( self.paragraphBackgroundColor(i)!=self.colorRef and self.paragraphBackgroundColor(i)!=self.colorName):
				if(i>=start and i<=end):

					self.setParagraphBackgroundColor( i, self.colorCurElement, curs)
				else:
					self.setParagraphBackgroundColor( i, white , curs)
					
			# curs.movePosition(  QTextCursor.NextBlock         , QTextCursor.MoveAnchor,1 );
			i+=1
			
		curs.setPosition(oldpos)
		self.setTextCursor(curs)



