

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import sys
from PyQt4.Qt import *
from general_Tree import Tree
from Tree_View import TreeView
PYSIGNAL=SIGNAL

class DocMenager(QObject):
	def __init__(self,parent,*args):
		apply(QObject.__init__,(self,)+args)
		self.parent=parent
		self.viewManager=parent.viewsSpace # ????

		self._docToView={}
		self._viewToDoc={}

	def _createView(self,document,viewClass):
	        #def _createView(self,document,,viewClass):

		view=viewClass(document,self.parent.actions,self.viewManager)#on a cree l'instance de QTextEdit
		view.show()
		view.setWindowTitle (document.title)# il faut creer cette propriete !!!
		#view.setMaximumSize(self.parent.maxSizeForDoc)
		#view.setMinimumSize (self.parent.maxSizeForDoc)

		

		self.emit(PYSIGNAL('sigCreatview'),(view,))
		return view

	#creation des documents
	def createDocument(self,docClass=Tree,viewClass=TreeView):
		document=docClass()# on a cree une instence du Tree
		view=self._createView(document,viewClass)#on a cree le view pour ce document
		#on a ajoute dans les dict de liens
		if self._docToView.has_key(document):
			self._docToView[document].append(view)
		else:
			self._docToView[document]=[view]
		self._viewToDoc[view]=document

		self.emit(PYSIGNAL('sigCreatDoc'),(document,))
		self.emit(PYSIGNAL('sigChangedNumberOfDoc'),())
		return document

	def numberOfDocuments(self):
		return len(self._docToView)

	def numberOfView(self):
		return len(self._viewToDoc)

	#renvoie la liste de touts les view (c'est pour le cas general)
	def view(self,document):

		views=[]
		if self._docToView.has_key(document):
			views=self._docToView[document]
		return views

	#renvoie le document correspondant a ce view
	def document(self,view):
		try:
			view	=view.widget()
		except:
			pass

		document=None

		if self._viewToDoc.has_key(view):
			document=self._viewToDoc[view]

		return document

	def documentWhithName(self,fileName):
		res=None
		listOfDoc=self._docToView.keys()#la liste de touts les doc.
		i=0
		n=len(listOfDoc)
		while(i<n and listOfDoc[i].getFileName()!=fileName):
			i+=1
		if (i!=n):
			res=listOfDoc[i]
		return res


	# verifie si le document a ete modifie, dans ce cas il demande a utilisateur
	'''def _queryCloseDocument(self,document):
		closeDocument=1
		if (document.isModified()):
			closeDocument=self.parent.queryCloseDocument(document)
		return closeDocument'''

	def _removeView(self,document,view):
		try:
			# il faut l'enlee dans les 2 dicts
			self._docToView[document].remove(view)
			del self._viewToDoc[view]
		except: pass

	def closeView(self,view):
		try:
			view	=view.widget()
		except:
			pass

		view.closeView=1
  		document=self._viewToDoc[view]
		if (len(self._docToView[document])==1): #le cas de fermeture du dernier view
			#if(self._queryCloseDocument(document)):# l'utilisateur a permi de fermer ce document
			if(self.parent.queryCloseDocument(document)):# l'utilisateur a permi de fermer ce document
				self._removeView(document,view)
				del self._docToView[document]
				self.emit(PYSIGNAL('sigChangedNumberOfDoc'),())
			else:
				view.closeView=0
		else: # c'est pas le dernier view
			self._removeView(document,view)
		#il faut changer actifView dans App si on a ferme le view
		if (view.closeView and len(self._viewToDoc)>0):
			self.parent.actifView=None #self._viewToDoc[-1]
		return view.closeView

	def closeDocument(self,document):
		closeDoc=1
		listOfViews=self._docToView[document]
		for view in listOfViews:
			self.parent.setActifView(view)
			if (not view.close()): # a verifier si cela provoque closeView
				closeDoc=0
		return closeDoc


	def closeAll(self):
		closeAll=1
		for document in self._docToView.keys():
			if (not self.closeDocument(document)):
				closeAll=0
				strWarning="Document "+ document.title+" can't be close !"
				self.emit(PYSIGNAL('sigMessageWarning'),(strWarning,))#a connecter dans TreeApp
		return closeAll

