
/**
 * \file
 * \brief Other Kernels
 *
 * Defines the CUDA kernels that compute the Propagations,
 * Interfaces and the Reflectivity
 */
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <stdio.h>
#include <cuComplex.h>
#define BLOCKSIZE 32
#define _MAX_CONSTANT_SIZE 200

// printf() is only supported
 // for devices of compute capability 2.0 and above
#if defined(__CUDA_ARCH__) && (__CUDA_ARCH__ < 200)
#define printf(f, ...) ((void)(f, __VA_ARGS__),0)
#endif

__device__ __constant__ int scanLen_;
__device__ __constant__ int numthick_;
__device__ __constant__ int uniquelayers_;
__device__ __constant__ int wheretolook[_MAX_CONSTANT_SIZE];
__device__ __constant__ double Thickness[_MAX_CONSTANT_SIZE];
__device__ __constant__ double Roughness[_MAX_CONSTANT_SIZE];

/**
 * \brief Populates the CUDA constant memory space
 */
cudaError_t setCudaSymbols_other_cu(const int &scanLen, int * wheretolook, double * Thickness,
                                    double * Roughness,const int &numthick,const int &uniquelayers){

  cudaError_t cuErr;
  char msg[1000];

  if(numthick>_MAX_CONSTANT_SIZE){
    sprintf(msg,"%s (@ %d): layers > %d\n",__FUNCTION__,__LINE__,_MAX_CONSTANT_SIZE);
    throw msg;
  }

  cudaMemcpyToSymbol("scanLen_",&scanLen,sizeof(int));
  cudaMemcpyToSymbol("numthick_",&numthick,sizeof(int));
  cudaMemcpyToSymbol("uniquelayers_",&uniquelayers,sizeof(int));
  cudaMemcpyToSymbol("wheretolook",wheretolook,sizeof(int)*numthick);
  cudaMemcpyToSymbol("Thickness",Thickness,sizeof(double)*numthick);
  cudaMemcpyToSymbol("Roughness",Roughness,sizeof(double)*numthick);
  

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr));
    throw msg;
  }

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": Symbols copied" <<std::endl;
#endif
  
  return cuErr;
};


__device__ static __inline__ cuDoubleComplex cuCRmul(cuDoubleComplex x,double y)
{
    cuDoubleComplex prod;
    prod = make_cuDoubleComplex ( cuCreal(x) * y , cuCimag(x) * y );
    return prod;
}

/*
__device__ static __inline__ cuDoubleComplex __Cadd(cuDoubleComplex x,cuDoubleComplex y)
{
    cuDoubleComplex prod;
    prod = make_cuDoubleComplex ( __dadd_rz( cuCreal(x) , cuCreal(y) )  , __dadd_rz( cuCimag(x) , cuCimag(y)) );
    return prod;
}
// __dadd_rn()
// __dmul_rn()
*/
__device__ static __inline__ cuDoubleComplex cuCRdiv(cuDoubleComplex x,double y)
{
    cuDoubleComplex prod;
    prod = make_cuDoubleComplex ( cuCreal(x) / y , cuCimag(x) / y );
    return prod;
}

__device__ static __inline__ cuDoubleComplex cuCexp(cuDoubleComplex x)
{
    cuDoubleComplex prod;
    prod = make_cuDoubleComplex( exp(x.x)*cos(x.y)  ,  exp(x.x)*sin(x.y) );
    return prod;
}

__device__ static __inline__ cuDoubleComplex negC(cuDoubleComplex x)
{
    cuDoubleComplex res;
    res = make_cuDoubleComplex ( -cuCreal(x), -cuCimag(x));
    return res;
}

//Calculates the Minors for the determinant of the m 4x4 matrix
__device__ static __inline__ cuDoubleComplex cuMinor(cuDoubleComplex *m, const int r0, const int r1,
                                                      const int r2, const int c0, const int c1, const int c2){

#define m(iwave,j)     m[ idx + scanLen_* ( blockIdx.y + numthick_* ( (iwave) + 4 * (j) )  ) ]

  cuDoubleComplex res,tmp,tmp1;
  int idx = threadIdx.x + blockIdx.x*blockDim.x;

  tmp  = cuCmul( m(c1,r1) , m(c2,r2) );
  tmp1 = cuCmul( m(c1,r2) , m(c2,r1));
  tmp1 = cuCsub( tmp, tmp1);
  res  = cuCmul( m(c0,r0) , tmp1);

  tmp  = cuCmul( m(c0,r1) , m(c2,r2) );
  tmp1 = cuCmul( m(c0,r2) , m(c2,r1) );
  tmp1 = cuCsub( tmp, tmp1);
  tmp1 = cuCmul( m(c1,r0) , tmp1);
  res  = cuCsub( res, tmp1);

  tmp  = cuCmul( m(c0,r1) , m(c1,r2) );
  tmp1 = cuCmul( m(c0,r2) , m(c1,r1) );
  tmp1 = cuCsub( tmp, tmp1);
  tmp1 = cuCmul( m(c2,r0) , tmp1);
  res  = cuCadd( res, tmp1);
  
//     m[r0*4+ c0] * (m[r1*4+c1] * m[r2*4+c2] - m[r2*4+c1] * m[r1*4+c2]) -
//           m[r0*4+c1] * (m[r1*4+c0] * m[r2*4+c2] - m[r2*4+c0] * m[r1*4+c2]) +
//           m[r0*4+c2] * (m[r1*4+c0] * m[r2*4+c1] - m[r2*4+c0] * m[r1*4+c1]);
#undef m
  return res;
}

//Calculates the Determinant of a 4x4 m Matrix
__device__ static __inline__ cuDoubleComplex cuDet(cuDoubleComplex *m){

#define m(j,iwave)     m[ idx + scanLen_* ( blockIdx.y + numthick_* ( (iwave) + 4 * (j) )  ) ]

  cuDoubleComplex res,tmp;
  int idx = threadIdx.x + blockIdx.x*blockDim.x;

  tmp  = cuCmul( m(0,0), cuMinor(m, 1, 2, 3, 1, 2, 3) );
  res  = tmp;

  tmp  = cuCmul( m(0,1), cuMinor(m, 1, 2, 3, 0, 2, 3) );
  res  = cuCsub( res, tmp);

  tmp  = cuCmul( m(0,2), cuMinor(m, 1, 2, 3, 0, 1, 3) );
  res  = cuCadd( res, tmp);

  tmp  = cuCmul( m(0,3), cuMinor(m, 1, 2, 3, 0, 1, 2) );
  res  = cuCsub( res, tmp);

#undef m
  return res;  
}


//Calculates the DOT product of 4x4 matrix "a" with 4x4 matrix b
__device__ static __inline__ void cuMAPDOT4(cuDoubleComplex *a, cuDoubleComplex *b, cuDoubleComplex *res){

#define b(iwave,j)      b[  idx + scanLen_* ( blockIdx.y + numthick_*    ( (iwave) + 4 * (j) )  ) ]
#define a(iwave,j)      a[  idx + scanLen_* ( blockIdx.y + 1 + numthick_*    ( (iwave) + 4 * (j) )  ) ]
#define res(j,iwave)    res[idx + scanLen_* ( blockIdx.y + (numthick_-1)*( (iwave) + 4 * (j) )  ) ]

  int idx = threadIdx.x + blockIdx.x*blockDim.x;

  for(int ri=0;ri<4;ri++){
    for(int iw=0;iw<4;iw++){
      res(iw,ri) = make_cuDoubleComplex(0.0,0.0);
      for(int j=0;j<4;j++){
        //res(iw,ri) = cuCadd ( res(iw,ri) , cuCmul( a(j,ri) , b(iw,j) ) );
        res(iw,ri) = cuCadd ( res(iw,ri) , cuCmul( b(iw,j) , a(j,ri) ) );        
      }
    }
  }

#undef a
#undef b
#undef res
  return;
}


//Calculates the INV of a 4x4 matrix contained in "in".
__device__ static __inline__ void cuMAPINV4(cuDoubleComplex *in, cuDoubleComplex *out){

#define in(iwave,j)      in[ idx + scanLen_* ( blockIdx.y + numthick_* ( (iwave) + 4 * (j) )  ) ]
#define out(j,iwave)    out[ idx + scanLen_* ( blockIdx.y + numthick_* ( (iwave) + 4 * (j) )  ) ]

  int idx = threadIdx.x + blockIdx.x*blockDim.x;

  cuDoubleComplex det =  cuDet(in);

  out(0,0) = cuCdiv( cuMinor(     in, 1, 2, 3,  1, 2, 3), det);
  out(1,0) = cuCdiv( negC(cuMinor(in, 1, 2, 3,  0, 2, 3)), det);
  out(2,0) = cuCdiv( cuMinor(     in, 1, 2, 3,  0, 1, 3), det);
  out(3,0) = cuCdiv( negC(cuMinor(in, 1, 2, 3,  0, 1, 2)), det);

  out(0,1) = cuCdiv( negC(cuMinor(in, 0, 2, 3,  1, 2, 3)), det);
  out(1,1) = cuCdiv( cuMinor(     in, 0, 2, 3,  0, 2, 3), det);
  out(2,1) = cuCdiv( negC(cuMinor(in, 0, 2, 3,  0, 1, 3)), det);
  out(3,1) = cuCdiv( cuMinor(     in, 0, 2, 3,  0, 1, 2), det);


  out(0,2) = cuCdiv( cuMinor(     in, 0, 1, 3,  1, 2, 3), det);
  out(1,2) = cuCdiv( negC(cuMinor(in, 0, 1, 3,  0, 2, 3)), det);
  out(2,2) = cuCdiv( cuMinor(     in, 0, 1, 3,  0, 1, 3), det);
  out(3,2) = cuCdiv( negC(cuMinor(in, 0, 1, 3,  0, 1, 2)), det);

  out(0,3) = cuCdiv( negC(cuMinor(in, 0, 1, 2,  1, 2, 3)), det);
  out(1,3) = cuCdiv( cuMinor(     in, 0, 1, 2,  0, 2, 3), det);
  out(2,3) = cuCdiv( negC(cuMinor(in, 0, 1, 2,  0, 1, 3)), det);
  out(3,3) = cuCdiv( cuMinor(     in, 0, 1, 2,  0, 1, 2), det);

#undef in
#undef out
  return;
}


//-------------Kernels
#define dP(i) d_Propagations[idx + scanLen_*( (i) + 4* (blockIdx.y) )]
__global__ void cuPropagation(cuDoubleComplex *d_Propagations, cuDoubleComplex *d_Kz, double * d_K0){

//   """ the propagations matrices are diagonal matrices obtained from
//       knowledge of Kz eigenvalues.
//       The return value is a list of NW X 4 arrays.
//       Where is dimension having lenght=4
//       correspond just the diagonal.
//       (NW X4 X 4 would be useless )
//   """
  int idx = threadIdx.x + blockIdx.x*blockDim.x;
  int index;
  int whichlayer = wheretolook[blockIdx.y];
  cuDoubleComplex t;

  t = make_cuDoubleComplex(0.0, Thickness[blockIdx.y]);

  //Protect from overflow of global memory
  if(idx < scanLen_){
    //Kz is already on the GPU at this point and it is already Transposed, as in the GPU version
    // it is calculated that way!
    for(int i=0;i<4;i++){
      index= idx + scanLen_*(i + 4*( whichlayer ) );
      dP(i) = cuCRmul( d_Kz[ index ], d_K0[idx]);
      dP(i) = cuCexp (cuCmul( dP(i), t) );
    }
  }
}
#undef dP

#define d_E(j,k)     d_E[ idx + scanLen_*( (j) + 4* ( (k) + 3 * wla ) )]
#define d_B(j,k)     d_B[ idx + scanLen_*( (j) + 4* ( (k) + 3 * wla ) )]
//Swapped iwave with j to do on the fly axes swap
#define d_C(iwave,j)     d_Conditions[ idx + scanLen_* ( blockIdx.y + numthick_* ( (iwave) + 4 * (j) )  ) ]
#define d_Ci(iwave,j)    d_Conditions_inv[ idx + scanLen_* ( blockIdx.y + numthick_* ( (iwave) + 4 * (j) )  ) ]
__global__ void getConditions(cuDoubleComplex *d_E, cuDoubleComplex *d_B, cuDoubleComplex *d_Conditions,
                              cuDoubleComplex *d_Conditions_inv){

  int idx = threadIdx.x + blockIdx.x*blockDim.x;
  int wla = wheretolook[blockIdx.y];
  //Protect from overflow of global memory
  if(idx < scanLen_) {
    //In GPU iwaves,k subarray is already transposed so we copy directly:
    for(int iwave=0;iwave<4;iwave++){
      d_C(iwave,0) = d_E(iwave,0);
      d_C(iwave,1) = d_E(iwave,1);
      d_C(iwave,2) = d_B(iwave,0);
      d_C(iwave,3) = d_B(iwave,1);
    }
  //Now we invert conditions and save it to conditions_inv
  //Warning, this kernel may look small, but it invokes many inlined device functions. that is the reason of this split
  cuMAPINV4(d_Conditions,d_Conditions_inv);
  }

}
#undef d_E
#undef d_B
#undef d_C
#undef d_Ci

//Calculate the interfaces. Here the gridDim.y is numthick-1.
#define di(layer,j,i) d_Interfaces[idx + scanLen_*( (layer) + (numthick_-1)* ( (i) + 4* (j) ) )]
__global__ void cuInterfaces(cuDoubleComplex *d_Kz, cuDoubleComplex *d_Conditions,
                              cuDoubleComplex *d_Conditions_inv, double *d_K0, cuDoubleComplex *d_Interfaces){

//  """ this routine is in charge of creating
//      an array of matrices representing
//      the interfaces.
//  """

  int idx = threadIdx.x + blockIdx.x*blockDim.x;
  int wla0,wla1;
  cuDoubleComplex dw,toapp_inset,tmp;

  wla0 = wheretolook[blockIdx.y];
  wla1 = wheretolook[blockIdx.y+1];

  //Protect from overflow of global memory
  if(idx < scanLen_) {
    cuMAPDOT4( d_Conditions_inv, d_Conditions, d_Interfaces);
 
  //And now the roughness:
//     for k1 in range(4):
//       for k2 in range(4):
//         toapp_inset = toapp[:,k1,k2] #1000 elements. -> SCALAR here on a 1000 loop. same for the rest!
//         dw = K0*Roughness[i]*( eigenstuff[i][1][:,k2] - eigenstuff[i+1][1][:,k1] );
//         Numeric.multiply(toapp_inset, Numeric.exp(  -dw* (  Numeric.conjugate(dw) ) /2.), toapp_inset)
//     res.append( toapp )
    for (int k1=0;k1<4;k1++){
      for(int k2=0;k2<4;k2++){
        toapp_inset = di(blockIdx.y,k1,k2);
        tmp = cuCsub( d_Kz[idx + scanLen_ *(k1 + 4* wla0 )] , d_Kz[idx + scanLen_ *(k2 + 4* wla1 )] );
        dw  = cuCmul( make_cuDoubleComplex(d_K0[idx] * Roughness[(blockIdx.y)],0.0) , tmp);

        tmp = cuCmul( negC(dw) , cuCRdiv( cuConj(dw) , 2.0 ) );
        tmp = cuCexp( tmp );
        di(blockIdx.y,k1,k2) = cuCmul( toapp_inset, tmp);
      }
    }
  }
}
#undef di

//Calculate the reflectivity for all the layers and the substrate
//The layers here are sequential as the formula is recursive! Mind that gridDim.y MUST be 0 when invoking this kernel!!!
__global__ void cuReflectivity(cuDoubleComplex *d_Reflectivity,cuDoubleComplex *d_Interfaces, cuDoubleComplex *d_Propagations, 
                               cuDoubleComplex *d_propa)
{

#define dI(i,j) d_Interfaces[idx + scanLen_*( (0) + (numthick_-1)* ( (i) + 4* (j) ) )]
#define di(layer,i,j) d_Interfaces[idx + scanLen_*( (layer) + (numthick_-1)* ( (i) + 4* (j) ) )]
#define dP(layer,i) d_Propagations[idx + scanLen_*( (i) + 4*(layer) )]
#define dp(i,j) d_propa[idx + scanLen_*(  (i) + 4* (j) )]
#define dR(i,j) d_Reflectivity[idx + scanLen_*( (i) + 2* (j) )]

  cuDoubleComplex det;
  cuDoubleComplex buf[4],buf1[4],res[4],tmp;
  int idx = threadIdx.x + blockIdx.x*blockDim.x;

  //Protect from overflow of global memory
  if(idx < scanLen_){
    //First the substrate
    //For the substrate: res = PPMcoreTens.PPM_MAPDOT( inter[:,0:2,2:4 ]  , PPMcoreTens.PPM_MAPINV(inter[:,2:4,2:4 ])  )

    //Inverse matrix
    det = cuCsub ( cuCmul( dI(2,2) , dI(3,3) ) , cuCmul( dI(3,2) , dI(2,3) ) );
    buf[0] = cuCdiv( dI(3,3) , det ); //00
    buf[1] = negC( cuCdiv( dI(2,3) , det ) ); //01
    buf[2] = negC( cuCdiv( dI(3,2) , det ) ); //10
    buf[3] = cuCdiv( dI(2,2) , det ); //11

    //Dot product
    res[0] = cuCadd( cuCmul( dI(0,2) , buf[0] ) , cuCmul( dI(0,3) , buf[2] ) );//00
    res[1] = cuCadd( cuCmul( dI(0,2) , buf[1] ) , cuCmul( dI(0,3) , buf[3] ) );//01
    res[2] = cuCadd( cuCmul( dI(1,2) , buf[0] ) , cuCmul( dI(1,3) , buf[2] ) );//10
    res[3] = cuCadd( cuCmul( dI(1,2) , buf[1] ) , cuCmul( dI(1,3) , buf[3] ) );//11

    //Now the rest of the layers
    for(int ilayer=1;ilayer<numthick_-1;ilayer++){

      for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
          dp(i,j) = cuCmul( di(ilayer,i,j) , dP(ilayer,j) );
        }
      }

      //we use buf as aR and buf1 as cR
      //aR
      buf[0] = cuCadd( cuCadd( cuCmul( dp(0,0) , res[0] ) , cuCmul( dp(0,1) , res[2] ) ) , dp(0,2) );
      buf[1] = cuCadd( cuCadd( cuCmul( dp(0,0) , res[1] ) , cuCmul( dp(0,1) , res[3] ) ) , dp(0,3) );
      buf[2] = cuCadd( cuCadd( cuCmul( dp(1,0) , res[0] ) , cuCmul( dp(1,1) , res[2] ) ) , dp(1,2) );
      buf[3] = cuCadd( cuCadd( cuCmul( dp(1,0) , res[1] ) , cuCmul( dp(1,1) , res[3] ) ) , dp(1,3) );

      //cR
      buf1[0] = cuCadd( cuCadd( cuCmul( dp(2,0) , res[0] ) , cuCmul( dp(2,1) , res[2] ) ) , dp(2,2) );
      buf1[1] = cuCadd( cuCadd( cuCmul( dp(2,0) , res[1] ) , cuCmul( dp(2,1) , res[3] ) ) , dp(2,3) );
      buf1[2] = cuCadd( cuCadd( cuCmul( dp(3,0) , res[0] ) , cuCmul( dp(3,1) , res[2] ) ) , dp(3,2) );
      buf1[3] = cuCadd( cuCadd( cuCmul( dp(3,0) , res[1] ) , cuCmul( dp(3,1) , res[3] ) ) , dp(3,3) );

      //Inversion of cR
      det    = cuCsub( cuCmul( buf1[0] , buf1[3] ) , cuCmul( buf1[1] , buf1[2] ) );

      tmp     = buf1[0];
      buf1[0] = cuCdiv( buf1[3] , det );
      buf1[1] = negC( cuCdiv( buf1[1] , det ) );
      buf1[2] = negC( cuCdiv( buf1[2] , det ) );
      buf1[3] = cuCdiv(     tmp , det );

      res[0] = cuCadd( cuCmul( buf[0] , buf1[0] ) , cuCmul( buf[1] , buf1[2] ) );
      res[2] = cuCadd( cuCmul( buf[2] , buf1[0] ) , cuCmul( buf[3] , buf1[2] ) );
      res[1] = cuCadd( cuCmul( buf[0] , buf1[1] ) , cuCmul( buf[1] , buf1[3] ) );
      res[3] = cuCadd( cuCmul( buf[2] , buf1[1] ) , cuCmul( buf[3] , buf1[3] ) );

    }//Loop of layers
    dR(0,0) = res[0];
    dR(1,0) = res[2];
    dR(0,1) = res[1];
    dR(1,1) = res[3];
  }
}
