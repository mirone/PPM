

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import string

#Dans general_Tree pour creer le texte a partir d'arbre
class StrObject:
	def __init__(self):
		self.stringObj=''

	def append(self,line):
		self.stringObj+=line
		
	def append_searchRise(self,line, search):
		if search.para is None:
			self.stringObj+=line
		else:
			nlines = string.count(self.stringObj,"\n")
			#if(nlines-1> search.para):
			if(nlines> search.para):
				search.status=("error search point already passed ")
				search.found=0
				raise "error search point already passed "
			self.stringObj+=line
			nlines = string.count(self.stringObj,"\n")
			#if(nlines-1 >  search.para):
			if(nlines >  search.para):
				search.found=1
				raise "Trouve'"
			#elif(nlines-1 ==  search.para):
			elif(nlines ==  search.para):
			     lastlinepos=string.rfind(self.stringObj,"\n")
			     linelen   = len(self.stringObj)-lastlinepos
			     if( linelen>search.col):
				     search.found=1
				     raise "Trouve"
			else:
				pass
	


	def clear(self):
		self.stringObj=''

	def getString(self):
		return self.stringObj
# pour la fonction WhereAmIFValue (recherch et parcour)
class Search:
	""" passe comme argument aux routines recursive
	L'idee est de generer une exception pour sortir de la ricorsion.
	Et de mettre le message dans status
	"""
	def __init__(self):
		self.para=None
		self.col=None
		self.status=None
		self.found=0


'''
class BoolObject:
	def __init__(self):
		self.boolean=1

	def setBoolValue(self,boolVal):
		self.boolean=boolVal

	def getBoolValue(self):
		return self.boolean
'''
# Dans le type DictObject et dans le listd'information pour la copy spec.(Tree_App)
class Element:
	def __init__(self,object,flague):
		self.boolField=flague #comme bool type
		self.object=object # objet de n'importe quel type

	def isTrue(self):
		return self.boolField

	def getObect(self):
		return self.object

# dans general_Tree pour les dict_link
class DictObject:
	def __init__(self):
		self.linkKnotView={}
		self.countLines=0
	def clear(self):
		self.linkKnotView.clear()
		self.countLines=0

	def append(self,object,flague=1):
		#creation d'un Element
		element=Element(object,flague)
		self.linkKnotView[self.countLines]=element

	def CountPlusOne(self):
		self.countLines+=1

	def getObject(self,key):
		#get object d'un element
		return self.linkKnotView[key].getObect()

	def has_key(self,key):
		return self.linkKnotView.has_key(key)

	def keys(self):
		return self.linkKnotView.keys()

## 	'''
## 	# elle servait pour recuperer le cle a partir d'un objet -> getKey
## 	def values(self):
## 		#on prend touts les objets
## 		keys=self.linkKnotView.keys()
## 		listObj=[]
## 		for key in keys :
## 			listObj.append(self.linkKnotView[key].getObect())
## 		return listObj
## 		#return self.linkKnotView.values()
## 	'''

	#return le marqueur du debut de la tag
	def isTagOpende(self,key):
		return self.linkKnotView[key].isTrue()

	def getKey(self,object):
		keys_list=self.keys()
		key=-1
		for key in keys_list:
			if (self.getObject(key)==object):
				break
		if key==-1:
			return -1
		if(self.getObject(key)!=object):
			key=-1
		return key

#le dictionaire des listes (pour garder les enfants possible par example)
class DictOfList:
	def __init__(self):
		self.dictOfList={}

	def append(self,key,name):
		if not self.dictOfList.has_key(key):
			self.dictOfList[key]=[]
		valueList=self.dictOfList[key]
		if (valueList.count(name)==0):
			valueList.append(name)

	def values(self,key):
		res=[]
		if self.dictOfList.has_key(key):
			res=self.dictOfList[key]
		return res
	# on n'utilise plus pour instin
	def getStrOfList(self, key):
		strValue=''
		if self.dictOfList.has_key(key):
			valueList=self.dictOfList[key]
			for value in valueList:
				strValue+=value+'\n'
		return strValue

	#pour construire le dict par le main
	def items(self):
		return self.dictOfList.items()


# pour la liste des objets sur les quels il y a des references et pour garder les changemnet sucsessive
class ListObject:
	def __init__(self):
		self.listObject=[]

	def append(self,object):
		self.listObject.append(object)

	def internRef(self,variables):
		i=0
		noRef=1
		while(i<len(self.listObject) and noRef):
			tag=self.listObject[i].tag
			name=self.listObject[i].name
			nbReference=variables.getNbReference(name)

			if (nbReference!=0):noRef=0
			i+=1
		return noRef

	def length(self):
		return len (self.listObject)

	def clear(self):
		self.listObject=[]

	#***********************pour les changement********************

	def delThisText(self):
		del self.listObject[-1]

	def getOldText(self):
		res=''
		if (len(self.listObject)!=0):
			res=self.listObject[-1]
		return res


	'''
	def ReconstructionPoint(self,variables):
		for object in self.listPoint:
			tag=object.pointer.tag
			name=object.pointer.name
			variables.CountPlusOne(tag,name)'''



