
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <complex>
#include <CL/opencl.h>

#if defined (_WIN32) && defined(_DLLEXPORT)
#define __w32dll_compat __declspec(dllexport)
#else 
#define __w32dll_compat 
#endif

using namespace std;

/**
 * \brief clProgagation class is responsible for handling propagation kernels.
 */
class __w32dll_compat clPropagation{

public:
  clPropagation();
  ~clPropagation();
  void Propagations(int&scanLen, int&numthick,ocl_config_type *oclconfig);
  void getPropagationsResults(int&scanLen, int&numthick, cl_mem d_Prop,complex<double> *Prop,ocl_config_type *oclconfig);

  /**
   * Internal time counter in ms. It holds the total execution time of Progagations
   */
  float clPropagation_GPU_time;
};

/**
 * \brief clInterfaces class is responsible for handling interfaces kernels.
 */
//getResults returns the results of Interfaces
class __w32dll_compat clInterfaces{

public:
  clInterfaces();
  ~clInterfaces();
  void Interfaces(int&scanLen, int&numthick,ocl_config_type *oclconfig);
  void getInterfacesResults(int&scanLen, int&numthick, cl_mem d_Interfaces, complex<double> *Interfaces,cl_mem d_Conditions,
                                cl_mem d_Conditions_inv, complex<double> *Conditions, complex<double> *Conditions_inv,ocl_config_type *oclconfig);

  /**
   * Internal time counter in ms. It holds the total execution time of Interfaces
   */
  float clInterfaces_GPU_time;
};

/**
 * \brief clReflectivity class is responsible for handling reflectivity kernels and returning the final result.
 */
//getReflResults returns the results of clPPM
class __w32dll_compat clReflectivity{

public:
  clReflectivity();
  ~clReflectivity();
  void Reflectivity(int&scanLen, ocl_config_type *oclconfig);

  void getReflResults(int&scanLen, cl_mem d_Reflectivity, complex<double> *Reflectivity,ocl_config_type *oclconfig);

  /**
   * Internal time counter in ms. It holds the total execution time of Reflectivity
   */  
  float clReflectivity_GPU_time;
};