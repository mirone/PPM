

#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import string
import sys
from qt import *
from xml.sax import ContentHandler
from Testargument import testIfMathExp,testIfName
from Variable_reference import Variable_reference

#liste des variables utilisees
variables=Variable_reference()


def isFloat(string):
     is_float=1
     try:
         float(string)
     except:
         is_float=0
     return is_float
#------------------------parcour d'arbre, utillise pour le test---------------------------------------
def afficher_variable(variable):
	if (variable.pointer==None):
		if (variable.name!=''): print variable.name
		for val in variable.value:
			print val
	else:
		print variable.pointer.name
def parcourt(elem):
	print elem.tag
	if (elem.pointer==None):
		if len(elem.name)!=0 :print elem.name
	else:
		print elem.pointer.name
	if len(elem.items())!=0:print elem.keys()[0],elem.attrib[elem.keys()[0]]
	list_child=elem.getchildren()
	for child in list_child:
		if (child.toend):
			print child.tag
			if (child.tag=='f' and len(child.key)!=0): print child.key
			if len(child.items())!=0: print child.items()
			afficher_variable(child.getchildren()[0])
			print child.tag
		else:
			parcourt(child)
	print elem.tag

#-------------------------------Les classes----------------------------------
class General_object (QListViewItem):
	def __init__(self,parent,after,name,attrs):
		#print attrs.keys()
		self.tag=name
		self.attrib=attrs
		self.name=''
		self.pointer=None
		self.toend=0
		QListViewItem.__init__(self,parent,after)

	def process_startElement(self,name,attrs):
		pass

	def process_endElement(self, name):
		pass

	def process_characters(self,ch):
		line=''
		for c in ch:
          		if( (c not   in " \n\t") ):
             			line=line+c
		if (len(line)!=0):
			res=variables.getPointer(line)
			if (res!=None):
				self.pointer=res
				#print self.pointer.name
				self.setText(0,self.tag+'  '+self.pointer.name)
			else:
				self.name=line
				variables.append(self)
				#print self.name
				#print self.tag+' '+self.name
				self.setText(0,self.tag+'  '+self.name)
	def write_xml_text(self,file_xml):
		pass


	def write_xml_block(self,file_xml):
		line_attr=' '
		for key in self.attrib.keys():
			line_attr+=key+'="'+ self.attrib[key]+'" '
		if (self.pointer!=None):
			file_xml.write('<'+self.tag+line_attr+'>'+self.pointer.name+'\n')
		else:
			if (self.toend):# element est terminal
				file_xml.write('<'+self.tag+line_attr+'>'+self.name)
				self.write_xml_text(file_xml)
			else:#pas terminal
				file_xml.write('<'+self.tag+line_attr+'>'+self.name+'\n')
				#le boucle selon des enfants
				child=self.firstChild()
				while(child!=None):
					child.write_xml_block(file_xml)
					child=child.nextSibling()
		file_xml.write('</'+self.tag+'>'+'\n')

#************Class Variable_object

class Variable_object(General_object):
	def __init__(self,parent,after):
		General_object.__init__(self,parent,after,'text',{})
		self.value=[]

#************Class Float_object
class Float_object(General_object):
	def __init__(self,*args):
		apply(General_object.__init__,(self,)+ args)
		self.toend=1 #???
		self.key='' #?????

	def process_characters(self,ch):
        	chl = string.split(ch)
        	# print ch
        	Nfloat=0
        	#pos_float=None
        	for c in chl:
          		if(isFloat(c)):
	      			Nfloat=Nfloat+1
		vo = Variable_object(self, self)
		vo.setText(0,ch)
  	 	for token in chl: # parcour mot par mot du text entre balise
             		if( isFloat(token)==0): #si le mot n'est pas numerique
                		if(token[0]!="\""): #si ce n'est pas le cle
					if (Nfloat!=0): #le cas de la definition de variable
						vo.name=token
						variables.append(vo)
						#self.setText(1,vo.name)
					else:# le cas de reference sur une autre variable
						#recherche dans l'arbre
						res=variables.getPointer(token)
						if (res!=None): #on a trouve la reference
							vo.pointer=res
							#self.setText(1,vo.pointer.name)
						else:#pas trouve
							if (testIfName(token) or testIfMathExp(token)):
								vo.value.append(token)
							'''else: #erreur
								print token
								raise 'variable is not defined'''
	       			else:#c'est le cle
					self.key=token #?????
             		else:#on ajoute un chiffre dans la loste des valeurs
				vo.value.append(token)

	def write_xml_text(self,file_xml):
		file_xml.write(self.key+' ')
		child=self.firstChild()
		if (child.pointer!=None):#c'est une reference
			file_xml.write(child.pointer.name)
		else:# c'est un vraie objet
			file_xml.write(child.name+' ')
			for val in child.value:
				file_xml.write(val+' ')

#************Class String_object
class String_object (General_object):
	def __init__(self,*args):
		apply(General_object.__init__,(self,)+args)
		self.toend=1 #???

	def process_characters(self,ch):
		vo = Variable_object(self,self)
		line=''
		for c in ch:
          		if( (c not   in " \n\t") ):
             			line=line+c
		vo.value.append(line)
		vo.setText(0,line)

	def write_xml_text(self,file_xml):
		child=self.firstChild()
		text=child.value[0]
		file_xml.write(text)


#************* class PPMHandler
class TreeView(ContentHandler,QListView):
    classes_to_use = {"f": Float_object, "s": String_object, "layer":General_object,
    			"bff":General_object,"stack":General_object, 							"scan":General_object,"scanlist":General_object,
			"ift":General_object, "ifo":General_object,
			"dv":General_object,"KK":General_object,"bff":General_object,
			"bjoin":General_object, "iff":General_object,
			"KK_write":General_object, "tf0":General_object, "tf12":General_object,
			"Beta_write":General_object,"comparison_write":General_object,
			"fit":General_object, "Minimise":General_object ,
			"MinimiseGeFit":General_object , "prova": General_object,
                      "bfc":General_object , "bfl":General_object , "bsum":General_object,
                      "ifos":General_object, "Write_Stack": General_object,
                     "arrofvar":General_object,"addvars": General_object,
                       "bfinterp":General_object,"setLayerInterpolation":General_object}

    def __init__(self,*args):
   	apply(QListView.__init__,(self,)+args)
        self.processors_list=[]
	self.precedent_list=[]
	self.root=None
	self.setSorting(-1,-1)
	self.setRootIsDecorated(1)
	self.addColumn("Element")
	#self.addColumn("Name")

    def startElement(self, name, attrs):
        if name=="system": #return
		self.root=General_object(self,None,name,attrs)
		self.root.setText(0,name)
		self.precedent_list.append(self.root)
		self.processors_list.append(self.root)
		return
        parent=None
	after=None
	parent=self.processors_list[-1]
	after=self.precedent_list[-1]
	diff=after.depth()-parent.depth()
	if (diff==0):
		after=parent
	elif (diff> 1):
		i=0
		while (i< diff-1):
			del self.precedent_list[-1]
			i+=1
		after=self.precedent_list[-1]
        knot = self.classes_to_use[name](parent,after,name,attrs)
	if (after==parent):
		self.precedent_list.append(knot)
	else:
		self.precedent_list[-1]=knot
	knot.setText(0,name)
	self.processors_list.append(knot)

    def endElement(self, name):
	del self.processors_list[-1]

    def characters(self, ch):
        if self.processors_list==[]: return
	self.processors_list[-1].process_characters(ch)




from xml.sax import make_parser
from xml.sax.handler import feature_namespaces

if __name__ == '__main__':
    #Creat application
    app_Interface=QApplication(sys.argv)
    QObject.connect(app_Interface,SIGNAL('lastWindowClosed()'),app_Interface,SLOT('quit()'))
    # Create a parser
    parser = make_parser()

    # Tell the parser we are not interested in XML namespaces
    parser.setFeature(feature_namespaces, 0)

    # Create the handler
    tree = TreeView()

    #Liaison avec application
    app_Interface.setMainWidget(tree)

    # Tell the parser to use our handler
    parser.setContentHandler(tree)


    # Parse the input
    parser.parse(sys.argv[1])

    #Visualisation
    tree.show()
    #parcourt(dh.root)
    file_name='res.xml'
    f=open(file_name,'w')
    tree.root.write_xml_block(f)
    #print len(dh.root.getchildren())
    f.close()
    app_Interface.exec_loop()
