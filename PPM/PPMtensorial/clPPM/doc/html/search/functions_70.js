var searchData=
[
  ['pfn_5fnotify',['pfn_notify',['../ocl__tools_8cc.html#ae9af4c1f7479f59a1f7f0a213d9cbd2e',1,'pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data):&#160;ocl_tools.cc'],['../ocl__tools_8h.html#ae9af4c1f7479f59a1f7f0a213d9cbd2e',1,'pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data):&#160;ocl_tools.cc']]],
  ['ppmtensorial_5focl',['PPMTensorial_ocl',['../class_p_p_m_tensorial__ocl.html#ac986258e7a80b7dff46d0e89daa5f89f',1,'PPMTensorial_ocl::PPMTensorial_ocl()'],['../class_p_p_m_tensorial__ocl.html#a7721d18c461f7bb2305d9d31def34e95',1,'PPMTensorial_ocl::PPMTensorial_ocl(const char *fname)']]],
  ['profiler',['profiler',['../timer_8h.html#a19192fb5aba5a795c294c5de705437ea',1,'timer.h']]],
  ['propagations',['Propagations',['../classcl_propagation.html#a1bb91398e6c9255904fd34adfc57f981',1,'clPropagation']]]
];
