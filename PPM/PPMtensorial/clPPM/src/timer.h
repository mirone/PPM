
/**
 * \file
 * \brief Header-source C profiler
 */

#include <stdio.h>
#include <string.h>

#ifdef __linux
#include <sys/time.h>
#endif
#ifdef _WIN32
#include <windows.h>
#define WIN32_LEAN_AND_MEAN
#ifndef _CRT_SECURE_NO_WARNINGS
	#define _CRT_SECURE_NO_WARNINGS
#endif
#pragma comment(lib,"winmm.lib")
#endif

/**
 * \brief Profiler struct
 */
typedef struct{

#ifdef _WIN32
  double start;
  double end;
#elif defined(__linux)
  timeval s,e;
  double start;
  double end;
#endif

}proftype;



/**
 * \brief Profiler function with millisec precision
 *
 * The profiler function uses the timing functions found in
 * sys/time.h for Linux or windows.h for Windows. Unlike the common
 * clock() function, it has superion precision and can measure
 * runs of few milliseconds.
 *
 * @param T A proftype profiler-struct that holds or will hold the information for
 *                 a specific profiler runs
 * @param mode The type of operation. Namely "START","STOP","PRINT"
 * @param msg An optional message to append when in "PRINT" mode
 *
 * @return Always 0
 */
static int profiler(proftype &T, const char *mode, const char *msg=NULL){

  //Windows timeGetTime resolution is about 5ms but can be adjusted by timeBeginPeriod/timeEndPeriod
  //Linux gettimeofday resolution is in millisec scale. 
 if(!strcmp(mode,"START")){
#ifdef _WIN32
  T.start=(double)timeGetTime();
  T.start*=1000.; //convert to usec
#elif defined(__linux)
   gettimeofday(&(T.s), 0);
   T.start = (double)(T.s.tv_sec)*1e6 + (double)(T.s.tv_usec);
#endif
 }else if(!strcmp(mode,"STOP")){
#ifdef _WIN32
   T.end=(double)timeGetTime();
   T.end*=1000.; //convert to usec
#elif defined(__linux)
   gettimeofday(&(T.e),0);
   T.end = (double)(T.e.tv_sec)*1e6 + (double)(T.e.tv_usec);
#endif
 }else if(!strcmp(mode,"PRINT")){
   if(msg)printf("%s: %f (ms) %f (s)\n",msg,((T.end - T.start))*1e-3,((T.end - T.start))*1e-6);
   else printf("%f (ms) %f (s)\n",((T.end - T.start))*1e-3,((T.end - T.start))*1e-6);
 }
   return 0;
 }
