
# Autoconfiguration add-on for the installation script of cuPPM and clPPM plugins
#   It takes care of OpenCL and CUDA parameters and the creation of the modules

import sys,os
import glob
import platform
import string
import shutil

from distutils.core import Extension, setup
from distutils.command.install_data import install_data

try:
    import numpy
except ImportError:
    text  = "You must have numpy installed.\n"
    text += "See http://sourceforge.net/project/showfiles.php?group_id=1369&package_id=175103\n"
    raise ImportError, text

import distutils.sysconfig

if sys.platform=="win32":
    import _winreg

jn = os.sep.join

packages = ['PPM.PPMtensorial.cuPPM','PPM.PPMtensorial.clPPM']

Common_libraries = 'lib'

if sys.platform == "win32":
    define_macros = [('WIN32',None)]
    script_files = []
else:
    define_macros = [(None,)]

class smart_install_data(install_data):
    def run(self):
        global PPM_INSTALL_DIR
        #need to change self.install_dir to the library dir
        install_cmd = self.get_finalized_command('install')
        self.install_dir = getattr(install_cmd, 'install_lib')
        PPM_INSTALL_DIR = self.install_dir
        print "PPM to be installed in %s" %  self.install_dir
        return install_data.run(self)


def get_build_path():
    build_path='build'
    mach = platform.machine()
    pyver= platform.python_version_tuple()[0] + '.' + platform.python_version_tuple()[1]
    if sys.platform == 'linux2':
        build_path=jn([build_path ,'lib.' + 'linux-' + mach + '-' + pyver ])
    elif sys.platform == 'win32':
        build_path=jn([build_path ,'lib.' + sys.platform + '-' + pyver])
    else:
        raise Exception, "Currently only Linux2 and Win32 platforms are supported: %s" % tuple([sys.platform])
    return build_path

def get_vcvarsall_vc9():
    if sys.platform=="win32":
        value = None
        type = None
        key_name = r'SOFTWARE\Microsoft\VisualStudio\SxS\VC7'
        key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, key_name)
        value,type = _winreg.QueryValueEx(key, '9.0')
        vcvarpath = value + 'vcvarsall.bat'
        if not os.path.exists(vcvarpath):
            raise Exception, "'%s' is missing." %tuple([vcvarpath])
    else:
        raise Exception, "get_vcvarsall_vc9 should only be used in Windows"
    return vcvarpath

def get_latest_CUDA_linux(CUDA_PATH):
    if not os.path.islink(CUDA_PATH):
        return CUDA_PATH
    else:
        max_was_Set=False #Signal that a max value was encountered at least once
        max_version=0.0
        alt_paths=glob.glob(CUDA_PATH + '-*')     # Get all the CUDA installs
        for icuda in range(len(alt_paths)):
            #The path is /usr/local/cuda-x.x.xx
            #We split and take the 4th (1st is the "", second "local" and fourth "cuda-x.x.xx")
            #We take the x.x.xx and keep only the first three
            cur_version = alt_paths[icuda].split(os.sep)[3][5:][:3]
            #And then we convert it to float!
            cur_version = float(cur_version)
            if cur_version >= max_version:
                max_version = cur_version
                CUDA_PATH = alt_paths[icuda]
                max_was_set=True
        if not max_was_set:
            raise Exception, "Something went wrong, /usr/local/cuda is a symlink but I could not find the best version"
    return CUDA_PATH

def get_OpenCL_ICD_list(allow_no_ICD):
    ICD_vendors=[]
    ICD_LIST=[]
    NO_ICD = False

    if sys.platform=="linux2":
        if os.path.exists('/etc/OpenCL/vendor'):
            ICD_LIST = glob.glob('/etc/OpenCL/vendors/*.icd')
            print "ICD LIST: ", ICD_LIST
            if not ICD_LIST:
                NO_ICD=True
                if not allow_no_ICD:
                    raise Exception, "No OpenCL vendor ICD registered"
            else:
                for iicd in range(len(ICD_LIST)):
                    ICD_vendors.append( (os.path.splitext(ICD_LIST[iicd])[0]).split(os.sep)[4] )

    elif sys.platform=="win32":
        value = None
        type = None
        key_name = r'SOFTWARE\Khronos\OpenCL\Vendors'
        key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, key_name)

        end_reached=False
        counter=0
        while not end_reached:
            try:
                tmp=_winreg.EnumValue(key,counter)
            except:
                end_reached=True
            if not end_reached:
                ICD_LIST.append(tmp[0])
                ICD_vendors.append(os.path.splitext(tmp[0])[0])
                counter = counter + 1
        if not counter:
                NO_ICD=True
                if not allow_no_ICD:
                    raise Exception, "No OpenCL vendor ICD registered"

    else:
        raise Exception, "Currently only Linux2 and Win32 platforms are supported: %s" % tuple([sys.platform])
    return NO_ICD,ICD_LIST,ICD_vendors

def get_CUDA_path():
    CUDA_LD_PATH=None
    CUDA_INC=None
    CUDA_PATH=None
    #Default CUDA Toolkit environment var
    if os.getenv('CUDA_PATH'):
        if sys.platform=='win32':
            CUDA_PATH=os.environ['CUDA_PATH']
            CUDA_LD_PATH = os.environ['CUDA_LIB_PATH'] #Valid in Windows
            CUDA_INC = os.environ['CUDA_INC_PATH']
        else:
            CUDA_PATH = get_latest_CUDA_linux(os.getenv('CUDA_PATH'))
            CUDA_INC = jn([CUDA_PATH,'include'])
            if platform.architecture()[0]=='32bit':
                CUDA_LD_PATH = jn([CUDA_PATH,'lib'])
            else:
                CUDA_LD_PATH = jn([CUDA_PATH,'lib64'])

    #Deprecated CUDA environment var
    elif os.getenv('CUDADIR'):
        CUDA_PATH = os.environ['CUDADIR']
        CUDA_INC = jn([CUDA_PATH,'include'])
        if platform.architecture()[0]=='32bit':
            if sys.platform=='win32':
                CUDA_LD_PATH = jn([CUDA_PATH,'lib','Win32'])
            else:
                CUDA_LD_PATH = jn([CUDA_PATH,'lib'])
        else:
            if sys.platform=='Win32':
                raise Exception, 'Do not Know YET where CUDA stores libs in 64bit windows'
            else:
                CUDA_LD_PATH = jn([CUDA_PATH,'lib64'])
    #Standard Linux installation path
    elif os.path.exists('/usr/local/cuda') and sys.platform!="win32":
        CUDA_PATH = get_latest_CUDA_linux('/usr/local/cuda')
        CUDA_INC  = jn([CUDA_PATH,'include'])
        if platform.architecture()[0]=='32bit':
            CUDA_LD_PATH = jn([CUDA_PATH,'lib'])
        else:
            #64bit libraries can be in lib64 or lib
            if os.path.exists(jn([CUDA_PATH,'lib64'])):
                CUDA_LD_PATH = jn([CUDA_PATH,'lib64'])
            else:
                CUDA_LD_PATH = jn([CUDA_PATH,'lib'])
    else:
        raise Exception, "Could not find a path of a CUDA installation"
    return CUDA_PATH,CUDA_INC,CUDA_LD_PATH                

def get_OpenCL_paths():
    #If we know we have at least one library registered on Linux we can go check the Paths.
    #Default Paths: CUDA (CUDA_PATH), AMD (AMDAPPSDKROOT), INTEL (INTELOCLSDKROOT). These are always defines in Windows, but not
    # necessarily on Linux
    #For Linux we only need these paths to get a valid include directory as libOpenCL is always in the system libraries
    # only the vendor specific drivers are in the local vendor folder.
    # But For Windows they both reside in each vendors folder, so we need to set that path too.
    #Library path is trickier. Vendors use different schemes. CUDA Lin(lib,lib64) CUDA Win(lib/Win32,lib/Win64) AMD (lib/x86,lib/x86_64) INTEL(lib/x86,lib/x86_64)

    OCL_PATH=None
    OCL_INC=None
    OCL_LD_PATH=None
    exception_string="get_OpenCL_paths could not find a valid OpenCL path"

    CUDA_found=True
    #CUDA
    try:
        OCL_PATH,OCL_INC,OCL_LD_PATH=get_CUDA_path()
    except:
        CUDA_found=False      
        pass

    if CUDA_found:
        return OCL_PATH,OCL_INC,OCL_LD_PATH
    else:    
      #Standard AMD APP environment var
      if os.getenv('AMDAPPSDKROOT'):
          OCL_PATH = os.environ['AMDAPPSDKROOT']
          OCL_INC = jn([OCL_PATH,'include'])
          if platform.architecture()[0]=='32bit':
              OCL_LD_PATH = jn([OCL_PATH,'lib','x86'])
          else:
              OCL_LD_PATH = jn([OCL_PATH,'lib','x86_64'])

      #Typical installation path for AMD APP 2.5 and 2.6 on Linux
      elif os.path.exists('/opt/AMDAPP') and sys.platform!="win32":
          OCL_PATH = '/opt/AMDAPP' #Linux only
          OCL_INC = jn([OCL_PATH,'include'])
          if platform.architecture()[0]=='32bit':
              OCL_LD_PATH = jn([OCL_PATH,'lib','x86'])
          else:
              OCL_LD_PATH = jn([OCL_PATH,'lib','x86_64'])

      #Standart Intel OpenCL environment var
      elif os.getenv('INTELOCLSDKROOT'):
          OCL_PATH = os.environ['INTELOCLSDKROOT']
          OCL_INC = jn([OCL_PATH,'include'])
          if platform.architecture()[0]=='32bit':
              OCL_LD_PATH = jn([OCL_PATH,'lib','x86'])
          else:
              OCL_LD_PATH = jn([OCL_PATH,'lib','x86_64'])

      #Try some common Linux paths if everything fails
      elif os.path.isfile('/usr/lib/libOpenCL.so') and sys.platform!="win32":
          OCL_PATH ='/usr'
          if os.path.exists(jn([OCL_PATH,'include','CL'])):
              OCL_INC = jn([OCL_PATH,'include'])
              OCL_LD_PATH = jn([OCL_PATH,'lib'])
          else:
              raise Exception, exception_string

      elif os.path.isfile('/usr/lib64/libOpenCL.so') and sys.platform!="win32":
          OCL_PATH ='/usr'
          if os.path.exists(jn([OCL_PATH,'include','CL'])):
              OCL_INC = jn([OCL_PATH,'include'])
              OCL_LD_PATH = jn([OCL_PATH,'lib64'])
          else:
              raise Exception, exception_string

    return OCL_PATH,OCL_INC,OCL_LD_PATH


def cuppm_dynamic(internal_clibraries,arch,Xptxas_v_on):

    if not os.path.exists(Common_libraries):
        os.mkdir(Common_libraries)
        
    if sys.platform=='win32':
        compilation_string="""vcbuild /nologo /useenv /showenv PPM\PPMtensorial\cuPPM\sln\cuPPM.vcproj \"Release|Win32\" """
        print " CUPPM going to compile with the command "
        print compilation_string
        os.system(compilation_string)
        if os.path.isfile("PPM\PPMTensorial\cuPPM\sln\Release\cuPPM.lib") and os.path.isfile("PPM\PPMTensorial\cuPPM\sln\Release\cuPPM.dll"):
          shutil.copy("PPM\PPMTensorial\cuPPM\sln\Release\cuPPM.lib",jn([Common_libraries , "cuPPM.lib"]))
          shutil.copy("PPM\PPMTensorial\cuPPM\sln\Release\cuPPM.dll",jn([Common_libraries , "cuPPM.dll"]))
          assert(os.path.isfile(jn([Common_libraries , "cuPPM.lib"])))
          assert(os.path.isfile(jn([Common_libraries , "cuPPM.dll"])))
          internal_clibraries.append(jn([Common_libraries , "cuPPM.lib"]))
          internal_clibraries.append(jn([Common_libraries , "cuPPM.dll"]))
        else:
            raise Exception,"Error encountered while trying to build cuPPM.dll and cuPPM.lib"
    else:
        #Check to see if the library exists in the installation path and if it is older than the source
        #If it is older remove it and build. If it does not exist build it.
        docompile=0
        installed_cuppmlib = jn([Common_libraries,'libcuPPM.so'])
        if os.path.isfile(installed_cuppmlib):
            stat_target = os.stat( installed_cuppmlib ).st_mtime
            for cufile in ["cuPPMextension_base.cu", "cuPPMextension_modes.cu",
                           "cuPPMextension_other.cu", "cuPPMkernels_modes.cu", "cuPPMkernels_other.cu"
                           ]:
                stat_dep = os.stat("PPM/PPMtensorial/cuPPM/src/%s"%cufile  ).st_mtime
                print   stat_dep ,   stat_target
                if docompile==0  and stat_dep >   stat_target:
                    os.system("""rm -f %s""" % tuple([installed_cuppmlib]) )
                    assert( not os.path.isfile(installed_cuppmlib ))
                    docompile=1
                else:
                    pass
        else:
            docompile=1

	archs = " %s" * len(arch)
	archs = archs % tuple(arch)

        compilation_string="""
nvcc %s %s --compiler-options -fPIC,-O3 -o %s/libcuPPM.so --shared -I/usr/local/cuda/include %s/cuPPMextension_base.cu %s/cuPPMextension_modes.cu %s/cuPPMextension_other.cu %s/cuPPMkernels_modes.cu %s/cuPPMkernels_other.cu -lcudart
    """ %  tuple( [Xptxas_v_on[0]] + [archs] + [Common_libraries] + ["PPM/PPMtensorial/cuPPM/src/"] *5 )


        if docompile:
            #first check for any left-over library on the building path
            if os.path.isfile(jn([Common_libraries,'libcuPPM.so'])):
                os.system("rm -f %s" % tuple ([jn([Common_libraries,'libcuPPM.so'])]))
                assert( not os.path.isfile(jn([Common_libraries,'libcuPPM.so'])))
            #then compile
            print " CUPPM going to compile with the command "
            print compilation_string
            os.system(compilation_string)
            #if the build failed raise an exception, otherwise append libcuPPM.so to the
            # project libraries
            if os.path.isfile(jn([Common_libraries,'libcuPPM.so'])):
                internal_clibraries.append(jn([Common_libraries,'libcuPPM.so']))
            else:
                raise Exception,"Error encountered while trying to build libcuPPM.so"
    return internal_clibraries

def cuppm_cudafe(arch,Xptxas_v_on,sources):

    CUPPM_PATH = jn(['PPM','PPMtensorial','cuPPM'])
    cufiles = glob.glob(jn([CUPPM_PATH,'src','cuPPMextension_*.cu'])) + glob.glob(jn([CUPPM_PATH,'src','cuPPMkernels_*.cu']))
    cppfiles = glob.glob(jn([CUPPM_PATH,'src','cuPPMextension_*.cu.cpp'])) + glob.glob(jn([CUPPM_PATH,'src','cuPPMkernels_*.cu.cpp']))

    docompile=0
    files_to_process=[]
    if len(cppfiles) == 0:
      files_to_process = cufiles
    else:
      for cufile in cufiles:
        stat_dep = os.stat(cufile).st_mtime
        to_process = False
        found = False
        for cppfile in cppfiles:
          if (cufile + '.cpp') == cppfile:
            found = True
            stat_target = os.stat(cppfile).st_mtime
            if stat_dep > stat_target:
              to_process = True
        if not found or to_process:
          files_to_process.append(cufile)
          

    if len(files_to_process):
        #ingenious trick by Armando(TM)
        src = " %s" * len(files_to_process)
        src = src % tuple(files_to_process)
    	
	archs = " %s" * len(arch)
	archs = archs % tuple(arch)

        preprocess_string="""nvcc -cuda -odir %s %s %s %s"""%  tuple( [jn([CUPPM_PATH,'src'])] + [Xptxas_v_on[0]] + [archs] + [src])
        print " CUPPM going to process cuda files with the command "
        print preprocess_string
        os.system(preprocess_string)

    #With compilation or not check that we have all the cpp files. If not fail
    cppfiles = glob.glob(jn([CUPPM_PATH,'src','cuPPMextension_*.cu.cpp'])) + glob.glob(jn([CUPPM_PATH,'src','cuPPMkernels_*.cu.cpp']))
    print len(cppfiles), len(cufiles)
    if len(cppfiles) == len(cufiles):
        sources  = cppfiles
    else:
        raise Exception,"Error encountered while trying to create CUDAFE preprocessed files."

    return sources

def build_cuppm(ext_modules,internal_clibraries):

    print get_build_path()
    #Fixed -CUBIN- codes for sm13 to 21 and one forward compatible compute20 PTX code
    #Capabilities less than 1.3 are not supported since they lack double precision
    cuda_archs=["-gencode", "arch=compute_30,code=compute_30"                
    ]
    Xptxas_v_on=[" "," -Xptxas=-v"]

    #Options are "CUDAFE" or "DYNAMIC"
    #MODULE_FROM="CUDAFE"
    MODULE_FROM = os.getenv('CUPPM')
    if MODULE_FROM != 'CUDAFE' and MODULE_FROM != 'DYNAMIC':
      print "!!CUPPM build Warning: Options are CUDAFE or DYNAMIC. eg.: CLPPM=CUDAFE python ..."
      MODULE_FROM = 'CUDAFE'
      
    print " Building of cuPPM library is still experimental"
    print " cuPPM module from: ",MODULE_FROM

    CUDA_PATH=None
    CUDA_INC=None
    CUDA_LIB=None
    try:
        CUDA_PATH,CUDA_INC,CUDA_LIB=get_CUDA_path()
    except Exception as error:
        print error
        pass
    
    CUPPM_LIB=[]
    CUPPMsources = []
    CUPPMmacros = []
    try:
        if MODULE_FROM == "CUDAFE":
          CUPPMsources = cuppm_cudafe(cuda_archs,Xptxas_v_on,CUPPMsources)
        elif MODULE_FROM == "DYNAMIC":
          internal_clibraries = cuppm_dynamic(internal_clibraries,cuda_archs,Xptxas_v_on)
          CUPPM_LIB = ["cuPPM"]
          CUPPMmacros.append(("_DLLEXPORT",None))
    except Exception as error:
        print error
        return #Skip the module

    #We set the wrapper
    CUPPMsources.insert (0, jn(['PPM','PPMtensorial','cuPPM','src','cuPPMwrapper.cc']) )

    LINKARGS=[]
    if sys.platform=="win32":
        #Force compilation againt LIBCMT.lib for CUDA to work
        #Disable Warnings - Especially C4996 (Default is /W3, but this warning can also go away with /W2)
        #Enable C++ exceptions
        #If CUDAFE was used, force the creation of a Manifest
        CXXFLAGS=['/MT','/w','/EHsc']
        LINKARGS=['/MANIFEST']
    else:
        CXXFLAGS=[]

    print "-----For the cuPPM Python module:----"
    print " Module from:", MODULE_FROM
    #print " Sources:    ", CUPPMsources
    print " LIB_DIRS:   ", [Common_libraries,CUDA_LIB]
    print " CUPPM_LIB:  ", CUPPM_LIB
    print " CUDA_INC:   ", CUDA_INC
    print " CXXFLAGS:   ", CXXFLAGS
    print " LINKARGS:   ", LINKARGS
    print " CUPPMmacros:", CUPPMmacros
    print "-------------------------------------"
    if CUDA_LIB is not None:
        library_dirs = [Common_libraries,CUDA_LIB]
    else:
        library_dirs = [Common_libraries]
        


    if CUDA_INC is not None:
        include_dirs=[ 'PPM/PPMtensorial/cuPPM/src', numpy.get_include(),CUDA_INC]
    else:
        include_dirs=[ 'PPM/PPMtensorial/cuPPM/src', numpy.get_include()]
        

    module = Extension('PPM.PPMtensorial.cuPPM',
                       sources=CUPPMsources,
                       library_dirs =library_dirs ,
                       libraries= CUPPM_LIB + ["cudart"],
                       include_dirs=include_dirs,
                       extra_compile_args=CXXFLAGS,
                       extra_link_args=LINKARGS,
                       define_macros = CUPPMmacros + define_macros,
                       )
    ext_modules.append(module)


def clppm_dynamic(internal_clibraries,OCL_INC,OCL_LD_PATH,OCL_LIB,OCL_DEF):

    if not os.path.exists(Common_libraries):
        os.mkdir(Common_libraries)
        
    if sys.platform=='win32':

        compilation_string="""vcbuild /nologo /useenv /showenv PPM\PPMtensorial\clPPM\sln\clPPM.vcproj \"Release|Win32\" """
        print " CLPPM going to compile with the command "
        print compilation_string
        os.system(compilation_string)
        if os.path.isfile("PPM\PPMTensorial\clPPM\sln\Release\clPPM.lib") and os.path.isfile("PPM\PPMTensorial\clPPM\sln\Release\clPPM.dll"):
            shutil.copy("PPM\PPMTensorial\clPPM\sln\Release\clPPM.lib",jn([Common_libraries , "clPPM.lib"]))
            shutil.copy("PPM\PPMTensorial\clPPM\sln\Release\clPPM.dll",jn([Common_libraries , "clPPM.dll"]))
            assert(os.path.isfile(jn([Common_libraries , "clPPM.lib"]) ))
            assert(os.path.isfile(jn([Common_libraries , "clPPM.dll"]) ))
            internal_clibraries.append(jn([Common_libraries , "clPPM.lib"]))
            internal_clibraries.append(jn([Common_libraries , "clPPM.dll"]))
        else:
            raise Exception,"Error encountered while trying to build clPPM.dll and clPPM.lib."
    else:
        #Check to see if the library exists in the installation path and if it is older than the source
        #If it is older remove it and build. If it does not exist build it.
        docompile=0
        installed_clppmlib = jn([Common_libraries , "libclPPM.so"])
        if os.path.isfile(installed_clppmlib):
            stat_target = os.stat( installed_clppmlib ).st_mtime
            for clfile in ["clPPMextension_base.cc", "clPPMextension_modes.cc",
                             "clPPMextension_other.cc",
                             "ocl_tools.cc"
                          ]:
                stat_dep = os.stat("PPM/PPMtensorial/clPPM/src/%s"%clfile  ).st_mtime
                print   stat_dep ,   stat_target
                if docompile==0  and stat_dep >   stat_target:
                    os.system("""rm -f %s""" % tuple([installed_clppmlib]) )
                    assert( not os.path.isfile(installed_clppmlib ))
                    docompile=1
                else:
                    pass
        else:
            docompile=1

        ldpath=''
        defines=''
        for ld in range(len(OCL_LD_PATH)):
          ldpath += " -L%s " %tuple([OCL_LD_PATH[ld]])
        for defstr in range(len(OCL_DEF)):
          if OCL_DEF[defstr] != None:
            if OCL_DEF[defstr][1] != None:
              defines += " -D%s %s " %tuple([OCL_DEF[defstr][0]],[ OCL_DEF[defstr][1]  ])
            else:
              defines += " -D%s " %tuple([OCL_DEF[defstr][0]])

        print "ldpath:  ", ldpath
        print "defines: ", defines
        compilation_string="""
g++ -fPIC -O2 -o %s/libclPPM.so --shared %s -I%s %s/clPPMextension_base.cc %s/clPPMextension_modes.cc %s/clPPMextension_other.cc %s/ocl_tools.cc %s -l%s
    """ %  tuple( [Common_libraries] + [defines] + [OCL_INC] + ["PPM/PPMtensorial/clPPM/src"] *4 + [ldpath] + [OCL_LIB] )


        if docompile:
            #first check for any left-over library on the building path
            if os.path.isfile(jn([Common_libraries , "libclPPM.so"])):
                os.system("rm -f %s"% tuple ([jn([Common_libraries , "libclPPM.so"])]))
                assert( not os.path.isfile("libclPPM.so"))
            #then compile
            print " CLPPM going to compile with the command "
            print compilation_string
            os.system(compilation_string)
            #Check that the compilation was successfull, if not raise
            if os.path.isfile(jn([Common_libraries , "libclPPM.so"])):
                internal_clibraries.append( jn([Common_libraries , "libclPPM.so"])  )
            else:
                raise Exception,"Error encountered while trying to build libclPPM.so."
    return internal_clibraries

def clppm_direct(sources):
    #Just append the sources and get out
    CLPPM_PATH = jn(['PPM','PPMtensorial','clPPM'])
    clfiles = glob.glob(jn([CLPPM_PATH,'src','clPPMextension_*.cc'])) + glob.glob(jn([CLPPM_PATH,'src','ocl_tools.cc']))
    sources = clfiles
    return sources

def build_clppm(ext_modules,internal_clibraries):

    #Method to be used for building the module.
    #Options are: "DIRECT" or "DYNAMIC"
#    MODULE_FROM="DIRECT"
    MODULE_FROM = os.getenv('CLPPM')
    if MODULE_FROM != 'DIRECT' and MODULE_FROM != 'DYNAMIC':
      print "!!CLPPM build Warning: Options are DIRECT or DYNAMIC. eg.: CLPPM=DIRECT python ..."
      MODULE_FROM = 'DIRECT'

    #USE this only for testing with specific libraries and only in LINUX.
    #Set the flag autoconfig_ocl to false to be able to link directly to amdocl or intelocl, avoiding
    # libOpenCL and the ICD limitation
    #The following settings, when AUTOCONFIG is FALSE will override any automatic detection of OpenCL and paths
    AUTOCONFIG_OCL = True #True is the default
    #First set CLLB to handpick a library.
    CLLB  =["OpenCL",
            "amdocl64",
            "intelocl"]
    #Then set the library path
    CLLDP =[" ",
              "/opt/AMDAPP/lib/x86_64",
              "/usr/lib64/OpenCL/vendors/intel",
              "/scisoft/users/karkouli/INTEL_OCL/usr/lib64/OpenCL/vendors/intel"]
    #And finally the include path
    CLINC =[     " ",
                 "/opt/AMDAPP/include",
                 "/usr/include",
                 "/scisoft/users/karkouli/INTEL_OCL/usr/include"]

    print " Building of clPPM is still experimental "

    #Check for OpenCL through ICD first.
    #This can be useful if we want to build with alternative libraries when
    # OpenCL is not installed properly locally
    allow_no_ICD = True
    NO_ICD,ICD_LIST,ICD_vendors=get_OpenCL_ICD_list(allow_no_ICD)

    OCL_LIB="OpenCL"

    try:
        OCL_PATH,OCL_INC,OCL_LD_PATH=get_OpenCL_paths()
    except Exception as error:
        if AUTOCONFIG_OCL==True:
            print error
            return #skip the module
        else:
            pass

    #Change the list item to set manual include, library and ld_path
    if AUTOCONFIG_OCL == False:
        OCL_INC = CLINC[0]
        OCL_LD_PATH = CLLDP[0]
        OCL_LIB = CLLB[0]

    if OCL_LD_PATH:
        OCL_LD_PATH = [OCL_LD_PATH]
    else:
        OCL_LD_PATH = []

    print " OCL INC PATH: ", OCL_INC
    print " OCL LIB PATH: ", OCL_LD_PATH
    print " OCL LIB USED: ", OCL_LIB

    CLdef = [(None,), ("_VERBOSE",None)]

    print " Building of cuPPM library is still experimental"

    CLPPM_LIB=[]
    CLPPMsources = []
    CLPPMmacros = []

    try:
        #Direct method just returns the sources
        if MODULE_FROM == "DIRECT":
            CLPPMsources = clppm_direct(CLPPMsources)
            CLPPMmacros = [CLdef[0]]
        #Dynamic method returns a shared library upon success
        elif MODULE_FROM == "DYNAMIC":
            internal_clibraries = clppm_dynamic(internal_clibraries,OCL_INC,OCL_LD_PATH,OCL_LIB,CLdef[0])
            CLPPM_LIB = ["clPPM"]
    except Exception as error:
        print error
        return #skip the module

    #Now we prepend the wrapper in the existing sources if any
    CLPPMsources.insert(0, jn(['PPM','PPMtensorial','clPPM','src','clPPMwrapper.cc']) )

    if sys.platform=='win32':
        CXXFLAGS=['/w','/EHsc']
    else:
        CXXFLAGS=[]

    print "-----For the clPPM Python module:----"
    print " Module from: ", MODULE_FROM
    #print " Sources:    ", CLPPMsources
    print " LIB_DIRS:    ", [Common_libraries] + OCL_LD_PATH
    print " CLPPM_LIB:   ", CLPPM_LIB
    print " OCL_LIB:     ", [OCL_LIB]
    print " OCL_INC:     ", OCL_INC
    print " CXXFLAGS:    ", CXXFLAGS
    print " CLPPMmacros: ", CLPPMmacros
    print "-------------------------------------"
    module = Extension('PPM.PPMtensorial.clPPM',
                    sources = CLPPMsources,
                    library_dirs = [Common_libraries] + OCL_LD_PATH,
                    libraries= CLPPM_LIB + [OCL_LIB],
                    include_dirs=[ 'PPM/PPMtensorial/clPPM/src', numpy.get_include(),OCL_INC],
                    extra_compile_args=CXXFLAGS,
                    define_macros = define_macros + CLPPMmacros
                    )
    ext_modules.append(module)

ext_modules = []
internal_clibraries = []

build_cuppm(ext_modules, internal_clibraries )
build_clppm(ext_modules, internal_clibraries )

description = ""
long_description = """
"""
print internal_clibraries
distrib = setup(name="PPM",
                license = "GPL - Please read LICENSE.GPL for details",
                # version= logs,
                description = description,
                author = "Alessandro Mirone",
                author_email="mirone@esrf.fr",
                url = "http://forge.epn-campus.fr/projects/PPM",
                long_description = long_description,
                packages = packages,
                platforms='any',
                ext_modules = ext_modules,
                )
