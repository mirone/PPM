
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string.h>

#ifndef OCLTOOLS_H
#include "ocl_tools.h"
#define OCLTOOLS_H
#endif

#include <CL/opencl.h>
#include "ocl_ckerr.h"

#ifndef OTHER_H
#include "clPPMextension_other.h"
#define OTHER_H
#endif

#define blockSize 32

#define CT CL_CHECK_ERR_THROW

//Progagations class. Responsible for handling propagation kernels
//getResults returns the results of Propagations
clPropagation::clPropagation(){
  clPropagation_GPU_time = 0.0;
};

clPropagation::~clPropagation(){

};

/**
 * Configures and calls propagation kernels in clPPMkernels_other.cl
 *
 * @param scanLen The number of scanpoints
 * @param numthick The number of all layers
 * @param oclconfig The OpenCL configuration
 */
void clPropagation::Propagations(int&scanLen, int&numthick,ocl_config_type *oclconfig){

  cl_int clErr;
  float clTime_ms;
  char msg[1000];

  size_t wdim[] = { ( scanLen/blockSize + ((scanLen%blockSize)>0) ) * blockSize , numthick, 1};
  size_t tdim[] = { blockSize , 1, 1};
  
//   grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),numthick,1);
//   block=dim3(blockSize,1,1);

  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[9],2,0,wdim,tdim,0,0,&oclconfig->t_s[0]);
    clWaitForEvents(1,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  
  clTime_ms = ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[0]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel cuPropagation time (ms): "<< clTime_ms << std::endl;
#endif
  
  clPropagation_GPU_time =clTime_ms;
  
  CT(clReleaseEvent(oclconfig->t_s[0]));

  return;
};

/**
 * Returns the results of Propagations. There results are intermediate and not needed,
 * except for testing.
 *
 * @param scanLen The number of scanpoints
 * @param numthick The number of all layers
 * @param cl_mem OpenCL buffers
 * @param oclconfig The OpenCL configuration
 */
void clPropagation::getPropagationsResults(int&scanLen, int&numthick, cl_mem d_Prop,complex<double> *Prop,ocl_config_type *oclconfig){

  cl_int clErr;
  float clTime_ms;
  char msg[1000];

  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_Prop,CL_TRUE,0,scanLen * 4 * 4 * (numthick) * sizeof(cl_double2),(void*)Prop,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }

#ifdef _VERBOSE
  sprintf(msg,"%s: elapsed time",__FUNCTION__);
  ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[0],msg);
#endif  

  CT(clReleaseEvent(oclconfig->t_s[0]));

  return;
}

//Interfaces class. Responsible for handling interfaces kernels
//getResults returns the results of Interfaces
clInterfaces::clInterfaces(){
  clInterfaces_GPU_time = 0.0;
};

clInterfaces::~clInterfaces(){

};

/**
 * Configures and calls interfaces kernels in clPPMkernels_other.cl
 *
 * @param scanLen The number of scanpoints
 * @param numthick The number of all layers
 * @param oclconfig The OpenCL configuration 
 */
void clInterfaces::Interfaces(int&scanLen, int&numthick, ocl_config_type *oclconfig){

  cl_int clErr;
  float clTime_ms,totTime_ms=0;
  char msg[1000];

  size_t wdim[] = { ( scanLen/blockSize + ((scanLen%blockSize)>0) ) * blockSize , numthick, 1};
  size_t tdim[] = { blockSize , 1, 1};
  
//   grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),numthick,1);
//   block=dim3(blockSize,1,1);

  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[10],2,0,wdim,tdim,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[0]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel getConditions time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms;

  //We reset the gridDim.y dim for the calculation of the Interfaces
  size_t wdimI[] = { ( scanLen/blockSize + ((scanLen%blockSize)>0) ) * blockSize ,(numthick - 1), 1};
//   //We reset the gridDim.y dim for the calculation of the Interfaces
//   grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),numthick-1,1);

  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[11],2,0,wdimI,tdim,0,0,&oclconfig->t_s[1]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[1],&oclconfig->t_s[1]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel cuInterfaces time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms;

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": Total processing time: " <<totTime_ms << " (ms)" <<std::endl;
#endif  

  clInterfaces_GPU_time = totTime_ms;
  
  CT(clReleaseEvent(oclconfig->t_s[0]));
  CT(clReleaseEvent(oclconfig->t_s[1]));

  return;
}

/**
 * Returns the results of Interfaces. There results are intermediate and not needed,
 * except for testing.
 *
 * @param scanLen The number of scanpoints
 * @param numthick The number of all layers
 * @param cl_mem OpenCL buffers
 * @param oclconfig The OpenCL configuration
 */
void clInterfaces::getInterfacesResults(int&scanLen, int&numthick, cl_mem d_Interfaces, complex<double> *Interfaces,cl_mem d_Conditions,
                                cl_mem d_Conditions_inv, complex<double> *Conditions, complex<double> *Conditions_inv,ocl_config_type *oclconfig){

  cl_int clErr;
  float clTime_ms=0;
  char msg[1000];

  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_Interfaces,CL_TRUE,0,scanLen * 4 * 4 * (numthick-1)  * sizeof(cl_double2),(void*)Interfaces,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_Conditions,CL_TRUE,0,scanLen * 4 * 4 * (numthick) * sizeof(cl_double2),(void*)Conditions,0,0,&oclconfig->t_s[1]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_Conditions_inv,CL_TRUE,0,scanLen * 4 * 4 * (numthick) * sizeof(cl_double2),(void*)Conditions_inv,0,0,&oclconfig->t_s[2]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }

#ifdef _VERBOSE  
  sprintf(msg,"%s: elapsed time",__FUNCTION__);
  ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[2],msg);
#endif  

  for(int i=0;i<3;i++) CT(clReleaseEvent(oclconfig->t_s[i]));

  return;
}

//Reflectivity class. Responsible for handling reflectivity kernels
//getReflResults returns the results of clPPM
clReflectivity::clReflectivity(){
  clReflectivity_GPU_time = 0.0;
};

clReflectivity::~clReflectivity(){
};

/**
 * Configures and calls Reflectivity kernels in clPPMkernels_other.cl
 *
 * @param scanLen The number of scanpoints
 * @param oclconfig The OpenCL configuration
 */
void clReflectivity::Reflectivity(int&scanLen, ocl_config_type *oclconfig){

  cl_int clErr;
  float clTime_ms;
  char msg[1000];

  size_t wdim[] = { ( scanLen/blockSize + ((scanLen%blockSize)>0) ) * blockSize , 1, 1};
  size_t tdim[] = { blockSize , 1, 1};
  
//   grid=dim3(scanLen/blockSize + ((scanLen%blockSize)>0),1,1);
//   block=dim3(blockSize,1,1);

  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[12],1,0,wdim,tdim,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[0]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel cuReflectivity time (ms): "<< clTime_ms << ::endl;
#endif  

  clReflectivity_GPU_time = clTime_ms;

  CT(clReleaseEvent(oclconfig->t_s[0]));

  return;
};

/**
 * Returns the result of Reflectivity. This is the result that the cuPPM module must return back to PPM.
 */
void clReflectivity::getReflResults(int&scanLen, cl_mem d_Reflectivity, complex<double> *Reflectivity,ocl_config_type *oclconfig){

  cl_int clErr;
  float clTime_ms;
  char msg[1000];

  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_Reflectivity,CL_TRUE,0,scanLen * 4 * sizeof(cl_double2),(void*)Reflectivity,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }

#ifdef _VERBOSE
  sprintf(msg,"%s: elapsed time",__FUNCTION__);
  ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[0],msg);
#endif
  
  CT(clReleaseEvent(oclconfig->t_s[0]));
  
  return;
}