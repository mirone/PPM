
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <complex>
#include "cuda_runtime_api.h"
#include "cuComplex.h"

#if defined (_WIN32) && defined(_DLLEXPORT)
#define __w32dll_compat __declspec(dllexport)
#else 
#define __w32dll_compat 
#endif

using namespace std;

/**
 * \brief cudaProgagation class is responsible for handling propagation kernels.
 */
class __w32dll_compat cudaPropagation{

public:
  cudaPropagation();
  ~cudaPropagation();
  void Propagations(int&scanLen, int&numthick, cuDoubleComplex *d_Prop, cuDoubleComplex *d_Kz, double * d_K0);
  void getPropagationsResults(int&scanLen, int&numthick, cuDoubleComplex *d_Prop,complex<double> *Prop);

  /**
   * Internal time counter in ms. It holds the total execution time of Progagations
   */  
  float cudaPropagation_GPU_time;
};

/**
 * \brief cudaInterfaces class is responsible for handling interfaces kernels.
 */
class __w32dll_compat cudaInterfaces{

public:
  cudaInterfaces();
  ~cudaInterfaces();
  void Interfaces(int&scanLen, int&numthick, cuDoubleComplex *d_Kz, cuDoubleComplex *d_E, cuDoubleComplex *d_B, cuDoubleComplex *d_Conditions,
                                cuDoubleComplex *d_Conditions_inv, double *d_K0, cuDoubleComplex *d_Interfaces);
  void getInterfacesResults(int&scanLen, int&numthick, cuDoubleComplex *d_Interfaces, complex<double> *Interfaces,cuDoubleComplex *d_Conditions,
                                cuDoubleComplex *d_Conditions_inv, complex<double> *Conditions, complex<double> *Conditions_inv);

  /**
   * Internal time counter in ms. It holds the total execution time of Interfaces
   */
  float cudaInterfaces_GPU_time;  
};

/**
 * \brief cudaReflectivity class is responsible for handling reflectivity kernels and returning the final result.
 */
class __w32dll_compat cudaReflectivity{

public:
  cudaReflectivity();
  ~cudaReflectivity();
  void Reflectivity(int&scanLen,cuDoubleComplex *d_Reflectivity,cuDoubleComplex *d_Interfaces, cuDoubleComplex *d_Propagations, 
                               cuDoubleComplex *d_propa);

  void getReflResults(int&scanLen, cuDoubleComplex *d_Reflectivity, complex<double> *Reflectivity);

  /**
   * Internal time counter in ms. It holds the total execution time of Reflectivity
   */    
  float cudaReflectivity_GPU_time;
};