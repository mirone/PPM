
/**
 * \file
 * \brief clPPM module testing executable.
 */
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fstream>

//Include the header of clPPM API
#include "clPPMextension.h"

#ifdef _WIN32
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif

/**
* \brief PPMTensorial_ocl testing functionality
* 
* This class extends the functionality of PPMTensorial_ocl in the sense that
* it can read input and feed it to PPMTensorial_ocl, execute the algorithms and then
* perform tests.
* In other words SelfTest wraps around PPMTensorial_ocl sending input
* and receiving and processing its output
*/
class SelfTest:public PPMTensorial_ocl{
public:
  SelfTest();
  ~SelfTest();
  int startTest_Modes(const char *devicetype,int platformid,int devid);

  void getInputFromFiles();
  int evalKx();
  int evalAs();
  int evalKz();
  int evalPolyn();
  int evalSistem();
  int evalNonZeri();
  int evalE();
  int evalB();
  int evalConditions();
  int evalConditions_inv();
  int evalPropagations();
  int evalInterfaces();
  int evalReflectivity();

  ofstream outf;
  std::streambuf* cout_sbuf;
protected:

  void loadResData();
  int evaluateRes();
  template<typename T>
  void reader(const char* fname, T *out,  size_t size, int count);

  //We do not need to declare K0 and angles as they are declared in the base class
  // but there they are used only as pointers
  complex<double> *indexes;
  complex<double> *reflectivity;
  double *roughness;
  double *thickness;

};

SelfTest::SelfTest(){
  debug_flag=1; //Make sure the base class allocates all the host side arrays and returns all data

  //Since this is a console application, in Windows the console is limited in
  // size and will obscure the results. We redirect internally the stdout to a file
  // (and set it back to normal in the destructor)
  #ifdef _WIN32
	  std::cout<<"Warning! In Windows the output is redirected to file clPPM_SelfTest_log.txt" <<std::endl;
    outf.open("clPPM_SelfTest_log.txt",ios::out | ios::trunc);
    cout_sbuf = std::cout.rdbuf(); // Save cout buffer
    std::cout.rdbuf(outf.rdbuf()); //Redirect cout to outf buffer
  #endif

  //Read the input files. Try separately and exit if failure  
  try{
    SelfTest::getInputFromFiles();
  }catch (char *msg){
    std::cout<< msg <<std::endl;
    exit(1);
  }
}

SelfTest::~SelfTest(){

  //Reset the stdout before we close the file
#ifdef _WIN32
  std::cout.rdbuf(cout_sbuf); //reset cout
  outf.close();
#endif
  delete[] Modes;
  delete[] K0;
  delete[] angles;
  delete[] indexes;
  delete[] roughness;
  delete[] thickness;
  delete[] wheretolook;
  delete[] reflectivity;

}

//For a given binary file, reads count elements of type T and size
//It asserts that the all the requested elements are read.
template<typename T>
void SelfTest::reader(const char* fname, T *out, size_t size, int count){

  int record;
  FILE *infile;
  static char msg[512];

  infile=fopen(fname,"rb");
  if(!infile){
    sprintf(msg,"%s: Failed to open file %s",__FUNCTION__,fname);
    throw msg;
  }
  record = fread(out,size,count,infile);
  fclose(infile);
  std::cout << __FUNCTION__ << ": got " << record << " elements" << std::endl;  
  assert(record==count);//check if all reqested data are read

}

//As the name suggests, it gets the input data from files that are created
// when the unittest flag is set in PPMlayerBaseClass.
void SelfTest::getInputFromFiles(){

  FILE *infile;
  static char msg[512];
  int records[3];
  std::cout << __PRETTY_FUNCTION__ << ": Reading sizes.dat" << std::endl;
  infile=fopen("sizes.dat","rb");
  if(!infile){
    sprintf(msg,"%s: Failed to open file %s",__FUNCTION__,"sizes.dat");
    throw msg;
  }
  records[0] = fread(&scanLen,sizeof(int),1,infile);
  records[1] = fread(&numthick,sizeof(int),1,infile);
  records[2] = fread(&uniquelayers,sizeof(int),1,infile);
  fclose(infile);

  std::cout << __PRETTY_FUNCTION__ << ": got wavelenghts " << scanLen << ", layers " << numthick<< " and ntodo "
            << uniquelayers <<  std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Warning! num of layers must INCLUDE the substrate!" << std::endl;

  //First close the file and then abort if error
  assert(records[0]);
  assert(records[1]);
  assert(records[2]);

  Modes = new int [uniquelayers];
  std::cout << __PRETTY_FUNCTION__ << ": Reading modes.dat" << std::endl;
  reader("modes.dat",Modes,sizeof(int),uniquelayers);

  angles = new double [scanLen];
  std::cout << __PRETTY_FUNCTION__ << ": Reading angles.dat" << std::endl;
  reader("angles.dat",angles,sizeof(double),scanLen);

  K0 = new double [scanLen];
  std::cout << __PRETTY_FUNCTION__ << ": Reading K0.dat" << std::endl;
  reader("K0.dat",K0,sizeof(double),scanLen);

  wheretolook = new int [numthick];
  std::cout << __PRETTY_FUNCTION__ << ": Reading layers_lookup_table.dat" << std::endl;
  reader("layers_lookup_table.dat",wheretolook,sizeof(int),numthick);

  for(int i=0;i<numthick;i++)std::cout<< "Layer " << i << " is connected to " <<wheretolook[i] << ", m: "<< (i<uniquelayers ? Modes[i] : 0) <<std::endl;
  int tmp=0;
  for(int imode =0;imode<uniquelayers;imode++){
    tmp+=Modes[imode] * Modes[imode];
  }

  indexes = new complex<double> [scanLen*tmp];
  std::cout << __PRETTY_FUNCTION__ << ": Reading indexes.dat" << std::endl;
  reader("indexes.dat",indexes,sizeof(complex<double>),scanLen*tmp);

//   FILE *test;
//   test=fopen("cuSindex.txt","w");
//   for(int i=0;i<scanLen;i++)fprintf(test,"%+.7e %+.7e\n",indexes[i].real(),indexes[i].imag());
//   fclose(test);

  roughness = new double[scanLen * (numthick)];
  std::cout << __PRETTY_FUNCTION__ << ": Reading roughness.dat" << std::endl;
  reader("roughness.dat",roughness,sizeof(double),numthick);

  thickness = new double[scanLen * (numthick)];
  std::cout << __PRETTY_FUNCTION__ << ": Reading thickness.dat" << std::endl;
  reader("thickness.dat",thickness,sizeof(double),numthick);

  reflectivity = new complex<double> [scanLen *2 * 2];

  return;
};


#define _MAX_RATIO 1e-16
#define _MIN_RATIO 1e-8
#define _MIN_FLOAT 1e-7
#define _MIN_RATIO_PREC 10
#define _MIN_RATIO_RELAXED_PREC 7
#define _MIN_RATIO_RELAXED 1e-5
#define _MAX_ERR_COUNT 16


int SelfTest::evalKx(){

  double err_ratio=_MAX_RATIO;
  double *eval= new double [scanLen];
  double ev;
  std::cout << __PRETTY_FUNCTION__ << ": Reading Kx.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;

  reader("Kx.res", eval, sizeof(double), scanLen);

  int errcount=0;
  for(int iscan=0;iscan<scanLen;iscan++){
    ev=fabs(Kx[iscan] / eval[iscan]);
    if( fabs(ev-1.0)>err_ratio ){
      errcount++;
      std::cout << __PRETTY_FUNCTION__ << ": p"<<iscan<<" | dKx / Kx | = " << ev << " dKx = " << Kx[iscan] << " Kx = "<< eval[iscan] << std::endl;
    }
    if(errcount>=_MAX_ERR_COUNT){
      std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
      break;
    }
  }
  std::cout.precision( 18);
  std::cout.setf(ios::scientific,ios::floatfield);
  std::cout << "Kx[0]   " << Kx[0] <<std::endl;
  std::cout << "eval[0] " << eval[0] <<std::endl; 
  std::cout << "angle[0]" << angles[0] <<std::endl;
  std::cout.unsetf(ios::floatfield);
  std::cout.precision( 6);

  delete[] eval;
  if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;

return 0;
};

int SelfTest::evalAs(){

  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading As.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, As on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;

  int nd3=0;
  for(int i=0;i<uniquelayers-1;i++)if(Modes[i]==3)nd3++;
  complex<double> *eval= new complex<double> [SelfTest::scanLen * 4 * nd3];
  reader("As.res",eval,sizeof(complex<double>),scanLen*4*nd3);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << nd3 << " layers"<< std::endl;

  std::cout.precision( _MIN_RATIO_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
  nd3=0;
  //uniquelayers-1 is important here. The last layer is always calculated as scalar so As will not have a value
  for(int ilayer=0;ilayer<uniquelayers-1;ilayer++){
    if(Modes[ilayer]!=3)continue;
    std::cout << "Checking layer: " <<ilayer << " (nd "<< nd3<<")"<<std::endl;
    errcount=0;
    for(int iscan=0;iscan<scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        ev= abs(As[iscan + scanLen*(iw + 4 * ilayer )].real() / eval[iw + 4*(iscan + scanLen*nd3)].real());
        ev1=abs(As[iscan + scanLen*(iw + 4 * ilayer )].imag() / eval[iw + 4*(iscan + scanLen*nd3)].imag());
        if( (ev-1.0)>err_ratio || (ev-1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio){
          errcount++;
          std::cout  << ": p"<<iscan <<" iw" <<iw << showpos << " R (dAs / As)  = "  << fabs((ev-1.0))
                    <<" I (dAs / Kz)  = "  << fabs((ev1-1.0))
                    << " dAs = " << As[iscan + scanLen*(iw + 4 * ilayer )] << " As = "<< eval[iw + 4*(iscan + scanLen*nd3)] <<
                    noshowpos << std::endl;
        }
        if(errcount>=_MAX_ERR_COUNT){
          std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
          break;
        }
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    nd3++;
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }

  delete[] eval;
  std::cout.unsetf(ios::floatfield);
return 0;
}

int SelfTest::evalKz(){

  complex<double> *eval= new complex<double> [SelfTest::scanLen * 4 * uniquelayers];
  double ev,ev1;

  double err_ratio=_MAX_RATIO;
  std::cout << __PRETTY_FUNCTION__ << ": Reading Kz.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, Kz on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;

  reader("Kz.res",eval,sizeof(complex<double>),scanLen*4*uniquelayers);
  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << uniquelayers << " layers" << std::endl;

  std::cout.precision( _MIN_RATIO_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
  for(int ilayer=0;ilayer<uniquelayers;ilayer++){
    std::cout << "Checking layer: " <<ilayer <<std::endl;
    errcount=0;
    for(int iscan=0;iscan<SelfTest::scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        ev= abs(Kz[iscan + scanLen*(iw + 4 * ilayer )].real() / eval[iw + 4*(iscan + scanLen*ilayer)].real());
        ev1=abs(Kz[iscan + scanLen*(iw + 4 * ilayer )].imag() / eval[iw + 4*(iscan + scanLen*ilayer)].imag());
        if((ev-1.0)>err_ratio || (ev-1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio){
          errcount++;
          std::cout  << ": p"<<iscan <<" iw" <<iw << showpos << " R (dKz / Kz)  = "  << (ev-1.0)
                    <<" I (dKz / Kz)  = "  << (ev1-1.0)
                    << " dKz = " << Kz[iscan + scanLen*(iw + 4 * ilayer )] << " Kz = "<< eval[iw + 4*(iscan + scanLen*ilayer)] <<
                    noshowpos << std::endl;
        }
        if(errcount>=_MAX_ERR_COUNT){
          std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
          break;
        }
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }

  delete[] eval;
  std::cout.unsetf(ios::floatfield);
return 0;
}

int SelfTest::evalPolyn(){

  FILE *infile;
  int incount;
  complex<double> *eval= new complex<double> [SelfTest::scanLen * 4];
  complex<double> *eval1= new complex<double> [SelfTest::scanLen * 4];
  complex<double> ev,ev1,ev2,ev3;

  std::cout << __PRETTY_FUNCTION__ << ": Evaluating polynomial" << std::endl;
/*  std::cout << __PRETTY_FUNCTION__ << ": Remember, Kz on GPU is TRANSPOSED!" << std::endl;*/
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << _MIN_RATIO<< std::endl;
  infile=fopen("Kz.res","r");
  incount=fread(eval,sizeof(complex<double>),SelfTest::scanLen * 4,infile);
  fclose(infile);
  infile=fopen("Kz_sol.res","r");
  incount=fread(eval1,sizeof(complex<double>),SelfTest::scanLen * 4,infile);
  fclose(infile);
  std::cout << __PRETTY_FUNCTION__ << ": got " << incount << " elements" << std::endl;

  std::cout.precision(8);
  std::cout.setf(ios::scientific,ios::floatfield);

  for(int iscan=0;iscan<SelfTest::scanLen;iscan++){
      if(iscan%100)continue;
      ev = As[iscan] + Kz[iscan]* ( As[iscan + scanLen] + Kz[iscan] *( As[iscan + 2*scanLen] + Kz[iscan]* (As[iscan + 3*scanLen] + Kz[iscan]) ) );
      ev1 = As[iscan] + Kz[iscan + scanLen]* ( As[iscan + scanLen] + Kz[iscan+ scanLen] *( As[iscan + 2*scanLen] + Kz[iscan+ scanLen]* (As[iscan + 3*scanLen] + Kz[iscan+ scanLen]) ) );
      ev2 = As[iscan] + Kz[iscan + 2*scanLen]* ( As[iscan + scanLen] + Kz[iscan+2*scanLen] *( As[iscan + 2*scanLen] + Kz[iscan+2*scanLen]* (As[iscan + 3*scanLen] + Kz[iscan+2*scanLen]) ) );
      ev3 = As[iscan] + Kz[iscan + 3*scanLen]* ( As[iscan + scanLen] + Kz[iscan + 3*scanLen] *( As[iscan + 2*scanLen] + Kz[iscan + 3*scanLen]* (As[iscan + 3*scanLen] + Kz[iscan + 3*scanLen]) ) );
      std::cout << "i" << iscan << "sol0 "  <<showpos << ev << " sol1 " << ev1 << " sol2 " << ev2 << " sol3 " << ev3 << noshowpos <<std::endl;

       ev =  Kz[iscan];
      ev1 =  Kz[iscan + scanLen];
      ev2 =  Kz[iscan + 2*scanLen];
      ev3 =  Kz[iscan + 3*scanLen];
      std::cout << "i" << iscan << "Sol0 "  <<showpos << ev << " Sol1 " << ev1 << " Sol2 " << ev2 << " Sol3 " << ev3 << noshowpos <<std::endl;
      std::cout <<std::endl;

      ev =  eval[0 + 4*iscan] + eval1[0 + 4*iscan      ]* ( eval[1 + 4*iscan] + eval1[0 + 4*iscan      ] *( eval[2 + 4*iscan] + eval1[0 + 4*iscan      ]* (eval[3 + 4*iscan] + eval1[0 + 4*iscan      ]) ) );
      ev1 = eval[0 + 4*iscan] + eval1[1 + 4*iscan      ]* ( eval[1 + 4*iscan] + eval1[1 + 4*iscan      ] *( eval[2 + 4*iscan] + eval1[1 + 4*iscan      ]* (eval[3 + 4*iscan] + eval1[1 + 4*iscan      ]) ) );
      ev2 = eval[0 + 4*iscan] + eval1[2 + 4*iscan      ]* ( eval[1 + 4*iscan] + eval1[2 + 4*iscan      ] *( eval[2 + 4*iscan] + eval1[2 + 4*iscan      ]* (eval[3 + 4*iscan] + eval1[2 + 4*iscan      ]) ) );
      ev3 = eval[0 + 4*iscan] + eval1[3 + 4*iscan      ]* ( eval[1 + 4*iscan] + eval1[3 + 4*iscan      ] *( eval[2 + 4*iscan] + eval1[3 + 4*iscan      ]* (eval[3 + 4*iscan] + eval1[3 + 4*iscan      ]) ) );
      std::cout << "i" << iscan << "col0 "  <<showpos << ev << " col1 " << ev1 << " col2 " << ev2 << " col3 " << ev3 << noshowpos <<std::endl;

       ev =  eval1[0 + 4*iscan      ]    ;
      ev1 =  eval1[1 + 4*iscan      ]     ;
      ev2 =  eval1[2 + 4*iscan      ]      ;
      ev3 =  eval1[3 + 4*iscan      ]       ;
      std::cout << "i" << iscan << "Col0 "  <<showpos << ev << " Col1 " << ev1 << " Col2 " << ev2 << " Col3 " << ev3 << noshowpos <<std::endl;
      std::cout <<std::endl;
  }

  delete[] eval;
  delete[] eval1;
  std::cout.unsetf(ios::floatfield);
return 0;
}

int SelfTest::evalSistem(){

  
  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading Sistem.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, Sistem on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio << std::endl;

  int nd3=0;
  for(int i=0;i<uniquelayers-1;i++)if(Modes[i]==3)nd3++;
  complex<double> *eval= new complex<double> [SelfTest::scanLen * 9 *4 * nd3];
  reader("Sistem.res",eval,sizeof(complex<double>),scanLen * 4 * 9 * nd3);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << nd3 << " layers"<< std::endl;

  std::cout.precision( _MIN_RATIO_RELAXED_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
  nd3=0;
  for(int ilayer=0;ilayer<uniquelayers-1;ilayer++){
    if(Modes[ilayer]!=3)continue;
    std::cout << "Checking layer: " <<ilayer << " (nd " <<nd3<< ")"<<std::endl;
    errcount=0;
    for(int iscan=0;iscan<scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        for(int ix=0;ix<3;ix++){
          for(int iy=0;iy<3;iy++){
            ev= abs(Sistem[Offsets[ilayer] + iscan + scanLen*(iw + 4*(ix + 3*iy))].real() / eval[iy + 3*(ix + 3*(iw + 4*(iscan + scanLen*nd3)))].real());
            ev1=abs(Sistem[Offsets[ilayer] + iscan + scanLen*(iw + 4*(ix + 3*iy))].imag() / eval[iy + 3*(ix + 3*(iw + 4*(iscan + scanLen*nd3)))].imag());
            if( (ev-1.0)>err_ratio || (ev-1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio ){
              errcount++;
              std::cout  << ": p"<<iscan<<" w"<<iw<<" x"<<ix<<" y"<<iy<< showpos << " R (dS / S)  = " << (ev-1.0)
                        <<" I (dS / S)  = "  << (ev1-1.0)
                        << " dS = " << Sistem[Offsets[ilayer] + iscan + scanLen*(iw + 4*(ix + 3*iy))] << " S = "<< eval[iy + 3*(ix + 3*(iw + 4*(iscan + scanLen*nd3)))] <<
                        noshowpos << std::endl;
            }
            if(errcount>=_MAX_ERR_COUNT){
              std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
              break;
            }
          }
          if(errcount>=_MAX_ERR_COUNT)break;
        }
        if(errcount>=_MAX_ERR_COUNT)break;
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    nd3++;
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }

  delete[] eval;
  std::cout.unsetf(ios::floatfield);
  return 0;
}

int SelfTest::evalNonZeri(){

  int ev;

  std::cout << __PRETTY_FUNCTION__ << ": Reading NonZeri.res" << std::endl;

  int nd3=0;
  for(int i=0;i<uniquelayers-1;i++)if(Modes[i]==3)nd3++;
  int *eval= new int [SelfTest::scanLen *4*nd3];
  reader("NonZeri.res",eval,sizeof(int),scanLen*4*nd3);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << nd3 << " layers"<< std::endl;

  int errcount;
  nd3=0;
  for(int ilayer=0;ilayer<uniquelayers-1;ilayer++){
    if(Modes[ilayer]!=3)continue;
    std::cout << "Checking layer: " <<ilayer<< " (nd " <<nd3<< ")"<<std::endl;
    errcount=0;
    for(int iscan=0;iscan<SelfTest::scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        ev= abs(NonZeri[iscan + scanLen*(iw + 4*ilayer)] - eval[iscan + scanLen*(iw + 4*nd3)]);
        if( ev ){
          errcount++;
          std::cout << ": p"<<iscan<<" w"<<iw<< " (dZ - Z)  = "  << ev
                    << " dZ = " << NonZeri[iscan + scanLen*(iw + 4*ilayer)] << " Z = "<< eval[iscan + scanLen*(iw + 4*nd3)] << std::endl;
        }
        if(errcount>=_MAX_ERR_COUNT){
          std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
          break;
        }
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    nd3++;
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }
  delete[] eval;
  return 0;
}

int SelfTest::evalE(){

  complex<double> *eval= new complex<double> [scanLen *4 *3*uniquelayers];
  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading E.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, E on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;

  reader("E.res",eval,sizeof(complex<double>),scanLen*4*3*uniquelayers);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << uniquelayers << " layers"<< std::endl;

  std::cout.precision( _MIN_RATIO_RELAXED_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
    
  for(int ilayer=0;ilayer<uniquelayers;ilayer++){
    std::cout << "Checking layer: " <<ilayer <<std::endl;
    errcount=0;
    for(int iscan=0;iscan<SelfTest::scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        for(int k=0;k<3;k++){
          ev= abs(E[iscan + scanLen*(iw + 4*(k +3 * ilayer))].real() / eval[k + 3*(iw + 4*(iscan + scanLen*ilayer))].real());
          ev1=abs(E[iscan + scanLen*(iw + 4*(k +3 * ilayer))].imag() / eval[k + 3*(iw + 4*(iscan + scanLen*ilayer))].imag());
          if( ( (ev - 1.0)>err_ratio || (ev - 1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio)){
            errcount++;
            std::cout << ": p"<<iscan<<" w"<<iw<< " k"<<k<< showpos << " R (dE / E)  = "  << (ev-1.0) << " R (dE / E) = "  << (ev1-1.0)
                      << " dE = " << E[iscan + scanLen*(iw + 4*(k +3 * ilayer))] << " E = "<< eval[k + 3*(iw + 4*(iscan + scanLen*ilayer))] << noshowpos<<std::endl;
          }
          if(errcount>=_MAX_ERR_COUNT*3){
            std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
            break;
          }
        }
        if(errcount>=_MAX_ERR_COUNT*3)break;
      }
      if(errcount>=_MAX_ERR_COUNT*3)break;
    }
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }
  delete[] eval;
  std::cout.unsetf(ios::floatfield);
  return 0;
}

int SelfTest::evalB(){

  complex<double> *eval= new complex<double> [scanLen *4 *3*uniquelayers];
  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading B.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, B on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;
  
  reader("B.res",eval,sizeof(complex<double>),scanLen*4*3*uniquelayers);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << uniquelayers << " layers"<< std::endl;

  std::cout.precision( _MIN_RATIO_RELAXED_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
    
  for(int ilayer=0;ilayer<uniquelayers;ilayer++){
    std::cout << "Checking layer: " <<ilayer <<std::endl;
    errcount=0;
    for(int iscan=0;iscan<SelfTest::scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        for(int k=0;k<3;k++){
          ev= abs(B[iscan + scanLen*(iw + 4*(k +3 * ilayer) )].real() / eval[k + 3*(iw + 4*(iscan + scanLen*ilayer))].real());
          ev1=abs(B[iscan + scanLen*(iw + 4*(k +3 * ilayer))].imag() / eval[k + 3*(iw + 4*(iscan + scanLen*ilayer))].imag());
          if( ( (ev - 1.0)>err_ratio || (ev - 1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio)){
            errcount++;
            std::cout << ": p"<<iscan<<" w"<<iw<< " k"<<k<< showpos << " R (dB / B)  = "  << ev << " R (dB / B) = "  << ev1
                      << " dB = " << B[iscan + scanLen*(iw + 4*(k +3 * ilayer))] << " B = "<< eval[k + 3*(iw + 4*(iscan + scanLen*ilayer))] << noshowpos<<std::endl;
          }
          if(errcount>=_MAX_ERR_COUNT*3){
            std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
            break;
          }
        }
        if(errcount>=_MAX_ERR_COUNT*3)break;
      }
      if(errcount>=_MAX_ERR_COUNT*3)break;
    }
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }
  delete[] eval;
  std::cout.unsetf(ios::floatfield);
  return 0;
}

int SelfTest::evalPropagations(){

  complex<double> *eval= new complex<double> [scanLen *4 *numthick];
  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading Propagations.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, Propagations on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;
  
  reader("Propagations.res",eval,sizeof(complex<double>),scanLen*4 *numthick);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << numthick << " layers"<< std::endl;

  std::cout.precision( _MIN_RATIO_RELAXED_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
    
  for(int ilayer=0;ilayer<numthick;ilayer++){
    std::cout << "Checking layer: " <<ilayer <<std::endl;
    errcount=0;
    for(int iscan=0;iscan<scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
          ev= abs(Prop[iscan + scanLen*(iw + 4* ilayer)].real() / eval[iw + 4*(iscan + scanLen*ilayer)].real());
          ev1=abs(Prop[iscan + scanLen*(iw + 4* ilayer)].imag() / eval[iw + 4*(iscan + scanLen*ilayer)].imag());
          if( ( (ev - 1.0)>err_ratio || (ev - 1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio)){
            errcount++;
            std::cout << ": p"<<iscan<<" w"<<iw << showpos << " R (dP / P)  = "  << ev << " R (dP / P) = "  << ev1
                      << " dP = " << Prop[iscan + scanLen*(iw + 4* ilayer)] << " P = "<< eval[iw + 4*(iscan + scanLen*ilayer)] << noshowpos<<std::endl;
          }
          if(errcount>=_MAX_ERR_COUNT){
            std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
            break;
          }
          if(errcount>=_MAX_ERR_COUNT)break;
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }
  delete[] eval;
  std::cout.unsetf(ios::floatfield);

  return 0;
}

int SelfTest::evalConditions(){

  complex<double> *eval= new complex<double> [scanLen * numthick * 4 * 4];
  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading Conditions.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, Conditions on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": If you see a numerical (small) error on eg w0 i2, this should the same as E w2 i0!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": If it is so then the Conditions array is transposed correctly.." << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;
  
  reader("Conditions.res",eval,sizeof(complex<double>),scanLen* numthick * 4 * 4);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << numthick << " layers"<< std::endl;

  std::cout.precision( _MIN_RATIO_RELAXED_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
    
  for(int ilayer=0;ilayer<numthick;ilayer++){
    std::cout << "Checking layer: " <<ilayer <<std::endl;
    errcount=0;
    for(int iscan=0;iscan<scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        for(int i=0;i<4;i++){
          //On CPU Contitions has switched axes of iwaves and components. from scanlen,iw,k to scanlen,k.iw
          //However, swapaxes of numpy returns a VIEW so it does not actually transpose. But it writes to file based on that view
          //On the GPU side, the transposition is done on the fly by doing (k,iw) = (iw,k)
          ev= abs(Conditions[iscan + scanLen*(ilayer + numthick * ( iw + 4 * i))].real() / eval[iw + 4*(i + 4*(iscan + scanLen*ilayer))].real());
          ev1=abs(Conditions[iscan + scanLen*(ilayer + numthick * ( iw + 4 * i))].imag() / eval[iw + 4*(i + 4*(iscan + scanLen*ilayer))].imag());
          if( ( (ev - 1.0)>err_ratio || (ev - 1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio) ){
            errcount++;
            std::cout << ": p"<<iscan<<" w"<<iw <<" i" << i<< showpos << " R (dC / C)  = "  << ev << " R (dC / C) = "  << ev1
                      << " dC = " << Conditions[iscan + scanLen*(ilayer + numthick * ( iw + 4 * i))] << " C = "<< eval[iw + 4*(i + 4*(iscan + scanLen*ilayer))] << noshowpos<<std::endl;
          }
          if(errcount>=_MAX_ERR_COUNT){
            std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
            break;
          }
          if(errcount>=_MAX_ERR_COUNT)break;
        }
        if(errcount>=_MAX_ERR_COUNT)break;
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }
  delete[] eval;
  std::cout.unsetf(ios::floatfield);

  return 0;
}

int SelfTest::evalConditions_inv(){

  complex<double> *eval= new complex<double> [scanLen * numthick * 4 * 4];
  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading Conditions_inv.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, Conditions_inv on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;
  
  reader("Conditions_inv.res",eval,sizeof(complex<double>),scanLen* numthick * 4 * 4);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << numthick << " layers"<< std::endl;

  std::cout.precision( _MIN_RATIO_RELAXED_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
    
  for(int ilayer=0;ilayer<numthick;ilayer++){
    std::cout << "Checking layer: " <<ilayer <<std::endl;
    errcount=0;
    for(int iscan=0;iscan<scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        for(int i=0;i<4;i++){
          ev= abs(Conditions_inv[iscan + scanLen*(ilayer + numthick * ( iw + 4 * i))].real() / eval[iw + 4*(i + 4*(iscan + scanLen*ilayer))].real());
          ev1=abs(Conditions_inv[iscan + scanLen*(ilayer + numthick * ( iw + 4 * i))].imag() / eval[iw + 4*(i + 4*(iscan + scanLen*ilayer))].imag());
          if( ( (ev - 1.0)>err_ratio || (ev - 1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio) ){
            errcount++;
            std::cout << ": p"<<iscan<<" w"<<iw <<" i" << i<< showpos << " R (dCi / Ci)  = "  << ev << " R (dCi / Ci) = "  << ev1
                      << " dCi = " << Conditions_inv[iscan + scanLen*(ilayer + numthick * ( iw + 4 * i))] << " Ci = "<< eval[iw + 4*(i + 4*(iscan + scanLen*ilayer))] << noshowpos<<std::endl;
          }
          if(errcount>=_MAX_ERR_COUNT){
            std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
            break;
          }
          if(errcount>=_MAX_ERR_COUNT)break;
        }
        if(errcount>=_MAX_ERR_COUNT)break;
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }
  delete[] eval;
  std::cout.unsetf(ios::floatfield);

  return 0;
}

int SelfTest::evalInterfaces(){

  complex<double> *eval= new complex<double> [scanLen * numthick * 4 * 4];
  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading Interfaces.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, Interfaces on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;
  
  reader("Interfaces.res",eval,sizeof(complex<double>),scanLen* (numthick-1) * 4 * 4);

  std::cout << __PRETTY_FUNCTION__ << ": got elements from " << numthick-1 << " layers"<< std::endl;

  std::cout.precision( _MIN_RATIO_RELAXED_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
    
  for(int ilayer=0;ilayer<numthick-1;ilayer++){
    std::cout << "Checking layer: " <<ilayer <<std::endl;
    errcount=0;
    for(int iscan=0;iscan<scanLen;iscan++){
      for(int iw=0;iw<4;iw++){
        for(int i=0;i<4;i++){
          ev= abs(Interfaces[iscan + scanLen*(ilayer + (numthick-1) * ( iw + 4 * i))].real() / eval[i + 4*(iw + 4*(iscan + scanLen*ilayer))].real());
          ev1=abs(Interfaces[iscan + scanLen*(ilayer + (numthick-1) * ( iw + 4 * i))].imag() / eval[i + 4*(iw + 4*(iscan + scanLen*ilayer))].imag());
          if( ( (ev - 1.0)>err_ratio || (ev - 1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio) ){
            errcount++;
            std::cout << ": p"<<iscan<<" w"<<iw <<" i" << i<< showpos << " R (dI / I)  = "  << ev << " R (dI / I) = "  << ev1
                      << " dI = " << Interfaces[iscan + scanLen*(ilayer + (numthick-1) * ( iw + 4 * i))] << " I = "<< eval[i + 4*(iw + 4*(iscan + scanLen*ilayer))] << noshowpos<<std::endl;
          }
          if(errcount>=_MAX_ERR_COUNT){
            std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
            break;
          }
          if(errcount>=_MAX_ERR_COUNT)break;
        }
        if(errcount>=_MAX_ERR_COUNT)break;
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  }
  delete[] eval;
  std::cout.unsetf(ios::floatfield);

  return 0;
}

int SelfTest::evalReflectivity(){

  complex<double> *eval= new complex<double> [scanLen * numthick * 4 * 4];
  double ev,ev1;
  double err_ratio=_MAX_RATIO;

  std::cout << __PRETTY_FUNCTION__ << ": Reading Reflectivity.res" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Remember, Reflectivity on GPU is TRANSPOSED!" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << ": Error Limit: " << err_ratio<< std::endl;
  
  reader("Reflectivity.res",eval,sizeof(complex<double>),scanLen * 2 * 2);


  std::cout.precision( _MIN_RATIO_RELAXED_PREC);
  std::cout.setf(ios::scientific,ios::floatfield);

  int errcount;
    
    errcount=0;
    for(int iscan=0;iscan<scanLen;iscan++){
      for(int iw=0;iw<2;iw++){
        for(int i=0;i<2;i++){
          ev= abs(Reflectivity[iscan + scanLen*(iw + 2 * i)].real() / eval[i + 2*(iw + 2*iscan)].real());
          ev1=abs(Reflectivity[iscan + scanLen*(iw + 2 * i)].imag() / eval[i + 2*(iw + 2*iscan)].imag());
          if( ( (ev - 1.0)>err_ratio || (ev - 1.0)<-err_ratio || (ev1-1.0)>err_ratio || (ev1-1.0)<-err_ratio) ){
            errcount++;
            std::cout << ": p"<<iscan<<" w"<<iw << " i" <<i << showpos << " R (dR / R)  = "  << ev << " R (dR / R) = "  << ev1
                      << " dR = " << Reflectivity[iscan + scanLen*(iw + 2 * i)] << " R = "<< eval[i + 2*(iw + 2*iscan)] << noshowpos<<std::endl;
          }
          if(errcount>=_MAX_ERR_COUNT){
            std::cout << __PRETTY_FUNCTION__ << errcount <<" errors. Halt" <<std::endl;
            break;
          }
          if(errcount>=_MAX_ERR_COUNT)break;
        }
        if(errcount>=_MAX_ERR_COUNT)break;
      }
      if(errcount>=_MAX_ERR_COUNT)break;
    }
    if(!errcount)std::cout << __PRETTY_FUNCTION__ <<": PASS" << std::endl;
  delete[] eval;
  std::cout.unsetf(ios::floatfield);

  return 0;
}

//When we have the input data we can pass them to clPPM.
//We then execute clPPM and test eack kernel
int SelfTest::startTest_Modes(const char *devicetype,int platformid,int devid){

  getInput(scanLen,numthick,uniquelayers,Modes,indexes,
    angles,roughness,thickness,K0,wheretolook,reflectivity);
  
  launchPPMTensorial_ocl(devicetype,platformid,devid);

//  SelfTest::evalIndexes();
//   SelfTest::evalKx();
//   if(debug_flag)SelfTest::evalAs();
//   SelfTest::evalKz();
//   SelfTest::evalSistem();
//   SelfTest::evalNonZeri();
//   SelfTest::evalE();
//   SelfTest::evalB();
  
//   SelfTest::evalPropagations();
//   SelfTest::evalConditions();
//   SelfTest::evalConditions_inv();

  SelfTest::evalInterfaces();
  SelfTest::evalReflectivity();
  
//  SelfTest::evalPolyn();

  return 0;
};

//Usage message
const char *message(){
  
 const char *msg={
 "\nMain function for clPPM regression test\n"
 " The executable can work with either no argument\n"
 "  or an argument list.\n"
 " If no argument is supplied, the program will\n"
 "  use the first CL_DEVICE_ALL device type that\n"
 "  resides on the first platform.\n"
 " The argument list is:\n"
 "  ./clppm_selftest device_type platform_id device_id\n"
 "  i.e:\n"
 "  ./clppm_selftest gpu 0 1  (second gpu of the first platform)\n"
 "  ./clppm_selftest cpu 0 0  (first cpu of the first platform)\n"
 "  ./clppm_selftest ALL 1 1  (any kind of device that is first on the first platform)\n"
 " \n" 
 " Device types acceptable:\n"
 "  GPU,gpu,CPU,cpu,DEF,ALL,ACC"
 "  DEF = Default (depends on the primary platform)\n"
 "  ALL = Any\n"
 "  ACC = Accelerator (Cell/BE, FPGA)\n\n"         
  };
  return msg;
}

/**
 * Main function for clPPM regression test
 * The executable can work with either no argument
 *  or an argument list.
 * If no argument is supplied, the program will
 *  use the first CL_DEVICE_ALL device type that
 *  resides on the first platform.
 * The argument list is:
 *  ./clppm_selftest device_type platform_id device_id
 *  i.e:
 *  ./clppm_selftest gpu 0 1  (second gpu of the first platform)
 *  ./clppm_selftest cpu 0 0  (first cpu of the first platform)
 *  ./clppm_selftest ALL 1 1  (any kind of device that is first on the first platform)
 *
 * Device types acceptable:
 *  GPU,gpu,CPU,cpu,DEF,ALL,ACC
 *  DEF = Default (depends on the primary platform)
 *  ALL = Any
 *  ACC = Accelerator (Cell/BE, FPGA)
 *
 * If you are running the Windows version, then the stdout of the tests is
 *  internally redirected to a file
 */
int main(int argc, char *argv[]){

std::cout<<argv[0]<<std::endl;

SelfTest *tester;

char dev[10];
sprintf(dev,"ALL");
int platformid,devid;
platformid=-1;
devid=-1;

if(argc==2){
  if(!strcmp(argv[1],"help")){
    std::cout << message() <<std::endl;
    exit(1);
  }else{
    strcpy(dev,argv[1]);
  }
}

if(argc==4){
  strcpy(dev,argv[1]);
  platformid = atoi(argv[2]);
  devid = atoi(argv[3]);
}

try{
  tester = new SelfTest;  
}catch (char const *msg){
  std::cout << msg <<std::endl;
  exit(1);
}

try{
  //tester->query_for_gpu();
  tester->startTest_Modes(dev,platformid,devid);
}catch (char *msg){
  std::cout << msg <<std::endl;  
}catch (const char *msg){
  std::cout << msg <<std::endl;  
}catch (...){
  std::cout << "Exception raised" <<std::endl;
}

delete tester;

return 0;
};





