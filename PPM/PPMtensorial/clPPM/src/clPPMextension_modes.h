
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <complex>
#include <CL/opencl.h>

#if defined (_WIN32) && defined(_DLLEXPORT)
#define __w32dll_compat __declspec(dllexport)
#else 
#define __w32dll_compat 
#endif

using namespace std;

/**
 * \brief Class clOnda calculates the Electric and Magnetic field
 *
 * clOnda class is the OpenCL port, in terms of functionality, of the
 * PPM Onda class.
 *
 * clOnda configures and launches kernels that calculate the Electric and Magnetic field
 * of the substrate, the layer and the free surface.
 */
class __w32dll_compat clOnda{

public:
  clOnda();
  ~clOnda();

  //main calculation (invocation of kernels)
  void CreateModes(int &scanLen,int &uniquelayers,ocl_config_type *oclconfig);

  //results
  void getModesResults(int &scanLen,int &uniquelayers,cl_mem d_Kz,cl_mem d_E,cl_mem d_B,
                        complex<double> *Kz,complex<double> *E,complex<double>*B,ocl_config_type *oclconfig);

  //intermediate results of each kernel
  void getModesResults_debug(int &scanLen,int &uniquelayers,int &summode,cl_mem d_Kx,cl_mem d_Sistem, cl_mem d_NonZeri,
                                  cl_mem d_As, double *Kx, complex<double> *Sistem, int *NonZeri, complex< double > *As,
                                  ocl_config_type *oclconfig
                            );

  /**
   * Internal time counter in ms. It holds the total execution time of CreateModes
   */
  float clOnda_GPU_time;
};

