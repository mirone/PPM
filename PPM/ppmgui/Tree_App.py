


#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/



import sys
import os
import subprocess
import PyQt4.Qt as qt 
from PyQt4.Qt import *

from PyQt4.QtGui  import *
from PyQt4.QtCore  import *


# from PyQt4.Qt.QTableWidget  import *
from DocMenager import *
from general_Tree import Tree
from Tree_View import TreeView
from QTypeObject import *
import constVariable

QIconSet = QIcon


dirname = os.path.dirname(__file__)
ICONDIR= os.path.join( dirname,"..",  "data" , "Icons")


while   (len(dirname) > 4):
	filenew=os.path.join(ICONDIR ,'new.png')
	if(os.path.exists(filenew)):
		# print filenew, " esiste "
		break
	# else:
	# 	print filenew, "non esiste "
	dirname = os.path.dirname(dirname)
	ICONDIR= os.path.join( dirname,  "data", "Icons" )


PYSIGNAL=SIGNAL


import tipo

#*************************************CLASS WindowForCopy***********************************************************-

#*******************************CLASS TreeApp*******************************************************
class TreeApp(QMainWindow):

	def __init__(self,*args):
		QMainWindow.__init__(self, *args)
		

		#self.showFullScreen ()
		#setWState(Qt.WindowFullScreen)
		
		if tipo.TENSORIAL:
			self.setWindowTitle('XMLGUI TENSORIAL')
		else:
			self.setWindowTitle('XMLGUI  SCALAR')
			
		self.setMinimumSize (QSize (300,300))
		
		self.windowActifList=[] #pour la passage antre les fenetres
		
		#self.start=1

		self.initActions()
		self.initMenuBar()

		self.initToolBar()
		self.initStatusBar()

		self.initWidjet()
		self.initElements()
		self.initChildren()
		self.initModification()
		self.initDefinition()

		self.slotNewTree()
		#self.initTree()
		#self.initView()

	def initActions(self):
		basedir=os.path.dirname(__file__)
		if basedir=="":
			basedir="."
		dirname=basedir+"/"
		filenew=os.path.join(ICONDIR ,'new.png')

		fileopen = os.path.join(ICONDIR ,'open.png')
		filesave = os.path.join(ICONDIR ,'Save.png')

		editundo = os.path.join(ICONDIR ,'Undo.png')
		editdef  = os.path.join(ICONDIR ,'no_GoToDef.png')
		editcut  = os.path.join(ICONDIR ,'cut2.png')
		editcopy = os.path.join(ICONDIR ,'copy.png')
		editcopySpec = os.path.join(ICONDIR ,'no_copySpec.png')
		editpast =  os.path.join(ICONDIR ,'no_past.png')
		editdel = os.path.join(ICONDIR ,'no_delete.png')
		editfind = os.path.join(ICONDIR ,'no_Find.png')

		iconNew=QIconSet(QPixmap(filenew))
		iconOpen=QIconSet(QPixmap(fileopen))
		iconSave=QIconSet(QPixmap(filesave))

		iconUndo=QIconSet(QPixmap(editundo))
		iconDef=QIconSet(QPixmap(editdef))
		iconCut=QIconSet(QPixmap(editcut))
		iconCopy=QIconSet(QPixmap(editcopy))
		iconCopySpec=QIconSet(QPixmap(editcopySpec))
		iconPast=QIconSet(QPixmap(editpast))
		iconDel=QIconSet(QPixmap(editdel))
		iconFind=QIconSet(QPixmap(editfind))


		self.actions={}
		#les actions pour le menu File
		self.actions['fileNew']=QAction(iconNew,'&New',self)
		self.actions['fileNew'].setShortcut('CTRL+N'),
		
		self.actions['fileOpen']=QAction(iconOpen,'&Open',self)
		self.actions['fileOpen'].setShortcut('CTRL+O')
		
## 		self.actions['fileOpenExamples']=QAction(iconOpen,'Open Examp&les',self)
## 		self.actions['fileOpenExamples'].setShortcut('CTRL+l')
		
		self.actions['fileSave']=QAction(iconSave,'&Save',self)
		self.actions['fileSave'].setShortcut('CTRL+S')
		self.actions['fileSaveAs']=QAction('Save as...' ,    self)

		self.actions['fileExit']=QAction('Exit',self)
		self.actions['fileExit'].setShortcut('CTRL+Q')
		self.actions['cascade'] = QAction('Cascade',  self)

		#les actions pour le menu Edit
		self.actions['editUndo']=QAction(iconUndo,'&Undo', self)
		self.actions['editUndo'].setShortcut('CTRL+Z') 
		self.actions['editDef']=QAction(iconDef,'&Go to def', self)
		self.actions['editDef'].setShortcut('CTRL+G') 
		self.actions['editCut']=QAction(iconCut,'&Cut',self)
		self.actions['editCut'].setShortcut('CTRL+X')
		self.actions['editCopy']=QAction(iconCopy,'&Copy',self)
		self.actions['editCopy'].setShortcut('CTRL+C')
		# self.actions['editCopySpec']=QAction(iconCopySpec,'&Copy special',self)
		# self.actions['editCopySpec'].setShortcut('CTRL+P'),
		self.actions['editPast']=QAction(iconPast,'&Past',self)
		self.actions['editPast'].setShortcut('CTRL+V')
		self.actions['editDel']=QAction(iconDel,'&Delete',self)
		self.actions['editDel'].setShortcut('CTRL+D')
		self.actions['editFind']=QAction(iconFind,'&Find',self)
		self.actions['editFind'].setShortcut('CTRL+F')
		self.actions['editFindNext']=QAction('Find next',self)
		self.actions['editFindNext'].setShortcut('F3')

		#les actions pour le menu View
		self.actions['viewElem']=QAction('Add element',self)
		self.actions['viewElem'].setShortcut('CTRL+A')
		self.actions['viewElem'].setCheckable(1)
		self.actions['viewElem'].setChecked (1)

		self.actions['viewChild']=QAction('Add subelements',self)
		self.actions['viewChild'].setShortcut('CTRL+T')
		self.actions['viewChild'].setCheckable(1)
		self.actions['viewChild'].setChecked (1)

		self.actions['viewEdit']=QAction('Edit element',self)
		self.actions['viewEdit'].setShortcut('CTRL+E')
		self.actions['viewEdit'].setCheckable(1)
		self.actions['viewEdit'].setChecked (1)

		self.actions['viewDef']=QAction('Definition of element',self)
		self.actions['viewDef'].setShortcut('CTRL+I')
		self.actions['viewDef'].setCheckable(1)
		self.actions['viewDef'].setChecked (1)

		#les actions pour le menu Run
		self.actions['run']=QAction('Run',self)
		self.actions['run'].setShortcut('CTRL+R')

		self.actions['rload']=QAction('Load Vars',self)

		#self.actions['stop']=QAction('Stop',self)
		# self.actions['stop'].setShortcut('CTRL+B')
		self.actions['graph']=QAction('Graph',self)
		self.actions['graph'].setShortcut('CTRL+M')

		#les actions pour le menu Help
		self.actions['help']=QAction('Help',self)
		self.actions['help'].setShortcut('F1')
		self.actions['helpAut']=QAction('Automatical help',self)
		self.actions['helpAut'].setShortcut('CTRL+H')
		self.actions['helpAut'].setCheckable(1)
		self.actions['helpAut'].setChecked (0)
		self.actions['helpAuthor']=QAction('Author',self)
		self.actions['helpAuthor'].setShortcut('F2')


		#les signals pour  les actions de le menu File
		self.connect(self.actions['fileNew'],SIGNAL('triggered(bool)'),self.slotNewTree)
		self.connect(self.actions['fileOpen'],SIGNAL('triggered(bool)'),self.slotOpenFile)

# 		self.connect(self.actions['fileOpenExamples'],SIGNAL('triggered(bool)'),self.slotOpenFileExamples)
		
		self.connect(self.actions['fileSave'],SIGNAL('triggered(bool)'),self.slotGetStrForSave)
		self.connect(self.actions['fileSaveAs'],SIGNAL('triggered(bool)'),self.slotGetStrForSaveAs)
		self.connect(self.actions['fileExit'],SIGNAL('triggered(bool)'),self.slotExit)
		self.connect(self.actions['cascade'],SIGNAL('triggered(bool)'),self.Cascade)

		#les signals pour  les actions de le menu Edit
		self.connect(self.actions['editUndo'],SIGNAL('triggered(bool)'),self.slotUndo)
		self.connect(self.actions['editDef'],SIGNAL('triggered(bool)'),self.slotFindDef)
		self.connect(self.actions['editCut'],SIGNAL('triggered(bool)'),self.slotCut)
		self.connect(self.actions['editCopy'],SIGNAL('triggered(bool)'),self.slotCopy)
		# self.connect(self.actions['editCopySpec'],SIGNAL('triggered(bool)'),self.slotCopySpecial)
		self.connect(self.actions['editPast'],SIGNAL('triggered(bool)'),self.slotInsert)
		self.connect(self.actions['editDel'],SIGNAL('triggered(bool)'),self.slotDelete)
		self.connect(self.actions['editFind'],SIGNAL('triggered(bool)'),self.slotFind)
		self.connect(self.actions['editFindNext'],SIGNAL('triggered(bool)'),self.slotFindNext)


		##les signals pour  les actions de le menu View
		self.connect(self.actions['viewElem'],SIGNAL('triggered ( bool)'),self.slotShowHideElement)
		self.connect(self.actions['viewChild'],SIGNAL('triggered ( bool)'),self.slotShowHideChildrenGroup)
		self.connect(self.actions['viewEdit'],SIGNAL('triggered ( bool)'),self.slotShowHideEdit)
		self.connect(self.actions['viewDef'],SIGNAL('triggered ( bool)'),self.slotShowHideDefinition)

		##les signals pour  les actions de le menu Run
		self.connect(self.actions['run'],  SIGNAL('triggered()'),self.slotRun)
		self.connect(self.actions['rload'],  SIGNAL('triggered()'),self.slotRunLoad)
		#self.connect(self.actions['stop'],SIGNAL('triggered()'),self.slotStop)
		self.connect(self.actions['graph'],SIGNAL('triggered()'),self.slotGraph)

		##les signals pour  les actions de le menu Help
		self.connect(self.actions['help'],SIGNAL('triggered(bool )'),self.slotGiveWhatsThis)

		self.connect(self.actions['helpAut'],SIGNAL('triggered(bool )'),self.slotHelpAut)
		self.connect(self.actions['helpAuthor'],SIGNAL('triggered(bool )'),self.slotGiveAuthor)

	def Cascade(self):
		self.viewsSpace.cascadeSubWindows () 

	def initMenuBar(self):
		#menu File

		self.menuFile= self.menuBar().addMenu("&File")
	        self.menuFile.addAction(   self.actions['fileNew'])
		self.actions['fileOpen'].setWhatsThis ('create a new file' )

	
		self.menuFile.addAction(self.actions['fileOpen'] )
		self.actions['fileOpen'].setWhatsThis ('Open the file' )
		
# 		self.menuFile.addAction(self.actions['fileOpenExamples'] )
#		self.actions['fileOpenExamples'].setWhatsThis ('Open an Example file' )
		self.menuFile.insertSeparator(None)

		self.menuFile.addAction(self.actions['fileSave'] )
		self.actions['fileSave'].setWhatsThis ('Save the file' )

		self.menuFile.addAction(self.actions['fileSaveAs'] )
		self.menuFile.insertSeparator(None)

		self.menuFile.addAction(self.actions['fileExit'] )
		self.menuFile.addAction(self.actions['cascade'] )

		#menu Edit
		self.menuEdit=self.menuBar().addMenu("&Edit")
		
		self.menuEdit.addAction(self.actions['editUndo'] )
		self.actions['editUndo'].setWhatsThis ('Undo' )

		self.menuEdit.addAction(self.actions['editDef'] )
		self.actions['editDef'].setWhatsThis ('Go to definition' )
		self.menuEdit.insertSeparator(None)

		self.menuEdit.addAction(self.actions['editCut'] )
		self.actions['editCut'].setWhatsThis ('Cut the element' )
		self.menuEdit.insertSeparator(None)


		self.menuEdit.addAction(self.actions['editCopy'] )
		self.actions['editCopy'].setWhatsThis ('Copy the element (without names)' )

		# self.menuEdit.addAction(self.actions['editCopySpec'] )
		# self.actions['editCopySpec'].setWhatsThis ('Copy the element (with names)' )
		# self.menuEdit.insertSeparator(None)


		self.menuEdit.addAction(self.actions['editPast'] )
		self.actions['editPast'].setWhatsThis ('Past the element' )

		self.menuEdit.addAction(self.actions['editDel'] )
		self.actions['editDel'].setWhatsThis ('Delete the element' )

		self.menuEdit.addAction(self.actions['editFind'] )
		self.actions['editFind'].setWhatsThis ('Find variables' )

		self.menuEdit.addAction(self.actions['editFindNext'] )
		self.actions['editFindNext'].setWhatsThis ('Find next variable' )

		#menu View
		self.menuView=self.menuBar().addMenu("&View") 
		self.menuView.addAction(self.actions['viewElem'] )
		self.menuView.addAction(self.actions['viewChild'] )
		self.menuView.addAction(self.actions['viewEdit'] )
		self.menuView.addAction(self.actions['viewDef'] )
		#self.actions['viewElem'].setWhatsThis ('Show' )

		#menu Run
		self.menuRun=self.menuBar().addMenu("&Run") 
		self.menuRun.addAction(self.actions['run'] )
		self.menuRun.addAction(self.actions['rload'] )
		#self.menuRun.addAction(self.actions['stop'] )
		# self.menuRun.addAction(self.actions['graph'] )

		#menu Help
		self.menuHelp=self.menuBar().addMenu("&Help") 
		self.menuHelp.addAction(self.actions['help'] )
		self.menuHelp.addAction(self.actions['helpAut'] )
		self.menuHelp.addAction(self.actions['helpAuthor'] )



	def initToolBar(self):
		self.toolBar= self.addToolBar("Operations")
		# QToolBar('operations')
		self.toolBar.addAction(self.actions['fileNew'] )
		self.toolBar.addAction(self.actions['fileOpen'] )
		self.toolBar.addAction(self.actions['fileSave'] )
		self.toolBar.addSeparator ()

		self.toolBar.addAction(self.actions['editUndo'] )
		self.toolBar.addAction(self.actions['editDef'] )
		self.toolBar.addSeparator ()
		self.toolBar.addAction(self.actions['editCut'] )
		self.toolBar.addAction(self.actions['editCopy'] )
		# self.toolBar.addAction(self.actions['editCopySpec'] )
		self.toolBar.addAction(self.actions['editPast'] )
		self.toolBar.addAction(self.actions['editDel'] )
		self.toolBar.addSeparator ()
		self.toolBar.addAction(self.actions['editFind'] )
		self.toolBar.addSeparator ()
		# QWhatsThis.whatsThisButton(self.toolBar)

	def initStatusBar(self):
		basedir=os.path.dirname(__file__)
		if basedir=="":
			basedir="."
		dirname=basedir+"/"

		self.statusErrorIcon=os.path.join(ICONDIR ,'warning.png')
		self.statusIcon=os.path.join(ICONDIR ,'no_Find.png')

		self.statusBar= self.addToolBar(  "StatusBar" )

		self.statusLabel=QLabel()
		self.statusBar.addWidget(self.statusLabel )
		self.statusLbForIcon=QLabel()
		self.statusBar.addWidget(self.statusLbForIcon)
		self.statusLbForIcon.setPixmap (QPixmap(self.statusIcon ))
		self.statusLbForIcon.hide()

		self.strMessage='        Use "Add subelements with attributes"'

		self.connect(self.statusBar,SIGNAL('messageChanged ( const QString &)'),self.slotMessageChanged)


	def initWidjet(self):

		#self.cursor=QCursor()
		self.font=QFont("Courier",14)

		
		# self.mainWidget=QWidget(self)
		

		# self.setCentralWidget(self.mainWidget)

		self.actifView=None
		self.bufferApp=None
		self.helpAut=0
		self.stringWarning=''


		self.viewsSpace=QMdiArea ()
		self.setCentralWidget(self.viewsSpace)
		# self.viewsSpace.setViewMode(QMdiArea.SubWindowView)

		
		self.connect(self.viewsSpace,SIGNAL('subWindowActivated ( QMdiSubWindow *)'),self.slotWindowActivated)


		self.rightLayWidgetA = QWidget()
		self.rightLayoutA=QVBoxLayout(self.rightLayWidgetA)# 5)

		
		self.rightLayWidgetB = QWidget()
		self.rightLayoutB=QVBoxLayout(self.rightLayWidgetB)# 5)

		
		self.docMenager=DocMenager(self)
		self.connect(self.docMenager,PYSIGNAL('sigChangedNumberOfDoc'),self.slotChangedNumberOfDoc )
		self.connect(self.docMenager,PYSIGNAL('sigShowMassegeWarning'),self.slotShowMessageWarning )
		self.connect(self.docMenager,PYSIGNAL('sigCreatview'),self.slotConnectView)
		self.connect(self.docMenager,PYSIGNAL('sigCreatDoc'),self.slotConnectDoc)
		#self.connect(self,SIGNAL('pixmapSizeChanged(bool)'),self.slotSizeChanged)


		dockWidgetA =  QDockWidget("Dock Widget", self)
		dockWidgetA.setAllowedAreas(Qt.LeftDockWidgetArea |  Qt.RightDockWidgetArea);

		dockWidgetB =  QDockWidget("Dock Widget", self)
		dockWidgetB.setAllowedAreas(Qt.LeftDockWidgetArea |  Qt.RightDockWidgetArea);

		dockWidgetA.setWidget(self.rightLayWidgetA );
		self.addDockWidget(Qt.RightDockWidgetArea, dockWidgetA);

		dockWidgetB.setWidget(self.rightLayWidgetB );
		self.addDockWidget(Qt.RightDockWidgetArea, dockWidgetB);



	def initElements(self):
		self.fontOther=QFont("Courier",11)
     
                self.elementsGroup=QGroupBox( " Add Things "  )
		self.elementsGroup.setFont(self.fontOther)

		self.elements=TableOfElem()
		self.elements.setFont(self.fontOther)
		self.connect(self.elements,PYSIGNAL('sigInsertHelp'),self.slotInsertHelp)

		self.lbType=QLabel('Enter the name of type')
		self.lbType.setFont(self.fontOther)
		self.oldText=''

		self.leType=LnEditOfElem()
		self.leType.setFont(self.fontOther)
		self.connect(self.leType,PYSIGNAL('sigDoInsert'),self.slotDoInsertHelp)
		self.connect(self.leType,SIGNAL('textChanged ( const QString & )'),self.slotTextChanged )


		vbox = QVBoxLayout()
		
		
		vbox.addWidget( self.elements , 4)
		# vbox.addSpacing(2)
		vbox.addWidget( self.lbType )
		# vbox.addSpacing(2)
		vbox.addWidget( self.leType  )

		self.elementsGroup.setLayout(vbox)
		# self.rightLayout.addSpacing(2)
		self.rightLayoutA.addWidget(self.elementsGroup,1 )


		
		
# 		self.elementsGroup=QVGroupBox( 'Add a new element',self.mainWidget)
# 		self.elementsGroup.setFont(self.fontOther)
# 		self.elements=TableOfElem(self.elementsGroup)
# 		self.elements.setFont(self.fontOther)
# 		self.connect(self.elements,PYSIGNAL('sigInsertHelp'),self.slotInsertHelp)
# 		self.lbType=QLabel('Enter the name of type',self.elementsGroup)
# 		self.lbType.setFont(self.fontOther)
# 		self.oldText=''
# 		self.leType=LnEditOfElem(self.elementsGroup)
# 		self.leType.setFont(self.fontOther)
# 		self.connect(self.leType,PYSIGNAL('sigDoInsert'),self.slotDoInsertHelp)
# 		self.connect(self.leType,SIGNAL('textChanged ( const QString & )'),self.slotTextChanged )

# #		self.rightLayout.addWidget(self.titleElements)
# 		self.rightLayout.addWidget(self.elementsGroup,3 )



	def initChildren(self):
		basedir=os.path.dirname(__file__)
		if basedir=="":
			basedir="."
		dirname=basedir+"/"

		self.childrenGroup=QGroupBox( "Add subelements with attributes"  )
		self.childrenGroup.setFont(self.fontOther)

		self.children= TableForchildren()
		self.connect(self.children,PYSIGNAL('sigTextChanged'),self.slotTextChangedInLE )
		self.connect(self.children,PYSIGNAL('sigTakeChildrenChecked'),self.slotTakeChildrenChecked)
		self.connect(self.children,PYSIGNAL('sigMessageError'),self.slotShowMessageError)

		self.buttonGroup=QGroupBox()
		# self.buttonGroup.setInsideSpacing (8)
		# self.buttonGroup.setSpacing (8)

		ChoiceAll=os.path.join(ICONDIR ,'choiceAll2.png')
		NotChoice=os.path.join(ICONDIR ,'Notchoice.png')
		ContrChoice=os.path.join(ICONDIR ,'Contraire.png')

		iconChoiceAll=QIconSet(QPixmap(ChoiceAll))
		iconNotChoice=QIconSet(QPixmap(NotChoice))
		iconContrChoice=QIconSet(QPixmap(ContrChoice))

		self.btChoiceAll=QPushButton(iconChoiceAll,'')
		self.btChoiceAll.setMaximumSize(QSize (25,25))

		self.btNotChoice=QPushButton(iconNotChoice,'')
		self.btNotChoice.setMaximumSize(QSize (25,25))

		self.btContrChoice=QPushButton(iconContrChoice,'')
		self.btContrChoice.setMaximumSize(QSize (25,25))

		self.btOk=QPushButton('Ok')
		self.btOk.setMaximumSize(QSize (25,25))
		self.buttonGroup.setFlat (1)

		self.connect(self.btChoiceAll,SIGNAL('clicked()'),self.children.slotCooseAll)
		self.connect(self.btNotChoice,SIGNAL('clicked()'),self.children.slotNoCoice)
		self.connect(self.btContrChoice,SIGNAL('clicked()'),self.children.slotContCoice)
		self.connect(self.btOk,SIGNAL('clicked()'),self.slotGetChildrenChecked)
		self.connect(self.children,PYSIGNAL('sigKeepFocus'),self.slotKeepFocus)

		vbox = QHBoxLayout()
		# vbox.addWidget(   iconChoiceAll  )
		vbox.addWidget( self.btChoiceAll )
		vbox.addWidget( self.btNotChoice )
		vbox.addWidget( self.btContrChoice )
		vbox.addWidget(  self.btOk) 
		self.buttonGroup.setLayout(vbox)

		hbox = QVBoxLayout()
		# hbox.setSpacing (8)
		hbox.addWidget( self.children, 1 )
		hbox.addWidget( self.buttonGroup, 1 )
		self.childrenGroup.setLayout(hbox)
		# self.rightLayout.addSpacing(2)
		self.rightLayoutA.addWidget(self.childrenGroup,1)		
		#
## 		# --------------------------------------------------------
## 		#

		
## 		self.childrenGroup=QHGroupBox( 'Add subelements with attributes',self.mainWidget)
## 		self.childrenGroup.setFont(self.fontOther)

		

## 		#self.childrenGroup.setAlignment (Qt.AlignHCenter)
## 		self.children= TableForchildren(self.childrenGroup)
## 		self.connect(self.children,PYSIGNAL('sigTextChanged'),self.slotTextChangedInLE )
## 		self.connect(self.children,PYSIGNAL('sigTakeChildrenChecked'),self.slotTakeChildrenChecked)
## 		self.connect(self.children,PYSIGNAL('sigMessageError'),self.slotShowMessageError)

## 		#self.childrenLayout=QVBoxLayout(self.childrenGroup,5,1)
## 		self.buttonGroup=QVButtonGroup(self.childrenGroup)
## 		self.buttonGroup.setInsideSpacing (8)

## 		ChoiceAll=dirname+'Icons/choiceAll2.png'
## 		NotChoice=dirname+'Icons/Notchoice.png'
## 		ContrChoice=dirname+'Icons/Contraire.png'

## 		iconChoiceAll=QIconSet(QPixmap(ChoiceAll))
## 		iconNotChoice=QIconSet(QPixmap(NotChoice))
## 		iconContrChoice=QIconSet(QPixmap(ContrChoice))

## 		self.btChoiceAll=QPushButton(iconChoiceAll,'',self.buttonGroup)
## 		self.btChoiceAll.setMaximumSize(QSize (50,25))

## 		self.btNotChoice=QPushButton(iconNotChoice,'',self.buttonGroup)
## 		self.btNotChoice.setMaximumSize(QSize (50,25))

## 		self.btContrChoice=QPushButton(iconContrChoice,'',self.buttonGroup)
## 		self.btContrChoice.setMaximumSize(QSize (50,25))

## 		self.btOk=QPushButton('Ok',self.buttonGroup)
## 		self.btOk.setMaximumSize(QSize (50,25))
## 		self.buttonGroup.setFlat (1)

## 		self.connect(self.btChoiceAll,SIGNAL('clicked()'),self.children.slotCooseAll)
## 		self.connect(self.btNotChoice,SIGNAL('clicked()'),self.children.slotNoCoice)
## 		self.connect(self.btContrChoice,SIGNAL('clicked()'),self.children.slotContCoice)
## 		self.connect(self.btOk,SIGNAL('clicked()'),self.slotGetChildrenChecked)
## 		self.connect(self.children,PYSIGNAL('sigKeepFocus'),self.slotKeepFocus)


## 		self.rightLayout.addWidget(self.childrenGroup,2)

	def initModification(self):
		#la cellule ou on change la valeur pour <f> et <s>
		self.positionValue=0

		self.editGroup=QGroupBox( 'Edit an element'  )
		self.editGroup.setFont(self.fontOther)

		self.modification= TableForEdit()
		self.comment=TextForComment()
		self.comment.hide()

		self.btAddComment=QPushButton('Add new comment')
		self.btAddComment.setMaximumSize(QSize (170,25))
		self.btOkComment=QPushButton('Modify')
		self.btOkComment.setMaximumSize(QSize (100,25))
		self.btOkComment.hide()
		#self.connect(self.comment,PYSIGNAL('sigDoChangeComment'),self.slotDoChangeComment)
		self.connect(self.btAddComment,SIGNAL('clicked()'),self.slotAddComment)
		self.connect(self.btOkComment,SIGNAL('clicked()'),self.slotDoChangeComment)





		vbox = QVBoxLayout()
		vbox.addWidget( self.modification, 1 )
		vbox.addWidget( self.comment)
		vbox.addWidget(self.btAddComment )
		vbox.addWidget( self.btOkComment)
		self.editGroup.setLayout(vbox)
		# self.rightLayout.addSpacing(2)
		self.rightLayoutB.addWidget(self.editGroup,1)


		self.connect(self.modification,PYSIGNAL('sigTypeChanged'),self.slotTypeChanged)
		self.connect(self.modification,PYSIGNAL('sigPointerChanged'),self.slotPointerChanged)
		self.connect(self.modification,PYSIGNAL('sigNameChanged'),self.slotNameChanged)
		self.connect(self.modification,PYSIGNAL('sigValueChanged'),self.slotValueChanged)
		self.connect(self.modification,PYSIGNAL('sigDoChangeType'),self.slotDoChangeType)
		self.connect(self.modification,PYSIGNAL('sigAttrValueChanged'),self.slotAttrValueChanged)
		self.connect(self.modification,PYSIGNAL('sigShowBigText'),self.slotShowBigText)
		self.connect(self.modification,PYSIGNAL('sigShowMessageWarning'),self.slotShowMessageWarning)



## 		self.editGroup=QVGroupBox( 'Edit an element',self.mainWidget)
## 		self.editGroup.setFont(self.fontOther)

## 		self.modification= TableForEdit(self.editGroup)
## 		#le travail avec les commentaire
## 		self.comment=TextForComment(self.editGroup)
## 		self.comment.hide()


## 		self.btAddComment=QPushButton('Add new comment',self.editGroup)
## 		self.btAddComment.setMaximumSize(QSize (170,25))

## 		self.btOkComment=QPushButton('Modify',self.editGroup)
## 		self.btOkComment.setMaximumSize(QSize (100,25))
## 		self.btOkComment.hide()


## 		#self.connect(self.comment,PYSIGNAL('sigDoChangeComment'),self.slotDoChangeComment)
## 		self.connect(self.btAddComment,SIGNAL('clicked()'),self.slotAddComment)
## 		self.connect(self.btOkComment,SIGNAL('clicked()'),self.slotDoChangeComment)

## 		self.rightLayout.addWidget(self.editGroup,2)
## 		#self.editGroup.hide()

## 		self.connect(self.modification,PYSIGNAL('sigTypeChanged'),self.slotTypeChanged)
## 		self.connect(self.modification,PYSIGNAL('sigPointerChanged'),self.slotPointerChanged)
## 		self.connect(self.modification,PYSIGNAL('sigNameChanged'),self.slotNameChanged)
## 		self.connect(self.modification,PYSIGNAL('sigValueChanged'),self.slotValueChanged)
## 		self.connect(self.modification,PYSIGNAL('sigDoChangeType'),self.slotDoChangeType)
## 		self.connect(self.modification,PYSIGNAL('sigAttrValueChanged'),self.slotAttrValueChanged)
## 		self.connect(self.modification,PYSIGNAL('sigShowBigText'),self.slotShowBigText)
## 		self.connect(self.modification,PYSIGNAL('sigShowMessageWarning'),self.slotShowMessageWarning)


	def initDefinition(self):
		self.defGroup=QGroupBox( 'Definition')
		self.defGroup.setFont(self.fontOther)

		self.definition=QTextEdit()
		self.definition.setWordWrapMode(QTextOption.WrapAnywhere)
		# self.definition.setWordWrap (QTextEdit.NoWrap )
		self.definition.setReadOnly(1)
		self.definition.setFont(self.fontOther)
		#self.definition.setMinimumWidth(500)


		vbox = QVBoxLayout()
		vbox.addWidget (self.definition, 1)
		self.defGroup.setLayout(vbox)
		
		self.rightLayoutB.addWidget(self.defGroup,1)


	'''
	def slotSizeChanged(self,changed):
		if changed :

			newWidth=2*int(self.width()/3)
			self.view.setFixedWidth (newWidth)
	'''

	def queryCloseDocument(self,document):
		closeDocument=1
		if (document.isModified()):
			BoxQuestion=QMessageBox(self)
			strQuestion='Do you want to save the chages in  '+ document.title + ' ?'
			#BoxWarning.setFont (QFont("Courier",16))# self.font
			#BoxWarning.setTextFormat(Qt.PlainText )
			answer=BoxQuestion.question(self,'Warning',strQuestion,QMessageBox.Yes, QMessageBox.No,QMessageBox.Cancel)
			if(answer==BoxQuestion.Yes):
				closeDocument=self.slotGetStrForSave()
			elif (answer==BoxQuestion.Cancel):
				closeDocument=0
		return closeDocument

	def setActifView(self,view):
		self.actifView=view

	def isRunning(self,fileName):
		return 0

	def runFile(self,*args):
		import sys
		import os.path
		import os
		print " il programma e stato lanciato come " , sys.argv[0]
		print " gli argomenti sono " , args
		import tipo
		repe = os.path.dirname( sys.argv[0])
		if sys.platform != "win32":
			if repe[0]!="/":
				repe=os.path.join( os.getcwd() , repe)
			
		if tipo.TENSORIAL:
			program = os.path.join(repe, "ppmxmlTens.py")
			alternative = os.path.join(repe, "ppmxmlTens.exe")
		else:
			program = os.path.join(repe, "ppmxml.py")
			alternative = os.path.join(repe, "ppmxml.exe")

		inputfile=args[0]
		dirinput=os.path.dirname(inputfile)
		baseinput=os.path.basename(inputfile)

		oldir=os.getcwd()
		os.chdir(dirinput)
		print " chdir to ", dirinput
		print " alternative " , alternative
		try:
			if os.path.exists(alternative):
				args=(alternative,)+(baseinput, )
			else:
				args=("python",program,)+(baseinput, )

			print " args " , args

			res=subprocess.call(args)
			# res=subprocess.call(args, shell=True)
			print " fine comando " 
		except:
			os.chdir(oldir)
			raise
		os.chdir(oldir)

    		return res


	def runFileLoad(self,*args):
		import sys
		import os.path
		import os

		import tipo
		repe = os.path.dirname( sys.argv[0])
		if tipo.TENSORIAL:
			program = os.path.join(repe, "ppmxmlTens.py")
			alternative = os.path.join(repe, "ppmxmlTens.exe")
		else:
			program = os.path.join(repe, "ppmxml.py")
			alternative = os.path.join(repe, "ppmxml.exe")

		inputfile=args[0]
		dirinput=os.path.dirname(inputfile)
		baseinput=os.path.basename(inputfile)
		oldir=os.getcwd()
		os.chdir(dirinput)
		try:
			if os.path.exists(alternative):
				args=(alternative,)+(baseinput, )
			else:
				args=("python",program,)+(baseinput, )

			# fileName = str(QFileDialog.getOpenFileName(directory='',filter="all files (*)",parent=self,caption="choose the file with variables",))

			fileName = str(QFileDialog.getOpenFileName(self,"choose the file with variables",'',"all files (*)"))
		
			if len(fileName):

				args= args+(fileName, ) 
				
				print " args " , args

				res=subprocess.call(args, shell=True)
				print " fine comando " 
			os.chdir(oldir)
			return res
		except:
			os.chdir(oldir)
			raise


#-----------SLOTS of widjets-------------------------------------------------------------------
	def slotOpenFile(self):
		# fileName = str(QFileDialog.getOpenFileName(directory='',filter="XML files (*.xml)",parent=self,caption="open file dialog",))
		fileName = str(QFileDialog.getOpenFileName(self,"open file dialog", '',"XML files (*.xml)",))

                    # "Choose a XML file" ))
		if (len(fileName)!=0):
			document=self.docMenager.documentWhithName(fileName)
			if (document!=None):
				#il faut faire la vue de ce fichier comme la vue active
				view=self.docMenager.view(document)[0]
				view.setFocus()
			else:
				
				f=open(fileName,'r')
				document=self.docMenager.createDocument()
				document.slotInitialise(f)
				document.setFileName(fileName)
				view=self.docMenager.view(document)[0]
				view.setWindowTitle(document.title)
				view.setFocus()
				self.setEnabledUndo(0)
			
	def slotOpenFileExamples(self):
		fileName = str(QFileDialog.getOpenFileName(os.getenv("PPMEXAMPLES"),"XML files (*.xml)",self,"open file dialog",
                    "Choose a XML file" ))
		if (len(fileName)!=0):
			document=self.docMenager.documentWhithName(fileName)
			if (document!=None):
				#il faut faire la vue de ce fichier comme la vue active
				view=self.docMenager.view(document)[0]
				view.setFocus()
			else:
				f=open(fileName,'r')
				document=self.docMenager.createDocument()
				document.slotInitialise(f)
				document.setFileName(fileName)
				view=self.docMenager.view(document)[0]
				view.setWindowTitle(document.title)
				view.setFocus()
				self.setEnabledUndo(0)


	def slotNewTree(self):
	
		document=self.docMenager.createDocument()
		 #if (self.start):
		self.actifView=self.docMenager.view(document)[0]
			#self.slotConnectView(self.actifView)
			#self.slotConnectDoc(document)
		document.slotNew()
		self.setEnabledUndo(0)
		#self.start=0

	def slotGetStrForSave(self):
		doSave=1
		#on recupere le document actif
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			if(document.isCorrect()):
				strErrorOfValidation=document.isValidated()
				if(len(strErrorOfValidation)==0):
					if (document.isModified()):
						document.slotGetStrForSave(0)
				else:
					doSave=0
					stringWarning='This document is not validated'

			else:
				doSave=0
				stringWarning='This document is not correct'
		if not doSave:self.slotShowBoxMessageWarning((stringWarning,))
		return doSave

	def slotGetStrForSaveAs(self):
		doSave=1
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			if(document.isCorrect()):
				strErrorOfValidation=document.isValidated()
				if(len(strErrorOfValidation)==0):
					#if (document.isModified()):
					document.slotGetStrForSave(1)
				else:
					doSave=0
					stringWarning='This document is not validated'
			else:
				doSave=0
				stringWarning='This document is not correct'
		if not doSave:self.slotShowBoxMessageWarning((stringWarning,))
		return doSave

	def slotExit(self):
		self.close()

	def closeEvent ( self,event):
		#self.emit(PYSIGNAL('sigCloseView'),())
		if (self.docMenager.closeAll()):
			apply( self.__class__.__bases__[0].closeEvent, (self,event))

	def slotWindowActivated(self,view):

		if (view!=None):
			self.actifView=view
			document=self.docMenager.document(view)
			if (document!=None): document.slotCanUndo()

	#on n'utilise pas
	def slotChangeWindow(self,indexOfWindow):

		self.windowActifList[indexOfWindow].releaseKeyboard()
		self.windowActifList[indexOfWindow].clearFocus ()
		#self.windowActifList[indexOfWindow].unsetCursor ()
		newIndexOfWindow=(indexOfWindow+1)% len(self.windowActifList)

		self.windowActifList[newIndexOfWindow].grabKeyboard ()
		self.windowActifList[newIndexOfWindow].setFocus ()
		#self.windowActifList[(indexOfWindow+1)% len(self.windowActifList)].setCursor(self.cursor)
		self.windowActifList[newIndexOfWindow].slotSetCurrentPosition()

	def slotHelpAut(self,helpAut):

		self.helpAut=helpAut


	def slotGiveAuthor(self):

		text="""
#/*##########################################################################
# Copyright (C) 2004-2012 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com 
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project 
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
		"""
		
		self.slotShowMessageInformation(text)

	def slotUndo(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			document.slotUndo()


	def setEnabledUndo(self,status):
		self.actions['editUndo'].setEnabled(status)

	# apres l'appuie sur CTRL+F
	def slotFind(self):

		self.wFind=WindowForFind()

		self.wFind.move(QPoint(300,300))

		self.wFind.show()


		self.connect(self.wFind,PYSIGNAL('sigMessageError'),self.slotShowMessageError)
		self.connect(self.wFind,PYSIGNAL('sigFind'),self.slotFindVariables)

	#slot est appele apres clique sur Ok de la fenetre Find
	def slotFindVariables(self,nameToFind):
		if (self.actifView!=None):

			document=self.docMenager.document(self.actifView)

			document.slotFindVariable(nameToFind)


	# apres l'appuie sur F3
	def slotFindNext(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			document.slotFindNextVariable()

#------------SLOTS of DocMenager----------
	#def initTree(self):
	def slotConnectDoc(self,document):

		document=document[0]

		self.connect(document,PYSIGNAL('sigShowWindowCopy'),self.slotShowWindowCopy)
		self.connect(document,PYSIGNAL('sigWriteName'),self.slotWriteName)
		self.connect(document,PYSIGNAL('sigShowHelp'),self.slotShowHelp)
		self.connect(document,PYSIGNAL('sigShowChildren'),self.slotShowChildren)
		self.connect(document,PYSIGNAL('sigShowEdit'),self.slotShowEdit)
		self.connect(document,PYSIGNAL('sigShowComment'),self.slotShowComment)#A ajouter dans le general
		self.connect(document,PYSIGNAL('sigShowDef'),self.slotShowDef)
		self.connect(document,PYSIGNAL('sigSave'),self.slotSave)
		self.connect(document,PYSIGNAL('sigFillBuffer'),self.slotFillBuffer)
		self.connect(document,PYSIGNAL('sigEnabledUndo'),self.slotEnabledUndo)



		self.connect(document,PYSIGNAL('sigMessageQuestion'),self.slotShowMessageQuestion)
		self.connect(document,PYSIGNAL('sigMessageWarning'),self.slotShowMessageWarning)
		self.connect(document,PYSIGNAL('sigMessageError'),self.slotShowMessageError)
		self.connect(document,PYSIGNAL('sigShowBoxMessageWarning'),self.slotShowBoxMessageWarning)

	#def initView(self):
	def slotConnectView(self,view):
		view=view[0]
		view.setFont(self.font)

		self.connect(view,PYSIGNAL('sigViewDoubleClicked'),self.slotShowHideChildren)
		self.connect(view,PYSIGNAL('sigViewPressed'),self.slotShowHideChildren)
		self.connect(view,PYSIGNAL('sigDelete'),self.slotDelete)
		self.connect(view,PYSIGNAL('sigCopy'),self.slotCopy)
		self.connect(view,PYSIGNAL('sigInsert'),self.slotInsert)
		self.connect(view,PYSIGNAL('sigCut'),self.slotCut)
		# self.connect(view,PYSIGNAL('sigCopySpecial'),self.slotCopySpecial)
		self.connect(view,PYSIGNAL('sigUndo'),self.slotUndo)


		self.connect(view,PYSIGNAL('sigGetHelp'),self.slotGetHelp)
		self.connect(view,PYSIGNAL('sigGetChildren'),self.slotGetChildren)
		self.connect(view,PYSIGNAL('sigGetEdit'),self.slotGetEdit)
		self.connect(view,PYSIGNAL('sigGetDef'),self.slotGetDef)
		self.connect(view,PYSIGNAL('sigChangeWindow'),self.slotChangeWindow)
		self.connect(view,PYSIGNAL('sigShowHelp'),self.slotShowHelpAut)

		self.connect(view,PYSIGNAL('sigCloseView'),self.slotCloseView)
		self.connect(view,PYSIGNAL('sigFindDef'),self.slotFindDef)
		self.connect(view,PYSIGNAL('sigChangeStatus'),self.slotChangeStatus)
		self.connect(view,PYSIGNAL('sigKeepFocus'),self.slotKeepFocus)
		self.connect(view,PYSIGNAL('sigShowMessageWarning'),self.slotShowMessageWarning)
		self.connect(view,PYSIGNAL('sigShowBoxMessageWarning'),self.slotShowBoxMessageWarning)
		self.connect(view,PYSIGNAL('sigDefinProblem'),self.slotDefinProblem)
		self.connect(view,PYSIGNAL('sigSelectCurrentElement'),self.slotSelectCurrentElement)



		#self.connect(view,PYSIGNAL('sigGiveWhatsThis'),self.slotGiveWhatsThis)
		#self.connect(view,PYSIGNAL('sigPutText'),self.slotPutText)



	def slotChangedNumberOfDoc(self):
		if (self.docMenager.numberOfDocuments()<1):
			self.actions['fileSave'].setEnabled(0)
			self.actions['fileSaveAs'].setEnabled(0)
			self.actions['editUndo'].setEnabled(0)
			self.actions['editCopy'].setEnabled(0)
			self.actions['editCut'].setEnabled(0)
			self.actions['editPast'].setEnabled(0)
			# self.actions['editCopySpec'].setEnabled(0)
			self.actions['editDel'].setEnabled(0)
			self.actions['editDef'].setEnabled(0)
			self.actifView=None# atention !!!!!!!!!!!!!!!11
		else:
			self.actions['fileSave'].setEnabled(1)
			self.actions['fileSaveAs'].setEnabled(1)
			self.actions['editCopy'].setEnabled(1)
			self.actions['editCut'].setEnabled(1)
			self.actions['editPast'].setEnabled(1)
			# self.actions['editCopySpec'].setEnabled(1)
			self.actions['editDel'].setEnabled(1)
			self.actions['editDef'].setEnabled(1)

#----------SLOTS of View-------------------------------------------------------------------
	def slotShowHideChildren(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()
			line=position[0]
			if (document.dict_link.has_key(line)):
				# l'ouverture/fermeture d'un element est possible si c'est la tag d'ouv.
				if (document.dict_link.isTagOpende(line)):
					#si la tag d'ouverture, on montre ou cache des enfants
					document.slotOpenClose(document.dict_link.getObject(line))
					pass
				else:
					#si la tag de fermeture ou toend on refreches les trois fentres
					self.slotGetHelp()
					self.slotGetEdit()
					self.slotGetDef()

	def slotDelete(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()
			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotDelete(document.dict_link.getObject(line))
				#self.setEnabledUndo(1)

	def slotCopy(self,spectialList=None):
		if (self.actifView!=None):
			
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()
				
			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotCopy(document.dict_link.getObject(line),spectialList)

	def slotInsert(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				# l'ouverture/fermeture d'un element est possible si c'est la tag d'ouv.
				if (document.dict_link.isTagOpende(line)):
					#on ajoute comme le premier enfant
					parent=document.dict_link.getObject(line)
					document.slotInsert(self.bufferApp,parentKnot=parent)
				else:
					#on ajoute le frere apres cet element
					after=document.dict_link.getObject(line)
					document.slotInsert(self.bufferApp,afterKnot=after)
				#self.setEnabledUndo(1)

	#insertion d'element dans l'rdre concret (pour les enfants avec les attribut,Children)
	def slotInsertOrder(self,ind):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				parent=document.dict_link.getObject(line)
				document.slotInsertOrder(self.bufferApp,parent,ind)
				#self.setEnabledUndo(1)


	def slotCut(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotCopy(document.dict_link.getObject(line))
				document.slotDelete(document.dict_link.getObject(line))


	def slotCopySpecial(self):
		if (self.actifView!=None):

			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotCopySpecial(document.dict_link.getObject(line))

	def slotGetHelp(self):
		if (self.actifView!=None):

			document=self.docMenager.document(self.actifView)

			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				# l'ouverture/fermeture d'un element est possible si c'est la tag d'ouv.
				if (document.dict_link.isTagOpende(line)):
					#on ajoute comme le premier enfant
					child=document.dict_link.getObject(line)
					document.slotGetHelp(childKnots=child)
				else:
					#on ajoute le frere apres cet element
					sibling=document.dict_link.getObject(line)
					document.slotGetHelp(siblingKnots=sibling)

	def slotGetChildren(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				if (document.dict_link.isTagOpende(line)):
					document.slotGetChildren(document.dict_link.getObject(line))
				else:self.children.fillChildren([],[])


	def slotGetEdit(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				element=document.dict_link.getObject(line)
				if(element.isComment()): #a ecrire dans general_Tree
					document.slotGetComment(element)#a ecrire dans general_Tree
				else:
					document.slotGetEdit(element)


	def slotGetDef(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotGetDef(document.dict_link.getObject(line))

	def slotCloseView(self):
		if (self.actifView!=None):
			self.docMenager.closeView(self.actifView)

	def slotFindDef(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotFindDef(document .dict_link.getObject(line))

	# affichage de help qui depend quel widget est active
	def slotGiveWhatsThis(self):

		if (self.actifView!=None):
			# si l'utilisateur travail sur QtextEdit
			try:
				av=self.actifView.widget()
			except:
				av=self.actifView
					
			if(av.hasFocus ()):

				document=self.docMenager.document(av)
				position=av.getCurrentPosition()

				line=position[0]

				if (document.dict_link.has_key(line)):
					elementobject=document .dict_link.getObject(line)
					text=document.slotGiveWhatsThis(elementobject)
					self.definition.setPlainText( text)
			# si l'utilisateur est sur les blocque Elements
			elif (self.elements.hasFocus ()):
				text=self.elements.ShowWhatsThis()
				self.definition.setPlainText(QString(text))

	'''def slotPutText(self,text):
		self.modification.setName(text)'''
	def slotShowHelpAut(self):

		if(self.helpAut):
			self.slotGiveWhatsThis()

	def slotChangeStatus(self,(line,col) ):
		#self.statusBar.message('Line: '+str(line)+' Col: '+str(col))
		# self.statusBar.clearMessage()
		self.statusLbForIcon.setPixmap(QPixmap(self.statusIcon))
		self.statusLbForIcon.hide()

		self.statusLabel.setText('Line: '+str(line+1)+' Col: '+str(col))
		
	# pour garder le focus sur QTextEdit
	def slotKeepFocus(self):
		if (self.actifView!=None):
			#pour positionner le focus sur QTextEdit
			self.children.setCurrentCell (0,0)
			self.modification.setCurrentCell (0,0)

	#permet de comprendre mieux le probleme avec l'element reference
	def slotDefinProblem(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				#si cet element a perdu son l'element-definition
				if document.defLost(document.dict_link.getObject(line)):
					warning="This element reference lost the link with the element-defenition"
				else:#on a place l'element-reference plus haut que son l'element-definition correspondant
					warning="This element-'reference'  is placed either under the element-definition or in other document"
				self.slotShowMessageWarning((warning, ))

	#recupere les lines de debut et de fin d'element courrant
	def slotSelectCurrentElement(self):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				knot=document.dict_link.getObject(line)
				keys=document.dict_link.keys()
				minLine=line
				maxLine=line
				for key in keys:
					if (key!=line and document.dict_link.getObject(key)==knot):
						if(key<minLine):
							minLine=key
						elif(key>maxLine):
							maxLine=key
				try:
					self.actifView.widget().slotShowCurrentElement(minLine,maxLine)
				except:
					try:
						self.actifView.slotShowCurrentElement(minLine,maxLine)
					except:
						self.actifView.widget().slotShowCurrentElement(minLine,maxLine)
					

#---------SLOTS of Tree------------------------------------------------------------------
	def slotSave(self,(strForSave,isSaveAs) ):
		document=self.docMenager.document(self.actifView)
		import pickle
		# operation save
		if (not isSaveAs):
			fileName=document.getFileName()
		if(isSaveAs or len(fileName)==0):
			showDialog=1
			# fileName = str(QFileDialog.getSaveFileName(directory="",filter="XML files (*.xml)",parent=self,caption="save file dialog",))
			fileName = str(QFileDialog.getSaveFileName(self, "save file dialog", "","XML files (*.xml)"))
#				"Choose a filename to save under" ))
			while(os.path.exists(fileName) and showDialog) : # le fichier avec ce nom est deja existe
				# on demande si l'utilisateur veut le renplacer
				strQuestion='The file '+os.path.basename(fileName)+' already exists. Do you want to replace it?'
				BoxQuestion=QMessageBox(self)
				answer=BoxQuestion.question(self,'Warning',strQuestion,QMessageBox.Yes,QMessageBox.No)
				if(answer==BoxQuestion.No):
					# fileName = str(QFileDialog.getSaveFileName(directory="",filter="XML files (*.xml)",parent=self,caption="save file dialog",))
					fileName = str(QFileDialog.getSaveFileName(self, "save file dialog", "","XML files (*.xml)"))
				else:
					showDialog=0
		if (len(fileName)!=0):
			document.setFileName(fileName)
			document.setModified()
			#file_name='res.xml'
			f=open(fileName,'w')
			f.write(strForSave)
			f.close()
			self.actifView.setWindowTitle(document.title)

	def slotShowWindowCopy(self,text):
		self.winCopy=WindowForCopy(text,self)
		self.connect(self.winCopy,PYSIGNAL('sigHasName'),self.slotHasName)
		self.connect(self.winCopy,PYSIGNAL('sigCopy'),self.slotCopy)
		self.winCopy.move(QPoint(250,250))
		self.winCopy.show()

	def slotWriteName(self,show,nLine):
		if (show):self.winCopy.slotShowLineEdit(nLine)

	def slotShowHelp(self,args):
		listElement,listInfo=args
		self.contentOfElements=listElement
		self.contentOfInfo=listInfo
		self.leType.setText('')
		self.elements.fillTable(self.contentOfElements,self.contentOfInfo)
		#self.elements.setText(text)

	#def slotShowChildren(self,listAttrVal):
		#self.children.fillChildren(listAttrVal,self.contentOfElements)
	def slotShowChildren(self,args):
		listAttrVal,listTypeDefault=args
		self.children.fillChildren(listAttrVal,listTypeDefault)

	def slotShowEdit(self,(dictEdit,) ):
		if(self.modification.isHidden ()):
			self.modification.show()
			self.btAddComment.show()
			self.comment.hide()
			self.btOkComment.hide()
		self.modification.fillEdit(dictEdit)

	def slotShowComment(self,(textComment,)):
		if(self.comment.isHidden ()):
			self.comment.show()
			self.btOkComment.show()
			self.modification.hide()
			self.btAddComment.hide()
		self.comment.fillComment(textComment)

	def slotShowDef(self,(text,)):
		self.definition.setPlainText(text)

	def slotFillBuffer(self,(object,)):
		self.bufferApp=object

        # le 3eme argument c'est le slot dans le tree a executer (pour que cette fonction soit generale)
	def slotShowMessageQuestion(self,aargs): # strQuestion,slotFunction,*args):
		strQuestion, slotFunction=aargs[:2]
		args=aargs[2:]
	
		BoxWarning=QMessageBox(self)
		#BoxWarning.setFont (QFont("Courier",16))# self.font
		#BoxWarning.setTextFormat(Qt.PlainText )
		answer=BoxWarning.question(self,'Warning',strQuestion,QMessageBox.Yes,QMessageBox.No)
		if(answer==BoxWarning.Yes):
			#self.tree.slotDeleteKnot(knot)
			apply(slotFunction,(args))

	def slotShowMessageWarning(self,args): #stringWarning,showIcon=1):
		
		# print args
		# import traceback
		# traceback.print_stack()

		showIcon=1
		if type(args)==type((), ):
			if len(args)>1:
				showIcon=args[1]
			else:
				showIcon=1
			args=args[0]
							
		stringWarning=args
		
			
	
		self.stringWarning='        '+stringWarning
		
		self.statusLabel.setText(self.stringWarning)
		if(showIcon):

			self.statusLbForIcon.show()
			self.statusLbForIcon.setPixmap(QPixmap(self.statusErrorIcon))
		else:
			self.statusLbForIcon.setPixmap(QPixmap(self.statusIcon))
			self.statusLbForIcon.hide()

		'''BoxWarning=QMessageBox(self)
		BoxWarning.warning(self,'Warning',stringWarning)'''

	def slotShowBoxMessageWarning(self,(stringWarning, )):
		BoxWarning=QMessageBox(self)
		BoxWarning.warning(self,'Warning',stringWarning)

	def slotShowMessageError(self,(strError, ))	:
		BoxWarning=QMessageBox(self)
		BoxWarning.setFont ( self.font )
		BoxWarning.critical(self,'Error',strError)

	def slotShowMessageInformation(self,strInform):
		BoxWarning=QMessageBox(self)
		BoxWarning.setFont ( self.font )
		BoxWarning.information(self,'Information',strInform)

	#pour faire Enabled Undo
	def slotEnabledUndo(self,(status, )):
		self.setEnabledUndo(status)



#--------Slots of winCopy---------------
	def slotHasName(self,(nLine, )):
		document=self.docMenager.document(self.actifView)
		if (document.dict_linkCopy.has_key(nLine)):
			document.slotHasName(document.dict_linkCopy.getObject(nLine),nLine)

#--------------Slots d'Elements--------------------
	#pour inserer ou donner le message d'erreur a parit de lineEdit dans Elements
	def slotDoInsertHelp(self):
		if(self.elements.numRows()==1):
			self.elements.doInsert(0,0)
		else:
			if(self.elements.numRows()!=0):
				stringWarning='The choice of element is not unique'
			else:
				stringWarning='There are not element to insert'
			self.slotShowMessageWarning(stringWarning)

	#pour inserer dans l'abre a partir de l'aide
	#def slotInsertHelp(self,key,tag,ind=-1):
	def slotCreatInsert(self,args): # tag,key,ind=-1):
		tag, key=args[:2]
		if(len(args)>2):
			ind=args[2]
		else:
			ind=-1
			
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			#document.slotFillBuffer(key, tag)
			object=document.creatNewKnot(tag,[key])
			self.slotFillBuffer((object, ))
			#self.slotInsert(line)
			if(ind==-1):
				self.slotInsert()
			else:
				self.slotInsertOrder((ind, ))

	def slotInsertHelp(self,(tag,key) ):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()
			
			line=position[0]
			if (document.dict_link.has_key(line)):
				if(document.dict_link.isTagOpende(line)):
					object=document.dict_link.getObject(line)
					sibling=None
				else:
					object=document.dict_link.getObject(line).parent
					sibling=document.dict_link.getObject(line)
				if(object!=None):#pas pour la root de l'arbre
					if(object.tag in constVariable.perCoppia.keys()):
						#boucle pour ajouter la groupe des elements
						if((sibling!=None and sibling.tag == constVariable.perCoppia[object.tag][0]   ) or (sibling==None)):
							i=len(constVariable.perCoppia[object.tag])-1
							while (i>=0):
								tag=constVariable.perCoppia[object.tag][i]
								i-=1
								self.slotCreatInsert((tag,key))
					else:
						# si les enfants n'ont pas d'attrib on peut les ajouter "Add element"
						if not(document.knotHasKey(object)):
							self.slotCreatInsert((tag,key))
						else: # sinon "Add subelements with attr"
							# il faut donner le message dans status bar
							self.statusLbForIcon.setPixmap(QPixmap(self.statusErrorIcon))
							self.statusLabel.setText(self.strMessage)
							self.statusLbForIcon.show()


	def slotTextChanged(self,text):
		#print 'ok leType'
		self.slotTextChangedInLE(  ( text,self.leType,self.contentOfElements) )

	# pour changer le list des elements quand on tappe dans LineEdit
	def slotTextChangedInLE(self,(text,leObjet,listType) ):
		strText=str(text)
		#strText=str(self.leType.displayText ())
		newListEl=[]
		newListInfo=[]
		for element in listType :
			if (element.startswith(strText)): #if le debut de str est le meme
				newListEl.append(element)
				#ind=listType.index(element)
				ind=self.contentOfElements.index(element)
				newListInfo.append(self.contentOfInfo[ind])# l'info correspondante a element
		self.elements.fillTable(newListEl,newListInfo)
		if (len(newListEl)==1 and (len(strText)-len(self.oldText)>0)):
			leObjet.setText(newListEl[0])
		self.oldText=str(leObjet.displayText ())

	def slotShowHideElement(self,checked):
		if (checked):
			self.elementsGroup.show()
		else:
			self.elementsGroup.hide()
#----------SLOTS of Children------------------------
	#def slotTakeChildrenChecked(self,listKeyChecked,listTypeChecked):
	def slotTakeChildrenChecked(self,(listKeyChecked,listTypeChecked,listIndice)):
		if (self.actifView!=None):
			
			document=self.docMenager.document(self.actifView)
			document.memoriseOldText()
			i=0
			nbChildren=len(listKeyChecked)
			while (i< nbChildren):
				key=listKeyChecked[i]
				tag=listTypeChecked[i]
				ind=listIndice[i]
				#self.slotInsertHelp(key,tag,ind)
				self.slotCreatInsert((tag,key,ind) )
				i+=1

	def slotShowHideChildrenGroup(self,(checked,)):
		if (checked):
			self.childrenGroup.show()
		else:
			self.childrenGroup.hide()
		#self.children.item(0,1).setPaletteBackgroundColor( QColor (255,0,0) )

	def slotGetChildrenChecked(self):
		self.children.slotGetChildrenChecked() # a verifier pouquoi il etait comme l'arg self.contentOfElements?


#-----------Slots de Edit-------------
	def slotTypeChanged(self,(newType, )):
		if (self.actifView!=None):

			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotChangeType(document.dict_link.getObject(line),newType)
				#self.setEnabledUndo(1)

	def slotPointerChanged(self,(isPointer,)):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				if(isPointer):
					document.slotFillPointer(document.dict_link.getObject(line))
				else:
					document.slotClearPonter(document.dict_link.getObject(line))
				#self.setEnabledUndo(1)

	def slotNameChanged(self,(newText, )):
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotChangeName(document.dict_link.getObject(line),newText)
				#self.setEnabledUndo(1)


	def slotValueChanged(self,(newVal,index)):
		if (self.actifView!=None):

			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotChangeValue(document.dict_link.getObject(line),newVal,index)
				#self.setEnabledUndo(1)

	def slotShowHideEdit(self,(checked, )):
		if (checked):
			self.editGroup.show()
		else:
			self.editGroup.hide()

	#si le coursor est sur la tag d'ouverture et on veut chager le type d'element on doit motrer quand meme les types des freres
	def slotDoChangeType(self):
		if (self.actifView!=None):
			contentType=self.contentOfElements
			contentInfo=self.contentOfInfo

			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()
			

			line=position[0]
			if (document.dict_link.has_key(line)):
				#if (document.dict_link.isTagOpende(line)):
					#on montre dans la fenetre d'elements les types possibles pour changer ce type
				sibling=document.dict_link.getObject(line)
				document.slotGetHelp(siblingKnots=sibling,genericCase=0)
			self.contentOfElements=contentType
			self.contentOfInfo=contentInfo

	def slotAttrValueChanged(self,(val,key,valDefaut)):
		if (self.actifView!=None):
			try:
				document=self.docMenager.document(self.actifView.widget())
				position=self.actifView.widget().getCurrentPosition()
			except:
				document=self.docMenager.document(self.actifView)
				position=self.actifView.getCurrentPosition()
			
			line=position[0]
			if (document.dict_link.has_key(line)):
				document.slotChangeAttrValue(document.dict_link.getObject(line),val,key,valDefaut)
				#self.setEnabledUndo(1)
	#permet d'ecrire la valeur pour <f> ou <s> dans une grande text pour comment
	def slotShowBigText(self,(text,position)):
		'''if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			position=self.actifView.getCurrentPosition()
			line=position[0]
			if (document.dict_link.has_key(line)):
				object=document.dict_link.getObject(line)
				if (object.toend):'''
		#if(self.modification.isShown()):
		#pour garder le numero de celulle ou on change la valeur
		self.positionValue=position
		self.modification.hide()
		self.btAddComment.hide()
		self.comment.fillComment(text)
		self.comment.show()
		self.btOkComment.show()
		'''else:
			self.comment.hide()
			self.btOkComment.hide()
			self.modification.show()
			self.btAddComment.show()'''

	def slotAddComment(self):
		if (self.actifView!=None):
			self.slotCreatInsert(('c','') )
			#deplace le coursor sur le commentaire ajoute
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()

			self.actifView.slotChangeCursor((position[0]+1,position[1], ))
			self.slotSelectCurrentElement()
			self.modification.hide()
			self.btAddComment.hide()
			self.comment.fillComment('')
			self.comment.show()
			self.btOkComment.show()

	def slotDoChangeComment(self):#A ecrire dans le general
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			try:
				position=self.actifView.widget().getCurrentPosition()
			except:
				position=self.actifView.getCurrentPosition()
			
			line=position[0]
			if (document.dict_link.has_key(line)):
				newText=self.comment.getComment()
				object=document.dict_link.getObject(line)
				if (object.isComment()):#on modofie le commentaire
					document.slotChangeComment(object,newText)
				else:#on modifie la valeur pour <f> ou <s>
					if (object.toend):
						newText=newText.replace('\n','')
						newText=newText.replace('\t','')
						newText=newText.replace(' ','')
						self.slotValueChanged((newText,self.positionValue) )


#----------Slots of Definition--------------
	def slotElementsClicked(self,(line,pos)):
		typeObject=str(self.definition.text(line))
		index=typeObject.find('\n')
		typeObject=typeObject[0:index]

	def slotShowHideDefinition(self,checked):
		if (checked):
			self.defGroup.show()
		else:
			self.defGroup.hide()

#-----------Slots of Menu Run
	def slotRun(self):
		import sys
		import tipo
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			if(self.queryCloseDocument(document)):
				fileName=document.getFileName()
				print " il file da girare est " , fileName 
				if(len(fileName)!=0):
					if(not self.isRunning(fileName)):#a ecrire
						#fileProg='general_Tree.py'
						#self.statusBar.message('start of process...',10)
						status=self.runFile(fileName)#a ecrire
						if(status!=0):
							strError='The execution did not finish successfully'
							self.slotShowMessageError((strError, ))
						else:
							BoxQuestion=QMessageBox(self)
							strQuestion='The execution terminated correctly .'
							answer=BoxQuestion.question(self,'INFO',strQuestion,QMessageBox.Ok)
							
				else: 
					BoxQuestion=QMessageBox(self)
					strQuestion='The document is not saved on any file.'
					answer=BoxQuestion.question(self,'Warning',strQuestion,QMessageBox.Ok)
	def slotRunLoad(self):
		import sys
		import tipo
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			if(self.queryCloseDocument(document)):
				fileName=document.getFileName()
				if(len(fileName)!=0):
					if(not self.isRunning(fileName)):#a ecrire
						#fileProg='general_Tree.py'
						#self.statusBar.message('start of process...',10)
						status=self.runFileLoad(fileName)#a ecrire
						if(status!=0):
							strError='The execution did not finish successfully'
							self.slotShowMessageError((strError, ))
						else:
							BoxQuestion=QMessageBox(self)
							strQuestion='The execution terminated correctly .'
							answer=BoxQuestion.question(self,'INFO',strQuestion,QMessageBox.Ok)
							
				else: 
					BoxQuestion=QMessageBox(self)
					strQuestion='The document is not saved on any file.'
					answer=BoxQuestion.question(self,'Warning',strQuestion,QMessageBox.Ok)
	def slotStop(self):
		pass

	def slotGraph(self):
		args=''
		if (self.actifView!=None):
			document=self.docMenager.document(self.actifView)
			args=document.getValKnot(parentTag='comparison_write',childTag='s')
		if(len(args)!=0):
			args+='1'
		basdir=os.path.dirname(__file__)
		if(basdir==""): basdir="./"
		command=basdir+'newplot '+args+' &'
		os.system(command)

#------------Slots of status bar------------
	# le changement du text dans satus bar
	def slotMessageChanged(self,message):
		if (message==self.strMessage):
			pass
		elif (message==self.stringWarning):
			pass
		else:

			self.statusLbForIcon.setPixmap(QPixmap(self.statusIcon))
			self.statusLbForIcon.hide()

			self.statusBar.clear()



if __name__ == '__main__':
    #Creat application
    app_Interface=QApplication(sys.argv)
    win=TreeApp()
    #win.showFullScreen ()
    win.move(QPoint(0,0))

    #Visualisation
    win.show()
    app_Interface.connect(app_Interface,SIGNAL('lastWindowClosed()'),app_Interface,SLOT('quit()'))
    app_Interface.exec_()
