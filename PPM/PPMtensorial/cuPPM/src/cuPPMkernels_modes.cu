
/**
 * \file
 * \brief Modes Kernels
 *
 * Defines the CUDA kernels that compute the Electic and Magnetic fields
 */
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <stdio.h>
#include <cuComplex.h>
#define BLOCKSIZE 128
#define _MAX_CONSTANT_SIZE 200

// printf() is only supported
 // for devices of compute capability 2.0 and above
#if defined(__CUDA_ARCH__) && (__CUDA_ARCH__ < 200)
#define printf(f, ...) ((void)(f, __VA_ARGS__),0)
#endif

__device__ __constant__ int scanLen;
__device__ __constant__ int uniquelayers;
__device__ __constant__ int Modes[_MAX_CONSTANT_SIZE];
__device__ __constant__ int Offsets[_MAX_CONSTANT_SIZE];
__device__ __constant__ int Offsets_die[_MAX_CONSTANT_SIZE];

/**
 * \brief Populates the CUDA constant memory space
 */
cudaError_t setCudaSymbols_modes_cu(const int &scanLen, int * modes, int * offsets, int * offsets_die,const int &uniquelayers){

  char msg[1000];

  if(uniquelayers>_MAX_CONSTANT_SIZE){
    sprintf(msg,"%s (@ %d): layers > %d\n",__FUNCTION__,__LINE__,_MAX_CONSTANT_SIZE);
    throw msg;
  }
  cudaError_t cuErr;

  cudaMemcpyToSymbol("scanLen",&scanLen,sizeof(int));
  cudaMemcpyToSymbol("uniquelayers",&uniquelayers,sizeof(int));
  cudaMemcpyToSymbol("Modes",modes,sizeof(int)*uniquelayers);
  cudaMemcpyToSymbol("Offsets",offsets,sizeof(int)*uniquelayers);
  cudaMemcpyToSymbol("Offsets_die",offsets_die,sizeof(int)*uniquelayers);

  if(cuErr=cudaGetLastError()){
    sprintf(msg,"%s (@ %d): %s\n",__FUNCTION__,__LINE__,cudaGetErrorString(cuErr));
    throw msg;
  }

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": Symbols copied" <<std::endl;
#endif
  
  return cuErr;
};


__device__ static __inline__ cuDoubleComplex cuCRmul(cuDoubleComplex x,double y)
{
    cuDoubleComplex prod;
    prod = make_cuDoubleComplex ( cuCreal(x) * y , cuCimag(x) * y );
    return prod;
}

/*
__device__ static __inline__ cuDoubleComplex __Cadd(cuDoubleComplex x,cuDoubleComplex y)
{
    cuDoubleComplex prod;
    prod = make_cuDoubleComplex ( __dadd_rz( cuCreal(x) , cuCreal(y) )  , __dadd_rz( cuCimag(x) , cuCimag(y)) );
    return prod;
}
// __dadd_rn()
// __dmul_rn()
*/
__device__ static __inline__ cuDoubleComplex cuCRdiv(cuDoubleComplex x,double y)
{
    cuDoubleComplex prod;
    prod = make_cuDoubleComplex ( cuCreal(x) / y , cuCimag(x) / y );
    return prod;
}

__device__ static __inline__ cuDoubleComplex cuCsqrt(cuDoubleComplex x)
{
    double r,th;
    cuDoubleComplex prod;
    r = sqrt ( cuCreal(x) * cuCreal(x) + cuCimag(x) * cuCimag(x) );
/*    th = acos( cuCreal(x) / r);*/
    th = atan2( cuCimag(x) , cuCreal(x));
    prod = make_cuDoubleComplex( sqrt(r) * cos(th/2)  ,  sqrt(r) * sin(th/2) );
    return prod;
}


__device__ static __inline__ cuDoubleComplex negC(cuDoubleComplex x)
{
    cuDoubleComplex res;
    res = make_cuDoubleComplex ( -cuCreal(x), -cuCimag(x));
    return res;
}

//Kx is the same for ALL layers! Thus it must be calculated with a grid -> scanLen once!
__global__ void setKx(double *d_Kx){

    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if(idx<scanLen){
      d_Kx[idx] = cos(d_Kx[idx]);
    }
};

//revise index - Maybe it would be better to make a transpose kernel
#define d(i,j)  (d_die[ Offsets_die[blockIdx.y] +  ((j)-1) + 3*((i)-1)  + 9*idx ])
#define dS(i)   ( (d_die[ idx + Offsets_die[blockIdx.y] ] ) )
//Fix d_Sistem coallescion
#define d_Sistem(i,j) ( d_Sistem[ Offsets[blockIdx.y] + idx + scanLen * (iwaves +  4*( (i) + 3*( (j) ) ) ) ] )
#define d_E(j,k)     d_E[ idx + scanLen*( (j) + 4* ( (k) + 3 * blockIdx.y ) )]
/*#define d_E(j,k)     d_E[ idx + scanLen*( blockIdx.y + gridDim.y*( (j) + 4*(k) ) )]*/
#define d_B(j,k)     d_B[ idx + scanLen*( (j) + 4* ( (k) + 3 * blockIdx.y ) )]
#define KZ(i) d_Kz[idx + scanLen *(i + 4* blockIdx.y )]

__global__ void getAs(double *d_Kx, cuDoubleComplex *d_die, cuDoubleComplex *d_As){

  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  double kx,kx2,kx3,kx4;
  cuDoubleComplex a0,a1,a2,a3,a4,ah;

  //
  a0 = make_cuDoubleComplex(0.0,0.0);
  a1 = make_cuDoubleComplex(0.0,0.0);
  a2 = make_cuDoubleComplex(0.0,0.0);
  a3 = make_cuDoubleComplex(0.0,0.0);
  //

  if(idx < scanLen && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers-1)){
    kx = d_Kx[idx];
    kx2= kx*kx;
    kx3= kx2*kx;
    kx4= kx2*kx2;
    a4= d(3,3)  ;

a0= cuCRmul( d(1,1), kx4);
     ah= cuCRmul ( cuCmul( d(1,2) , d(2,1) ) ,kx2 );//
     a0= cuCadd (a0 , ah);

     ah= cuCRmul ( cuCmul( d(1,1) , d(2,2) ) ,kx2 );
     a0= cuCsub (a0 , ah);

     ah= cuCRmul ( cuCmul ( d(1,3) , d(3,1) ) ,kx2);//
     a0= cuCadd (a0 , ah);

     ah= cuCRmul ( cuCmul ( d(1,1) , d(3,3) ) ,kx2);
     a0= cuCsub (a0 , ah);
 
     ah= cuCmul ( cuCmul ( d(1,3) , d(2,2) ) , d(3,1) );//
     a0= cuCsub (a0 , ah);
 
     ah= cuCmul ( cuCmul ( d(1,2) , d(2,3) ) , d(3,1) );//
     a0= cuCadd (a0 , ah);
 
     ah= cuCmul ( cuCmul ( d(1,3) , d(2,1) ) , d(3,2) );//
     a0= cuCadd (a0 , ah);
 
     ah= cuCmul ( cuCmul ( d(1,1) , d(2,3) ) , d(3,2) );//
     a0= cuCsub (a0 , ah);
 
     ah= cuCmul ( cuCmul ( d(1,2) , d(2,1) ) , d(3,3) );//
     a0= cuCsub (a0 , ah);




     ah= cuCmul ( cuCmul ( d(1,1) , d(2,2) ) , d(3,3) );
     a0= cuCadd (a0 , ah);
// 
    a3= cuCadd ( cuCRmul( d(1,3) , kx ) , cuCRmul( d(3,1), kx) ) ;

    a2= cuCadd( cuCRmul( d(1,1) , kx2) , cuCRmul( d(3,3) , kx2) );
    ah= cuCadd( cuCmul( d(1,3) , d(3,1) ) , cuCmul( d(2,3) , d(3,2) ) );

    a2= cuCadd( a2, ah);
    ah= cuCadd( cuCmul( d(1,1) , d(3,3) ) , cuCmul( d(2,2) , d(3,3) ) );
    a2= cuCsub( a2, ah);

    a1= cuCadd( cuCRmul( d(1,3) , kx3 ) , cuCRmul( d(3,1) , kx3 ) );
    ah= cuCRmul( cuCmul( d(1,3) , d(2,2) )  , kx);
    a1= cuCsub( a1, ah);

    ah= cuCRmul( cuCmul( d(1,2) , d(2,3) )  , kx);
    a1= cuCadd( a1, ah);

    ah= cuCRmul( cuCmul( d(2,2) , d(3,1) )  , kx);
    a1= cuCsub( a1, ah);

    ah= cuCRmul( cuCmul( d(2,1) , d(3,2) )  , kx);
    a1= cuCadd( a1, ah);


    a3=  cuCdiv( a3 , a4 );
    a2=  cuCdiv( a2 , a4 );
    a1=  cuCdiv( a1 , a4 );
    a0=  cuCdiv( a0 , a4 );

  }
  __syncthreads();
  if(idx < scanLen && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers-1)){
      d_As[idx + scanLen*(3 + 4 * blockIdx.y )] = a3 ;
      d_As[idx + scanLen*(2 + 4 * blockIdx.y )] = a2 ;
      d_As[idx + scanLen*(1 + 4 * blockIdx.y )] = a1 ;
      d_As[idx + scanLen*(0 + 4 * blockIdx.y )] = a0 ;
  }

}

#define quattro 4.0
#define mezzo 0.5
//1e-7 ratio
__global__ void getKz(cuDoubleComplex *d_As,cuDoubleComplex *d_Kz){


  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  cuDoubleComplex sol1,sol2,sol3,sol4;
  cuDoubleComplex ah,ah1,a0,a1,a2,a3;

  if(idx<scanLen && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers-1)){
    sol1=sol2=sol3=sol4=make_cuDoubleComplex(0.0,0.0);
    a0 = d_As[idx + scanLen*(0 + 4 * blockIdx.y )];
    a1 = d_As[idx + scanLen*(1 + 4 * blockIdx.y )];
    a2 = d_As[idx + scanLen*(2 + 4 * blockIdx.y )];
    a3 = d_As[idx + scanLen*(3 + 4 * blockIdx.y )];

    for(int i=0; i<3; i++){

      ah= cuCmul( cuCmul( sol1 ,sol1 ) , a3 );
      ah= cuCadd( a1, ah);
      ah= cuCadd( a0, cuCmul(sol1 , ah));
      ah= cuCRmul( ah, quattro );
      ah1=cuCmul( a2, a2);
      ah= cuCsub( ah1, ah);
      ah = cuCsqrt(ah);
      ah1= cuCadd( negC(a2) , ah);
      ah=  cuCRmul( ah1, mezzo);
      sol1=cuCsqrt(ah);

      
      ah= cuCmul( cuCmul( sol2 ,sol2 ) , a3 );
      ah= cuCadd( a1, ah);
      ah= cuCmul( sol2, ah);
      ah= cuCadd( a0, ah);
      ah= cuCRmul( ah, quattro );
      ah1=cuCmul( a2, a2);
      ah= cuCsub( ah1, ah);

      ah = cuCsqrt(ah);
      ah1= cuCsub( negC(a2) , ah );
      ah=  cuCRmul( ah1, mezzo);
      sol2=cuCsqrt(ah);


      ah= cuCmul( cuCmul( sol3 ,sol3 ) , a3 );
      ah= cuCadd( ah, a1);
      ah= cuCmul( ah, sol3);
      ah= cuCadd( ah, a0);
      ah= cuCRmul( ah, quattro );
      ah1=cuCmul( a2, a2);
      ah= cuCsub( ah1, ah);

      ah = cuCsqrt(ah);
      ah1= cuCadd( negC(a2) , ah);
      ah=  cuCRmul( ah1, mezzo);
      sol3= negC( cuCsqrt(ah) );

      
      ah= cuCmul( cuCmul( sol4 ,sol4 ) , a3 );
      ah= cuCadd( ah, a1);
      ah= cuCmul( ah, sol4);
      ah= cuCadd( ah, a0);
      ah= cuCRmul( ah, quattro );
      ah1=cuCmul( a2, a2);
      ah= cuCsub( ah1, ah);

      ah = cuCsqrt(ah);
      ah1= cuCsub( negC(a2) , ah);
      ah=  cuCRmul( ah1, mezzo);
      sol4= negC( cuCsqrt(ah) );
    }
  }
  __syncthreads();
  if(idx < scanLen && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers-1)){
      d_Kz[idx + scanLen*(0 + 4 * blockIdx.y )] = sol1;
      d_Kz[idx + scanLen*(1 + 4 * blockIdx.y )] = sol2;
      d_Kz[idx + scanLen*(2 + 4 * blockIdx.y )] = sol3;
      d_Kz[idx + scanLen*(3 + 4 * blockIdx.y )] = sol4;
  }

}

#undef quattro
#undef mezzo

//d_Kz[idx + scanLen*(iwave + 4 * blockIdx.y )]
//#define d_Sistem(i,j) ( d_Sistem[ Offsets[blockIdx.y] + idx + scanLen * (iwaves +  4*( (i) + 3*( (j) ) ) ) ] )
__global__ void setSistem(cuDoubleComplex *d_Sistem, double *d_Kx, cuDoubleComplex *d_Kz, cuDoubleComplex *d_die){

  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  cuDoubleComplex K0,K1,K2;
//   double KK0,KK1;
//  __shared__ cuDoubleComplex KK0[BLOCKSIZE];

  if(idx < scanLen && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers-1)){
//     KK0=d_Kx[idx + scanLen*blockIdx.y];
//     KK1=0.0;
    K0 = make_cuDoubleComplex(d_Kx[idx] , 0.0);
    K1 = make_cuDoubleComplex(0.0 , 0.0);
    for (int iwaves=0; iwaves<4; iwaves++){
      K2= d_Kz [idx + scanLen *(iwaves + 4 * blockIdx.y)];

      //       x,y             x, y   ; Fast is x (while on C its y)
      d_Sistem(0,0) = cuCmul( K0, K0) ;
      d_Sistem(0,1) = cuCmul( K0, K1) ;//0
      d_Sistem(0,2) = cuCmul( K0, K2) ;
      d_Sistem(1,0) = cuCmul( K1, K0) ;//0
      d_Sistem(1,1) = cuCmul( K1, K1) ;//0
      d_Sistem(1,2) = cuCmul( K1, K2) ;//0
      d_Sistem(2,0) = cuCmul( K2, K0) ;
      d_Sistem(2,1) = cuCmul( K2, K1) ;//0
      d_Sistem(2,2) = cuCmul( K2, K2) ;

      d_Sistem(0,0) = cuCadd( d_Sistem(0,0) , d(1,1) );
      d_Sistem(0,1) = cuCadd( d_Sistem(0,1) , d(1,2) );//0
      d_Sistem(0,2) = cuCadd( d_Sistem(0,2) , d(1,3) );
      d_Sistem(1,0) = cuCadd( d_Sistem(1,0) , d(2,1) );//0
      d_Sistem(1,1) = cuCadd( d_Sistem(1,1) , d(2,2) );
      d_Sistem(1,2) = cuCadd( d_Sistem(1,2) , d(2,3) );//0
      d_Sistem(2,0) = cuCadd( d_Sistem(2,0) , d(3,1) );
      d_Sistem(2,1) = cuCadd( d_Sistem(2,1) , d(3,2) );//0
      d_Sistem(2,2) = cuCadd( d_Sistem(2,2) , d(3,3) );


      d_Sistem(0,0) = cuCsub( d_Sistem(0,0) , cuCmul( K0, K0));
      d_Sistem(1,1) = cuCsub( d_Sistem(1,1) , cuCmul( K0, K0));
      d_Sistem(2,2) = cuCsub( d_Sistem(2,2) , cuCmul( K0, K0));

      d_Sistem(0,0) = cuCsub( d_Sistem(0,0) , cuCmul( K2, K2));
      d_Sistem(1,1) = cuCsub( d_Sistem(1,1) , cuCmul( K2, K2));
      d_Sistem(2,2) = cuCsub( d_Sistem(2,2) , cuCmul( K2, K2));

//       d_Sistem[idx + scanLen*( iwaves + 4 * (0 + 3*0) ) ] = cuCsub( d_Sistem[idx + scanLen*( iwaves + 4 * (0 + 3*0) ) ] , K2);
//       d_Sistem[idx + scanLen*( iwaves + 4 * (1 + 3*1) ) ] = cuCsub( d_Sistem[idx + scanLen*( iwaves + 4 * (1 + 3*1) ) ] , K2);
//       d_Sistem[idx + scanLen*( iwaves + 4 * (2 + 3*2) ) ] = cuCsub( d_Sistem[idx + scanLen*( iwaves + 4 * (2 + 3*2) ) ] , K2);

//       d_Sistem[idx + scanLen*( iwaves + 4 * (0 + 3*0) ) ] =  d_Sistem[idx + scanLen*( iwaves + 4 * (0 + 3*0) ) ];
//       d_Sistem[idx + scanLen*( iwaves + 4 * (1 + 3*1) ) ] =  d_Sistem[idx + scanLen*( iwaves + 4 * (1 + 3*1) ) ];
//       d_Sistem[idx + scanLen*( iwaves + 4 * (2 + 3*2) ) ] =  d_Sistem[idx + scanLen*( iwaves + 4 * (2 + 3*2) ) ];
        //Note the + sign: d_Sistem[ix][ix] -= +( (KZ(iwaves)* KZ(iwaves))) ;
    }
  }
}

//#define d_Sistem(i,j) ( d_Sistem[ Offsets[blockIdx.y] + idx + scanLen * (iwaves +  4*( (i) + 3*( (j) ) ) ) ] )
__global__ void calcNonzeri(cuDoubleComplex *d_Sistem, cuDoubleComplex *d_E, int *d_Zeri){

  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  cuDoubleComplex minori[9],mh;
  double moduli[3];
  double stima,massimo;
  int nonzeri,imassimo;

  if(idx < scanLen && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers-1)){
    for(int iwaves=0;iwaves<4;iwaves++){

      nonzeri = 0;
      massimo = 0.0;
      stima=0;

      //TODO optimise loop order
      for (int i=0; i<3; i++) {
          for (int j=0; j<3; j++) {
              stima=stima +
              cuCreal( cuCmul( d_Sistem(i,j) , cuConj(d_Sistem(i,j)) ) ) ;
          }
      }
      stima=stima*stima;

      for (int i=0; i<3; i++) {
          int i1,i2;
          i1=(i+1)%3;
          i2=(i+2)%3;
          moduli[i]=0.0;
          for (int j=0; j<3; j++) {
              int j1,j2;
              j1=(j+1)%3;
              j2=(j+2)%3;

              minori[j + 3*i]= cuCmul( d_Sistem(i1,j1), d_Sistem(i2,j2) );
              mh= cuCmul( d_Sistem(i1,j2), d_Sistem(i2,j1) );
              minori[j + 3*i]= cuCsub( minori[j + 3*i], mh);

              mh= cuCmul( minori[j + 3*i], cuConj( minori[j + 3*i]) );
              moduli[i] += cuCreal(mh);

//                 mh= cuCmul( d_Sistem[idx + scanLen*( iwaves + 4 * (i + 3*j) ) ], cuConj( d_Sistem[idx + scanLen*( iwaves + 4 * (i + 3*j) ) ] ) );
//                 moduliriga[i] +=   cuCreal(mh);
          }

          if (moduli[i]>1.0e-15*stima) {
            nonzeri++;
          }
          if (moduli[i]>=massimo) {
              imassimo=i;
              massimo=moduli[i];
          }
      }
      d_Zeri[idx + scanLen*(iwaves + 4*blockIdx.y)]=nonzeri;
      if(nonzeri){
        massimo=sqrt(massimo);
        d_E( iwaves  , 0 ) = cuCRdiv( minori[0 + 3 * imassimo],massimo);
        d_E( iwaves  , 1 ) = cuCRdiv( minori[1 + 3 * imassimo],massimo);
        d_E( iwaves  , 2 ) = cuCRdiv( minori[2 + 3 * imassimo],massimo);
      }

    }
  }
}

//#define d_Sistem(i,j) ( d_Sistem[ Offsets[blockIdx.y] + idx + scanLen * (iwaves +  4*( (i) + 3*( (j) ) ) ) ] )
__global__ void calcZeri(cuDoubleComplex *d_Sistem, cuDoubleComplex *d_E, int *d_Zeri){

  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  cuDoubleComplex newvects[9],mh;
  double massimoriga, moduliriga[3], massimocol,norma;
  int imassimoriga, icol, zeri;

  if(idx < scanLen && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers-1)){
    for(int iwaves=0;iwaves<4;){
      zeri = d_Zeri[idx + scanLen*(iwaves + 4*blockIdx.y)];
      if(!zeri){

        massimoriga=0;
        imassimoriga=0;
        massimocol=0;
        icol=0;
        norma=0.0;

        //TODO optimise loop order
        for (int i=0; i<3; i++) {

          moduliriga[i] = 0;
          for (int j=0; j<3; j++) {
            mh= cuCmul( d_Sistem(i,j), cuConj( d_Sistem(i,j) ) );
            moduliriga[i] +=   cuCreal(mh);
          }

          if (moduliriga[i]>=massimoriga) {
              imassimoriga=i;
              massimoriga=moduliriga[i];
          }
        }

        massimoriga = sqrt(massimoriga);
        for (int k=0; k<3; k++)  {
            newvects[k] = cuCRdiv( d_Sistem(imassimoriga,k) , massimoriga );

            if ( cuCabs(newvects[k])>=massimocol) {
                icol=k;
                massimocol =cuCabs(newvects[k] );
            }
        }

        newvects[(icol+2)%3 +3] = make_cuDoubleComplex( 0.0 , 0.0);
        newvects[(icol+1)%3 +3] = newvects[icol];
        newvects[icol +3      ] = negC(newvects[(icol+1)%3 ]);

        mh = cuCadd( cuCmul( newvects[icol +3], cuConj(newvects[icol +3])) , cuCmul( newvects[(icol+1)%3 +3], cuConj(newvects[(icol+1)%3 +3]) ) );
        norma = cuCreal(mh);
        norma=sqrt(norma);
        newvects[ (icol+1)%3 +3] = cuCRdiv( newvects[ (icol+1)%3 +3], norma);
        newvects[ icol +3      ] = cuCRdiv( newvects[ icol +3      ], norma);

        for (int k=0; k<3; k++) {
          newvects[k + 6]= cuCsub( cuCmul( newvects[(k+1)%3], newvects[(k+2)%3 +3]) , cuCmul( newvects[(k+2)%3], newvects[(k+1)%3 +3]) ) ;
          d_E( iwaves  , k ) = newvects[k + 3];
        }
        iwaves++;
        for (int k=0; k<3; k++) {
          d_E( iwaves  , k ) = cuCsub( cuCmul( newvects[(k+1)%3], newvects[(k+2)%3 +3]) , cuCmul( newvects[(k+2)%3], newvects[(k+1)%3 +3]) ) ;
        }
        iwaves++;
      } else{
        iwaves+=4;
      }
    }
  }
}

__global__ void calcB(double *d_Kx, cuDoubleComplex *d_Kz, cuDoubleComplex *d_E, cuDoubleComplex *d_B){

  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  cuDoubleComplex K[3], E2B[9];
  double kx;
  if(idx < scanLen && Modes[blockIdx.y]==3 && blockIdx.y!=(uniquelayers-1)){
    kx=d_Kx[idx];
    for (int colo=0; colo<4; colo++)
    {
        K[1]= make_cuDoubleComplex(0.0,0.0);
        K[0]= make_cuDoubleComplex(kx, 0.0);
        K[2]= d_Kz[idx + scanLen*(colo + 4*blockIdx.y)];
        for(int i=0;i<9;i++)E2B[i]= make_cuDoubleComplex(0.0,0.0);
        E2B[1]= negC(K[2]);
        E2B[3]= K[2];
        E2B[2 + 3]=negC(K[0]);
        E2B[1 + 6]=K[0];
        E2B[6]=negC(K[1]);
        E2B[2]=K[1];

//#define E(j,k)      E[  scanPoint*3*4     +(j)*3        +k     ]
//#define B(j,k)      B[  scanPoint*3*4     +(j)*3        +k     ]

        for (int j=0; j<3; j++) {
            d_B(colo,j)= make_cuDoubleComplex(0.0,0.0);

            for (int k=0; k<3; k++) {
                d_B(colo,j) = cuCadd( d_B (colo,j), cuCmul(E2B[k + 3*j] , d_E(colo,k  ) ) );
            }
        }
    }
  }

}

__global__ void cuModesScalar(double *d_Kx, cuDoubleComplex *d_Kz, cuDoubleComplex *d_die, cuDoubleComplex *d_E, cuDoubleComplex *d_B){

  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  cuDoubleComplex eps,kx2,kx,one;

  one = make_cuDoubleComplex( 1.0, 0.0);

  if(idx <scanLen && Modes[blockIdx.y]==1 && blockIdx.y!=(uniquelayers-1)){
    eps = dS(0);
    kx  = make_cuDoubleComplex( d_Kx[idx], 0.0);
    kx2 = cuCmul( kx, kx);
    for ( int i=0; i<2; i++) {
        KZ(i)= cuCsqrt( cuCsub( cuCmul(one,eps), kx2 ) );
    }
    for ( int i=2; i<4; i++) {
        KZ(i)= negC( cuCsqrt( cuCsub( cuCmul(one,eps), kx2 ) ) );
    }

    d_E( 0 , 0 ) = make_cuDoubleComplex( 0.0, 0.0);
    d_E( 2 , 0 ) = make_cuDoubleComplex( 0.0, 0.0);

    d_E( 1 , 0 ) = negC( KZ(1) );
    d_E( 3 , 0 ) = negC( KZ(3) );


    d_E( 0 , 1 ) = one;
    d_E( 2 , 1 ) = one;
    d_E( 1 , 1 ) = make_cuDoubleComplex( 0.0, 0.0);
    d_E( 3 , 1 ) = make_cuDoubleComplex( 0.0, 0.0);


    d_E( 0 , 2 ) = make_cuDoubleComplex( 0.0, 0.0);
    d_E( 2 , 2 ) = make_cuDoubleComplex( 0.0, 0.0);
    d_E( 1 , 2 ) = kx;
    d_E( 3 , 2 ) = kx;

    /////////////////////////////////////////////////////
    d_B( 0 , 0 ) = negC( KZ(0) );
    d_B( 2 , 0 ) = negC( KZ(2) );

    d_B( 1 , 0 ) = make_cuDoubleComplex( 0.0, 0.0);
    d_B( 3 , 0 ) = make_cuDoubleComplex( 0.0, 0.0);


    d_B( 0 , 1 ) = make_cuDoubleComplex( 0.0, 0.0);
    d_B( 2 , 1 ) = make_cuDoubleComplex( 0.0, 0.0);
    d_B( 1 , 1 ) = negC( cuCmul( one, eps) );
    d_B( 3 , 1 ) = negC( cuCmul( one, eps) );


    d_B( 0 , 2 ) = kx;
    d_B( 2 , 2 ) = kx;
    d_B( 1 , 2 ) = make_cuDoubleComplex( 0.0, 0.0);
    d_B( 3 , 2 ) = make_cuDoubleComplex( 0.0, 0.0);
  }

}

//Here in the test the last layer was tensorial... but because it is last it is treated like this:
#define dSv(i)   ( (d_die[ 9*idx + Offsets_die[blockIdx.y] ] ) )
__global__ void cuModesTensor_Vide(double *d_Kx, cuDoubleComplex *d_Kz, cuDoubleComplex *d_die, cuDoubleComplex *d_E, cuDoubleComplex *d_B){

  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  cuDoubleComplex eps,kx2,kx,one;

  one = make_cuDoubleComplex( 1.0, 0.0);

  if(idx <scanLen && blockIdx.y==(uniquelayers-1) ){
    if(Modes[uniquelayers-1]==3)eps = dSv(0);
    else eps = dS(0);
    kx  = make_cuDoubleComplex( d_Kx[idx], 0.0);
    kx2 = cuCmul( kx, kx);
    for ( int i=0; i<2; i++) {
        KZ(i)= cuCsqrt( cuCsub( cuCmul(one,eps), kx2 ) );
    }
    for ( int i=2; i<4; i++) {
        KZ(i)= negC( cuCsqrt( cuCsub( cuCmul(one,eps), kx2 ) ) );
    }

        d_E( 0 , 0 ) = make_cuDoubleComplex( 0.0, 0.0);
        d_E( 2 , 0 ) = make_cuDoubleComplex( 0.0, 0.0);

        d_E( 1 , 0 ) = negC(KZ(1));
        d_E( 3 , 0 ) = negC(KZ(3));


        d_E( 0 , 1 ) = one;
        d_E( 2 , 1 ) = one;
        d_E( 1 , 1 ) = make_cuDoubleComplex( 0.0, 0.0);
        d_E( 3 , 1 ) = make_cuDoubleComplex( 0.0, 0.0);


        d_E( 0 , 2 ) = make_cuDoubleComplex( 0.0, 0.0);
        d_E( 2 , 2 ) = make_cuDoubleComplex( 0.0, 0.0);
        d_E( 1 , 2 ) = kx;
        d_E( 3 , 2 ) = kx;

        /////////////////////////////////////////////////////
        d_B( 0 , 0 ) = negC(KZ(0));
        d_B( 2 , 0 ) = negC(KZ(2));

        d_B( 1 , 0 ) = make_cuDoubleComplex( 0.0, 0.0);
        d_B( 3 , 0 ) = make_cuDoubleComplex( 0.0, 0.0);


        d_B( 0 , 1 ) = make_cuDoubleComplex( 0.0, 0.0);
        d_B( 2 , 1 ) = make_cuDoubleComplex( 0.0, 0.0);
        d_B( 1 , 1 ) = negC(one);
        d_B( 3 , 1 ) = negC(one);


        d_B( 0 , 2 ) = kx;
        d_B( 2 , 2 ) = kx;
        d_B( 1 , 2 ) = make_cuDoubleComplex( 0.0, 0.0);
        d_B( 3 , 2 ) = make_cuDoubleComplex( 0.0, 0.0);

  }

}
