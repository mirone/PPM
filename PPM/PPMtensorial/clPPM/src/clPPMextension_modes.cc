
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string.h>

//We need ocl_tools only for the profiling: ocl_get_profT
#ifndef OCLTOOLS_H
#include "ocl_tools.h"
#define OCLTOOLS_H
#endif

#include <CL/opencl.h>
#include "ocl_ckerr.h"

#ifndef MODES_H
#include "clPPMextension_modes.h"
#define MODES_H
#endif

#define blockSize 32

//If no CL_SUCCESS encountered, throw the error
#define CT CL_CHECK_ERR_THROW

//clOnda handles the execution of kernels related to the calculation of
// the Electric and Magnetic field.
//That is the Onda class of PPMcore
clOnda::clOnda(){
  clOnda_GPU_time=0.0;
}

clOnda::~clOnda(){

}

/**
 * getModesResults is only used when debug_flag is set in clPPM.
 * In a normal execution it is not needed (and should not be used to avoid latency).
 * getModesResults returns the results of clOnda
 *
 * @param scanLen The number of scanpoints
 * @param uniquelayers The number of layers with a unique set of dielectric matrices
 * @param cl_mem OpenCL buffers
 * @param oclconfig the OpenCL configuration
 */
void clOnda::getModesResults(int &scanLen,int &uniquelayers,cl_mem d_Kz,cl_mem d_E,cl_mem d_B,
                        complex<double> *Kz,complex<double> *E,complex<double>*B,ocl_config_type *oclconfig){

  cl_int clErr;
  char msg[1000];

  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_Kz,CL_TRUE,0,scanLen * 4 * (uniquelayers) * sizeof(cl_double2),(void*)Kz,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  
  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_E,CL_TRUE,0,scanLen * 4 * (uniquelayers) * 3 * sizeof(cl_double2),(void*)E,0,0,&oclconfig->t_s[1]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  
  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_B,CL_TRUE,0,scanLen * 4 * (uniquelayers) * 3 * sizeof(cl_double2),(void*)B,0,0,&oclconfig->t_s[2]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }

#ifdef _VERBOSE
  sprintf(msg,"%s: elapsed time",__FUNCTION__);
  ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[2],msg);
#endif
  
  for(int i=0;i<3;i++) CT(clReleaseEvent(oclconfig->t_s[i]));

  return;
}

/**
 * getModesResults_debug is only used when debug_flag is set in clPPM.
 * In a normal execution it is not needed (and should not be used to avoid latency).
 * getModesResults_debug returns intermediate results of clOnda
 *
 * @param scanLen The number of scanpoints
 * @param uniquelayers The number of layers with a unique set of dielectric matrices
 * @param cl_mem OpenCL buffers
 * @param oclconfig The OpenCL configuration*
 */
void clOnda::getModesResults_debug(int &scanLen,int &uniquelayers,int &summode,cl_mem d_Kx,cl_mem d_Sistem, cl_mem d_NonZeri,
                                  cl_mem d_As, double * Kx, complex<double> *Sistem, int *NonZeri, complex< double > *As,ocl_config_type *oclconfig){

  cl_int clErr;
  char msg[1000];

  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_Kx,CL_TRUE,0,scanLen * sizeof(double),(void*)Kx,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  
  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_As,CL_TRUE,0,scanLen * 4 * (uniquelayers) * sizeof(cl_double2),(void*)As,0,0,&oclconfig->t_s[1]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  
  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_Sistem,CL_TRUE,0,scanLen * 4 * summode * sizeof(cl_double2),(void*)Sistem,0,0,&oclconfig->t_s[2]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  
  clErr =
    clEnqueueReadBuffer(oclconfig->oclcmdqueue,d_NonZeri,CL_TRUE,0,scanLen * 4 * (uniquelayers) * sizeof(int),(void*)NonZeri,0,0,&oclconfig->t_s[3]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueReadBuffer failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }

#ifdef _VERBOSE
  sprintf(msg,"%s: elapsed time",__FUNCTION__);
  ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[3],msg);
#endif
  
  for(int i=0;i<4;i++) CT(clReleaseEvent(oclconfig->t_s[i]));

  return;
} 

/**
 * CreateModes invokes the kernels found in clPPMkerners_modes.cl.
 * Layers are treated as a bunch and are calculated all at once.
 *
 * @param scanLen Number of scanpoints
 * @param uniquelayers Number of layers with unique dieletric matrices
 * @param oclconfig The OpenCL configuration
 */
void clOnda::CreateModes(int &scanLen,int &uniquelayers,ocl_config_type *oclconfig){

  cl_int clErr;
  float clTime_ms,totTime_ms=0;
  char msg[1000];

  size_t wdim[] = { ( scanLen/blockSize + ((scanLen%blockSize)>0) ) * blockSize , 1, 1};
  size_t wdiml[]= { ( scanLen/blockSize + ((scanLen%blockSize)>0) ) * blockSize , uniquelayers/* * blockSize*/, 1};  
  size_t tdim[] = { blockSize , 1, 1};


  //----Kernel setKx
  clErr = 
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[0],1,0,wdim,tdim,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[0]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel setKx time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms;


  //----Kernel getAs
  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[1],2,0,wdiml,tdim,0,0,&oclconfig->t_s[1]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[1],&oclconfig->t_s[1]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel getAs time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms;  

  //----Kernel getKz
  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[2],2,0,wdiml,tdim,0,0,&oclconfig->t_s[2]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[2],&oclconfig->t_s[2]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel getKz time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms; 


  //----Kernel setSistem
  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[3],2,0,wdiml,tdim,0,0,&oclconfig->t_s[3]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[3],&oclconfig->t_s[3]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel setSistem time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms; 

  //----Kernel calcNonzeri
  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[4],2,0,wdiml,tdim,0,0,&oclconfig->t_s[4]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[4],&oclconfig->t_s[4]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel calcNonzeri time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms; 

  //----Kernel calcZeri
  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[5],2,0,wdiml,tdim,0,0,&oclconfig->t_s[5]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[5],&oclconfig->t_s[5]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel calcZeri time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms;   

  //----Kernel calcB
  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[6],2,0,wdiml,tdim,0,0,&oclconfig->t_s[6]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[6],&oclconfig->t_s[6]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel calcB time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms; 

  //----Kernel clModesScalar
  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[7],2,0,wdiml,tdim,0,0,&oclconfig->t_s[7]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[7],&oclconfig->t_s[7]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel cuModesScalar time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms;

  //----Kernel clModesTensor_Vide
  clErr =
    clEnqueueNDRangeKernel(oclconfig->oclcmdqueue,oclconfig->oclkernels[8],2,0,wdiml,tdim,0,0,&oclconfig->t_s[8]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueNDRangeKernel failed: %s",__FUNCTION__,__LINE__, ocl_perrc(clErr));
    throw msg;
  }
  clTime_ms = ocl_get_profT(&oclconfig->t_s[8],&oclconfig->t_s[8]);
#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": kernel cuModesTensor_Vide time (ms): "<< clTime_ms << ::endl;
#endif  
  totTime_ms+=clTime_ms;   

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": Total processing time: " <<totTime_ms << " (ms)" <<std::endl;
#endif  

  //De-allocate the events
  for(int i=0;i<9;i++) CT(clReleaseEvent(oclconfig->t_s[i]));

  clOnda_GPU_time += totTime_ms;
return;
}
