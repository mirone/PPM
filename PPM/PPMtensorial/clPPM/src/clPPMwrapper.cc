
/**
 * \file
 * \brief Python wrapper for the clPPM module
 */

#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include "Python.h"
#include "structmember.h"
#include <stdio.h>
#include <iostream>
#include <string.h>

using namespace std;

//Include the PPM extension for OpenCL (clPPM)
#include "clPPMextension.h"

#include <numpy/oldnumeric.h>
#include <numpy/arrayobject.h>

#ifdef WIN32
#define _USE_MATH_DEFINES
#include <cmath>
#endif


#include <math.h>
#include<complex>

//Include profiling header-source file
#include "timer.h" 

//#define _VERBOSE

/**
 * \brief Layers to uniquelayers look-up table.
 *
 * Layers with common dielectric matrices will yield the same Electric and Magnetic field.
 * Thus, we only need to calculate those for the first occurence of each "similar" layer.
 * The rest are set to point to this first occurence for later reference.
 */
struct indexlookup{
  PyArrayObject *ind;
  int nd;
};

#ifdef _VERBOSE
PPMTensorial_ocl clPPMobj;
#else
PPMTensorial_ocl clPPMobj ("clPPM.log");
#endif

/**
 * \brief A tweaked Onda-copy class for testing
 *
 * Onda is the heart of calculate_scanTensor (multicouche_calculatescanTensor).
 * This is a modified copy of the Onda class found in PPMcore, this however resides
 * inside a different module. It's purpose is to create test files.
 */
class Onda {
public:
  Onda();
  Onda(int benchmark_flag_);
  ~Onda();
  void CreateModes( );
  void CreateModesScalar( );
  void CreateModesTensor( );
  void CreateModesTensor_Vide( );
  PyObject * get_vectors();

  PyArrayObject* indexes;
  PyArrayObject* angles;

  complex<double> *Kx;
  complex<double> *Kz;
  complex<double> *E,*B;

  //for UnitTest:
  double *dKx;
  complex<double> *d_As, *d_Sistem;
  int *d_NonZeri;

  FILE *fKx,*fKz,*fAs,*fSistem,*fNonZeri,*fE,*fB;
};

/**
 * Checks that two PyArrayObjects contain the same data
 */
int ThoseArraysAreEqual( PyArrayObject *a  , PyArrayObject *b   );

static PyObject *ErrorObject;
#define onError(message)\
{ PyErr_SetString(ErrorObject, message); return NULL;}

/**
 * Python wrapped PPMTensorial_ocl.show_devices() doc string
 */
static char multicouche_show_devices_doc[]=""
"* No Arguments are expected and no value is returned.\n"
"* This functions simply prints the available OpenCL devices.\n"
;

/**
 * Python wrapped PPMTensorial_ocl.show_devices().
 * No input parameter no output parameter.
 * Just a message print
 */
static PyObject *
multicouche_show_devices(PyObject *self, PyObject *args){
  clPPMobj.show_devices();
  Py_INCREF(Py_None); //?not sure about this
  Py_RETURN_NONE;
};

/**
 * Python wrapped PPMTensorial_ocl.query_for_gpu() doc string
 */
static char multicouche_query_for_gpu_doc[]=""
"* 1 integer Argument  and no value is returned.\n"
"* This functions searches for a OpenCL GPU device.\n"
"* It can also alter the code-path inside clPPM: \n"
"*   If a GPU is discovered, it will override the selection\n"
"*   of any other device \n"
"* The integer argument will get the logical 0|1 value\n"
"*   depending if no gpu or a gpu is found"
;

/**
 * Python wrapped PPMTensorial_ocl.query_for_gpu().
 * No input argument.
 *
 * @return ext_hasGPU PyInt that resembles the PPMTensorial_ocl.hasGPU flag
 */
static PyObject *
multicouche_query_for_gpu(PyObject *self, PyObject *args){

  PyObject *ext_hasGPU;

  if(!PyArg_ParseTuple(args,":multicouche_query_for_gpu")) return NULL;
  
  clPPMobj.query_for_gpu();
  ext_hasGPU = PyInt_FromLong((long)clPPMobj.hasGPU);
  Py_INCREF(ext_hasGPU);
  return ext_hasGPU;
};


/**
 * Python wrapping of main functionality doc string
 */
static char multicouche_calculatescanTensorOcl_doc[] = ""
"*  5 arguments are taken by the function :\n"
"*  thickness, roughness, wavelenght, angle, indexes, K0\n"
"*    |          |           |         |      |        | \n"
"*    |          |           |         |      |        | \n"
"*    |          |           |         |      |        |  \n"
"*    |          |           |         |      |        --> PyArray_Complex. Dim= numwave+1 \n"
"*    |          |           |         |      |                                  --------- \n"
"*    |          |           |         |      --> PyArray_Complex. Dims= (numthick+1)X(numwave+1)\n"
"*    |          |           |         |                                 ----------------\n"
"*    |          |           |         --->PyArray_double. Dim= numwave+1\n"
"*    |          |           |                                  ------\n"
"*    |          |           --> PyArray_double. Dim= numwave+1\n"
"*    |          |                                   -------\n"
"*    |          -> A PyArray of double. Same dimensions as thickness\n"
"*    |\n"
"*    -> A PyArray of double. Dimension= numthick+1. Angstroms\n"
"*                                       --------\n"
"* Thickness,Indexes,Roughness are given starting from the substrate (Attention here. Different from PPMcore)\n"
"* Numwave+1 and numthick+1 mean that substrate is included\n"
"*\n"
"* RETURN VALUE\n"
"*    An array containing the reflectivity\n"
"*    as PyArray complex double. \n"
"*/\n"
;


/**
 * Python wrapping of main functionality.
 * calculatescanTensorOcl, as opposed to the original calculatescanTensor which calculated only
 * the Electric and Magnetic fields, calculates those plus the propagations, interfaces and the
 * reflectivity.
 *
 * @param args A list of the 5 input arguments. thickness, roughness, wavelenght, angle, indexes, K0 
 * @return reflectivity PyArrayObject containing the reflectivity
 */
static PyObject *
multicouche_calculatescanTensorOcl(PyObject *self, PyObject *args)
{

#ifdef _VERBOSE  
  printf("In multicouche_calculatescanTensorOcl\n");
#endif
  
  PyArrayObject *thickness, *roughness, *wavelenght, *angle, *K0;
  PyObject * indexes;  // indexes are now a python list of pyarrayobjects

  char *devstring = NULL;
  int platform  = -1;
  int device = -1;
  int benchmark_flag = 0;


  if(!PyArg_ParseTuple(args,"OOOOOO|siii:multicouche_calculatescanTensorOcl", (PyObject *) &thickness,(PyObject *)&roughness,
    (PyObject *)&wavelenght,(PyObject *)&angle,(PyObject *) &indexes, (PyObject *)&K0, &devstring, &platform, &device,
    &benchmark_flag)
  ) return NULL;

  /* check the Objects */
  if(!PyArray_Check((PyObject *)thickness ))    onError("not a PyArray, argument 1");
  if(!PyArray_Check((PyObject *)roughness))     onError("not a PyArray, argument 2");
  if(!PyArray_Check((PyObject *)wavelenght))    onError("not a PyArray, argument 3");
  if(!PyArray_Check((PyObject *)angle))         onError("not a PyArray, argument 4");
  if(!PyArray_Check((PyObject *)K0))            onError("not a PyArray, argument 6");
  if(!PyList_Check((PyObject *)indexes))        onError("not a PyList, argument 5");

  /* check the types */

  if( thickness->descr->type_num != PyArray_DOUBLE ) onError(" thickness is not double " ) ;
  if( roughness->descr->type_num != PyArray_DOUBLE ) onError(" roughness  is not double " ) ;
  if( wavelenght->descr->type_num != PyArray_DOUBLE ) onError(" wavelenght is not double " ) ;
  if( angle->descr->type_num != PyArray_DOUBLE ) onError(" angle is not double " ) ;
  if( K0->descr->type_num != PyArray_DOUBLE ) onError(" angle is not double " ) ;


  int num_thicks = PyList_Size(indexes) ;
  for(int k=0; k< num_thicks; k++) {
    if(!PyArray_Check((PyObject *)  PyList_GetItem(indexes,k )   )   ) {
      char errormessage[200];
      sprintf(errormessage,"not a PyArray, argument 5, item number %d",k);
      onError(errormessage);
    }
    if( ((PyArrayObject*) PyList_GetItem(indexes,k )) ->descr->type_num != PyArray_CDOUBLE )  {
      char errormessage[200];
      sprintf(errormessage,"not a CDOUBLE PyArray, argument 5, item number %d",k);
      onError(errormessage);
    }
  }

  /* check the Number of dimensions */

  if( thickness->nd != 1 )
    onError("The thickness array (arg. 1) has not the right number of dimensions");
  if(roughness ->nd != 1 )
    onError("The roughness array (arg. 2) has not the right number of dimensions");
  if( wavelenght->nd != 1 )
    onError("The wavelenght array (arg. 3) has not the right number of dimensions");
  if(angle->nd != 1 )
    onError("The angle array (arg. 4) has not the right number of dimensions");

  /*
   *    if(indexes->nd != 2 )
   *    onError("The indexes array (arg. 5) has not the right number of dimensions");
   */

  int *  nd_layer_i= new int [num_thicks];
  for(int k=0; k< num_thicks; k++) {
    int nd =  ((PyArrayObject *)  PyList_GetItem(indexes,k )   ) ->nd;
    if(  nd!=1 && nd!=3   ) {
      char errormessage[200];
      sprintf(errormessage,"PyArray, argument 5, item number %d is neither an array neither a tensor array, nd= %d",k,nd);
      onError(errormessage);
    }
    nd_layer_i[k]=nd;
  }

  /* check the Number of dimensions */

  int numthick, numwave;
  numthick=thickness->dimensions[0];

  if(numthick!=roughness->dimensions[0] )
    onError("The roughness array (arg. 2) has not the right  dimension");

  numwave  = wavelenght->dimensions[0] ;

  if(numwave!=angle->dimensions[0] )
    onError("The  angles array (arg. 4) has not the right  dimension");

  if(numthick!=PyList_Size(indexes)   )
    onError("The indexes  array (arg. 5) has not the right  lenght");

  for(int k=0; k< numthick; k++) {
    PyArrayObject *li;
    li = ((PyArrayObject *)  PyList_GetItem(indexes,k ))  ;
    if(   li->dimensions[0] != numwave   ) {
      onError("The indexes  array (arg. 5) has not the right second   dimension");
    }
    if( nd_layer_i[k]==3) {
      if(   li->dimensions[1] != 3 || li->dimensions[2] != 3   ) {
        onError("The indexes  array (arg. 5) has not the right third and/or  fourth where it should be a tensor");
      }
    }
    //Check the substrate
    if(k==0){
      if(numwave!=li->dimensions[0])
        onError("The Substrate index array (index[0]) has not the right  second dimension");
      if(li->nd==3) {
        if(   li->dimensions[1] != 3 || li->dimensions[2] != 3   ) {
          onError("The Substrate Index  array (index[0]) has not the right dimensions where it should be a tensor");
        }
      }
    }
  }

  /* check that everything is contiguous */

  PyArrayObject *toverify[]={thickness,roughness,wavelenght,angle,NULL};
  PyArrayObject **ptr;
  ptr=toverify;
  while(*ptr) {
    if((*ptr)->flags %2 == 0) onError(" All arrays have to be contiguous, either solve this problem at the python level or at the c level using PyArray_CopyFromObject");
    ptr++;
  }
  for(int k=0; k< numthick; k++) {
    PyArrayObject *li;
    li = ((PyArrayObject *)  PyList_GetItem(indexes,k ))  ;
    if((li)->flags %2 == 0) onError(" All arrays in Indexes have to be contiguous, either solve this problem at the python level or at the c level using PyArray_CopyFromObject");
  }

  /*Create a contiguous array for the indexes - this is a requirement for cuPPM*/
  /* take into account how many eigenmodes have to be calculated */
  struct indexlookup *index_list;
  int *where_to_look_for_cuda;
  int *inverse_loopup;
  int ntodo=0;

  index_list = new struct indexlookup [numthick];
  where_to_look_for_cuda = new int [numthick];
  inverse_loopup = new int [numthick];

  int summodes=0;

  for(int k=0; k< numthick; k++) {
    PyArrayObject *li;
    li = ((PyArrayObject *)  PyList_GetItem(indexes,k ))  ;
    if(!li) printf("problem\n");
    int isnew=1;
    for(int l=0; l< ntodo; l++) {
      if(   ThoseArraysAreEqual(li  ,  index_list[l].ind)    ) {
        isnew=0;
        where_to_look_for_cuda[k] = l;
        //printf("layer %d is connected to %d\n",k,l);
        break;
      }
    }
    if(isnew){
      summodes += li->nd * li->nd;
      index_list[ntodo].ind = li;
      where_to_look_for_cuda[k] = ntodo;
      inverse_loopup[ntodo] = k;
      //printf("layer %d is unique w dim %d\n",k,li->nd);
      ntodo++;
    }
  }

  complex< double > * cindexes = new complex< double > [numwave * summodes];
  int *modes = new int [numwave * ntodo];

  int cur_index=0;
  int cur_size=0;
  for(int k=0;k<ntodo;k++){
    modes[k] = index_list[k].ind->nd;
    cur_size = index_list[k].ind->nd * index_list[k].ind->nd;
    memcpy(&cindexes[cur_index],(complex< double >*)(index_list[k].ind->data),sizeof(complex< double >) * numwave * cur_size );
    cur_index += numwave * cur_size;
  };

  //Allocate PyArrayObject for the reflectivity
  void *reflhandle;
  int nd=3;
  npy_intp dims[]={2,2, numwave };
  PyObject * reflectivity;
  reflectivity=PyArray_SimpleNew(nd,dims, 'D') ;
  Py_INCREF(reflectivity);

  reflhandle = PyArray_DATA(reflectivity);

  try{  
    clPPMobj.getInput(numwave,numthick,ntodo,modes,cindexes,
                    (double *)(angle->data),
                    (double *)(roughness->data),
                    (double *)(thickness->data),
                    (double *)(K0->data),
                    where_to_look_for_cuda,
                    (complex< double > *)reflhandle
    );
    //if query_for_gpu is used this configuration will be overriden
    if(!devstring){
      if(benchmark_flag)     
        clPPMobj.benchPPMTensorial_ocl("ALL",-1,-1);
      else
        clPPMobj.launchPPMTensorial_ocl("ALL",-1,-1);
    }
    else{
      if(benchmark_flag)
        clPPMobj.benchPPMTensorial_ocl(devstring,platform,device);
      else
        clPPMobj.launchPPMTensorial_ocl(devstring,platform,device);
    }
  }catch(char *msg){
    std::cout <<"Caught: " << msg << std::endl;
    PyErr_SetString(PyExc_RuntimeError,msg);
    return NULL;
  }
    
  Py_DECREF(reflectivity);

  /* creation of the return Values */
  delete [] nd_layer_i ;
  delete [] index_list;
  delete [] where_to_look_for_cuda;
  delete [] inverse_loopup;
  delete [] modes;

  return reflectivity;
}

#define float double
#define quattro 4.0
#define mezzo 0.5
/*----------------------------------------------------------------------------------------------------------------------*/
//C PPM. This is kept here for internal cuPPM tests


Onda::Onda() {
  this->Kx=NULL;
  this->Kz=NULL;
  this->E=NULL;
  this->B=NULL;
  this->dKx=NULL;
  this->d_As=NULL;
  this->d_NonZeri=NULL;
  this->d_Sistem=NULL;

}

Onda::~Onda() {
  // printf(" DISTRUGGO ONDA\n");
  complex<double> **ptr;
  complex<double> *ptrlist[] = { this->Kx,this->Kz,  this->E,  this->B  ,      NULL};
  ptr=&ptrlist[0];
  while(*ptr) {
    delete(*ptr);
    ptr++;
  }
}

PyObject * Onda::get_vectors() {
  int scanLen ;
  scanLen = this-> angles->dimensions[0];
  PyObject * res = PyList_New(0);
  {
    int nd=1;
    npy_intp dims[]={scanLen};
    PyObject * nuovo;
    nuovo=PyArray_SimpleNew(nd,dims, 'D') ;
    memcpy( ((PyArrayObject*)nuovo)->data, this->  Kx,       scanLen*sizeof(complex<double>) );
    PyList_Append(  res ,  nuovo     ) ;
    Py_DECREF(nuovo);
  }
  {
    int nd=2;
    npy_intp dims[]={scanLen,4};
    PyObject * nuovo;
    nuovo=PyArray_SimpleNew(nd,dims, 'D' ) ;
    memcpy( ((PyArrayObject*)nuovo)->data, this->  Kz,       scanLen*4*sizeof(complex<double>) );
    PyList_Append(res ,     nuovo    ) ;
    Py_DECREF(nuovo);
  }
  {
    int nd=3;
    npy_intp dims[]={scanLen,4,3};
    PyObject * nuovo;
    nuovo=PyArray_SimpleNew(nd,dims, 'D' ) ;
    memcpy( ((PyArrayObject*)nuovo)->data, this->  E,       4*3*scanLen*sizeof(complex<double>) );
    PyList_Append(res ,     nuovo    ) ;
    Py_DECREF(nuovo);
  }
  {
    int nd=3;
    npy_intp dims[]={scanLen,4,3};
    PyObject * nuovo;
    nuovo=PyArray_SimpleNew(nd,dims, 'D' ) ;
    memcpy( ((PyArrayObject*)nuovo)->data, this->  B,       4*3*scanLen*sizeof(complex<double>) );
    PyList_Append(res ,     nuovo    ) ;
    Py_DECREF(nuovo);
  }
  return  res;
}

//****************************************************************
//** Trova le 4 diverse polarizzazioni propagantesi nel mezzo die
//** La onda che si passa in argomento deve essere inizializzata
//** precedentemente con l'angolo e l'energia desiderati
//**
void Onda::CreateModes( )
{

  fKx = fopen("Kx.res","ab");
  fAs = fopen("As.res","ab");
  fKz = fopen("Kz.res","ab");
  fSistem = fopen("Sistem.res","ab");
  fNonZeri = fopen("NonZeri.res","ab");
  fE = fopen("E.res","ab");
  fB = fopen("B.res","ab");
  if(!fKx || !fAs || !fKz || !fSistem || !fNonZeri || !fE || !fB){
    std::cout <<__FUNCTION__ << "Error opening files for writing. " << __LINE__  <<std::endl;
    exit(1);
  }

  if(this->indexes->nd==1) {
    // cout << " Scalar implementato !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! "<< endl;
    this->CreateModesScalar();
  } else if(this->indexes->nd==3) {
    this->CreateModesTensor();
  } else {
    cout << " Problem : neither nd==1 nor nd==3 in CreateModes\n";
    exit(0);
  }
  fclose(fKx); fclose(fAs); fclose(fKz); fclose(fSistem); fclose(fNonZeri); fclose(fE); fclose(fB);

}

void Onda::CreateModesTensor( ) {
  int scanLen ;
  scanLen = this-> angles->dimensions[0];

  this-> Kx = new  complex<double> [ scanLen ] ;
  // creazione di Kz , E,  B
  this-> Kz= new complex<double> [ scanLen *4];
  this-> E = new complex<double> [ scanLen *4 *3 ];
  this-> B = new complex<double> [ scanLen *4 *3 ];

  dKx = new double [scanLen];
  d_As = new complex< double >[scanLen *4];
  d_Sistem = new complex< double >[scanLen *9*4];
  d_NonZeri = new int [scanLen * 4];

  memset(d_As,0,sizeof(complex< double >) * scanLen *4);
  memset(d_Sistem,0,sizeof(complex< double >) * scanLen *9*4);
  memset(d_NonZeri,0,sizeof(int) * scanLen *4);

  for(int scanPoint =0 ; scanPoint < scanLen; scanPoint++) {
    this->Kx[scanPoint]= cos(((double *) this->angles->data)[scanPoint]);
  }

  for(int i=0;i<scanLen;i++)dKx[i] = Kx[i].real();
             fwrite(dKx,sizeof(double),scanLen,fKx);


  static complex<double > S[16] ;

  static double kx;

  complex<double>  *die;

  for(int scanPoint =0 ; scanPoint < scanLen; scanPoint++) {

    die = ( (complex<double> *) this->indexes->data) + scanPoint*9 ;
    kx = (double) this->Kx[scanPoint].real();

    static double  kx2,kx3,kx4,kx5,kx6;
    static complex<double> a0,a1,a2,a3,a4;
    kx2=kx*kx;
    kx3=kx2*kx;
    kx4=kx2*kx2;
    kx5=kx2*kx3;
    kx6=kx3*kx3;
    #define d(i,j) ( (complex<double>) (die[(i-1)*3+(j)-1]) )
    #define Power(a,n)  a##n
    a4= d(3,3)  ;
    a0= d(1,1)*Power(kx,4) +
    d(1,2)*d(2,1)*Power(kx,2) - d(1,1)*d(2,2)*Power(kx,2) +
    d(1,3)*d(3,1)*Power(kx,2) - d(1,1)*d(3,3)*Power(kx,2) -
    d(1,3)*d(2,2)*d(3,1) + d(1,2)*d(2,3)*d(3,1)+
    d(1,3)*d(2,1)*d(3,2) - d(1,1)*d(2,3)*d(3,2) -
    d(1,2)*d(2,1)*d(3,3) + d(1,1)*d(2,2)*d(3,3) ;
    a3=d(1,3)*kx + d(3,1)*kx ;
    a2=d(1,1)*Power(kx,2) + d(3,3)*Power(kx,2) +
    d(1,3)*d(3,1) + d(2,3)*d(3,2) - d(1,1)*d(3,3) -
    d(2,2)*d(3,3)   ;
    a1= d(1,3)*Power(kx,3) + d(3,1)*Power(kx,3) -
    d(1,3)*d(2,2)*kx + d(1,2)*d(2,3)*kx -
    d(2,2)*d(3,1)*kx + d(2,1)*d(3,2)*kx  ;
    #undef Power
    #undef d

    a3=  a3/a4;
    a2=  a2/a4;
    a1=  a1/a4;
    a0=  a0/a4;

    #define d_As(i) (d_As[scanPoint*4+i])

    d_As(0)=a0;
    d_As(1)=a1;
    d_As(2)=a2;
    d_As(3)=a3;

    complex<double> sol1,sol2,sol3,sol4, b,c;
    sol1=sol2=sol3=sol4=0.0;
    for(int i=0; i<3; i++){
      b = a2;
      c = a0+sol1*(  a1 +sol1*sol1*a3 );
      sol1=sqrt((-b+sqrt(b*b-quattro*c))*mezzo);
      c = a0+sol2*(  a1 +sol2*sol2*a3 );
      sol2=sqrt((-b-sqrt(b*b-quattro*c))*mezzo);
      c = a0+sol3*(  a1 +sol3*sol3*a3 );
      sol3=-sqrt((-b+sqrt(b*b-quattro*c))*mezzo);
      c = a0+sol4*(  a1 +sol4*sol4*a3 );
      sol4=-sqrt((-b-sqrt(b*b-quattro*c))*mezzo);
    }

    #define KZ(i) (this->Kz[scanPoint*4+i])

    KZ(0)=sol1;
    KZ(1)=sol2;
    KZ(2)=sol3;
    KZ(3)=sol4;

    int stacco0=0;
    for(int iwaves=0; iwaves<4; )
    {
      static  complex<double> Sistem[3][3];

      complex<double> K[3];

      K[0] = kx ; K[1]=0; K[2]= KZ(iwaves) ;
      for(int ix=0; ix<3; ix++)
      {
        for(int iy=0; iy<3; iy++)
        {
          Sistem[ix][iy] = K[ix]*K[iy] ;

          #define d(i,j)  die[(i)*3+(j)]
          Sistem[ix][iy]+=d(ix,iy);
          d_Sistem[iy + 3*(ix + 3*(iwaves + 4*scanPoint))] = Sistem[ix][iy];

          #undef d

        }
        Sistem[ix][ix] -= kx*kx;
        Sistem[ix][ix] -= +( (KZ(iwaves)* KZ(iwaves))) ;
        d_Sistem[ix + 3*(ix + 3*(iwaves + 4*scanPoint))] = Sistem[ix][ix];

      }

      {


        static complex<double> minori[3][3], newvects[3][3];
        static double moduli[3], stima, massimo, massimoriga, moduliriga[3];
        stima=0;
        static int nonzeri, imassimo, imassimoriga;
        nonzeri=0;
        massimo=0;
        massimoriga=0;
        for(int i=0; i<3; i++) {
          for(int j=0; j<3; j++) {
            stima=stima+ ( Sistem[i][j]*conj( Sistem[i][j])).real() ;
          }
        }
        stima=stima*stima;

        for(int i=0; i<3; i++) {
          int i1,i2;
          i1=(i+1)%3;
          i2=(i+2)%3;
          moduli[i]=0.0;
          moduliriga[i]=0.0;
          for(int j=0; j<3; j++) {
            int j1,j2;
            j1=(j+1)%3;
            j2=(j+2)%3;
            minori[i][j]= Sistem[i1][j1]*Sistem[i2][j2] - Sistem[i1][j2]*Sistem[i2][j1];
            moduli[i] +=   ( minori[i][j]*conj( minori[i][j])).real();
            moduliriga[i] +=   ( Sistem[i][j]*conj( Sistem[i][j])).real();
          }

          if(moduli[i]>1.0e-15*stima) nonzeri++;
             d_NonZeri[scanPoint + scanLen*iwaves]=nonzeri;
          if(moduli[i]>=massimo) {
            imassimo=i;
            massimo=moduli[i];
          }
          if(moduliriga[i]>=massimoriga) {
            imassimoriga=i;
            massimoriga=moduliriga[i];
          }


        }
        #define E(i,j,k)      E[  i*3*4     +(j)*3        +k     ]
        if(nonzeri) {

          massimo=sqrt(massimo);
          for(int k=0; k<3; k++) {
            E( scanPoint , iwaves  , k ) =     minori[imassimo][k]/massimo;
          }
          iwaves++;
        } else {

          massimoriga=sqrt(massimoriga);
          static double massimocol;
          static int icol;
          massimocol=0;

          for(int k=0; k<3; k++)  {
            newvects[0][k] = Sistem[imassimoriga][k]/massimoriga ;

            if(abs(newvects[0][k]    )>=massimocol) {
              icol=k;
              massimocol =abs(newvects[0][k] );
            }
          }
          static double norma;
          norma=0.0;
          newvects[1][ (icol+2)%3 ] = 0.0;
          newvects[1][ (icol+1)%3 ] =  newvects[0][ icol];
          newvects[1][ icol       ] = -newvects[0][(icol+1)%3 ];
          norma = ((newvects[1][icol])*conj(newvects[1][icol])
          +(newvects[1][(icol+1)%3])*conj(newvects[1][(icol+1)%3])    ).real();
          norma=sqrt(norma);
          newvects[1][ (icol+1)%3 ] /= norma  ;
          newvects[1][ icol       ] /= norma ;


          for(int k=0; k<3; k++) {
            newvects[2][k]= newvects[0][(k+1)%3]* newvects[1][(k+2)%3] -newvects[0][(k+2)%3]* newvects[1][(k+1)%3];
          }
          for(int k=0; k<3; k++) {
            E( scanPoint , iwaves  , k ) = newvects[1][k]    ;
          }
          iwaves++;

          for(int k=0; k<3; k++) {
            E( scanPoint , iwaves  , k ) = newvects[2][k]    ;
          }
          #undef E
          iwaves++;
        }

      }
    }



    for(int colo=0; colo<4; colo++)
    {
      static complex<double> E2B[3][3], K[3];
      K[1]=0;
      K[0]=kx;
      K[2]=KZ(colo);
      for(int i=0; i<3; i++) for(int k=0; k<3; k++)  E2B[i][k]=0;
             E2B[0][1]=-K[2];        E2B[1][0]=+K[2];
      E2B[1][2]=-K[0];        E2B[2][1]=+K[0];
      E2B[2][0]=-K[1];        E2B[0][2]=+K[1];
      {
        for( int i=0; i<0*3; i++) {
          for(int j=0; j<3; j++) {
            cout << E2B[i][j] << " " ;
          }
          cout << endl;
        }
      }
      #define E(j,k)      E[  scanPoint*3*4     +(j)*3        +k     ]
      #define B(j,k)      B[  scanPoint*3*4     +(j)*3        +k     ]

      for(int j=0; j<3; j++) {
        B(colo,j)=0;

        for(int k=0; k<3; k++) {
          B (colo,j) +=   E2B[j][k] * E(colo,k  ) ;
        }
      }
      #undef E
      #undef B
    }
  }
  //cout << "T Now dumping Kz" <<endl;
  fwrite(this->Kz,sizeof(complex< double >)*4,scanLen,fKz);
  fwrite(d_As,sizeof(complex< double >)*4,scanLen,fAs);
  /*    std::cout<< "V Now dumping E"<<std::endl;*/
  fwrite(E,sizeof(complex<double>),scanLen*4*3,fE);
  /*    std::cout<< "V Now dumping B"<<std::endl;*/
  fwrite(B,sizeof(complex<double>),scanLen*4*3,fB);
  fwrite(d_NonZeri,sizeof(int),scanLen*4,fNonZeri);
  //cout << "T Now dumping Sistem" <<endl;
  fwrite(d_Sistem,sizeof(complex< double >),scanLen*9*4,fSistem);

  #undef KZ



}


void Onda::CreateModesTensor_Vide( ) {
  int scanLen ;
  scanLen = this-> angles->dimensions[0];

  this-> Kx = new  complex<double> [ scanLen ] ;
  this-> Kz= new complex<double> [ scanLen *4];
  this-> E = new complex<double> [ scanLen *4 *3 ];
  this-> B = new complex<double> [ scanLen *4 *3 ];

  for(int scanPoint =0 ; scanPoint < scanLen; scanPoint++) {
    this->Kx[scanPoint]= cos(((double *) this->angles->data)[scanPoint]);
  }
  fKx = fopen("Kx.res","ab");
  fAs = fopen("As.res","ab");
  fKz = fopen("Kz.res","ab");
  fSistem = fopen("Sistem.res","ab");
  fNonZeri = fopen("NonZeri.res","ab");
  fE = fopen("E.res","ab");
  fB = fopen("B.res","ab");
  if(!fKx || !fAs || !fKz || !fSistem || !fNonZeri || !fE || !fB){
    std::cout <<__FUNCTION__ << "Error opening files for writing. " << __LINE__  <<std::endl;
    exit(1);
  }

  static double kx;


  for(int scanPoint =0 ; scanPoint < scanLen; scanPoint++) {

    kx = this->Kx[scanPoint].real();

    #define KZ(i) this->Kz[scanPoint*4+i]
    for ( int i=0; i<2; i++) {
      KZ(i)=+sqrt(1.0-kx*kx);
    }
    for ( int i=2; i<4; i++) {
      KZ(i)=-sqrt(1.0-kx*kx);
    }



    #define E(i,j,k)      E[  i*3*4     +(j)*3        +k     ]
    #define B(i,j,k)      B[  i*3*4     +(j)*3        +k     ]

    E( scanPoint , 0 , 0 ) = 0.0;
    E( scanPoint , 2 , 0 ) = 0.0;

    E( scanPoint , 1 , 0 ) = -KZ(1);
    E( scanPoint , 3 , 0 ) = -KZ(3);


    E( scanPoint , 0 , 1 ) = 1.0;
    E( scanPoint , 2 , 1 ) = 1.0;
    E( scanPoint , 1 , 1 ) = 0.0;
    E( scanPoint , 3 , 1 ) = 0.0;


    E( scanPoint , 0 , 2 ) = 0.0;
    E( scanPoint , 2 , 2 ) = 0.0;
    E( scanPoint , 1 , 2 ) = kx;
    E( scanPoint , 3 , 2 ) = kx;

    ///////////////////////////////////////////////////////////
    B( scanPoint , 0 , 0 ) = -KZ(0);
    B( scanPoint , 2 , 0 ) = -KZ(2);

    B( scanPoint , 1 , 0 ) = 0.0;
    B( scanPoint , 3 , 0 ) = 0.0;


    B( scanPoint , 0 , 1 ) = 0.0;
    B( scanPoint , 2 , 1 ) = 0.0;
    B( scanPoint , 1 , 1 ) = -1.0;
    B( scanPoint , 3 , 1 ) = -1.0;


    B( scanPoint , 0 , 2 ) = kx;
    B( scanPoint , 2 , 2 ) = kx;
    B( scanPoint , 1 , 2 ) = 0.0;
    B( scanPoint , 3 , 2 ) = 0.0;

    #undef E

    #undef KZ
  }
  /*    std::cout<< "V Now dumping Kz"<<std::endl;*/
  fwrite(Kz,sizeof(complex<double>),scanLen*4,fKz);
  /*    std::cout<< "V Now dumping E"<<std::endl;*/
  fwrite(E,sizeof(complex<double>),scanLen*4*3,fE);
  /*    std::cout<< "V Now dumping B"<<std::endl;*/
  fwrite(B,sizeof(complex<double>),scanLen*4*3,fB);
  //TODO remove this
  fclose(fKx); fclose(fAs); fclose(fKz); fclose(fSistem); fclose(fNonZeri); fclose(fE); fclose(fB);
}


void Onda::CreateModesScalar( ) {
  int scanLen ;
  scanLen = this-> angles->dimensions[0];

  this-> Kx = new  complex<double> [ scanLen ] ;
  this-> Kz= new complex<double> [ scanLen *4];
  this-> E = new complex<double> [ scanLen *4 *3 ];
  this-> B = new complex<double> [ scanLen *4 *3 ];

  for(int scanPoint =0 ; scanPoint < scanLen; scanPoint++) {
    this->Kx[scanPoint]= cos(((double *) this->angles->data)[scanPoint]);
  }


  static double kx;


  for(int scanPoint =0 ; scanPoint < scanLen; scanPoint++) {

    kx = this->Kx[scanPoint].real();

    complex<double> nind,eps;
    nind =  *(((complex<double>*) (this->indexes->data))+scanPoint);

    eps=nind;

    #define KZ(i) this->Kz[scanPoint*4+i]
    for ( int i=0; i<2; i++) {
      KZ(i)=+sqrt(1.0*eps-kx*kx);
    }
    for ( int i=2; i<4; i++) {
      KZ(i)=-sqrt(1.0*eps-kx*kx);
    }



    #define E(i,j,k)      E[  i*3*4     +(j)*3        +k     ]
    #define B(i,j,k)      B[  i*3*4     +(j)*3        +k     ]

    E( scanPoint , 0 , 0 ) = 0.0;
    E( scanPoint , 2 , 0 ) = 0.0;

    E( scanPoint , 1 , 0 ) = -KZ(1);
    E( scanPoint , 3 , 0 ) = -KZ(3);


    E( scanPoint , 0 , 1 ) = 1.0;
    E( scanPoint , 2 , 1 ) = 1.0;
    E( scanPoint , 1 , 1 ) = 0.0;
    E( scanPoint , 3 , 1 ) = 0.0;


    E( scanPoint , 0 , 2 ) = 0.0;
    E( scanPoint , 2 , 2 ) = 0.0;
    E( scanPoint , 1 , 2 ) = kx;
    E( scanPoint , 3 , 2 ) = kx;

    ///////////////////////////////////////////////////////////
    B( scanPoint , 0 , 0 ) = -KZ(0);
    B( scanPoint , 2 , 0 ) = -KZ(2);

    B( scanPoint , 1 , 0 ) = 0.0;
    B( scanPoint , 3 , 0 ) = 0.0;


    B( scanPoint , 0 , 1 ) = 0.0;
    B( scanPoint , 2 , 1 ) = 0.0;
    B( scanPoint , 1 , 1 ) = -1.0*eps;
    B( scanPoint , 3 , 1 ) = -1.0*eps;


    B( scanPoint , 0 , 2 ) = kx;
    B( scanPoint , 2 , 2 ) = kx;
    B( scanPoint , 1 , 2 ) = 0.0;
    B( scanPoint , 3 , 2 ) = 0.0;

    #undef E

    #undef KZ

  }
  /*    std::cout<< "S Now dumping Kz"<<std::endl;*/
  fwrite(Kz,sizeof(complex<double>),scanLen*4,fKz);
  /*    std::cout<< "S Now dumping E"<<std::endl;*/
  fwrite(E,sizeof(complex<double>),scanLen*4*3,fE);
  /*    std::cout<< "S Now dumping B"<<std::endl;*/
  fwrite(B,sizeof(complex<double>),scanLen*4*3,fB);
}


int ThoseArraysAreEqual( PyArrayObject *a  , PyArrayObject *b   ) {
  int size_a=1;
  if( a->nd!=b->nd) return 0;
             for (int k=0; k<a->nd; k++){
               size_a*=a->dimensions[k];
               if(a->dimensions[k]!=b->dimensions[k]) return 0;
             };
  if(  (a->descr->type  != b->descr->type) || a->descr->type !='D' ) {
    cout << " ThoseArraysAreEqual is made for Complex \n";
    exit(0);
  }
  int res= ( memcmp( a->data,  b->data, size_a*sizeof(complex<double>) )   ==0);
  return res ;
}

/**
 * Python wrapping of Onda functionality doc string
 */
static char multicouche_calculatescanTensorTest_doc[] = ""
"*  7 arguments are taken by the function :\n"
"*  thickness, roughness, wavelenght, angle, indexes, Sindex, Srough\n"
"*    |          |           |         |      |        |       |\n"
"*    |          |           |         |      |        |       |\n"
"*    |          |           |         |      |        |       |\n"
"*    |          |           |         |      |        |       =>PyDoubleObject\n"
"*    |          |           |         |      |        ->PyArray_Complex. Dim= numwave\n"
"*    |          |           |         |      --> PyArray_Complex. Dims= numthickXnumwave\n"
"*    |          |           |         |                                 ----------------\n"
"*    |          |           |         --->PyArray_double. Dim= numwave\n"
"*    |          |           |                                  ------\n"
"*    |          |           --> PyArray_double. Dim= numwave\n"
"*    |          |                                   -------\n"
"*    |          -> A PyArray of double. Same dimensions as thickness\n"
"*    |\n"
"*    -> A PyArray of double. Dimension= numthick. Angstroms\n"
"*                                       --------\n"
"* Thickness are given starting from the bottom (first layer over the substrate)\n"
"* indexes for the substrate are given separately in Sindex\n"
"* Srough is the substrate roughness.\n"
"*\n"
"* RETURN VALUE\n"
"*    A tuple containing the S reflectivity an Preflectivity given\n"
"*    as PyArray double. \n"
"*/\n"
;

/**
 * Python wrapping of Onda functionality. This is a copy of the
 * multicouche_calculatescanTensor in PPMcore, but it uses the local
 * Onda class to enable tests
 */
static PyObject *
multicouche_calculatescanTensorTest(PyObject *self, PyObject *args)
{
  // static  complex<double> Sistem[9];



  PyArrayObject *thickness, *roughness, *wavelenght, *angle, /* *indexes , */*Sindex;
  PyObject * indexes;  // indexes are now a python list of pyarrayobjects
  PyFloatObject *Srough;



  if(!PyArg_ParseTuple(args,"OOOOOOO:multicouche_calculatescanTensor", (PyObject *) &thickness,(PyObject *)&roughness,
    (PyObject *)&wavelenght,
                       (PyObject *)&angle,(PyObject *) &indexes,
                       (PyObject *)&Sindex,(PyObject *)&Srough  )
  )
    return NULL;

    /* check the Objects */
    if(!PyArray_Check((PyObject *)thickness ))    onError("not a PyArray, argument 1");
             if(!PyArray_Check((PyObject *)roughness))     onError("not a PyArray, argument 2");
             if(!PyArray_Check((PyObject *)wavelenght))    onError("not a PyArray, argument 3");
                   if(!PyArray_Check((PyObject *)angle))         onError("not a PyArray, argument 4");
                  if(!PyList_Check((PyObject *)indexes))        onError("not a PyList, argument 5");
              if(!PyArray_Check((PyObject *)Sindex))        onError("not a PyArray, argument 6");
              if(!PyFloat_Check((PyObject *)Srough))        onError("not a PyFloat, argument 7");

              /* check the types */

              if( thickness->descr->type_num != PyArray_DOUBLE ) onError(" thickness is not double " ) ;
              if( roughness->descr->type_num != PyArray_DOUBLE ) onError(" roughness  is not double " ) ;
              if( wavelenght->descr->type_num != PyArray_DOUBLE ) onError(" wavelenght is not double " ) ;
              if( angle->descr->type_num != PyArray_DOUBLE ) onError(" angle is not double " ) ;


              int num_thicks = PyList_Size(indexes) ;
              for(int k=0; k< num_thicks; k++) {
                if(!PyArray_Check((PyObject *)  PyList_GetItem(indexes,k )   )   ) {
                  char errormessage[200];
                  sprintf(errormessage,"not a PyArray, argument 5, item number %d",k);
                  onError(errormessage);
                }
                if( ((PyArrayObject*) PyList_GetItem(indexes,k )) ->descr->type_num != PyArray_CDOUBLE )  {
                  char errormessage[200];
                  sprintf(errormessage,"not a CDOUBLE PyArray, argument 5, item number %d",k);
                  onError(errormessage);
                }
              }

              if( Sindex->descr->type_num != PyArray_CDOUBLE ) onError(" Sindex is not double " ) ;

              /* check the Number of dimensions */

              if( thickness->nd != 1 )
                onError("The thickness array (arg. 1) has not the right number of dimensions");
              if(roughness ->nd != 1 )
                onError("The roughness array (arg. 2) has not the right number of dimensions");
              if( wavelenght->nd != 1 )
                onError("The wavelenght array (arg. 3) has not the right number of dimensions");
              if(angle->nd != 1 )
                onError("The angle array (arg. 4) has not the right number of dimensions");


              int *  nd_layer_i= new int [num_thicks];
              for(int k=0; k< num_thicks; k++) {
                int nd =  ((PyArrayObject *)  PyList_GetItem(indexes,k )   ) ->nd;
                if(  nd!=1 && nd!=3   ) {
                  char errormessage[200];
                  sprintf(errormessage,"PyArray, argument 5, item number %d is neither an array neither a tensor array, nd= %d",k,nd);
                  onError(errormessage);
                }
                nd_layer_i[k]=nd;
              }
              int nd_subs_i;
              {
                int nd =  ((PyArrayObject *)  Sindex  ) ->nd;
                if(  nd != 1 && nd != 3  )
                  onError("The Sindex array (arg. 6) is neither an array neither a tensor array");
                nd_subs_i=nd;
              }

              /* check the Number of dimensions */

              int numthick, numwave;
              numthick=thickness->dimensions[0];

              if(numthick!=roughness->dimensions[0] )
                onError("The roughness array (arg. 2) has not the right  dimension");

              numwave  = wavelenght->dimensions[0] ;

              if(numwave!=angle->dimensions[0] )
                onError("The  angles array (arg. 4) has not the right  dimension");

              if(numthick!=PyList_Size(indexes)   )
                onError("The indexes  array (arg. 5) has not the right  lenght");

              for(int k=0; k< numthick; k++) {
                PyArrayObject *li;
                li = ((PyArrayObject *)  PyList_GetItem(indexes,k ))  ;
                if(   li->dimensions[0] != numwave   ) {
                  onError("The indexes  array (arg. 5) has not the right second   dimension");
                }
                if( nd_layer_i[k]==3) {
                  if(   li->dimensions[1] != 3 || li->dimensions[2] != 3   ) {
                    onError("The indexes  array (arg. 5) has not the right third and/or  fourth where it should be a tensor");
                  }
                }
              }

              if(numwave!=Sindex->dimensions[0] )
                onError("The Sindex  array (arg. 6) has not the right  second dimension");
              {
                if(nd_subs_i==3) {
                  if(   Sindex->dimensions[1] != 3 || Sindex->dimensions[2] != 3   ) {
                    onError("The Substrate Index  array (arg. 6) has not the right dimensions where it should be a tensor");
                  }
                }
              }

              /* check that everything is contiguous */

              PyArrayObject *toverify[]={thickness,roughness,wavelenght,angle,Sindex,NULL};
              PyArrayObject **ptr;
              ptr=toverify;
              while(*ptr) {
                if((*ptr)->flags %2 == 0) onError(" All arrays have to be contiguous, either solve this problem at the python level or at the c level using PyArray_CopyFromObject");
                ptr++;
              }
              for(int k=0; k< numthick; k++) {
                PyArrayObject *li;
                li = ((PyArrayObject *)  PyList_GetItem(indexes,k ))  ;
                if((li)->flags %2 == 0) onError(" All arrays in Indexes have to be contiguous, either solve this problem at the python level or at the c level using PyArray_CopyFromObject");
              }

              /* take into account how many eigenmodes have to be calculated */
              Onda * Eigenmodes= new Onda [numthick];
              Onda EigenSubstrate;
              Onda ** wheretolook = new   Onda* [numthick];
              int *where_to_look_for_cuda;
              int *inverse_loopup;
              int ntodo=0;

              std::cout << "Substrate has this dim " << Sindex->nd <<std::endl;

              where_to_look_for_cuda = new int [numthick+1];
              where_to_look_for_cuda[0] = 0; //Substrate
              inverse_loopup = new int [numthick];

              for(int k=0; k< numthick; k++) {
                PyArrayObject *li;
                li = ((PyArrayObject *)  PyList_GetItem(indexes,k ))  ;
                int isnew=1;
                for(int l=0; l< ntodo; l++) {
                  if(   ThoseArraysAreEqual(li  ,  Eigenmodes[l].indexes)    ) {
                    isnew=0;
                    wheretolook[k]= &( Eigenmodes[l]  );
                    where_to_look_for_cuda[k+1] = l+1;
                    printf("layer %d is connected to %d\n",k+1,l+1);
                    break;
                  }
                }
                if(isnew){
                  Eigenmodes[ntodo].indexes = li;
                  Eigenmodes[ntodo].angles=angle    ;
                  wheretolook[k]= &( Eigenmodes[ntodo]  );
                  where_to_look_for_cuda[k+1] = ntodo+1;
                  inverse_loopup[ntodo] = k;
                  printf("layer %d is unique w dim %d\n",k+1,li->nd);
                  ntodo++;
                }
              }

              FILE *fp;
              int dummy;
              std::cout << "Now dumping sizes" <<std::endl;
              fp=fopen("sizes.dat","wb");
              fwrite(&numwave,sizeof(int),1,fp);
              dummy = numthick + 1;
              fwrite(&dummy,sizeof(int),1,fp);
              dummy = ntodo + 1;
              fwrite(&dummy,sizeof(int),1,fp);
              fclose(fp);
              std::cout<< "Wrote numwave: " << numwave << ", numthick: "<<numthick+1<<" and unique layers: "<<ntodo+1 <<std::endl;

              std::cout << "Now dumping layers look up table" <<std::endl;
              fp=fopen("layers_lookup_table.dat","wb");
              fwrite(where_to_look_for_cuda,sizeof(int),numthick+1,fp);
              fclose(fp);

              std::cout << "Now dumping modes" <<std::endl;
              dummy = 1;
              fp=fopen("modes.dat","wb");
              fwrite(&dummy,sizeof(int),1,fp); //substrate
              for(int i=0;i<ntodo;i++){
                fwrite(&(nd_layer_i[ inverse_loopup[i] ]),sizeof(int),1,fp);
              }
              fclose(fp);

              std::cout << "Now dumping indexes for unique layers" <<std::endl;
              complex< double > *data;
              int nd_size;
              fp=fopen("indexes.dat","wb");
              data = (complex< double >*)(Sindex->data);
              nd_size = (Sindex->nd)*(Sindex->nd);
              fwrite(data,sizeof(complex<double>),numwave*nd_size,fp);
              for(int i=0;i<ntodo;i++){
                data = (complex< double >*)(Eigenmodes[i].indexes->data);
                nd_size = (Eigenmodes[i].indexes->nd)*(Eigenmodes[i].indexes->nd);
                fwrite(data,sizeof(complex<double>),numwave*nd_size,fp);
              }
              fclose(fp);

              EigenSubstrate.indexes = Sindex;
              EigenSubstrate.angles=angle;
              PyObject * res = PyList_New(0);

              EigenSubstrate.CreateModes();
              PyObject * nuovo = EigenSubstrate.get_vectors();
              PyList_Append(res ,   nuovo     ) ;
              Py_DECREF(nuovo);


              for(int i=0; i<ntodo; i++) {
                if(i==ntodo-1) {
                  Eigenmodes[i].CreateModesTensor_Vide();
                } else {
                  Eigenmodes[i].CreateModes();
                }
              }

              for(int k=0; k< numthick; k++) {
                PyObject * nuovo = wheretolook[k]-> get_vectors();
                PyList_Append(res ,     nuovo   ) ;
                Py_DECREF(nuovo);

              }

              /* creation of the return Values */
              delete [] nd_layer_i ;
              delete []  Eigenmodes;
              delete [] wheretolook ;
              delete [] where_to_look_for_cuda;
              delete [] inverse_loopup;
              return res;
}




/**
 * \brief The exposed to Python module functionality of clPPM
 */
static PyMethodDef multicouche_functions[] = {
  {"PPM_calculatescanTensorOcl", multicouche_calculatescanTensorOcl, METH_VARARGS , multicouche_calculatescanTensorOcl_doc },
  {"PPM_calculatescanTensor", multicouche_calculatescanTensorTest, METH_VARARGS , multicouche_calculatescanTensorTest_doc },
  {"PPM_show_devices",multicouche_show_devices, METH_VARARGS,multicouche_show_devices_doc},
  {"PPM_query_for_gpu",multicouche_query_for_gpu,METH_VARARGS,multicouche_query_for_gpu_doc},
  { NULL, NULL}
};


extern "C" {
  void
  initclPPM();
}
void
initclPPM()
{
  PyObject *m, *d;
  m = Py_InitModule("clPPM", multicouche_functions);
  d = PyModule_GetDict(m);
  ErrorObject = Py_BuildValue("s","clPPM.error");
  PyDict_SetItemString(d,"error", ErrorObject);
  if(PyErr_Occurred())
    Py_FatalError("can't initialize module clPPM");

  #ifdef import_array
    import_array();
    #endif
}





















