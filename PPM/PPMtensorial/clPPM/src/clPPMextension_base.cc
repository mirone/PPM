
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef OCLTOOLS_H
#include "ocl_tools.h" /// \brief OpenCL tools
#define OCLTOOLS_H
#endif

#include <CL/opencl.h>
#include "ocl_ckerr.h"
#include "clPPMextension.h"

/*
 * \brief Special preconfigured strings created from the cl kernel files.
 * 
 * They automatically create 2 vars each:
 * unsigned char name_suffix
 * unsigned int name_suffix_len
 */
#include "clPPMkernels_modes.clh"
#include "clPPMkernels_other.clh"


///Profiling header-source
#include "timer.h"

#define blockSize 32

#define CT CL_CHECK_ERR_THROW

using namespace std;

/**
 * Apart from initialisation of variables, ocl_config_type for ocl_tools is allocated.
 * Also the output stream is set to stdout. If ocl_tools allocations fails a message is thrown
 */
PPMTensorial_ocl::PPMTensorial_ocl(){

  scanLen     = 0;
  numthick    = 0;
  uniquelayers= 0;
  debug_flag  = 0;
  first_run_flag=1;
  resident_alloc=0;
  angles      = NULL;
  wheretolook = NULL;
  Kx          = NULL;
  die         = NULL;
  Kz          = NULL;
  E           = NULL;
  B           = NULL;
  Sistem      = NULL;
  NonZeri     = NULL;
  Modes       = NULL;
  Offsets     = NULL;
  Offsets_die = NULL;

  K0          = NULL;
  Roughness   = NULL;
  Thickness   = NULL;
  Prop        = NULL;
  Conditions  = NULL;
  Conditions_inv = NULL;
  Interfaces  = NULL;
  Reflectivity= NULL;
  Overall_GPU_ms = 0.0;

  oclstream = stdout;
  hasStdout = 1;
  hasGPU    = 0;
	overrideCustomConfig = 0;
  
  oclconfig = NULL;
  oclconfig = new ocl_config_type;
  if(!oclconfig) throw "Oclconfig was not allocated\n";

	oclconfig->oclmemref = NULL;
	oclconfig->oclmemref = new cl_mem[24];
	if(!oclconfig->oclmemref) throw "oclmemref was not allocated\n";
}

/**
 * Constructor identical to default but with file stream. The stream is used by the OpenCL-Toolbox only
 * 
 * The file stream only matters for ocl_tools, as it is used internally for messages.
 * Even if the stream is used for ocl_tools output, error will always be directed to
 * stderr.
 * PPMTensorial_ocl is resposible for opening and closing the file.
 *
 * @param fname const C-string with the filename to use instead of stdout(default).
 */
PPMTensorial_ocl::PPMTensorial_ocl(const char *fname){
  scanLen     = 0;
  numthick    = 0;
  uniquelayers= 0;
  debug_flag  = 0;
  first_run_flag=1;
  resident_alloc=0;
  angles      = NULL;
  wheretolook = NULL;
  Kx          = NULL;
  die         = NULL;
  Kz          = NULL;
  E           = NULL;
  B           = NULL;
  Sistem      = NULL;
  NonZeri     = NULL;
  Modes       = NULL;
  Offsets     = NULL;
  Offsets_die = NULL;

  K0          = NULL;
  Roughness   = NULL;
  Thickness   = NULL;
  Prop        = NULL;
  Conditions  = NULL;
  Conditions_inv = NULL;
  Interfaces  = NULL;
  Reflectivity= NULL;
  Overall_GPU_ms = 0.0;
  
  oclstream = fopen(fname,"w");
  hasStdout = 0;
  hasGPU    = 0;
	overrideCustomConfig = 0;
  
  oclconfig = NULL;
  oclconfig = new ocl_config_type;
  if(!oclconfig) throw "Oclconfig was not allocated\n";

	oclconfig->oclmemref = NULL;
	oclconfig->oclmemref = new cl_mem[24];
	if(!oclconfig->oclmemref) throw "oclmemref was not allocated\n";
}

/**
 * Before any execution may begin, clPPM needs a local reference of the input
 * as well as some of the problem's characteristics.
 *
 * Debug checks - assertions.
 * Keep local copies of top-level memory pointers.
 * Calculate some quantities needed for the allocations.
 */
void PPMTensorial_ocl::getInput(const int &scanLen_,const int &numthick_,const int &uniquelayers_,
                                  int *modes, complex<double> *indexes,
                                  double *angles, double *roughness,
                                  double *thickness, double *k0,
                                  int *lookup, complex<double> *reflectivity){

#ifdef _VERBOSE
  std::cout<<"Initialising PPMTensorial_ocl" <<std::endl;
#endif
  
  //sum up modes! Mode is the nd size. So 1 for Scalar and 3 for tensor. 1x1 Scalar, 3x3Tensor
  //Position 0 is the substrate which is always scalar
  summode=0;
  this->scanLen = scanLen_;
  this->numthick= numthick_;
  this->uniquelayers = uniquelayers_;

  assert(scanLen > 0);
  assert(numthick >= uniquelayers);
  assert(uniquelayers > 0);
  assert(modes);
  assert(indexes);
  assert(angles);
  assert(roughness);
  assert(thickness);
  assert(k0);
  assert(lookup);
  assert(reflectivity);

  wheretolook = lookup;
  Modes = modes;

  for(int imode =0;imode<uniquelayers;imode++){
    summode+=Modes[imode] * Modes[imode];
  } 

  Offsets=new int [uniquelayers];
  Offsets_die=new int [uniquelayers];

  Offsets[0]=0;
  Offsets_die[0]=0;

  int mmode;
  for(int imode=1;imode<uniquelayers;imode++){
    mmode = Modes[imode-1] * Modes[imode-1] ;
    assert(mmode==1 || mmode==9);//Hook for uninitialised elements

    Offsets[imode] = Offsets[imode-1] + (scanLen * 4 * mmode);
    Offsets_die[imode] = Offsets_die[imode-1] + (scanLen * mmode);

  }

  this->angles = angles; //Has size scanLen and is same for all layers
  K0 = k0;
  die    = indexes;
  Roughness = roughness;
  Thickness = thickness;
  Reflectivity = reflectivity;
  
  //allocatios
  try{
    hostAllocations();
  }catch(const std::bad_alloc& e){
    char msg[1000];
    sprintf(msg,"%s (@ %d): Caught a bad_alloc in hostAllocations: %s\n",__FUNCTION__,__LINE__,e.what());
    throw msg;
  }
  return;
}

PPMTensorial_ocl::~PPMTensorial_ocl(){

  if(!hasStdout) fclose(oclstream);
  
  delete[] Offsets;
  delete[] Offsets_die;

  if(debug_flag){
    delete[] Kx;
    delete[] Kz;
    delete[] As;
    delete[] E;
    delete[] B;
    delete[] Sistem;
    delete[] NonZeri;
    delete[] Prop;
    delete[] Interfaces;
    delete[] Conditions;
    delete[] Conditions_inv;
  }

	delete[] oclconfig->oclmemref;
  delete oclconfig;
}

/**
 * A dummy method since now all memory is provided by the top level.
 * If debug_flag is set, it calls hostAllocations_debug().
 * host_allocations() is called by getInput().
 */
void PPMTensorial_ocl::hostAllocations(){
  if(debug_flag) hostAllocations_debug();
  return;
}

/**
 * Debug level host-side allocations are used to store the
 * results of the kernels on each step
 */
void PPMTensorial_ocl::hostAllocations_debug(){

#ifdef _VERBOSE  
  printf("Debug level allocations\n");
#endif
  
  Kx     = new double [scanLen];
  Kz     = new complex<double> [scanLen * 4 * (uniquelayers)] ;
  As     = new complex<double> [scanLen * 4 * (uniquelayers)] ;
  E      = new complex<double> [scanLen * 4 * 3 * (uniquelayers)];
  B      = new complex<double> [scanLen * 4 * 3 * (uniquelayers)];
  Sistem = new complex<double> [scanLen * 4 * summode];
  NonZeri= new int [scanLen * 4 * (numthick + 1)];
  Prop   = new complex<double> [scanLen * 4 * 4 * (numthick) ];
  Conditions     = new complex<double> [scanLen * 4 * 4 * (numthick) ];
  Conditions_inv = new complex<double> [scanLen * 4 * 4 * (numthick) ];
  Interfaces     = new complex<double> [scanLen * 4 * 4 * (numthick-1) ];
  return;
}

/**
 * Execute kernels. The kernel enqeueing and configuration is done in the appropriate class.
 * Upon first invocation, setOclDevice will be called first in order to create a context with
 * an OpenCL device described by the arguments or discovered by query_for_gpu.
 * It allocates the required device memory and copies the input there and then calls
 * the appropriate kernel invocation classes.
 *
 * If query_for_gpu has been used the arguments will be discarded. Parameters are passed directly
 * to setOclDevice.
 *
 * @param devicetype A C-string that describes the type of the device desired. Possible
 *                               choices are "CPU","GPU","DEF","ALL","ACC".
 * @param platformid An integer with the id of the platform to be used for the devicetype device.
 *                       If the value is negative then the argument is discarded.
 * @param devid An integer with the id of the specific device to be used of a specific type and platform.
 */
void PPMTensorial_ocl::launchPPMTensorial_ocl(const char *devicetype,int platformid,int devid){

#ifdef _VERBOSE  
  std::cout<<"Running PPMTensorial_ocl" <<std::endl;
#endif
  
  //Cuda Initialisations
  if(first_run_flag){
    setOclDevice(devicetype,platformid,devid);
  }

  resetTimers();
  allocateOclBaseMemory();
  setInputMemory();
  //
  
  //Eigenmodes
  eigens.CreateModes(scanLen,uniquelayers,oclconfig);

  //If debug_flag is not set we only need the result (reflectivity)
  if(debug_flag){
    eigens.getModesResults(scanLen,uniquelayers,d_Kz,d_E,d_B,Kz,E,B,oclconfig);
    eigens.getModesResults_debug(scanLen,uniquelayers,summode,d_Kx,d_Sistem,d_NonZeri,d_As,Kx,Sistem,NonZeri,As,oclconfig);
  }
  addGpuTime(eigens.clOnda_GPU_time);
  
  releaseOclDeprecMemory();
  allocateOclOtheMemory();

  //Propagations
  props.Propagations(scanLen,numthick,oclconfig);
  
  //If debug_flag is not set we only need the result (reflectivity)  
  if(debug_flag){
    props.getPropagationsResults(scanLen,numthick,d_Props,Prop,oclconfig);
  }
  addGpuTime(props.clPropagation_GPU_time);

  //Interfaces
  inters.Interfaces(scanLen,numthick,oclconfig);
  
  //If debug_flag is not set we only need the result (reflectivity)  
  if(debug_flag){
    inters.getInterfacesResults(scanLen,numthick,d_Interfaces,Interfaces,d_Conditions,d_Conditions_inv,
                                 Conditions,Conditions_inv,oclconfig);
  }
  addGpuTime(inters.clInterfaces_GPU_time);

  //Reflectivity
  refls.Reflectivity(scanLen,oclconfig);
  refls.getReflResults(scanLen,d_Reflectivity,Reflectivity,oclconfig);
  addGpuTime(refls.clReflectivity_GPU_time);

  releaseOclRemainMemory();

#ifdef _VERBOSE  
  std::cout<< "eigens OpenCL time: "<< eigens.clOnda_GPU_time << std::endl;
  std::cout<< "props OpenCL time: "<< props.clPropagation_GPU_time << std::endl;
  std::cout<< "inters OpenCL time: "<< inters.clInterfaces_GPU_time << std::endl;
  std::cout<< "refls OpenCL time: "<< refls.clReflectivity_GPU_time << std::endl;  
  std::cout<< "Total OpenCL time: "<< Overall_GPU_ms << " (ms)" << std::endl;
#endif  
  first_run_flag=0;

  return;
}

/**
 * Perform benchmark by executing kernels multiple times.
 * The basic process is similar to launchPPMTensorial_ocl().
 * The kernel enqeueing and configuration is done in the appropriate class.
 * Upon first invocation, setOclDevice will be called first in order to create a context with
 * an OpenCL device described by the arguments or discovered by query_for_gpu.
 * It allocates the required device memory and copies the input there and then calls
 * the appropriate kernel invocation classes.
 *
 * In benchPPMTensorial_ocl(), the execution of kernels is done in a loop of Nruns. If the
 * library is called for the first time, Nruns is extended by +1 and the first iteration
 * counts as a warm-up execution. Results from the warm-up execution are not taken into account 
 * 
 * If query_for_gpu has been used the arguments will be discarded. Parameters are passed directly
 * to setOclDevice.
 *
 * @param devicetype A C-string that describes the type of the device desired. Possible
 *                               choices are "CPU","GPU","DEF","ALL","ACC".
 * @param platformid An integer with the id of the platform to be used for the devicetype device.
 *                       If the value is negative then the argument is discarded.
 * @param devid An integer with the id of the specific device to be used of a specific type and platform.
 * @param Nruns An integer with the desired number of iterations for the benchmark. Default value is 100.
 */
void PPMTensorial_ocl::benchPPMTensorial_ocl(const char *devicetype,int platformid,int devid, int Nruns){

  std::cout<<"Benchmarking PPMTensorial_ocl" <<std::endl;

  float totalBenchTime=0.0;
  float totalEigensTime=0.0;
  float totalPropsTime=0.0;
  float totalInterTime=0.0;
  float totalReflTime=0.0;
  int warm_up=0;

  if(first_run_flag){
    warm_up=1;
    //OpenCL Initialisations    
    setOclDevice(devicetype,platformid,devid);
  }
for(int testrun=0;testrun<(Nruns + first_run_flag);testrun++){

  resetTimers();
  allocateOclBaseMemory();
  setInputMemory();
  //

  //Eigenmodes
  eigens.CreateModes(scanLen,uniquelayers,oclconfig);
  addGpuTime(eigens.clOnda_GPU_time);
  if(!warm_up)totalEigensTime += eigens.clOnda_GPU_time;

  releaseOclDeprecMemory();
  allocateOclOtheMemory();

  //Propagations
  props.Propagations(scanLen,numthick,oclconfig);
  addGpuTime(props.clPropagation_GPU_time);
  if(!warm_up)totalPropsTime += props.clPropagation_GPU_time;

  //Interfaces
  inters.Interfaces(scanLen,numthick,oclconfig);
  addGpuTime(inters.clInterfaces_GPU_time);
  if(!warm_up)totalInterTime += inters.clInterfaces_GPU_time;

  //Reflectivity
  refls.Reflectivity(scanLen,oclconfig);
  addGpuTime(refls.clReflectivity_GPU_time);
  if(!warm_up)totalReflTime += refls.clReflectivity_GPU_time;

  releaseOclRemainMemory();
  warm_up=0;
}
  totalBenchTime = totalEigensTime + totalPropsTime +
                    totalInterTime + totalReflTime;
  first_run_flag=0;

  std::cout<< "OpenCL Benchmark results for " << Nruns << " runs (averaged)" <<std::endl;
  std::cout<<"_t_ocl_eigens "<< totalEigensTime / (float)Nruns<<std::endl;
  std::cout<<"_t_ocl_props  "<< totalPropsTime / (float)Nruns<<std::endl;
  std::cout<<"_t_ocl_inters "<< totalInterTime / (float)Nruns<<std::endl;
  std::cout<<"_t_ocl_refl   "<< totalReflTime / (float)Nruns<<std::endl;
  std::cout<<"_t_ocl_total  "<< totalBenchTime / (float)Nruns<<std::endl;


  return;
}

/**
 * setOclDevice is resposible for initialing a context with a OpenCL device,
 * evaluate it's possibility to use double precision, create the command queue,
 * compile the opencl programs and get the kernels.
 *
 * @param devicetype A C-string that describes the type of the device desired. Possible
 *                               choices are "CPU","GPU","DEF","ALL","ACC".
 * @param platformid An integer with the id of the platform to be used for the devicetype device.
 *                       If the value is negative then the argument is discarded.
 * @param devid An integer with the id of the specific device to be used of a specific type and platform.*
 */
void PPMTensorial_ocl::setOclDevice(const char *devicetype,int platformid,int devid){

  char msg[1000];
  cl_int err;

  //Use query_for_gpu discovered device, overriding the provided types
  if(overrideCustomConfig){
    if(int err=ocl_init_context(oclconfig,_platformid,_devid,oclstream)){
      sprintf(msg,"%s (@ %d): ocl_init_context failed (%d) on a configuration provided by \"query_for_gpu\"\n",
              __FUNCTION__,__LINE__,err);
      throw msg;      
    }
  }else if(platformid<0 && devid <0){
    //Pick any device of type type and initiate a context
    if(int err=ocl_init_context(oclconfig,devicetype,oclstream)){
      std::cout << "Configuration was: " << devicetype << std::endl;
      std::cout <<std::endl;
      ocl_check_platforms(stdout);
      sprintf(msg,"%s (@ %d): ocl_init_context failed (%d)\n",__FUNCTION__,__LINE__,err);
      throw msg;      
    }
  }else{
    //Pick a device and initiate a context
    if(int err=ocl_init_context(oclconfig,devicetype,platformid,devid,oclstream)){
      std::cout << "Configuration was: " << devicetype << " on platform " << platformid
        << " with device_id " << devid <<std::endl;
      std::cout << "<" << devicetype << "> @ (" <<platformid << "." << devid << ")"
        <<std::endl;
      std::cout <<std::endl;
      ocl_check_platforms(stdout);
      sprintf(msg,"%s (@ %d): ocl_init_context failed (%d)\n",__FUNCTION__,__LINE__,err);
      throw msg;
    }
  }
  //Check if device supports 64bits
  if(( err=ocl_eval_FP64(oclconfig,NULL,oclstream) )){
    sprintf(msg,"%s (@ %d): ocl_eval_FP64 failed (%d)\n",__FUNCTION__,__LINE__,err);
    throw msg;
  }

  //Now create the command queue
  oclconfig->oclcmdqueue = clCreateCommandQueue(oclconfig->oclcontext,oclconfig->ocldevice,CL_QUEUE_PROFILING_ENABLE,&err);
  if(err){
    sprintf(msg,"%s (@ %d): clCreateCommandQueue failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(err));
    throw msg;
  }

#ifdef _CLSRC_FILES
  //Now compile the opencl kernel files. 2 kernel files.
  const char *clList[2] = { "clPPMkernels_modes.cl" , "clPPMkernels_other.cl"};
  int clNum=2;

  if(err = ocl_compiler(oclconfig,clList,clNum,blockSize,"",oclstream)){
    sprintf(msg,"%s (@ %d): ocl_compiler failed: %d\n",__FUNCTION__,__LINE__,err);
    throw msg;
  };
#else
  //Now compile the opencl kernels found in the included strings
  unsigned char *clList[2] = { clPPMkernels_modes_cl     , clPPMkernels_other_cl    };
  unsigned int clLen[2] = { clPPMkernels_modes_cl_len , clPPMkernels_other_cl_len};
  int clNum=2;

  if(err = ocl_compiler(oclconfig,clList,clLen,clNum,blockSize,"",oclstream)){
    sprintf(msg,"%s (@ %d): ocl_compiler failed: %d\n",__FUNCTION__,__LINE__,err);
    throw msg;
  };
#endif
  
  //Now we create the kernel functions

  oclconfig->oclkernels = (cl_kernel*)malloc(13*sizeof(cl_kernel));
  if(!oclconfig->oclkernels){
    sprintf(msg,"%s (@ %d): oclkernels malloc failed.\n",__FUNCTION__,__LINE__);
    throw msg;  
  }
  
  const char *clKernels_modes[30]= {"setKx","getAs","getKz","setSistem","calcNonzeri","calcZeri","calcB",
    "clModesScalar","clModesTensor_Vide"
  };

  const char *clKernels_other[30]= {"clPropagation","getConditions","clInterfaces","clReflectivity"};
  
  oclconfig->Nkernels=13;
  for(int ikern=0;ikern<oclconfig->Nkernels;ikern++){
    try{
      if(ikern<9)oclconfig->oclkernels[ikern]= clCreateKernel(oclconfig->prgs[0].oclprogram,clKernels_modes[ikern],&err);
      else oclconfig->oclkernels[ikern]= clCreateKernel(oclconfig->prgs[1].oclprogram,clKernels_other[ikern-9],&err);
    }catch(...){
      //OpenCL now also has a C++ API that can be enabled by including cl.hpp instead of opencl.h
      // In the C++ API openCL exceptions are defined that can be enabled explicilty.
      // However, Intel's OpenCL completely neglects this and enables them internally, throwing
      // exceptions which cannot be specifically caught without Intel's cpp header.
      // By definition also the OpenCL error flag cannot be defined because the C++ API only throws
      // and does not mark the error flag. Thank you Intel!
      sprintf(msg,"%s (@ %d): clCreateKernel \"%s\" failed but I do not know why. Blame Intel\n",
              __FUNCTION__,__LINE__,(ikern<9)?clKernels_modes[ikern]:clKernels_other[ikern-9]);
      throw msg;
    }

    if(err){
      sprintf(msg,"%s (@ %d): clCreateKernel \"%s\" failed: %s\n",__FUNCTION__,__LINE__,
              (ikern<9)?clKernels_modes[ikern]:clKernels_other[ikern-9],ocl_perrc(err));
      throw msg;
    };
  }  
  
  return;
};

/**
 * \brief Interface to ocl_tools device prob that prints the list of the OpenCL enabled devices.
 *
 * Useful when discovery of a requested device fails. clPPM will then automatically
 * invoke show_devices(), which will list all the available devices.
 */
void PPMTensorial_ocl::show_devices()
{
  std::cout<<"Showing OpenCL-enabled devices: " <<std::endl <<std::endl;
  ocl_check_platforms(stdout);
  std::cout<<std::endl;
  return;
}


/**
 * \brief Uses ocl_tools' device probing for automatic GPU discovery
 *
 * query_for_gpu only gets the first occurance.
 * For CUDA devices this is the most capable device.
 * The value of the platform and device are returned so they
 * can be used by setOclDevice.
 *
 * PPMTensorial_ocl will use this id to override any manual id provided. This method
 * does not require any initialisation from the library, so it is very useful
 * as it can provide information upon the loading of the library (i.e in Python).
 */
void PPMTensorial_ocl::query_for_gpu()
{
  std::cout<<"Querying for OpenCL-enabled GPU devices" <<std::endl;
  int status;
  status = ocl_find_devicetype_FP64(CL_DEVICE_TYPE_GPU, _platformid, _devid, oclstream);
	if(status){
		hasGPU = 0;
    std::cout << "System does not have GPU with FP64 support" <<std::endl;
		std::cout << "clPPM will be set to use the first available CPU" <<std::endl;
		status = ocl_find_devicetype_FP64(CL_DEVICE_TYPE_CPU, _platformid, _devid, oclstream);
		if(status) std::cout << "System does not have an OpenCL CPU either..." <<std::endl;
		else overrideCustomConfig=1;
	}
  else {
    hasGPU = 1;
		overrideCustomConfig=1;
    std::cout << "System has GPU with FP64 support" <<std::endl;
  }
  return;
}


/**
 * \brief Base device memory allocations.
 * 
 * At this point the memory that is needed for Modes
 * is allocated. If an allocation fails, all the previously allocated
 * OpenCL buffers get released.
 * The buffers with cnst_ prefix will reside on the constant
 * memory space on the device, and as such they are not counted on the required
 * buffers. Then these buffers are tied to the kernel arguments.
 */
void PPMTensorial_ocl::allocateOclBaseMemory(){

  size_t req_alloc=0;
  size_t dtype_size;
  char msg[1000];

  cl_int clErr;
  dtype_size = sizeof(cl_double2);

  req_alloc += scanLen * sizeof(double); //d_Kx - Input
  req_alloc += scanLen * sizeof(double); //d_K0 - Input
  req_alloc += scanLen * 4 * (uniquelayers) * dtype_size; //d_Kz - Result - needed by Other kernels
  req_alloc += scanLen * 4 * (uniquelayers) * 3 * dtype_size; //d_E - Result - needed by Other kernels
  req_alloc += scanLen * 4 * (uniquelayers) * 3 * dtype_size; //d_B - Result - needed by Other kernels
  resident_alloc = req_alloc;
  
  if(debug_flag){
    req_alloc += scanLen * 4 * (uniquelayers) * dtype_size; //d_As - auxiliary storage for intermediate testing
  }
  req_alloc += scanLen * summode * dtype_size; //d_die - Input - not needed by Other kernels
  req_alloc += scanLen * 4 * summode * dtype_size; //d_Sistem - Auxiliary storage - not needed by Other kernels
  req_alloc += scanLen * 4 * (uniquelayers) * sizeof(int); //d_NonZeri - Auxiliary storage - not needed by Other kernels

#ifdef _VERBOSE  
  printf("Dev %f, req %f\n",(float)oclconfig->dev_mem / 1024.0 / 1024.0,(float)req_alloc / 1024.0 / 1024.0);
#endif
  
  //5MB are always allocated on a device. There is also approximatelly 68MB of overhead. However we cannot check for the actual used memory
  if((oclconfig->dev_mem - 74*1024*1024)<= req_alloc){
    sprintf(msg,"%s (@ %d): Device does not meet memory requirements. %f requested, but only %f available. Take into account ~74MB of overhead.\n",
            __FUNCTION__,__LINE__,(float)req_alloc / 1024.0 / 1024.0,
            (float)oclconfig->dev_mem / 1024.0 / 1024.0);
    throw msg;
  }

	//Reset the referenced buffers count
	oclconfig->Nbuffers=0;
  d_Kx = 
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * sizeof(cl_double)),NULL,&clErr);
  if(clErr){
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
		//ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);//Not needed in the 1st alloc
    throw msg;
	}else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_Kx;
		oclconfig->Nbuffers++;
	};

  d_K0 =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(scanLen * sizeof(cl_double)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_K0;
		oclconfig->Nbuffers++;
	};

  d_Kz =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * (uniquelayers) * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_Kz;
		oclconfig->Nbuffers++;
	};

  d_E =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * (uniquelayers) * 3 * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_E;
		oclconfig->Nbuffers++;
	};

  d_B =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * (uniquelayers) * 3 * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_B;
		oclconfig->Nbuffers++;
	};

  if(debug_flag){
    d_As =
      clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * (uniquelayers) * dtype_size),0,&clErr);
    if(clErr){
			ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
      sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
      throw msg;
		}else{
			oclconfig->oclmemref[oclconfig->Nbuffers] = d_As;
			oclconfig->Nbuffers++;
		}
  }

  d_die =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(scanLen * summode * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_die;
		oclconfig->Nbuffers++;
	};

  d_Sistem =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * summode * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_Sistem;
		oclconfig->Nbuffers++;
	};

  d_NonZeri =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * (uniquelayers) * sizeof(cl_int)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_NonZeri;
		oclconfig->Nbuffers++;
	};

  //Now the constant memory
  cnst_scanlen =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(sizeof(cl_int)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_scanlen;
		oclconfig->Nbuffers++;
	};

  cnst_numthick =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(sizeof(cl_int)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_numthick;
		oclconfig->Nbuffers++;
	};

  cnst_uniquelayers =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(sizeof(cl_int)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_uniquelayers;
		oclconfig->Nbuffers++;
	};

  cnst_Modes =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(uniquelayers * sizeof(cl_int)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_Modes;
		oclconfig->Nbuffers++;
	};

  cnst_Offsets =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(uniquelayers * sizeof(cl_int)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_Offsets;
		oclconfig->Nbuffers++;
	};

  cnst_Offsets_die =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(uniquelayers * sizeof(cl_int)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_Offsets_die;
		oclconfig->Nbuffers++;
	};

  cnst_wheretolook =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(numthick * sizeof(cl_int)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_wheretolook;
		oclconfig->Nbuffers++;
	};

  cnst_Roughness =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(numthick * sizeof(cl_double)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_Roughness;
		oclconfig->Nbuffers++;
	};

  cnst_Thickness =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_ONLY,(size_t)(numthick * sizeof(cl_double)),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = cnst_Thickness;
		oclconfig->Nbuffers++;
	};

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": Base Memory allocated " << (float)req_alloc / 1024.0 / 1024.0 << " Mbyte " <<std::endl;
#endif  

  //Now attach the cl_buffers to the kernels!
  /*"setKx","getAs","getKz","setSistem","calcNonZeri","calcZeri","calcB",
    "clModesScalar","clModesTensor_Vode"  */
  
  //setKx
  CT( clSetKernelArg(oclconfig->oclkernels[0],0,sizeof(cl_mem),&d_Kx) );
  CT( clSetKernelArg(oclconfig->oclkernels[0],1,sizeof(cl_mem),&cnst_scanlen) );
  
  //getAs
  CT( clSetKernelArg(oclconfig->oclkernels[1],0,sizeof(cl_mem),&d_Kx) );
  CT( clSetKernelArg(oclconfig->oclkernels[1],1,sizeof(cl_mem),&d_die) );
  if(debug_flag) CT( clSetKernelArg(oclconfig->oclkernels[1],2,sizeof(cl_mem),&d_As) );
  else CT( clSetKernelArg(oclconfig->oclkernels[1],2,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[1],3,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[1],4,sizeof(cl_mem),&cnst_uniquelayers) );
  CT( clSetKernelArg(oclconfig->oclkernels[1],5,sizeof(cl_mem),&cnst_Modes) );
  CT( clSetKernelArg(oclconfig->oclkernels[1],6,sizeof(cl_mem),&cnst_Offsets_die) );

  //getKz
  if(debug_flag) CT( clSetKernelArg(oclconfig->oclkernels[2],0,sizeof(cl_mem),&d_As) );
  else CT( clSetKernelArg(oclconfig->oclkernels[2],0,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[2],1,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[2],2,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[2],3,sizeof(cl_mem),&cnst_uniquelayers) );
  CT( clSetKernelArg(oclconfig->oclkernels[2],4,sizeof(cl_mem),&cnst_Modes) );

  //setSistem
  CT( clSetKernelArg(oclconfig->oclkernels[3],0,sizeof(cl_mem),&d_Sistem) );
  CT( clSetKernelArg(oclconfig->oclkernels[3],1,sizeof(cl_mem),&d_Kx) );
  CT( clSetKernelArg(oclconfig->oclkernels[3],2,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[3],3,sizeof(cl_mem),&d_die) );
  CT( clSetKernelArg(oclconfig->oclkernels[3],4,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[3],5,sizeof(cl_mem),&cnst_uniquelayers) );
  CT( clSetKernelArg(oclconfig->oclkernels[3],6,sizeof(cl_mem),&cnst_Modes) );
  CT( clSetKernelArg(oclconfig->oclkernels[3],7,sizeof(cl_mem),&cnst_Offsets) );
  CT( clSetKernelArg(oclconfig->oclkernels[3],8,sizeof(cl_mem),&cnst_Offsets_die) );

  //calcNonZeri
  CT( clSetKernelArg(oclconfig->oclkernels[4],0,sizeof(cl_mem),&d_Sistem) );
  CT( clSetKernelArg(oclconfig->oclkernels[4],1,sizeof(cl_mem),&d_E) );
  CT( clSetKernelArg(oclconfig->oclkernels[4],2,sizeof(cl_mem),&d_NonZeri) );
  CT( clSetKernelArg(oclconfig->oclkernels[4],3,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[4],4,sizeof(cl_mem),&cnst_uniquelayers) );
  CT( clSetKernelArg(oclconfig->oclkernels[4],5,sizeof(cl_mem),&cnst_Modes) );
  CT( clSetKernelArg(oclconfig->oclkernels[4],6,sizeof(cl_mem),&cnst_Offsets) );

  //calcZeri
  CT( clSetKernelArg(oclconfig->oclkernels[5],0,sizeof(cl_mem),&d_Sistem) );
  CT( clSetKernelArg(oclconfig->oclkernels[5],1,sizeof(cl_mem),&d_E) );
  CT( clSetKernelArg(oclconfig->oclkernels[5],2,sizeof(cl_mem),&d_NonZeri) );
  CT( clSetKernelArg(oclconfig->oclkernels[5],3,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[5],4,sizeof(cl_mem),&cnst_uniquelayers) );
  CT( clSetKernelArg(oclconfig->oclkernels[5],5,sizeof(cl_mem),&cnst_Modes) );
  CT( clSetKernelArg(oclconfig->oclkernels[5],6,sizeof(cl_mem),&cnst_Offsets) );

  //calcB
  CT( clSetKernelArg(oclconfig->oclkernels[6],0,sizeof(cl_mem),&d_Kx) );
  CT( clSetKernelArg(oclconfig->oclkernels[6],1,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[6],2,sizeof(cl_mem),&d_E) );
  CT( clSetKernelArg(oclconfig->oclkernels[6],3,sizeof(cl_mem),&d_B) );
  CT( clSetKernelArg(oclconfig->oclkernels[6],4,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[6],5,sizeof(cl_mem),&cnst_uniquelayers) );
  CT( clSetKernelArg(oclconfig->oclkernels[6],6,sizeof(cl_mem),&cnst_Modes) );

  //clModesScalar
  CT( clSetKernelArg(oclconfig->oclkernels[7],0,sizeof(cl_mem),&d_Kx) );
  CT( clSetKernelArg(oclconfig->oclkernels[7],1,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[7],2,sizeof(cl_mem),&d_die) );
  CT( clSetKernelArg(oclconfig->oclkernels[7],3,sizeof(cl_mem),&d_E) );
  CT( clSetKernelArg(oclconfig->oclkernels[7],4,sizeof(cl_mem),&d_B) );
  CT( clSetKernelArg(oclconfig->oclkernels[7],5,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[7],6,sizeof(cl_mem),&cnst_uniquelayers) );
  CT( clSetKernelArg(oclconfig->oclkernels[7],7,sizeof(cl_mem),&cnst_Modes) );
  CT( clSetKernelArg(oclconfig->oclkernels[7],8,sizeof(cl_mem),&cnst_Offsets_die) );

  //clModesTensor_Vode
  CT( clSetKernelArg(oclconfig->oclkernels[8],0,sizeof(cl_mem),&d_Kx) );
  CT( clSetKernelArg(oclconfig->oclkernels[8],1,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[8],2,sizeof(cl_mem),&d_die) );
  CT( clSetKernelArg(oclconfig->oclkernels[8],3,sizeof(cl_mem),&d_E) );
  CT( clSetKernelArg(oclconfig->oclkernels[8],4,sizeof(cl_mem),&d_B) );
  CT( clSetKernelArg(oclconfig->oclkernels[8],5,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[8],6,sizeof(cl_mem),&cnst_uniquelayers) );
  CT( clSetKernelArg(oclconfig->oclkernels[8],7,sizeof(cl_mem),&cnst_Modes) );
  CT( clSetKernelArg(oclconfig->oclkernels[8],8,sizeof(cl_mem),&cnst_Offsets_die) );

  
  return;  
}

/**
 * \brief Other device memory allocations
 * 
 * Allocates memory for the
 * Propagations, Interfaces and Reflectivity kernels.
 * If an allocation fails, all the previously allocated
 *  OpenCL buffers get released.
 * Again these buffers are then tied to the respective kernel arguments
 */
void PPMTensorial_ocl::allocateOclOtheMemory(){

  size_t req_alloc=0;
  size_t dtype_size;
  char msg[1000];

  cl_int clErr;
  dtype_size = sizeof(cl_double2);

  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Props - Result - intermediate
  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Conditions - Auxiliary storage
  req_alloc += scanLen * 4 * 4 * (numthick) * dtype_size ; //d_Conditions_inv - Auxiliary storagr
  req_alloc += scanLen * 4 * 4 * (numthick - 1) * dtype_size ; //d_Interfaces - Result - intermediate
  req_alloc += scanLen * 4 * dtype_size ; //d_Reflectivity - The result
  req_alloc += scanLen * 4 * 4 * dtype_size ; //d_propa - Auxiliary storage

#ifdef _VERBOSE
  printf("Dev %f, req %f, req + resident %f\n",(float)oclconfig->dev_mem / 1024.0 / 1024.0,(float)req_alloc / 1024.0 / 1024.0,
    float((float)req_alloc + (float)resident_alloc)/1024.0/1024.0);
#endif
  
  //5MB are always allocated on a device. There is also approximatelly 68MB of overhead. However we cannot check for the actual used memory
  if((oclconfig->dev_mem - 74*1024*1024)<= req_alloc + resident_alloc){
    sprintf(msg,"%s (@ %d): Device does not meet memory requirements. %f requested, but only %f available. Take into account ~74MB of overhead.\n",
            __FUNCTION__,__LINE__,(float)req_alloc / 1024.0 / 1024.0,
            (float)oclconfig->dev_mem / 1024.0 / 1024.0);
    throw msg;
  }  

  d_Props =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * 4 * (numthick) * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_Props;
		oclconfig->Nbuffers++;
	};

  d_Conditions =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * 4 * (numthick) * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_Conditions;
		oclconfig->Nbuffers++;
	};

  d_Conditions_inv =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * 4 * (numthick) * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_Conditions_inv;
		oclconfig->Nbuffers++;
	};  

  d_Interfaces =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * 4 * (numthick - 1) * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_Interfaces;
		oclconfig->Nbuffers++;
	};

  d_Reflectivity =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_Reflectivity;
		oclconfig->Nbuffers++;
	};

  d_propa =
    clCreateBuffer(oclconfig->oclcontext,CL_MEM_READ_WRITE,(size_t)(scanLen * 4 * 4 * dtype_size),0,&clErr);
  if(clErr){
		ocl_relNbuffers_byref(oclconfig,oclconfig->Nbuffers);
    sprintf(msg,"%s (@ %d): clCreateBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }else{
		oclconfig->oclmemref[oclconfig->Nbuffers] = d_propa;
		oclconfig->Nbuffers++;
	};    

#ifdef _VERBOSE  
  std::cout << __FUNCTION__ << ": Other Memory allocated " << (float)req_alloc / 1024.0 / 1024.0 << " Mbyte " <<std::endl;
  std::cout << __FUNCTION__ << ": Total Memory in use " << float((float)req_alloc + (float)resident_alloc) / 1024.0 / 1024.0 << " Mbyte " <<std::endl;
#endif
  
  //Now attach the cl_buffers to the kernels!
  /* "clPropagation","getConditions","clInterfaces","clReflectivity"  */

  //clPropagation
  CT( clSetKernelArg(oclconfig->oclkernels[9],0,sizeof(cl_mem),&d_Props) );
  CT( clSetKernelArg(oclconfig->oclkernels[9],1,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[9],2,sizeof(cl_mem),&d_K0) );
  CT( clSetKernelArg(oclconfig->oclkernels[9],3,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[9],4,sizeof(cl_mem),&cnst_wheretolook) );
  CT( clSetKernelArg(oclconfig->oclkernels[9],5,sizeof(cl_mem),&cnst_Thickness) );
  
  //getConditions
  CT( clSetKernelArg(oclconfig->oclkernels[10],0,sizeof(cl_mem),&d_E) );
  CT( clSetKernelArg(oclconfig->oclkernels[10],1,sizeof(cl_mem),&d_B) );
  CT( clSetKernelArg(oclconfig->oclkernels[10],2,sizeof(cl_mem),&d_Conditions) );
  CT( clSetKernelArg(oclconfig->oclkernels[10],3,sizeof(cl_mem),&d_Conditions_inv) );
  CT( clSetKernelArg(oclconfig->oclkernels[10],4,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[10],5,sizeof(cl_mem),&cnst_numthick) );
  CT( clSetKernelArg(oclconfig->oclkernels[10],6,sizeof(cl_mem),&cnst_wheretolook) );

  //clInterfaces
  CT( clSetKernelArg(oclconfig->oclkernels[11],0,sizeof(cl_mem),&d_Kz) );
  CT( clSetKernelArg(oclconfig->oclkernels[11],1,sizeof(cl_mem),&d_Conditions) );
  CT( clSetKernelArg(oclconfig->oclkernels[11],2,sizeof(cl_mem),&d_Conditions_inv) );
  CT( clSetKernelArg(oclconfig->oclkernels[11],3,sizeof(cl_mem),&d_K0) );
  CT( clSetKernelArg(oclconfig->oclkernels[11],4,sizeof(cl_mem),&d_Interfaces) );
  CT( clSetKernelArg(oclconfig->oclkernels[11],5,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[11],6,sizeof(cl_mem),&cnst_numthick) );
  CT( clSetKernelArg(oclconfig->oclkernels[11],7,sizeof(cl_mem),&cnst_wheretolook) );
  CT( clSetKernelArg(oclconfig->oclkernels[11],8,sizeof(cl_mem),&cnst_Roughness) );
  
  //clReflectivity
  CT( clSetKernelArg(oclconfig->oclkernels[12],0,sizeof(cl_mem),&d_Reflectivity) );
  CT( clSetKernelArg(oclconfig->oclkernels[12],1,sizeof(cl_mem),&d_Interfaces) );
  CT( clSetKernelArg(oclconfig->oclkernels[12],2,sizeof(cl_mem),&d_Props) );
  CT( clSetKernelArg(oclconfig->oclkernels[12],3,sizeof(cl_mem),&d_propa) );
  CT( clSetKernelArg(oclconfig->oclkernels[12],4,sizeof(cl_mem),&cnst_scanlen) );
  CT( clSetKernelArg(oclconfig->oclkernels[12],5,sizeof(cl_mem),&cnst_numthick) );
  
  return;
}

/**
 * Release memory that was used for the execution of Modes and is not needed anymore.
 * This is done because PPM requires many buffers and otherwise may fail on big problems.
 */
void PPMTensorial_ocl::releaseOclDeprecMemory(){

  char msg[1000];
  cl_int clErr;

  CT(clReleaseMemObject(d_Kx));
  CT(clReleaseMemObject(d_die));
  CT(clReleaseMemObject(d_Sistem));
  CT(clReleaseMemObject(d_NonZeri));
  if(debug_flag)CT(clReleaseMemObject(d_As));
  
  return;
}

/**
 * Release the remaining buffers
 */
void PPMTensorial_ocl::releaseOclRemainMemory(){

  char msg[1000];
  cl_int clErr;

  CT(clReleaseMemObject(d_K0));
  CT(clReleaseMemObject(d_Kz));
  CT(clReleaseMemObject(d_E));
  CT(clReleaseMemObject(d_B));
  CT(clReleaseMemObject(d_Props));
  CT(clReleaseMemObject(d_Conditions));
  CT(clReleaseMemObject(d_Conditions_inv));
  CT(clReleaseMemObject(d_Interfaces));
  CT(clReleaseMemObject(d_Reflectivity));
  CT(clReleaseMemObject(d_propa));

  CT(clReleaseMemObject(cnst_scanlen));
  CT(clReleaseMemObject(cnst_numthick));
  CT(clReleaseMemObject(cnst_uniquelayers));
  CT(clReleaseMemObject(cnst_Modes));
  CT(clReleaseMemObject(cnst_Offsets));
  CT(clReleaseMemObject(cnst_Offsets_die));
  CT(clReleaseMemObject(cnst_wheretolook));
  CT(clReleaseMemObject(cnst_Roughness));
  CT(clReleaseMemObject(cnst_Thickness));
  
  return;
}

/**
 * Copy input memory to device buffers
 */
void PPMTensorial_ocl::setInputMemory(){

  float clTime_ms;
  cl_int clErr;
  char msg[1000];

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,d_Kx,CL_TRUE,0,scanLen * sizeof(cl_double),(void*)angles,0,0,&oclconfig->t_s[0]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;    
  }

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,d_K0,CL_TRUE,0,scanLen * sizeof(cl_double),(void*)K0,0,0,&oclconfig->t_s[1]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }
  
  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,d_die,CL_TRUE,0,scanLen * summode * sizeof(cl_double2),(void*)die,0,0,&oclconfig->t_s[2]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_scanlen,CL_TRUE,0,sizeof(cl_int),(void*)&scanLen,0,0,&oclconfig->t_s[3]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_numthick,CL_TRUE,0,sizeof(cl_int),(void*)&numthick,0,0,&oclconfig->t_s[4]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_uniquelayers,CL_TRUE,0,sizeof(cl_int),(void*)&uniquelayers,0,0,&oclconfig->t_s[5]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }  

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_Modes,CL_TRUE,0,uniquelayers * sizeof(cl_int),(void*)Modes,0,0,&oclconfig->t_s[6]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }  
  
  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_Offsets,CL_TRUE,0,uniquelayers * sizeof(cl_int),(void*)Offsets,0,0,&oclconfig->t_s[7]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_Offsets_die,CL_TRUE,0,uniquelayers * sizeof(cl_int),(void*)Offsets_die,0,0,&oclconfig->t_s[8]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_wheretolook,CL_TRUE,0,numthick * sizeof(cl_int),(void*)wheretolook,0,0,&oclconfig->t_s[9]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_Roughness,CL_TRUE,0,numthick * sizeof(cl_double),(void*)Roughness,0,0,&oclconfig->t_s[10]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }

  clErr =
    clEnqueueWriteBuffer(oclconfig->oclcmdqueue,cnst_Thickness,CL_TRUE,0,numthick * sizeof(cl_double),(void*)Thickness,0,0,&oclconfig->t_s[11]);
  if(clErr){
    sprintf(msg,"%s (@ %d): clEnqueueWriteBuffer failed: %s\n",__FUNCTION__,__LINE__,ocl_perrc(clErr));
    throw msg;
  }

#ifdef _VERBOSE  
  sprintf(msg,"%s: elapsed time",__FUNCTION__);
  ocl_get_profT(&oclconfig->t_s[0],&oclconfig->t_s[11],msg);
#endif
  
  for(int i=0;i<12;i++) CT(clReleaseEvent(oclconfig->t_s[i]));

  return;
}

/**
 * Simple method to add the execution time of another class to the internal total timer
 *
 * @param time Time value (in ms) to be added
 */
void PPMTensorial_ocl::addGpuTime(const float time){

  Overall_GPU_ms += time;
  return;
}

/**
 * Reset all timers each time we call clPPM, since the object might be active and called
 * multiple times
 */
void PPMTensorial_ocl::resetTimers(){

  eigens.clOnda_GPU_time=0.0;
  props.clPropagation_GPU_time=0.0;
  inters.clInterfaces_GPU_time=0.0;
  refls.clReflectivity_GPU_time=0.0;
  Overall_GPU_ms=0.0;
  return;
}

