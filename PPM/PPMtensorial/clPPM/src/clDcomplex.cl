
/**
 * \file
 * \brief Basic math operations for complex doubles in OpenCL
 */
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

/**
 * \brief Return the real part of a complex double
 */
inline double clCreal(double2 a){
  return a.x;
}

/**
 * \brief Return the imaginary part of a complex double
 */
inline double clCimag(double2 a){
  return a.y;
}

/**
 * \brief Return the conjugate of a complex double
 */
inline double2 clConj(double2 a){
  return (double2)( a.x , -a.y);
}

/**
 * \brief Addition of two complex doubles
 */
inline double2 clCadd(double2 a, double2 b){
  return (double2)(a.x + b.x, a.y + b.y);
};

/**
 * \brief Substraction of two complex doubles
 */
inline double2 clCsub(double2 a, double2 b){
  return (double2)(a.x - b.x, a.y - b.y);
};

/**
 * \brief Multiplication of two complex doubles
 */
inline double2 clCmul(double2 a, double2 b){
  return (double2)( (a.x * b.x) - (a.y * b.y) ,
                    (a.x * b.y) + (a.y * b.x) );
};

/**
 * \brief Division of two complex doubles
 */
inline double2 clCdiv(double2 a, double2 b){
  double denom = ( (b.x * b.x) + (b.y * b.y) );
  double real  = (a.x * b.x) + (a.y * b.y);
  double imag  = (a.y * b.x) - (a.x * b.y);
  return (double2)(real/denom, imag/denom);
};

/**
 * \brief Multiplication of a complex double with a real double
 */
inline double2 clCRmul(double2 a, double b){
  return (double2)(a.x * b, a.y * b);
};

/**
 * \brief Division of a complex double with a real double
 */
inline double2 clCRdiv(double2 a, double b){
  return (double2)(a.x / b, a.y / b);
};

/**
 * \brief Absolute value of a complex double
 */
inline double clCabs(double2 a){
  return sqrt( (a.x * a.x) + (a.y * a.y) );
}

/**
 * \brief Square root of a complex double
 */
inline double2 clCsqrt(double2 a){
  double r,th;
  double2 res;
  r = sqrt ( (a.x * a.x) + (a.y * a.y) );
  th= atan2( a.y, a.x);
  res= (double2)( sqrt(r) * cos(th/2), sqrt(r) * sin(th/2) );
  return res;
};

/**
 * \brief Exponent of a complex double
 */
inline double2 clCexp(double2 a){
  return (double2)( exp(a.x) * cos(a.y), exp(a.x) * sin(a.y) );
};

/**
 * \brief Negation of a complex double
 */
inline double2 negC(double2 a){
  return (double2)( -a.x, -a.y );
};


