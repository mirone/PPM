
nvcc $1 $2 -Xptxas=-v -arch=sm_20 -I/usr/local/cuda/include -c cuPPMSelfTest.cu
nvcc $1 $2 -Xptxas=-v -arch=sm_20 -o cuppm_selftest cuPPMSelfTest.o libcuPPM.a -lcudart
