
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <complex>
#include "cuda_runtime_api.h"
#include "cuComplex.h"

#if defined (_WIN32) && defined(_DLLEXPORT)
#define __w32dll_compat __declspec(dllexport)
#else 
#define __w32dll_compat 
#endif

#ifndef MODES_H
#include "cuPPMextension_modes.h"
#define MODES_H
#endif

#ifndef OTHER_H
#include "cuPPMextension_other.h"
#define OTHER_H
#endif

using namespace std;

/**
 *  \brief CUDA modification. Main class of cuPPM module
 *
 *  PPMTensorial_cuda is responsible for handling the CUDA algorithms and devices.
 *  It holds the addresses for the input and all the CUDA memory buffers.
 */
/* CUDA modification*/
class __w32dll_compat PPMTensorial_cuda{
    
public:
  PPMTensorial_cuda();
  PPMTensorial_cuda(int debug);  
  ~PPMTensorial_cuda();
  void getInput(const int &scanLen,const int &numthick,const int &uniquelayers_, int *modes,
                complex<double> *indexes, double *angles, double *roughness, 
                double *thickness, double *k0,
                int *lookup, complex< double > *reflectivity);
  void launchPPMTensorial_cuda(int dev=0);
  void benchPPMTensorial_cuda(int dev=0, int Nruns=100);
  void getCudaResults();

  //These are the results of the calculations nad intermediate results
  complex<double> *Kz,*As,*E,*B,*Prop,*Interfaces,*Reflectivity,*Conditions,*Conditions_inv;

  //These we might want to manipulate directly and are the input
  complex<double> *die;
  double *Kx,*angles,*K0,*Thickness,*Roughness;
  int *wheretolook;

  //Device memory
  double *d_Kx,*d_Thickness,*d_Roughness,*d_K0;
  int *d_NonZeri;
  cuDoubleComplex *d_die,*d_As,*d_Kz,*d_E,*d_B,*d_Props,*d_Interfaces;
  cuDoubleComplex *d_Sistem, *d_Conditions, *d_Conditions_inv;
  cuDoubleComplex *d_Reflectivity, *d_propa;

  float Overall_GPU_ms;

protected:
  void setCudaDevice(int dev);
  void allocateCudaMemory();
  void releaseCudaMemory();
  void allocateCudaBaseMemory();
  void allocateCudaOtheMemory();
  void releaseCudaRemainMemory();
  void releaseCudaDeprecMemory();
  void setCudaSymbols();
  void setInputMemory();
  void hostAllocations();
  void hostAllocations_debug();
  void addGpuTime(const float time);
  void resetTimers();

  //Other Host memory
  int *Modes,*Offsets,*Offsets_die,*NonZeri;
  complex<double> *Sistem;

  int cudaDevice_id;
  cudaDeviceProp cudaProperties;

  //The kernel invocation classes
  cuOnda eigens;
  cudaPropagation props;
  cudaInterfaces  inters;
  cudaReflectivity refls;

  int numthick;
  int uniquelayers;
  int scanLen;
  int summode;
  int debug_flag;
  int first_run_flag;
  size_t resident_alloc;

};

