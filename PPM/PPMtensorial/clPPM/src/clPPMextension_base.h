
#/*##########################################################################
# Copyright (C) 2004-2010 European Synchrotron Radiation Facility
#
# PPM  : Alessandro Mirone.
# GPU   Cuda     ( OCL is in progress.. )    Dimitris Karkoulis
#   Qt : Interface : Vorobeva Anastasiya   Vorobyeva Veronika
#           nvorobeva@hotmail.fr  vorobyevav@yahoo.com
#                     and Alessandro Mirone
#  European Synchrotron Radiation Facility, Grenoble,France
#
#
# PPM is  developed at
# the ESRF by the SciSoft  group.
# PPM CUDA is developed by  Dimitris Karkoulis, financed by:
#        LinkSCEEM-2 (INFRA-2010-1.2.3) Work Package 12 project
#           (grant number RI-261600)
#
# This toolkit is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# PPM is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# PPM; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# PPM follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program.
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license
# is a problem for you.
#############################################################################*/

#include <iostream>
#include <complex>
#include <CL/opencl.h>

#if defined (_WIN32) && defined(_DLLEXPORT)
#define __w32dll_compat __declspec(dllexport)
#else 
#define __w32dll_compat 
#endif

#ifndef MODES_H
#include "clPPMextension_modes.h"
#define MODES_H
#endif

#ifndef OTHER_H
#include "clPPMextension_other.h"
#define OTHER_H
#endif

using namespace std;

/**
 *  \brief OpenCL modification. Main class of clPPM module
 *
 *  PPMTensorial_ocl is a direct port from PPMTensorial_cuda.
 *  It is responsible for handling the OpenCL algorithms and devices.
 *  It holds the addresses for the input and all the OpenCL memory buffers.
 */

class __w32dll_compat PPMTensorial_ocl{
    
public:
  PPMTensorial_ocl();
    
  //Constructor with file stream. The stream is used by the OpenCL-Toolbox only  
  PPMTensorial_ocl(const char *fname);
  ~PPMTensorial_ocl();
  
  //First step to calling clPPM: get the input.
  void getInput(const int &scanLen,const int &numthick,const int &uniquelayers_, int *modes,
                complex<double> *indexes, double *angles, double *roughness, 
                double *thickness, double *k0,
                int *lookup, complex< double > *reflectivity);

  //Second step, run the kernels and get the result
  void launchPPMTensorial_ocl(const char *devicetype,int platformid,int devid);
  //Performs benchmarking by looping kernel executions.
  void benchPPMTensorial_ocl(const char *devicetype,int platformid,int devid,int Nruns=100);
  
  //Interface to ocl_tools device prob that prints the list of the OpenCL enabled devices.
  void show_devices();

  //Uses ocl_tools' device probing for automatic GPU discovery
  void query_for_gpu();


  //The result of the calculations and intermediate results  
  complex<double> *Kz,*As,*E,*B,*Prop,*Interfaces,*Reflectivity,*Conditions,*Conditions_inv;


  //Pointers referencing to the data of the input  
  complex<double> *die;
  double *Kx,*angles,*K0,*Thickness,*Roughness;
  int *wheretolook;

  
  //Device memory
  cl_mem d_Kx,d_Thickness,d_Roughness,d_K0; //double
  cl_mem d_NonZeri; //int
  cl_mem d_die,d_As,d_Kz,d_E,d_B,d_Props,d_Interfaces; //double2
  cl_mem d_Sistem, d_Conditions, d_Conditions_inv; //double2
  cl_mem d_Reflectivity, d_propa; //double2


  //General constant memory
  cl_mem cnst_scanlen, cnst_numthick, cnst_uniquelayers;

  //GPU constant memory for modes  
  cl_mem cnst_Modes, cnst_Offsets, cnst_Offsets_die;

  //GPU constant memory for other  
  cl_mem cnst_wheretolook, cnst_Roughness, cnst_Thickness;


  //Variables set by query_for_gpu  
  int hasGPU;
	int overrideCustomConfig;
  cl_device_id _devid;
  cl_platform_id _platformid;
  
  float Overall_GPU_ms;

protected:
  void setOclDevice(const char *devicetype,int platformid,int devid);
  void allocateOclBaseMemory();
  void allocateOclOtheMemory();
  void releaseOclRemainMemory();
  void releaseOclDeprecMemory();
  void setInputMemory();
  void hostAllocations();
  void hostAllocations_debug();
  void addGpuTime(const float time);
  void resetTimers();


  //Other Host memory  
  int *Modes,*Offsets,*Offsets_die,*NonZeri;
  complex<double> *Sistem;

  ocl_config_type *oclconfig;
  FILE *oclstream;
  int hasStdout;
  
  
  //The kernel invocation classes
  clOnda eigens;
  clPropagation props;
  clInterfaces  inters;
  clReflectivity refls;

  int numthick;
  int uniquelayers;
  int scanLen;
  int summode;
  int debug_flag;
  int first_run_flag;
  size_t resident_alloc;

};

