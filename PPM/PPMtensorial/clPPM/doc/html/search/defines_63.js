var searchData=
[
  ['cl_5fcheck',['CL_CHECK',['../ocl__ckerr_8h.html#a51b29a5a85a090727e2f7624ed2e316b',1,'ocl_ckerr.h']]],
  ['cl_5fcheck_5ferr',['CL_CHECK_ERR',['../ocl__ckerr_8h.html#ab4058ed9c5d33aa8278ea9ec75272337',1,'ocl_ckerr.h']]],
  ['cl_5fcheck_5ferr_5fpr',['CL_CHECK_ERR_PR',['../ocl__ckerr_8h.html#a3b85bc01ba35f04b92ee61a48d36a5e6',1,'ocl_ckerr.h']]],
  ['cl_5fcheck_5ferr_5fpr_5fret',['CL_CHECK_ERR_PR_RET',['../ocl__ckerr_8h.html#ae0bec255b14d22e3b4b9c149e55c1aae',1,'ocl_ckerr.h']]],
  ['cl_5fcheck_5ferr_5fprn',['CL_CHECK_ERR_PRN',['../ocl__ckerr_8h.html#a5bc5840f5bb8165ed2e8f523d91c864e',1,'ocl_ckerr.h']]],
  ['cl_5fcheck_5ferr_5fthrow',['CL_CHECK_ERR_THROW',['../ocl__ckerr_8h.html#a373bd389649d99e80f6cc3eb25e5cb9e',1,'ocl_ckerr.h']]],
  ['cl_5fcheck_5fpr',['CL_CHECK_PR',['../ocl__ckerr_8h.html#a5c31b7662a30779cd59c3a545af84cb6',1,'ocl_ckerr.h']]],
  ['cl_5fcheck_5fpr_5fret',['CL_CHECK_PR_RET',['../ocl__ckerr_8h.html#a424e8d6e46b1215c5601a5dbce695296',1,'ocl_ckerr.h']]],
  ['cl_5fcheck_5fprn',['CL_CHECK_PRN',['../ocl__ckerr_8h.html#a2e01a3a603b02d77236d0affb9942540',1,'ocl_ckerr.h']]]
];
